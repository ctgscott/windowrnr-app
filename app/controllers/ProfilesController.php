<?php

class ProfilesController extends BaseController {


	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('auth', array('except' => array('postSalesCheckBox', 'getSalesCheckBox', 'getSalesCalID')));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('profiles.index');
	}

	/**
	 * Return the avatar image name for the give id.
	 *
	 * @return Response
	 */
	public static function getAvatar($id)
	{
		try 
		{
			$results = DB::table('profiles')
				->where('user_id', '=', $id)
				->pluck('avatar');
			return $results;
		}
		catch (Exception $e) 
		{
			return $e;
		}		
	}
	
	public static function saveAvatar($id, $avatar)
	{
		try 
		{
			$results = DB::table('profiles')
				->where('id', $id)
				->update(array('avatar' => $avatar));
			return $results;
		}
		catch (Exception $e) 
		{
			return $e;
		}
	}

	public static function getProperties($id) {
			try 
			{
				$results = DB::table('profiles')
					->where('user_id', '=', $id)
					->select(['avatar', 'color', 'start_location'])
					->get();
				return $results;
			}
			catch (Exception $e) 
			{
				return $e;
			}
	}

	public static function saveProperties($id, $avatar, $color, $start_location)
	{
		try 
		{
			$results = DB::table('profiles')
				->where('user_id', $id)
				->update([
					'avatar' => $avatar,
					'color' => $color,
					'start_location' => $start_location
				]);
			return $results;
		}
		catch (Exception $e) 
		{
			return $e;
		}
	}
	
	/**
	* POSTs check/no check boolean to the
	* user's profile for use in the sales
	* estimate checkboxes.
	*/		
	public static function postSalesCheckBox()
	{
		$id = ($_POST["id"]);
		$value = ($_POST["value"]);

		$result = DB::table('profiles')
            ->where('user_id', $id)
            ->update(array('sales_check' => $value));
		
		if ($result == 0 || 1) {
			return 'success';
		} else {
			return $result;
		}
	}
	
	/**
	* Returns all user accounts expected to populate
	* the estimate appointments checkboxes 
	*/		
	public static function getSalesCheckBox()
	{
		$result = DB::table('profiles')
			->select('user_id')
			->where('sales_check', '=', '1')
			->get();
		
		return $result;
	}

	/**
	* Returns all user accounts expected to populate
	* the estimate appointments checkboxes 
	*/		
	public static function getSalesCalID()
	{
		$result = DB::table('profiles')
			->select('google_calendar_id')
			->where('sales_check', '=', '1')
			->get();
		
		return $result;
	}

	/**
	 * Return the google_id for the given id.
	 *
	 * @return Response
	 */
	public static function getGoogleID($id)
	{		
		try 
		{
			$results = DB::table('profiles')
				->where('id', '=', $id)
				->pluck('google_id');
			return $results;
		}
		catch (Exception $e) 
		{
			return $e;
		}
	}

	/**
	 * Return the avatar image name for the give id.
	 *
	 * @return Response
	 */
	public static function getGoogleCalendarID($id)
	{		
		try 
		{
			$results = DB::table('profiles')
				->where('user_id', '=', $id)
				->pluck('google_calendar_id');
			return $results;
		}
		catch (Exception $e) 
		{
			return $e;
		}
	}

	public static function saveGoogleCalID($id, $googleCalID)
	{
		try 
		{
			$results = DB::table('profiles')
				->where('user_id', $id)
				->update(array('google_calendar_id' => $googleCalID));
			return $results;
		}
		catch (Exception $e) {
			return $e;
		}
	}
	
	/** 
	 * changes the sales_check value to 1
	 * indicating this user is a sales person
	 *
	 */
	public static function updateSalesCheck($id, $val)
	{
		try 
		{
			$now = date("Y-m-d H:i:s");

			$results = DB::table('profiles')
				->where('user_id', $id)
				->update(array(
					'sales_check' => $val,
					'updated_at' => $now
				));
			return 'Success with '.$val;
		}
		catch (Exception $e) {
			return $e;
		}
	}
	
	/**
	 * Inserts new user into profile table
	 *
	 */
	public static function insertProfile($id, $email, $fname)
	{
		try 
		{
			$now = date("Y-m-d H:i:s");
			
			$results = DB::table('profiles')
				->insert(array(
					'user_id' => $id, 
					'google_id' => $email, 
					'google_calendar_id' => $email,
					'avatar' => 'WinPin2.png',
					'first_name' => $fname,
					'sales_check' => '0',
					'created_at' => $now
				));
			return 'Success';
		}
		catch (Exception $e) {
			return $e;
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('profiles.create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('profiles.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('profiles.edit');
	}
	
	public function postVisibleCheckBox() {
		$id = ($_POST["id"]);
		$value = ($_POST["value"]);

		DB::table('events_visibility')->where('session_user_id', Sentry::getUser()->id)->where('profile_id', $id)->delete();
        DB::table('events_visibility')->insert([
        	'session_user_id' => Sentry::getUser()->id,
        	'profile_id' => $id,
        	'visible' => intval($value)
        ]);
		
		return 'success';
	}
}
