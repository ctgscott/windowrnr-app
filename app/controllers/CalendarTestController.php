<?php

class CalendarTestController extends BaseController {

	public function __construct() {
		parent::__construct();
	} 

	public static function insertLostEvents() {

		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$cal = new Google_Service_Calendar($client);

		if (Session::has('token')) 
			$client->setAccessToken(Session::get('token'));

		if ($client->getAccessToken()) {

			//$calendarID   = "dev@windowrnr.com";
			$calendarID   = "tech@windowrnr.com";
			
			//select c.city, b.l_name, b.f_name, a.location, a.description, a.start, a.end, a.job_id from events a join customers b on a.cust_id=b.id join jobs c on a.job_id=c.id where a.start>='2016-05-01' and a.start<='2016-05-18' and a.google_cal_id='tech@windowrnr.com' order by a.id
			//CSV file contains data from the above query
			$inputFileType = 'CSV';
			$inputFileName = dirname(__FILE__) . DIRECTORY_SEPARATOR . "lost_calendar_entries.csv";
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
			$worksheet = $objPHPExcel->getActiveSheet();
			foreach ($worksheet->getRowIterator() as $row) {
			    //echo 'Row number: ' . $row->getRowIndex();
			    $results = []; 
			    $index = 0;
			    //If the csv file contains a large number of entries 
			    //insert the events into the calendar 5 or 10 at a time 
			    //to avoid "maximum execution time exceeded" error
			    //if($row->getRowIndex() > 1 && $row->getRowIndex() <= 5) {
			    if($row->getRowIndex() > 1) {
				    $cellIterator = $row->getCellIterator();
				    $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
				    foreach ($cellIterator as $cell) {
				        if (!is_null($cell)) {
				            //echo 'Cell: ' . $cell->getColumn() . ' - ' . $cell->getValue();
				            //echo "<br><hr>";
				            $results[] = $cell->getValue();
				        }
				    }
				    // print_r($results);
				    // echo "<br><hr>";
				    $event = new Google_Service_Calendar_Event();
					$summary      = '('.$results[$index++].') '.$results[$index++].', '.$results[$index++];
					$location     = $results[$index++];
					$description  = $results[$index++];
	
					$event->setSummary($summary);
					$event->setLocation($location);
					$event->setDescription($description);
					
					$start        = $results[$index++];
					$startTime    = strtotime($start);
					$start        = date('c', $startTime);

					$end          = $results[$index++];
					$endTime      = strtotime($end);
					$end          = date('c', $endTime);

					$eventStart = new Google_Service_Calendar_EventDateTime();
					$eventStart->setDateTime($start);
					$eventStart->setTimeZone('America/Los_Angeles');
					$event->setStart($eventStart);

					$eventEnd = new Google_Service_Calendar_EventDateTime();
					$eventEnd->setDateTime($end);
					$eventEnd->setTimeZone('America/Los_Angeles');
					$event->setEnd($eventEnd);


					$extendedProperties = New Google_Service_Calendar_EventExtendedProperties();
					$extendedProperties->setShared(['job_id' => $results[$index++]]);
					$event->setExtendedProperties($extendedProperties);

					// insert the event into google calendar
					$createdEvent = $cal->events->insert($calendarID, $event);

					print_r($createdEvent);
					echo "<br><hr>";
				}
			}
		}
	}
}
?>