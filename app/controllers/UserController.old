<?php

class UserController extends BaseController {

	protected $sendgrid;	

	/**
	 * Instantiate a new UserController
	 */
	public function __construct()
	{
		parent::__construct();
		//filter to check if user is logged in
		$this->beforeFilter('auth', array(
			'except' => array(
				'getRegister', 
				'postRegister', 
				'getActivate', 
				'getLogin', 
				'postLogin', 
				'getResetpassword', 
				'postResetpassword',
				'getReset',
			)
		));

		//Check CSRF token on POST
		$this->beforeFilter('csrf', array('on' => 'post'));
		
		//Enable the throttler.  [I am not sure about this...]
		// Get the Throttle Provider
		$throttleProvider = Sentry::getThrottleProvider();

		// Enable the Throttling Feature
		$throttleProvider->enable();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		// Index - show the user details.
		try
		{		   	
		    // Find the user using the user id
		    $data['user'] = Sentry::getUser();

		    if ( $data['user']->hasAccess('admin')) 
		    {
		    	$data['allUsers'] = Sentry::getUserProvider()->findAll();

		    	//Assemble an array of each user's status
		    	$data['userStatus'] = array();
		    	foreach ($data['allUsers'] as $user) 
		    	{
		    		if ($user->isActivated()) 
		    		{
		    			$data['userStatus'][$user->id] = "Active";
		    		} 
		    		else 
		    		{
		    			$data['userStatus'][$user->id] = "Not Active";
		    		}

		    		//Pull Suspension & Ban info for this user
		    		$throttle = Sentry::getThrottleProvider()->findByUserId($user->id);

		    		//Check for suspension
		    		if($throttle->isSuspended())
				    {
				        // User is Suspended
				        $data['userStatus'][$user->id] = "Suspended";
				    }

		    		//Check for ban
				    if($throttle->isBanned())
				    {
				        // User is Banned
				        $data['userStatus'][$user->id] = "Banned";
				    }
		    	}
		    } 

		    return View::make('users.index')->with($data);
		}
		catch (Cartalyst\Sentry\UserNotFoundException $e)
		{
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('users/login');
		}
	}
	
	/**
	 *  Return all user accounts with a Sales role.
	 *  Includes first name, avatar & calendar ID.
	 */
	public static function getSalesProfiles()
	{				
		$users = DB::table('profiles')->where('sales_check', '=', 1)->orderBy('first_name')->get();

		// ensure that the events for each profile user either unset or set as visible for the current logged-in user in table: events_visiblity
		$invisibleUsers = DB::table('events_visibility')->where('session_user_id', Sentry::getUser()->id)->where('visible', '=', 0)->get();

		//BaseController::firePhp()->addInfo('$users', $users);

		$profile = array();
		$id = 0;
		foreach ($users as $user) {

			$invisible = false;
			foreach ($invisibleUsers as $invisibleUser) {
				if($invisibleUser->profile_id === $user->user_id) {
					$invisible = true;
					break;
				}
			}
			//if($invisible == true) continue;

			$id++;
			$profile[$id]['id'] = $user->user_id;
			$profile[$id]['sales'] = $user->sales_check;
			$profile[$id]['first_name'] = $user->first_name;
			$profile[$id]['avatar'] = $user->avatar;
			$profile[$id]['google_id'] = $user->google_id;
			$profile[$id]['google_calendar_id'] = $user->google_calendar_id;
			$profile[$id]['color'] = $user->color;
			$profile[$id]['start_location'] = $user->start_location;
			$profile[$id]['visible'] = ($invisible ? 0 : 1);
		}
		return $profile;		
	}

	public static function getTechProfiles() 
	{

		// SQL
		// SELECT * FROM profiles WHERE user_id IN (SELECT user_id FROM users_groups WHERE group_id = (SELECT id FROM groups WHERE name = 'Tech'))

		// get users in tech group
		$users = DB::table('users_groups')->where('group_id', '=', DB::raw("(SELECT id FROM groups WHERE name = 'Tech')"))->select('user_id')->get();
		$userIDs = [];
		foreach ($users as $user) {
			$userIDs[] = $user->user_id;
		}
		//var_dump($userIDs);

		// get profiles
		$users = DB::table('profiles')->whereIn('user_id', $userIDs)->orderBy('first_name')->get();
		//var_dump($users);

		$profile = array();
		$id = 0;
		foreach ($users as $user) {
			$id++;
			$profile[$id]['id'] = $user->user_id;
			$profile[$id]['sales'] = $user->sales_check;
			$profile[$id]['first_name'] = $user->first_name;
			$profile[$id]['avatar'] = $user->avatar;
			$profile[$id]['google_id'] = $user->google_id;
			$profile[$id]['google_calendar_id'] = $user->google_calendar_id;
			$profile[$id]['color'] = $user->color;
			$profile[$id]['start_location'] = $user->start_location;
		}
		return $profile;		
	}

	/**
	 *  Display this user's details.
	 */	
	public function getShow($id)
	{
		try
		{
		    //Get the current user's id.
			Sentry::check();
			$currentUser = Sentry::getUser();

		   	//Do they have admin access?
			if ( $currentUser->hasAccess('admin') || $currentUser->getId() == $id)
			{
				//Either they are an admin, or:
				//They are not an admin, but they are viewing their own profile.
				$data['user'] = Sentry::getUserProvider()->findById($id);
				$data['myGroups'] = $data['user']->getGroups();
				return View::make('users.show')->with($data);
			} 
			else 
			{
				Session::flash('error', 'You don\'t have access to that user.');
				return Redirect::to('/');
			}

		}
		catch (Cartalyst\Sentry\UserNotFoundException $e)
		{
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/');
		}
	}


	/**
	 * Register a new user. 
	 *
	 * @return Response
	 */
	public function getRegister()
	{
		// Show the register form
		return View::make('users.register');
	}

	public function postRegister() 
	{	
		// Gather Sanitized Input
		$input = array(
			'email' => Input::get('email'),
			'password' => Input::get('password'),
			'password_confirmation' => Input::get('password_confirmation'),
			'fname' => Input::get('fname'),
			'lname' => Input::get('lname')
			);
		
		// Set Validation Rules
		$rules = array (
			'email' => 'required|min:4|max:32|email',
			'password' => 'required|min:6|confirmed',
			'password_confirmation' => 'required',
			'fname' => 'required|alpha',
			'lname' => 'required|alpha'
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{			
			return Redirect::to('users/register')->withErrors($v)->withInput();
		}
		
		try {
			//Attempt to register the user. 
			$user = Sentry::register(array(
				'email' => $input['email'], 
				'password' => $input['password'],
				'first_name' => $input['fname'],
				'last_name' => $input['lname']
				));

			//Get the activation code & prep data for email
			$data['activationCode'] = $user->GetActivationCode();
			$data['email'] = $input['email'];
			$data['userId'] = $user->getId();

			//send email with link to activate.
			Mail::send('emails.auth.welcome', $data, function($m) use($data)
			{
			    $m->to($data['email'])->subject('Welcome to WindowRnR');
			});
			
			ProfilesController::insertProfile($data['userId'], $input['email'], $input['fname']);

			//success!
	    	Session::flash('success', 'Your account has been created. Check your email for the confirmation link.');
	    	return Redirect::to('/');

		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    Session::flash('error', 'Login field required.');
		    return Redirect::to('users/register')->withErrors($v)->withInput();
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    Session::flash('error', 'User already exists.');
		    return Redirect::to('users/register')->withErrors($v)->withInput();
		}		
	}

	/**
	 * Activate a new User
	 */
	public function getActivate($userId = null, $activationCode = null) 
	{
		try 
		{
		    // Find the user
		    $user = Sentry::getUserProvider()->findById($userId);

		    // Attempt user activation
		    if ($user->attemptActivation($activationCode))
		    {	
		    	//Add this person to the user group. 
		    	$userGroup = Sentry::getGroupProvider()->findById(1);
		    	$user->addGroup($userGroup);

		        Session::flash('success', 'Your account has been activated. <a href="/users/login">Click here</a> to log in.');
				return Redirect::to('/');
		    }
		    else
		    {
		        // User activation failed
		        Session::flash('error', 'There was a problem activating this account. Please contact the system administrator.');
				return Redirect::to('/');
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('error', 'User does not exist.');
			return Redirect::to('/');
		}
		catch (Cartalyst\SEntry\Users\UserAlreadyActivatedException $e)
		{
		    Session::flash('error', 'You have already activated this account.');
			return Redirect::to('/');
		}
	}

	/**
	 * Login
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		// User is not logged in, but may have old token
		if (Session::has('token')) 
		{
			Session::forget('token');
		}		

		// Show the register form
		//BaseController::firePhp()->addInfo('getLogin - Session', Session::all());
		return View::make('users.login');
	}

	public function postLogin() 
	{
		//BaseController::firePhp()->addInfo('Session #1', Session::all());

		// Gather Sanitized Input
		$input = array(
			'email' => Input::get('email'),
			'password' => Input::get('password'),
			'rememberMe' => Input::get('rememberMe')
			);

		// Set Validation Rules
		$rules = array (
			'email' => 'required|min:4|max:32|email',
			'password' => 'required|min:6'
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{			
			return Redirect::to('users/login')->withErrors($v)->withInput();
		}
		
		try
		{
		    //Check for suspension or banned status
			$user = Sentry::getUserProvider()->findByLogin($input['email']);
			$throttle = Sentry::getThrottleProvider()->findByUserId($user->id);
		    $throttle->check();

		    // Set login credentials
		    $credentials = array(
		        'email'    => $input['email'],
		        'password' => $input['password']
		    );

		    // Try to authenticate the user
		    $user = Sentry::authenticate($credentials, $input['rememberMe']);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    // Sometimes a user is found, however hashed credentials do
		    // not match. Therefore a user technically doesn't exist
		    // by those credentials. Check the error message returned
		    // for more information.
		    Session::flash('error', 'Invalid username or password.' );
			return Redirect::to('users/login')->withErrors($v)->withInput();
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
		    echo 'User not activated.';
		    Session::flash('error', 'You have not yet activated this account.');
			return Redirect::to('users/login')->withErrors($v)->withInput();
		}

		// The following is only required if throttle is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
		    $time = $throttle->getSuspensionTime();
		    Session::flash('error', "Your account has been suspended for $time minutes.");
			return Redirect::to('users/login')->withErrors($v)->withInput();
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
		    Session::flash('error', 'You have been banned.');
			return Redirect::to('users/login')->withErrors($v)->withInput();
		}

		//Login was succesful.  
		//BaseController::firePhp()->addInfo('Session - final', Session::all());
		return $this->oauth2('customers');
	}

	/**
	 * Logout
	 */	
	public function getLogout() 
	{
		Sentry::logout();
		if (Session::has('token')) 
		{
			Session::forget('token');
		}
		return Redirect::to('users/login');
	}

	/**
	 * Forgot Password / Reset
	 */
	public function getResetpassword() 
	{	
		return View::make('users.reset');
	}

	public function postResetpassword () 
	{
		// Gather Sanitized Input
		$input = array(
			'email' => Input::get('email')
		);

		// Set Validation Rules
		$rules = array (
			'email' => 'required|min:4|max:32|email'
		);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{		
			return Redirect::to('users/resetpassword')->withErrors($v)->withInput();
		}
		
		try
		{
		    $user = Sentry::getUserProvider()->findByLogin($input['email']);
		    $data['resetCode'] = $user->getResetPasswordCode();
		    $data['userId'] = $user->getId();
		    $data['email'] = $input['email'];

		    // Email the reset code to the user
			Mail::send('emails.auth.reset', $data, function($m) use($data)
			{
			    $m->to($data['email'])->subject('Password Reset Confirmation | Laravel4 With Sentry');
			});

			Session::flash('success', 'Check your email for password reset information.');
		    return Redirect::to('/');

		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User does not exist';
		}		
	}


	/**
	 * Reset User's password
	 */
	public function getReset($userId = null, $resetCode = null) 
	{
		try
		{
		    // Find the user
		    $user = Sentry::getUserProvider()->findById($userId);
		    $newPassword = $this->_generatePassword(8,8);

		    // Attempt to reset the user password
		    if ($user->attemptResetPassword($resetCode, $newPassword))
		    {
			    //Prepare New Password body
			    $data['newPassword'] = $newPassword;
			    $data['email'] = $user->getLogin();

			    Mail::send('emails.auth.newpassword', $data, function($m) use($data)
				{
				    $m->to($data['email'])->subject('New Password Information | Laravel4 With Sentry');
				});

				Session::flash('success', 'Your password has been changed. Check your email for the new password.');
			    return Redirect::to('/');
		    }
		    else
		    {
		        // Password reset failed
		    	Session::flash('error', 'There was a problem.  Please contact the system administrator.');
			    return Redirect::to('users/resetpassword');
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User does not exist.';
		}
	}

	/**
	 *  Edit / Update User Profile
	 */	
	public function getEdit($id) 
	{		
		try
		{
		    //Get the current user's id.
			Sentry::check();
			$currentUser = Sentry::getUser();

		   	//Do they have admin access?
			if ( $currentUser->hasAccess('admin'))
			{
				$data['user']        = Sentry::getUserProvider()->findById($id);
				$data['userGroups']  = $data['user']->getGroups();
				$data['allGroups']   = Sentry::getGroupProvider()->findAll();
				$data['avatar']      = ProfilesController::getAvatar($id);
				$data['properties']  = ProfilesController::getProperties($id);
				$data['googleCalID'] = ProfilesController::getGoogleCalendarID($id);

				//BaseController::firePhp()->addInfo('$id = '.$id);
				//BaseController::firePhp()->addInfo('$data = '.$data['avatar']);

				//BaseController::firePhp()->addInfo('Session::all() = ', Session::all());
				return View::make('users.edit')->with($data);
			} 
			elseif ($currentUser->getId() == $id)
			{
				//They are not an admin, but they are viewing their own profile.
				$data['user'] 		 = Sentry::getUserProvider()->findById($id);
				$data['userGroups']  = $data['user']->getGroups();
				$data['avatar']      = ProfilesController::getAvatar($id);
				$data['properties']  = ProfilesController::getProperties($id);
				$data['googleCalID'] = ProfilesController::getGoogleCalendarID($id);

				return View::make('users.edit')->with($data);
			} 
			else 
			{
				Session::flash('error', 'You don\'t have access to that user.');
				return Redirect::to('/');
			}

		}
		catch (Cartalyst\Sentry\UserNotFoundException $e)
		{
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/');
		}
	}


	public function postEdit($id) 
	{
		// Gather Sanitized Input
		$input = array(
			'firstName' => Input::get('firstName'),
			'lastName'  => Input::get('lastName'),
			'avatar'    => Input::get('avatar'),
			'email'     => Input::get('email')
		);

		//BaseController::firePhp()->addInfo('$input = ', $input);

		// Set Validation Rules
		$rules = array (
			'firstName' => 'alpha',
			'lastName' => 'alpha',
		);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{		
			return Redirect::to('users/edit/' . $id)->withErrors($v)->withInput();
		}
		
		try
		{
			//Get the current user's id.
			Sentry::check();
			$currentUser = Sentry::getUser();

		   	//Do they have admin access?
			if ( $currentUser->hasAccess('admin')  || $currentUser->getId() == $id)
			{
				// Either they are an admin, or they are changing their own password. 
				// Find the user using the user id
				$user = Sentry::getUserProvider()->findById($id);	
				
			    // Update the user details
			    $user->first_name = $input['firstName'];
			    $user->last_name = $input['lastName'];
				$user->email = $input['email'];

				$saveAvatar      = ProfilesController::saveProperties($id, Input::get('avatar'),  Input::get('color'),  Input::get('start_location'));
				$saveGoogleCalID = ProfilesController::saveGoogleCalID($id, Input::get('googleCalID'));

				//BaseController::firePhp()->addInfo('$saveAvatar = ', array($saveAvatar));

			    // Update the user
			    if (($user->save()) && ($saveAvatar == 1 OR $saveAvatar == 0))
			    {
			        // User information was updated
					if ($saveAvatar == 1) 
					{
						$message = 'Profile updated & avatar changed.';
						Session::flash('success', 'Profile updated & avatar changed.');
					} 
					else 
					{
						Session::flash('success', 'Profile updated but avatar unchanged.');
					}

					//BaseController::firePhp()->addInfo('Session::all() = ', Session::all());
					return Redirect::to('users/edit/' . $id);
			    }
			    else
			    {
			        // User information was not updated
			        Session::flash('error', 'Profile could not be updated.');
					return Redirect::to('users/edit/' . $id);
			    }

			} 
			else 
			{
				Session::flash('error', 'You don\'t have access to that user.');
				return Redirect::to('/');
			}			   			    
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    Session::flash('error', 'User already exists.');
			return Redirect::to('users/edit/' . $id);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('error', 'User was not found.');
			return Redirect::to('users/edit/' . $id);
		}
	}

	/**
	 * Process changepassword form. 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function postChangepassword($id) 
	{
		// Gather Sanitized Input
		$input = array(
			'oldPassword' => Input::get('oldPassword'),
			'newPassword' => Input::get('newPassword'),
			'newPassword_confirmation' => Input::get('newPassword_confirmation')
		);

		// Set Validation Rules
		$rules = array (
			'oldPassword' => 'required|min:6',
			'newPassword' => 'required|min:6|confirmed',
			'newPassword_confirmation' => 'required'
		);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{			
			return Redirect::to('users/edit/' . $id)->withErrors($v)->withInput();
		}
		
		try
		{			    
			//Get the current user's id.
			Sentry::check();
			$currentUser = Sentry::getUser();

		   	//Do they have admin access?
			if ( $currentUser->hasAccess('admin')  || $currentUser->getId() == $id)
			{
				// Either they are an admin, or they are changing their own password. 
				$user = Sentry::getUserProvider()->findById($id);	
				if ($user->checkHash($input['oldPassword'], $user->getPassword())) 
		    	{
			    	//The oldPassword matches the current password in the DB. Proceed.
			    	$user->password = $input['newPassword'];

			    	if ($user->save())
				    {
				        // User saved
				        Session::flash('success', 'Your password has been changed.');
						return Redirect::to('users/show/'. $id);
				    }
				    else
				    {
				        // User not saved
				        Session::flash('error', 'Your password could not be changed.');
						return Redirect::to('users/edit/' . $id);
				    }
				} 
				else 
				{
					// The oldPassword did not match the password in the database. Abort. 
					Session::flash('error', 'You did not provide the correct password.');
					return Redirect::to('users/edit/' . $id);
				}
			} 
			else 
			{
				Session::flash('error', 'You don\'t have access to that user.');
				return Redirect::to('/');
			}			   			    
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    Session::flash('error', 'Login field required.');
			return Redirect::to('users/edit/' . $id);
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    Session::flash('error', 'User already exists.');
			return Redirect::to('users/edit/' . $id);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('error', 'User was not found.');
			return Redirect::to('users/edit/' . $id);
		}		
	}

	/**
	 * Process changes to user's group memberships
	 * @param  int 		$id The affected user's id
	 * @return [type]     [description]
	 */
	public function postUpdatememberships($id)
	{
		try 
		{
			//Get the current user's id.
			Sentry::check();
			$currentUser = Sentry::getUser();

		   	//Do they have admin access?
			if ( $currentUser->hasAccess('admin'))
			{
				$user = Sentry::getUserProvider()->findById($id);
				$allGroups = Sentry::getGroupProvider()->findAll();
				$permissions = Input::get('permissions');
				
				$statusMessage = '';
				foreach ($allGroups as $group) {
					
					if (isset($permissions[$group->id])) 
					{
						//The user should be added to this group
						if ($user->addGroup($group))
					    {
					        $statusMessage .= "Added to " . $group->name . "<br />";
							
							if($group->id === 3)
							{
								ProfilesController::updateSalesCheck($id, 1);
							}
					    }
					    else
					    {
					        $statusMessage .= "Could not be added to " . $group->name . "<br />";
					    }
					} else {
						// The user should be removed from this group
						if ($user->removeGroup($group))
					    {
					        $statusMessage .= "Removed from " . $group->name . "<br />";

							if($group->id === 3)
							{
								ProfilesController::updateSalesCheck($id, 0);
							}
					    }
					    else
					    {
					        $statusMessage .= "Could not be removed from " . $group->name . "<br />";
					    }
					}
				}
				Session::flash('info', $statusMessage);
				return Redirect::to('users/show/'. $id);
			} 
			else 
			{
				Session::flash('error', 'You don\'t have access to that user.');
				return Redirect::to('/');
			}
	
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('error', 'User was not found.');
			return Redirect::to('users/edit/' . $id);
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		    Session::flash('error', 'Trying to access unidentified Groups.');
			return Redirect::to('users/edit/' . $id);
		}
	}


	/**
	 * Prepare the "Ban User" form
	 * @param  int $id The user id
	 * @return View     The "Ban Form" view
	 */
	public function getSuspend($id)
	{
		try
		{
		    //Get the current user's id.
			Sentry::check();
			$currentUser = Sentry::getUser();

		   	//Do they have admin access?
			if ( $currentUser->hasAccess('admin'))
			{
				$data['user'] = Sentry::getUserProvider()->findById($id);
				return View::make('users.suspend')->with($data);
			} else {
				Session::flash('error', 'You are not allowed to do that.');
				return Redirect::to('/');
			}

		}
		catch (Cartalyst\Sentry\UserNotFoundException $e)
		{
		    Session::flash('error', 'There was a problem accessing that user\s account.');
			return Redirect::to('/users');
		}
	}

	public function postSuspend($id)
	{
		// Gather Sanitized Input
		$input = array(
			'suspendTime' => Input::get('suspendTime')
			);

		// Set Validation Rules
		$rules = array (
			'suspendTime' => 'required|numeric'
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{			
			return Redirect::to('users/suspend/' . $id)->withErrors($v)->withInput();
		}
		
		try
		{
			//Prep for suspension
			$throttle = Sentry::getThrottleProvider()->findByUserId($id);

			//Set suspension time
			$throttle->setSuspensionTime($input['suspendTime']);

			// Suspend the user
			$throttle->suspend();

			//Done.  Return to users page.
			Session::flash('success', "User has been suspended for " . $input['suspendTime'] . " minutes.");
			return Redirect::to('users');

		}
		catch (Cartalyst\Sentry\UserNotFoundException $e)
		{
		    Session::flash('error', 'There was a problem accessing that user\s account.');
			return Redirect::to('/users');
		}
	}


	public function postDelete($id)
	{
		try
		{
		    // Find the user using the user id
		    $user = Sentry::getUserProvider()->findById($id);

		    // Delete the user
		    if ($user->delete())
		    {
		    	// Delete the user from profiles table
		    	DB::table('profiles')
					->where('user_id', '=', $id)
					->delete();

		        // User was successfully deleted
		        Session::flash('success', 'That user has been deleted.');
				return Redirect::to('/users');
		    }
		    else
		    {
		        // There was a problem deleting the user
		        Session::flash('error', 'There was a problem deleting that user.');
				return Redirect::to('/users');
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('error', 'There was a problem accessing that user\s account.');
			return Redirect::to('/users');
		}
	}

	/**
	 * Generate password - helper function
	 * From http://www.phpscribble.com/i4xzZu/Generate-random-passwords-of-given-length-and-strength
	 * 
	 */	
	private function _generatePassword($length=9, $strength=4) 
	{
		$vowels = 'aeiouy';
		$consonants = 'bcdfghjklmnpqrstvwxz';
		if ($strength & 1) {
			$consonants .= 'BCDFGHJKLMNPQRSTVWXZ';
		}
		if ($strength & 2) {
			$vowels .= "AEIOUY";
		}
		if ($strength & 4) {
			$consonants .= '23456789';
		}
		if ($strength & 8) {
			$consonants .= '@#$%';
		}
	 
		$password = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		return $password;
	}
	
	private function oauth2($path) 
	{		
		try {
			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
//			$cal = new Google_Service_Calendar($client);

			//get user data from session
			$user = Sentry::getUser();

			//if has refresh_token
			if($user->google_refresh_token)
			{
				//set the refresh token
				$client->refreshToken($user->google_refresh_token);

				//save the new temp token into session
				Session::put('token', $client->getAccessToken());

				$this->checkCalendarChannel($user);

				//redirect to customers			
				return Redirect::to('/'.$path);
			}
			else //if has not then redirect to get a new one
			{
				$client->addScope("https://www.googleapis.com/auth/calendar");
				$client->addScope("https://www.googleapis.com/auth/drive");
				$authUrl = $client->createAuthUrl();				
				echo '<script type="text/javascript"> window.location.href="'.$authUrl.'"; </script>';
			}
		} catch (Exception $e) {
			Log::info('error: ' . $e->getMessage());
		}
	}
	
	public static function sendSMS($to, $body) 
	{
		require(base_path().'/vendor/twilio/sdk/Services/Twilio.php');

		$sid = "AC41ea4d78cdd4b4a0b035b4dd1842979c"; // Your Account SID from www.twilio.com/user/account
		$token = "f6abb54566571ef82de662877d7bd01b"; // Your Auth Token from www.twilio.com/user/account
		$from = "+15622505977"; // The phone # on our account

		$client = new Services_Twilio($sid, $token);
		$message = $client->account->messages->sendMessage(
		  $from, // From a valid Twilio number
		  $to, // Text this number
		  $body
		);
		
		return $message->sid;
	}

	// Form to test API's locally
	public function formLoad()
	{
		return View::make('customers.formtest');
	}

	/*
	 * This function is used to create a new channel if user hasn't one or expiration time is over.
	 * 
	 *
	 */
	public function checkCalendarChannel($user)
	{
		$addNew = false;

		try 
		{
			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_Service_Calendar($client);

			if (Session::has('token')) 
			{
				$client->setAccessToken(Session::get('token'));
			}

			$token = (Array) json_decode($client->getAccessToken());

			$user_channel = GoogleCalendarChannels::where('user_id', $user->id)->get()->last();

			if(!is_null($user_channel)) {
				//set the expiration time one day left
				$expiration_time = strtotime('-1 days', strtotime(date("d-m-Y", $user_channel->expiration)));

				if($expiration_time <= time()) 
				{
					GoogleCalendarChannels::where('user_id', $user->id)->delete();
					$addNew = true;
				}
			} 
			else {
				$addNew = true;
			}

			if($addNew)
			{
				$email = str_replace('.', '', str_replace('@', '', $user->email));
				$channel_id = $email . '_' . App::environment() . '_' . time();

				$chan = new Google_Service_Calendar_Channel();
				$chan->setId($channel_id);
				$chan->setToken($token['access_token']);
				$chan->setType("web_hook");
				$chan->setAddress("https://app.windowrnr.com/api/calpush");
				//$chan->setExpiration(strtotime("+1 week"));

				$watch = $cal->events->watch('primary', $chan);

				if($watch) {
					$channel = GoogleCalendarChannels::insert(array(
						'user_id' 			 => $user->id,
						'google_channel_id'  => $watch->id,
						'google_resource_id' => $watch->resourceId,
						'resource_uri' 		 => $watch->resourceUri,
						//'expiration' 		 => $watch->expiration,
						'token' 			 => $watch->token,
					));
					
				}
				Log::info("channel created");

				return true;
			}

		} catch (Exception $e) {
			var_dump($e->getMessage());
			Log::info("error creating channel -> ".$e->getMessage());
		}
	}

}
