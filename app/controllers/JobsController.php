<?php


class JobsController extends BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('auth');
	}
	
	public static function getJobDetail($job_id) 
	{
		$results = DB::table('jobs')
				->where('id', '=', $job_id)
				->get();

		//BaseController::firePhp()->addInfo('$results(jobs)', array($results));

		return $results;
	}

	public static function jobCustDetail($id)
	{
		try 
		{
			$results = Job::getJobCustNotes($id);
//			$results->window_totals = Window_totalsController::totalsByJobID($id);
			$results->events = (object)DB::table('events')->where('job_id', $id)->get();
			foreach ($results->events as $event) 
			{
				$user = Sentry::findUserById($event->cal_user_id);
				$event->who = $user->first_name;
				$event->appt = Appointment::getApptByEventID($event->id);
			};
			
			$results->notes = Note::notesByJobID($id);
			$results->estimates = Estimate::getEstimatesByJobID($id);

			$results->partsLists = [];
			foreach ($results->estimates as $estimate) {
				$estimatePartsList = DB::table('parts_lists')->where('estimate_id', '=', $estimate->id)->first();
				if($estimatePartsList) {
					$estimate->partsListID = $estimatePartsList->id;	
				}
				else {
					$estimate->partsListID = FALSE;
				}
			}

			$finalisedEstimatePartsList = DB::table('parts_lists')->where('job_id', '=', $id)->where('status', '=', 1)->first();
			if($finalisedEstimatePartsList) {
				$results->finalisedPartsListID = $finalisedEstimatePartsList->id;	
			}
			else {
				$results->finalisedPartsListID = FALSE;
			}

			return View::make('jobs.edit')->with('results', $results);
		}
		catch (Exception $e) 
		{	
			Session::flash('error', 'There was a problem: '.$e->getMessage());
			return $e;
		}

	}

	public static function jobEventDetail($job_id, $appt_id) 
	{
		try 
		{	
			$results = Job::getJobCustNotes($job_id);
//			$results->jobs = DB::table('jobs')->where('id', $job_id)->first();
//			$results->custDetail = DB::table('customers')->where('id', $results->job->customer_id)->first();
//			$results['window_totals'] = Window_total::totalsByJobID($job_id);
//			$results->appt = DB::table('appointments')->where('id', $appt_id)->first();
			$results->appt = Appointment::getAppt($appt_id);
			$results->sales = DB::table('profiles')
					->where('google_calendar_id', $results->appt->user)
					->pluck('first_name');

/*			foreach ($results['jobs'] as $job) 
			{
				$job->notes = Note::notesByJobID($job->id);
			};
*/
//			//BaseController::firePhp()->addInfo('JobsContr@jobDetail - $resultstotal', array($results));

			return json_encode($results);
		}
		catch (Exception $e) 
		{
			echo $e;
		}
	}

	public static function listEstimates()
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/logout');
		}
		else
		{
			// User is logged in
			try 
			{
				$results['jobs'] = DB::table('estimate_list')->get();

				//BaseController::firePhp()->addInfo('$results', array($results));
				
				return View::make('jobs.estimates')->with('estimates', $results['jobs']);
			}
			catch (Exception $e) 
			{	
				Session::flash('error', 'There was a problem: '.$e->getMessage());
				return $e;
			}
		}
	}

	public function postJobDetailByCustID()
	{	
		try 
		{
			if (isset($_POST["id"])) 
			{
				$custID = $_POST["id"];
			} 
			else 
			{
				return "POST failed";
			}

			$status = "1 or 2 or 3";
			$archive = "1 or 2";
		
			$results = DB::table('jobs')
					->where('jobs.customer_id', '=', $custID)
					->get();

			return $results;
		}
		catch (Exception $e) 
		{
			return $e;
		}
	}
	
	public static function jobDetailByCustID($custID, $status, $archive)
	{
		try 
		{
			if (isset($_POST["id"])) 
			{
				$custID = $_POST["id"];
			}

			if(!isset($status)) 
			{
				$status = "1 or 2 or 3";
			}

			if(!isset($archive)) 
			{
				$archive = "1 or 2";
			}

			$results = DB::table('jobs')				
				->select(
					'jobs.id as job_id', 
					'jobs.created_at as job_created_at', 
					'jobs.created_by as job_created_by', 
					'jobs.address as job_address', 
					'jobs.city as job_city', 
					'jobs.state as job_state', 
					'jobs.zip as job_zip', 
					'jobs.built as job_house_built',
					'jobs.type as job_type',
					'jobs.symptoms as job_symptoms',
					'jobs.lead_source as job_lead_source'
				)
				->where('jobs.status', '=', $status)
				->where('jobs.archive', '=', $archive)
				->where('jobs.customer_id', '=', $custID)
				->get();
			
			//BaseController::firePhp()->addInfo('$results(jobs)', array(print_r(var_dump($results))));
				
			return $results;
		}
		catch (Exception $e) 
		{
			Session::flash('error', 'There was a problem: '.$e);
			return $e;
		}
	}

	public function archive($id)
	{
		DB::table('jobs')
			->where('id', $id)
			->update(array('archive' => '1'));
		
		//create a new note
		$user = Sentry::getUser();
		$note = 'Lead archived on '.date("n/j/Y (g:ia)", time()).' by: '.$user->first_name;
		Note::createNote($id, $user->id, $note, 1);
		
		Session::flash("success", "Job #".$id." archived.");
		return Redirect::to('customers');
	}

	public function unarchive() {
		DB::table('jobs')
			->where('id', Input::get('id'))
			->update(array('archive' => '0'));
		return "OK";
	}

	public function unarchiveDirect($id) {
		DB::table('jobs')
			->where('id', $id)
			->update(array('archive' => '0'));
		Session::flash('success', 'Job unarchived successfully');
		return Redirect::to('/job/' . $id);
	}

	public function archiveNote()
	{
		$job_id = $_POST['job_id'];
		
		if( isset($_POST['returnURL']) ) {
			if ($_POST['returnURL'] == 'msgboard') {
				$returnURL = '/'.$_POST['returnURL'];
			} else {
				$returnURL = '/customers?status=' . Input::get('status') . '&scheduled=' . Input::get('scheduled');
			}
		} else {
			$returnURL = '/customers?status=' . Input::get('status') . '&scheduled=' . Input::get('scheduled');
		}
		
		DB::table('jobs')
			->where('id', $job_id)
			->update(array('archive' => '1'));
		
		//create a new note
		$user = Sentry::getUser();
		$note = 'Lead archived on '.date("n/j/Y (g:ia)", time()).' by: '.$user->first_name;
		$archiveNote = 'Lead archived with Note: '.$_POST['archiveNote'];
		Note::createNote($job_id, $user->id, $note, 1);
		Note::createNote($job_id, $user->id, $archiveNote, 1);
		
		Session::flash("success", "Job #".$job_id." archived.");
		return Redirect::to($returnURL);
	}

	public function updateStatus($id, $status) {
		Job::updateStatus($id, $status);
		return Redirect::to('/msgboard');
	}
}