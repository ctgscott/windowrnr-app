<?php



class AdminController extends BaseController {
	/**
	 * Provides admin support for the app
	 *
	 * Return JSON 
	 */
	
	public function index() 
	{
	    return View::make('admin');
	}

	public function refreshMap() 
	{
		$file = public_path().'/custMap.json';
		$data['jobs'] = DB::table('cust_map')
			->where('lat', '>', '0')
			->get();
		file_put_contents( $file, 'var data = ' . json_encode($data, JSON_NUMERIC_CHECK));
		
		Session::flash('success', 'Customer Map file refreshed: ' . $file);

		return Redirect::to('/customers');
	}

	public function reindexSolr() {

		// clear index
		file_get_contents("http://localhost:8983/solr/windowrnr/update?stream.body=<delete><query>*:*</query></delete>");
		file_get_contents("http://localhost:8983/solr/windowrnr/update?stream.body=<commit/>");

		// generate CSV from view: search
		$records = DB::table('search')->where('job_created', '>', '1990-01-01 00:00:00')->get();
		$lines = ['cust_id,"f_name","l_name","company_name","billing_address","billing_city","address","city",job_id,"job_created"'];
		foreach ($records as $record) {
			$line = implode(",", [
							$this->clean($record->cust_id), 
							$this->clean($record->f_name), 
							$this->clean($record->l_name), 
							$this->clean($record->company_name), 
							$this->clean($record->billing_address), 
							$this->clean($record->billing_city), 
							$this->clean($record->address), 
							$this->clean($record->city), 
							$this->clean($record->job_id), 
							$this->clean($record->job_created)
						]);
			array_push($lines, $line);
		}
		$temp_file = rtrim(sys_get_temp_dir(), '/') . '/' . str_random(16) . '.csv';
		file_put_contents($temp_file, implode("\n", $lines));

		// post data to solr
		$command = Config::get('solr.bin_path') . 'post -c windowrnr ' . $temp_file;
		$result = shell_exec($command);

		return '<h2>Reindexing Complete</h2>'.
				'<pre style="margin: 15px 0; background-color: #eee; padding: 10px;">' . $result . '</pre>'.
				'<a style="background-color: #ccc; padding: 10px; display: inline-block; text-decoration: none;" href="/search/">Back</a>';

	}

	public function updateAllUserChannels() {
		if(Request::getClientIp() === '127.0.0.1') {
			$users = User::where('activated', '=', 1)->get();
			$userController = new UserController();
			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			foreach ($users as $user) {
				if($user->google_refresh_token) { // possible only for users with a google_refresh_token in the database (i.e. need to login once)
					$client->refreshToken($user->google_refresh_token);
					Session::put('token', $client->getAccessToken());
					$userController->checkCalendarChannel($user);
				}
			}
			return "Completed";
		}
		return "Bad Request. Auth failure."; // Disallow this call from remote machines (local cron task only)
	}

	private function clean($_value) {
		return '"' . str_replace('"', '\\"', '' . $_value) . '"';
	}
}