<?php

class APIController extends BaseController {
	use TimeTrait;
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Functions to support the API side of the app
	 *
	 * Return JSON 
	 */	
	public function login() 
	{

		header('Content-Type: application/json; charset=utf-8');

		// Gather Sanitized Input
		$input = [
			'email'      => Input::get('email'),
			'password'   => Input::get('password'),
			'client_key'   => Input::get('client_key'),
			'rememberMe' => 1
		];

		// Set Validation Rules
		$rules = [
			'email'    => 'required|min:4|max:32|email',
			'password' => 'required|min:6',
			'client_key'    => 'required|min:1|max:255'
		];

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails()) {
			// Validation has failed
			$result = API::respondFailJSend('Login Error', 'Validation failed');
		}
		else  {
			try {
			    //Check for suspension or banned status
				$user = Sentry::getUserProvider()->findByLogin($input['email']);
				$throttle = Sentry::getThrottleProvider()->findByUserId($user->id);
			    $throttle->check();

			    // Set login credentials
			    $credentials = array(
			        'email'    => $input['email'],
			        'password' => $input['password']
			    );

			    // Try to authenticate the user
			    $user = Sentry::authenticate($credentials, $input['rememberMe']);
				if($user) {

					// auth success
					
					// NEW: Same persist_code used for a user across all logins
					/*
					// delete any existing mapping in user_sessions for the same user_id/client_key combo
					DB::table('user_sessions')->where('user_id', $user->id)->where('client_key', $input['client_key'])->delete();
					
					// store client_key/persist_code mapping to table: user_sessions
					DB::table('user_sessions')->insert([
						'user_id' => $user->getId(),
						'client_key' => $input['client_key'],
						'persist_code' => $user->persist_code,
						'last_used_at' => date("Y-m-d H:i:s")
					]);
					*/

					// Create an array of data to persist to the session and / or cookie
					$data = array('id' => $user->getId(), 'token' => base64_encode($user->persist_code));
	
					//Login was succesful.  
					$result = API::respondSuccess($data);
				}
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    // Sometimes a user is found, however hashed credentials do
			    // not match. Therefore a user technically doesn't exist
			    // by those credentials. Check the error message returned
			    // for more information.
			    Session::flash('error', 'Invalid username or password.' );
				$result = API::respondFailJSend('Login Error', 'Invalid Username or Password');
			}
			catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
			{
			    Session::flash('error', 'You have not yet activated this account.');
				$result = API::respondFailJSend('Login Error', 'You have not yet activated this account');
			}

			// The following is only required if throttle is enabled
			catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
			{
			    $time = $throttle->getSuspensionTime();
			    Session::flash('error', "Your account has been suspended for ".$time." minutes.");
				$result = API::respondFailJSend('Login Error', 'Your account has been suspended for '.$time.' minutes');
			}
			catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
			{
			    Session::flash('error', 'You have been banned.');
				$result = API::respondFailJSend('Login Error', 'User Banned');
			}
			catch (Exception $e) 
			{
				$result = API::respondFailJSend('Login Error', $e);
			}
		
		}

		return Response::make(json_encode($result, JSON_NUMERIC_CHECK), 200, ['Content-Type' => 'application/json; charset=utf-8']);
	}
	
	/*

	API end point for resetting the user's password - POST

	Expected parameters:
	
	1. email: The email id of the user's whose password is to be reset

	Response:

	Success: {'status': 'ok'}	--	means reset code has been emailed to the user
									app should now ask the user to check his email
	
	Failure: {'status': 'error', 'reason': '<reason>'}

	*/
	public function resetPassword() {

		$email = Input::get('email');

		// get user matching email address
		$user = Sentry::getUserProvider()->findByLogin($email);
		if(!$user) {
			return json_encode(['status' => 'error', 'reason' => 'User not found']);
		}

		// send reset code
		$data = [];
	    $data['resetCode'] = $user->getResetPasswordCode();
	    $data['userId'] = $user->getId();
	    $data['email'] = $email;

	    // Email the reset code to the user
		Mail::send('emails.auth.reset', $data, function($mail) use($email) {
		    $mail->to($email)->subject('Password Reset Confirmation | WindowRnR');
		});

		// send response
		return json_encode(['status' => 'ok']);
	}

	public static function appendToLog($msg, $file) {
		file_put_contents("/tmp/" . $file, $msg . PHP_EOL, FILE_APPEND);
	}
	
	public function calPush() 
	{

		// trouble - disabling calpush temporarily
		// if(true) return "";

		if(!Request::header('x-goog-resource-id')) {
			return "";
		}

		// check if its intended for this environment
		$channelID = Request::header('x-goog-channel-id');
		$channelID = explode("_", $channelID);
		if(count($channelID) !== 3 || $channelID[1] !== App::environment()) {
			return "";
		}
		$channelID = Request::header('x-goog-channel-id');

		// check if valid channel
		$stopChannel = false;
		$channelNum = DB::table('google_calendar_channels')->where('google_channel_id', '=', Request::header('x-goog-channel-id'))->count();
		if($channelNum != 1) {
			$stopChannel = true;
		}

		//set the google client API
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$calendar = new Google_Service_Calendar($client);

		$logFileName = Request::header('x-goog-channel-id') . "-" . date("Y-m-d-H-i-s") . "-" . rand(1001, 9999);

		$resource_id = Request::header('x-goog-resource-id');
		$expiration  = strtotime(Request::header('x-goog-channel-expiration'));

		//get the user refresh token based in resource_id
		$res = GoogleCalendarChannels::where('google_resource_id', $resource_id)->get()->first();

		$i = 0;

		if(!$stopChannel) {
			APIController::appendToLog("----------------------------------------\ncalPush() called at: " . date("Y-m-d H:i:s"), $logFileName);
			Log::info("complete header -> ", Request::header());
		}

		if($res) {
			//update the expiration time
			GoogleCalendarChannels::where('google_resource_id', $resource_id)->update(array('expiration' => $expiration));

			//set the refresh token
			$access_token = $client->refreshToken($res->user->google_refresh_token);
			$client->setAccessToken($client->getAccessToken());

			if ($client->getAccessToken()) 
			{

				// if stopChannel is true, stop and return
				if($stopChannel) {
					$chan = new Google_Service_Calendar_Channel();
					$chan->setId($channelID);
					$chan->setResourceId($resource_id);
					$channel = $calendar->channels->stop($chan);

					return "";
				}

				$numUpdated = 0;
				$numAdded = 0;
				$numAddedUnmapped = 0;
				$numIgnored = 0;

				// get the calendar id of the channel's owner
				try {
					$calendar_id = $res->user->profile->google_calendar_id;
					if(!empty($calendar_id)) {

						// list events in calendar
						$events = $calendar->events->listEvents($calendar_id, [
							'maxResults' => 500,
							'showDeleted' => true,
							'updatedMin' => date("Y-m-d\TH:i:sO", time() - 3600)
						]);

						while(true) {

							// process each event
							foreach ($events->getItems() as $event) {

								// check if event is deleted/cancelled, if YES - move to events_archived
								if($event->status === 'cancelled') {

									DB::table('events_archived')->where('google_event_id', '=', $event->id)->delete();
									DB::statement('INSERT INTO events_archived SELECT * FROM events WHERE google_event_id = :id', ['id' => $event->id]);
									DB::table('events')
										->where('google_event_id', '=', $event->id)
										->where('google_cal_id', '=', $calendar_id)
										->delete();

									APIController::appendToLog('Cancelled event: ' . $event->id . ' - moved to events_archived', $logFileName);

								}

								else {

									// if existing in db and older, update
									$localEvent = DB::table('events')
										->where('google_event_id', '=', $event->id)
										->where('updated_at', '<', date("Y-m-d H:i:s", strtotime($event->updated)))
										->get();

									if($localEvent) {
										
										APIController::appendToLog('Event exists locally but is older than the one in calendar: ' . $event->id, $logFileName);
										
										APIController::appendToLog('-----------', $logFileName);
										ob_start();
										var_dump($event);
										$tempOP = ob_get_clean();
										APIController::appendToLog($tempOP, $logFileName);
										APIController::appendToLog('-----------', $logFileName);

										$string = explode("\n", $localEvent->description, 2);
										$string2 = explode(": ", $string[0]);
										$event['event_type'] = $string2;

										Events::updateEventDataWithCalendar($event, 'events');
										$numUpdated++;
									}
									else {

										// check if existing but not older
										$existing = DB::table('events')->where('google_event_id', '=', $event->id)->count();

										// check if event has job_id extendedProperty
										$evJobId = false;
										try {
											$eps = $event->getExtendedProperties();
											if($eps) $eps = $eps->getShared();
											if(!empty($eps) && isset($eps['job_id'])) {
												$evJobId = intval($eps['job_id']);
											}
										}
										catch (Exception $e) {
											// no job id property in event
										}

										if(!empty($evJobId) && $evJobId && $existing == 0) { // use job_id extProp and add as normal event (if not existing)

											$event['job'] = $evJobId;
											$string = explode("\n", $createdEvent->description, 2);
											$string2 = explode(": ", $string[0]);
											$event['event_type'] = $string2;
											
											APIController::appendToLog('job_id value: ' . $evJobId, $logFileName);

											APIController::appendToLog('Event with job_id property does not exist locally: ' . $event->id, $logFileName);
											Events::insertEventData($event);	
											$numAdded++;
											
										}
										else { // add to events_unmapped (if already not added)

											if($existing == 0) {

												$unmapped = DB::table('events_unmapped')->where('google_event_id', '=', $event->id)->count();

												if($unmapped == 0) {

													if($existing == 0) {
														APIController::appendToLog('Event (unmapped) does not exist locally: ' . $event->id, $logFileName);
													}

													Events::insertEventData($event, 'events_unmapped');
													$numAddedUnmapped++;
												}

											}
										}
										
									}

								}

							}

							$pageToken = $events->getNextPageToken();
							APIController::appendToLog("nextPageToken = $pageToken", $logFileName);
							if ($pageToken) {
								$events = $calendar->events->listEvents($calendar_id, [
									'pageToken' => $pageToken,
									'maxResults' => 500,
									'showDeleted' => true,
									'updatedMin' => date("Y-m-d\TH:i:sO", time() - 3600)
								]);
							}
							else {
								APIController::appendToLog("No more event pages. Thats it. :)", $logFileName);
								break;
							}

						}

					}
				}
				catch(Exception $e) {
					APIController::appendToLog("Exception: $e", $logFileName);
				}
			}
			else {
				APIController::appendToLog("getAccessToken error!", $logFileName);
			}
		}

		Log::info($i.' events updated.');
		APIController::appendToLog("Done. Updated: $numUpdated, Added: $numAdded, Added_Unmapped: $numAddedUnmapped, Ignored: $numIgnored", $logFileName);
		return "";
	}

	public static function eventDetail($id) {
		try {
			$data = Events::getEventByGoogID($id);
			$result = API::respondSuccess($data);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function eventIDDetail($id) {
		try {
			$data = Events::getEventsByID($id);
			$result = API::respondSuccess($data);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function eventsList($start, $end, $calID = 0)
	{

		$statusEnum = [
			"confirmed" => 1,
			"tentative" => 2,
			"cancelled" => 3
		];

		$response = false;

		try {
			if($start == 1 && $end == 1) {
				if ($calID == 0) {
					$result = DB::table('events')->get();
				}
				else {
					$calIDs = explode(',', $calID);
					$result = DB::table('events')
						->whereIn('cal_user_id', $calIDs)
						->get();
				}
			} 
			else {
				$start = date("Y-m-d H:i:s", $start);
				$end = date("Y-m-d H:i:s", $end);
				if ($calID == 0) {
					$result = DB::table('events')
						->where('start', '>=', $start)
						->where('end', '<=', $end)
						->get();
				}
				else {
					$calIDs = explode(',', $calID);
					$result = DB::table('events')
						->whereIn('cal_user_id', $calIDs)
						->where('start', '>=', $start)
						->where('end', '<=', $end)
						->get();
				}
			}	

			foreach ($result as $event) {

				/*
				Status to be returned as an integer (instead of string)
				confirmed => 1
				tentative => 2
				cancelled => 3
				*/
				if(isset($statusEnum[$event->status])) {
					$event->status = $statusEnum[$event->status];
				}
				else {
					// unknown event status
					$event->status = 0;	
				}

				/*
				Lat and Lng fields to be returned as decimals (instead of string)
				*/
				$event->lat = (isset($event->lat) && !empty($event->lat) && is_numeric($event->lat)) ?
									doubleval($event->lat) :
									false;
				$event->lng = (isset($event->lng) && !empty($event->lng) && is_numeric($event->lng)) ?
									doubleval($event->lng) :
									false;
			}

			if(!$result) {
				$response = API::respondError('There are no events during the selected time period');
			}
			else {
				$response = API::respondSuccess($result);
			}
		}
		catch (Exception $e) {
			$response = API::respondError($e->getMessage());
		}
		return API::respond($response);
	}
	
	public static function jobDetail($id) {
		try {
			$data = DB::table('jobs')->where('id', '=', $id)->get();
			$result = API::respondSuccess($data);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}

	public static function customerDetail($id) {
		try {
			$data = DB::table('customers')->where('id', '=', $id)->get();
			$result = API::respondSuccess($data);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function postCustUpdate() {
		header('Content-Type: application/json; charset=utf-8');
		try {
			$id = $_POST['id'];
			$now = date("Y-m-d H:i:s");
			$cust = array(
				'f_name' => $_POST['f_name'],
				'l_name' => $_POST['l_name'],
				'company_name' => (isset($_POST['company_name']) ? $_POST['company_name'] : NULL),
				'phone' => $_POST['phone'],
				'email' => $_POST['email'],
				'alt_phone' => $_POST['alt_phone'],
				'billing_address' => $_POST['billing_address'],
				'billing_city' => $_POST['billing_city'],
				'billing_state' => $_POST['billing_state'],
				'billing_zip' => $_POST['billing_zip'],
				'updated_at' => $now);	

			$result = json_encode(DB::table('customers')
				->where('id', $id)
				->update($cust));
			
			echo $result;
		}
		catch (Exception $e) {
			echo $e;
		}
	}

	public static function postEventUpdate($id, $event) {
		header('Content-Type: application/json; charset=utf-8');
		try {
			$id = $_POST['id'];
			$now = date("Y-m-d H:i:s");
			$event = [
				'google_event_id' => $_POST['google_event_id'],
				'google_cal_id' => $_POST['google_cal_id'],
				'cust_id' => $_POST['cust_id'],
				'cal_user_id' => $_POST['cal_user_id'],
				'status' => $_POST['status'],
				'organizerEmail' => $_POST['organizerEmail'],
				'htmlLink' => $_POST['htmlLink'],
				'start' => $_POST['start'],
				'end' => $_POST['end'],
				'location' => $_POST['location'],
				'description' => $_POST['description'],
				'allDay' => $_POST['allDay'],
				'title' => $_POST['title'],
				'type'  => intval(Input::get('event_type')),
				'lat' => $_POST['lat'],
				'lng' => $_POST['lng'],
				'updated_at' => $now
			];

			switch(intval(Input::get('event_type'))) {
				case EVENT_EST_APPT:
					$event['event_type'] = 'Estimate Appt.';
					break;
				case EVENT_INSTALL_REPAIR:
					$event['event_type'] = 'Install/Repair Job';
					break;
				case EVENT_SERVICE_CALL:
					$event['event_type'] = 'Service Call';
					break;
			}

			$result = json_encode(DB::table('events')
				->where('id', $id)
				->update($event));
			
			echo $result;
		}
		catch (Exception $e) {
			echo $e;
		}
	}
	
	public function rebuildCustomersTable() {
		header('Content-Type: application/json; charset=utf-8');
		$source = DB::table('testSheet1')->get();
		$i = 0;
		foreach($source as $cust) {
			set_time_limit(10);
			$i++;
			$installDate = date('Y-m-d H:i:s', strtotime($cust->install_date));
			$customer = array(
				'f_name' => $cust->fname,
				'l_name' => $cust->lname,
				'phone' => $cust->phone,
				'alt_phone' => $cust->alt_phone,
				'billing_address' => $cust->address,
				'billing_city' => $cust->city,
				'billing_state' => $cust->state,
				'billing_zip' => $cust->zip,
				'created_at' => $installDate,
				'updated_at' => $installDate
			);	
			DB::table('customers')->insert($customer);
		};
		echo($i." rows inserted");
	}
	
	public function rebuildJobsTable() {
		header('Content-Type: application/json; charset=utf-8');
		$source = DB::table('testSheet1')->get();
		$i = 0;
		foreach($source as $cust) {			
			set_time_limit(10);
			$i++;
			$installDate = date('Y-m-d H:i:s', strtotime($cust->install_date));
			$customer = array(
				'f_name' => $cust->fname,
				'l_name' => $cust->lname,
				'phone' => $cust->phone,
				'alt_phone' => $cust->alt_phone,
				'billing_address' => $cust->address,
				'billing_city' => $cust->city,
				'billing_state' => $cust->state,
				'billing_zip' => $cust->zip,
				'created_at' => $installDate,
				'updated_at' => $installDate
			);	
			$id = DB::table('customers')->insertGetID($customer);
			$address = urlencode($cust->address.", ".$cust->city.", ".$cust->state.", ".$cust->zip);
			$latlng = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false"));
			if (isset($latlng->results[0]->geometry->location->lat)) {
				$lat = $latlng->results[0]->geometry->location->lat;
				$lng = $latlng->results[0]->geometry->location->lng;
			} 
			else {
				$lat = null;
				$lng = null;
			}
			$job = array(
				'customer_id' => $id,
				'status' => 4,
				'archive' => 1,
				'created_by' => 'import',
				'address' => $cust->address,
				'city' => $cust->city,
				'state' => $cust->state,
				'zip' => $cust->zip,
				'lat' => $lat,
				'lng' => $lng,
				'created_at' => $installDate,
				'updated_at' => $installDate,
				'job_completed' => $installDate
			);
			DB::table('jobs')->insert($job);
		};
		echo($i." rows inserted");
	}
	
	public function addNote() {
		$job_id = Input::get('job_id');
		$user_id = Input::get('user_id');
		$note = Input::get('note');
		if(empty($job_id)) $result = API::respondError('Parameter job_id is empty or missing');
		else if(empty($user_id)) $result = API::respondError('Parameter user_id is empty or missing');	
		else if(empty($note)) $result = API::respondError('Parameter note is empty or missing');	
		else {
			try {
				$now = date("Y-m-d H:i:s");
				DB::table('notes')->insert([
					'job_id' => $job_id, 
					'user_id' => $user_id, 
					'note' => urldecode($note), 
					'created_at' => $now
				]);
				$result = API::respondSuccess('Note successfully added');
			}
			catch (Exception $e) {
				$result = API::respondError($e->getMessage());
			}
		}
		return API::respond($result);
	}
	
	public function listNotes($job_id) {
		try {
			$data = DB::table('notes')
				->select('user_id', 'note', 'created_at', 'is_system')
				->where('job_id', '=', $job_id)
				->get();
			foreach ($data as $row) {
				$row->is_system = !!$row->is_system;
			}
			$result = API::respondSuccess($data);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function sashWeight($width, $height, $glass, $material, $sashThickness) {
		
		// $glass: # of 1/8" sheets of glass
		// can do fractions (ie. 3/16" glass = 1.5)

		// $material type of sash 
		// 1 = Douglas Fir
		// 2 = Pine
		// No others yet (should figure out oak)
		if($material == 1) {
			$woodType = .0191;
		}
		else {
			$woodType = .0128;
		}
		
		// $sashThickness: 
		// 1: 1 3/8" thickness
		// 2: 1 3/4" thickness
		if($sashThickness == 1) {
			$thick = 1.375;
		}
		else {
			$thick = 1.75;
		}

		$stile = (((($height/2)-3)*4)*$thick);
		$woodTop = (($width*(3*$thick))+$stile)*$woodType;
		$woodBottom = (($width*(4*$thick))+$stile)*$woodType;
		$woodTotal = $woodTop+$woodBottom;
		$glassType = $glass*.0059;
		$glassSash = (($width-4)*(($height/2)-3)*$glassType);

		// echo "Top sash wood weight = ".round($woodTop, 2)." Lbs.<br />";
		// echo "Bottom sash wood weight = ".round($woodBottom, 2)." Lbs.<br />";
		// echo "Sash glass weight = ".round($glassSash, 1)." Lbs.<br /><br />";
		// echo "Top sash total weight = ".round($woodTop+$glassSash, 1)."Lbs.<br />";
		// echo "Bottom sash total weight = ".round(($woodBottom+$glassSash), 1)."Lbs.<br />";

		return API::respond(API::respondSuccess([
			'top_wood_weight' => round($woodTop, 2) . ' Lbs',
			'bottom_wood_weight' => round($woodBottom, 2) . ' Lbs',
			'glass_weight' => round($glassSash, 1) . ' Lbs',
			'top_total_weight' => round($woodTop + $glassSash, 1) . ' Lbs',
			'bottom_total_weight' => round($woodBottom + $glassSash, 1) . ' Lbs'
		]));
		
	}
	
	public static function getStyle($id) {
		try {
			$data = DB::table('win_service')->where('win_style', $id)->get();
			$result = API::respondSuccess($data);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}

	public static function getOptions($serv_id) {
		try {
			$data = Option::optionsByServID($serv_id);
			$result = API::respondSuccess($data);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function getEstimateStart($job_id = null) {
		try {
			if(isset($job_id)) {
				$results['job'] = Job::getJobCustNotes($job_id);
			}
			$results['rooms'] = DB::table('rooms')->select('id as id', 'room as name')->get();
			$results['dir'] = DB::table('dir')->select('id as id', 'dir as name')->get();
			$results['winStyle'] = DB::table('win_style')->select('id as id', 'winStyle as name')->get();
			$result = API::respondSuccess($results);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function estimateSave() {
		try {
			$newEst = array(
				'job_id' => Input::get('job_id'),
				'description' => Input::get('description')
			);
			$estID = (string)Estimate::addEstimate($newEst);
			$items = Input::get('item');
			foreach ($items as $item) {
				$item['est_id'] = $estID;
				if(isset($item['itemID'])) {
					unset($item['itemID']);
				}
				$res = Estimate::addEstimateItems($item);
			}
			$result = API::respondSuccess('Estimate #'.$estID.' saved.');				
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function estimateUpdate() {
		try {
			$all = Input::all();
			$jobID = Input::get('job_id');
			$estimateID = Input::get('est_id');
			$estimate = [];
			$estimate['job_id'] = $jobID;
			$estimate['cust_id'] = Input::get('cust_id');
			$estimate['description'] = Input::get('description');
			$estimate['notes'] = Input::get('estimate_notes');
			$estimate['parts'] = Input::get('parts');

			if(Input::get('deposit')) {
				$estimate['deposit'] = Input::get('deposit');
			} else {
				$estimate['deposit'] = null;
			}

			$estResult = Estimate::updateEstimate($estimateID, $estimate);

			if($estResult === 'success') 
			{
				$items = Input::get('item');
 
				foreach ($items as $item) 
				{
					if($item['itemID'] == 0)
					{
						unset($item['itemID']);
						$item['est_id'] = Input::get('est_id');
						Estimate::addEstimateItems($item);
					} else {
						$itemID = $item['itemID'];
						unset($item['itemID']);
						$item['archive'] = 0;
						Estimate::updateEstimateItems($itemID, $item);
					}
				}
				
				Estimate::deleteArchive($estimateID);

				PartsListsController::createOrUpdate($jobID, $estimateID);
				
				$result = API::respondSuccess('Estimate #'.$estimateID.' updated.');
			} else {
				$result = API::respondFail('Estimate #'.$estimateID.' update failed with message: '.$estResult);
			}
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function getEstimateList($jobID) {
		try {
			$list = Estimate::getEstimatesByJobID($jobID);
			foreach ($list as $row) {
				$row->sold = !!$row->sold;
			}
			$result = API::respondSuccess($list);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}

	public static function getEstimate($estID) {
		try {
			$est = Estimate::getEstimateByID($estID);
			if(!isset($est['sold'])) $est['sold'] = 0; 
			$est['sold'] = !!$est['sold']; // make boolean

			// json_decode common parts
			if(isset($est['parts']) && !is_null($est['parts'])) {
				$est['parts'] = json_decode($est['parts']);
			}

			// include parts-list id & finalized status of estimate
			$partsList = DB::table('parts_lists')->where('estimate_id', '=', $estID)->first();
			$est['parts_list_id'] = ($partsList ? $partsList->id : NULL);
			if($est['parts_list_id']) {
				$est['parts_list_finalized'] = !!$partsList->status;
			}
			
			// make data structure changes as needed by api consumer
			if(isset($est['items']) && count($est['items'])) {
				foreach ($est['items'] as $item) {

					// json_decode parts in all items
					if(isset($item->parts)) {
						$item->parts = json_decode($item->parts);
					}
					
					// get any images for each estimate line item
					//if(isset($item->id)) {
						$item->files = json_decode(ImageController::images('estimate', $item->id));

					// objectify serv_options
					if(isset($item->serv_options) && !empty($item->serv_options)) {
						$servOptions = explode(",", $item->serv_options);
						$servOptionsArray = [];
						foreach ($servOptions as $servOption) {
							$optionParts = explode("-", $servOption);
							$servOptionsArray[] = ['id' => intval($optionParts[0]), "price" =>  floatval($optionParts[1])];
						}
						$item->serv_options = $servOptionsArray;
					}
					else {
						$item->serv_options = [];
					}
				}
			}

			$result = API::respondSuccess($est);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}

	public static function estimateSyncData() {
		try {
			$data = Estimate::syncData();
			$result = API::respondSuccess($data);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function getFolderID($jobID) {
		try {
			$folderID = Folder::getID($jobID);
			if($folderID != null) {
				$result = API::respondSuccess($folderID);
			}
			else {
				$result = API::respondError($folderID);
			}
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
				
	public static function createFolder($jobID, $folderID, $parentID) {
		try {
			$create = Folder::insert($jobID, $folderID, $parentID);
			$result = API::respondSuccess($create);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}
	
	public static function createCalendarEvent() {
		try {
			$create = CustomersController::calNewLead(true);
			$result = API::respondSuccess($create);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
	
		return API::respond($result);
	}
	/*
	
	API end point for uploading job related files from mobile/other clients - POST
	
	Expected parameters:
	
	1. title: file title
	2. description: file description
	3. owner_type: "job" or "estimate"
	4. owner_id: jobID or estimateID
	5. file: file data (payload)
	6. type: "image", "pdf", etc.

	*/
	public static function saveFile() {

		// upload to local directory
		$file = Input::file('file');
		$destinationPath = public_path().'/uploads';
		$filename = str_random(16) . '.' . $file->getClientOriginalExtension();
		$mime = $file->getMimeType();
		$upload_success = $file->move($destinationPath, $filename);

		$owner_type = Input::get('owner_type');
		$owner_id = Input::get('owner_id');

		$results = API::respondFail('Upload Failed');

		// upload to google drive
		if( $upload_success ) {

			$upload_success = false;

			// get job id
			$job_id = 0;
			if($owner_type === 'estimate') {
				$lineItem = DB::table('estimate_items')			
							->where('id', '=', $owner_id)
							->first();
				$estimate = DB::table('estimates')			
							->where('id', '=', $lineItem->est_id)
							->first();
				$job_id = $estimate->job_id;
			}
			else if($owner_type === 'job') {
				$job_id = $owner_id;
			}

			// upload to drive
			if($job_id > 0) {
				
				$data = file_get_contents($destinationPath . '/' . $filename);
				$meta = ['title' => Input::get('title'), 'description' => Input::get('description'), 'mimeType' => $mime];
				$result = Drive::saveFile($job_id, $data, $meta);
				
				if($result !== FALSE) {
					$result = json_decode($result);
					if(isset($result->webContentLink)) { // upload success
						$upload_success = true;
						$fileURL = $result->webContentLink;
					}
					else {
						// no content link in google's response
						$results = API::respondFail('No content link in Google Drive API response');
					}
				}
				
			}
			else {
				// couldn't find corresponding job in db
				$results = API::respondFail('Cannot find job# ' . $job_id);
			}

		}

		if( $upload_success ) {

			// store entry in images/files table as applicable
			$data = [];
			$data['owner_type'] = $owner_type;
			$data['owner_id'] = $owner_id;
			$data['title'] = Input::get('title');
			$data['path'] = $fileURL;
			$data['status'] = 1;

			$table = '';

			if(Input::get('type') === 'image') {
				$data['rotation'] = 0;
				$table = 'images';
			}
			else {
				$data['type'] = Input::get('type');
				$table = 'files';
			}

			try {
				DB::table($table)->insert($data);
				$results = API::respondSuccess($data);
			} catch (Exception $e) {
				$results = API::respondFail('Unable to insert attachment info into the database');
			}

		}

		return API::respond($results);

	}

	/*

	API for getting details of the specified user

	Expected parameters:
	
	1. userID: ID of the user whose details are required

	*/
	public function userDetail($userID) {
		$user = User::find($userID);
		$result = false;
		if($user) {
			$result = API::respondSuccess([
				'id' => $user->id,
				'email' => $user->email,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'avatar' => DB::table('profiles')->where('user_id', '=', $user->id)->pluck('avatar')
			]);
		}
		else {
			$result = API::respondFail('User not found');
		}
		return API::respond($result);
	}

	/*

	API for getting all users (activated) in the system

	Expected parameters: None

	*/
	public function userList() {
		
		$latest_modified_value = 0;
		$users = User::where('activated', '=', '1')->get();
		$retUsers = [];
		foreach ($users as $user) {
			$retUsers[] = [
				'id' => $user->id,
				'email' => $user->email,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name
			];
			
			$latest_modified_value = self::findLastUpdatedTime($user->created_at,$user->updated_at, $latest_modified_value);	
		}

		$retUsers[] = ['last_updated_at' => $latest_modified_value];
		$result = API::respondSuccess($retUsers);
		return API::respond($result);
	}

	/*

	API for getting deleting a calendared event

	Expected parameters: eventID

	*/
	public function eventDelete($id) {
		$delete = EventsController::delete($id);
		$deleted = $delete['message'];
		
		$result = API::respondSuccess($deleted);
		return API::respond($result);
	}

	/*

	API for getting deleting an estimate

	Expected parameters: estimate ID

	*/
	public function deleteEstimate($id) {
		$delete = Estimate::deleteEstimateByID($id);
		
		$result = API::respondSuccess($delete);
		return API::respond($result);
	}
	
	/*
	API for uploading files to Drive
	Expected parameters: 'owner' & 'id' & file payload
	*/
	public function imageUp($owner, $id) {
		$image = ImageController::upload($owner, $id);
		
		if($image['status'] = 'ok') {
			$result = API::respondSuccess($image);
		} elseif($image['status'] = 'error') {
			$result = API::respondFail($image);
		} else {
			$result = API::respondError($image);
		}
		
		return API::respond($result);
	}

	/*
	API for uploading files to Drive
	Expected parameters: 'owner' & 'id' & 'file' payload
	*/
	public function imageUpload() {
		$image = ImageController::upload(Input::get('owner'), Input::get('id'));
		$image = json_decode($image);
		if($image->status = 'ok') {
			$result = API::respondSuccess($image->path);
		} elseif($image->status = 'fail') {
			$result = API::respondFail($image->message);
		} else {
			$result = API::respondError($image->message);
		}
		return API::respond($result);
	}
	
	/*
	API for getting all payments for a given Job #
	Expected parameters: 'job_id'
	*/
	public function getPayments($id) {
		
	}

	/*
	API for posting new payments to DB
	Expected parameters: 'job_id' (int), 'pmt_date (dateTime), 'amount' (float), 
	'pmt_type' (Visa, M/C, Amex etc. - string), 'pmt_id' (last 4# - string),
	'trans_type' (1 - Deposit, 2-Regular/final - int)
	*/
	public function postPayment($id) {
		
	}

	/*
	API for getting the list of jobs for use when associating a new appointment with an existing job
	Expected parameters: 'status' (int), if 'status' is null or zero then all jobs are returned
	*/
	public function getJobList($status = 0) {
		try {
			$jobList = Job::getJobsByStatus($status);
			$result = API::respondSuccess($jobList);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		}
		return API::respond($result);
	}

	public function getServicePartMapping($win_service_id) {
		try {

			$partIDs = DB::table('win_service_parts')->select('parts_id')->where('win_service_id', '=', $win_service_id)->get();
			$partIDs = array_map(function($_row) {
				return $_row->parts_id;
			}, $partIDs);
			$partsArray = DB::table('parts')->whereIn('id', $partIDs)->get();

			$result = API::respondSuccess($partsArray);
		}
		catch (Exception $e) {
			$result = API::respondError($e);
		} 
		return API::respond($result);
	}
}