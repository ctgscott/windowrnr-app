<?php

class EmailTemplatesController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->beforeFilter('auth', ['except' => ['getEmailTemplate', 'getAllEmailTemplates', 'getEmailTemplateByID']]);
	}

	// manage templates page
	public function manage() {
		$user = Sentry::getUser();
		$templates = DB::table('email_templates')->where('owner_id', $user->id)->get();
		return View::make('email-templates.manage')->with('templates', $templates);
	}

	// add or update template
	public function update() {
		$user = Sentry::getUser();
		$id = Input::get('id');
		$data = [];
		$now = date("Y-m-d H:i:s");
		if($id) { // update
			$data['name'] = Input::get('name');
			$data['text'] = Input::get('text');
			$data['updated_at'] = $now;
			DB::table('email_templates')->where('id', $id)->update($data);
			Session::flash('success', 'Email template successfully updated.');
		}
		else { // add new
			$data['owner_id'] = $user->id;
			$data['name'] = Input::get('name');
			$data['text'] = Input::get('text');
			$data['created_at'] = $now;
			$data['updated_at'] = $now;
			DB::table('email_templates')->insert($data);
			Session::flash('success', 'Email template successfully added.');
		}
		return Redirect::to('/email-templates?src=' . Input::get('src'));
	}

	// delete template
	public function delete() {
		$user = Sentry::getUser();
		$id = Input::get('id');
		if($id) { // update
			DB::table('email_templates')->where('id', $id)->delete();
			Session::flash('success', 'Email template deleted.');
		}
		return Redirect::to('/email-templates?src=' . Input::get('src'));
	}

	// list templates - called via AJAX only
	public function getTemplates() {
		$user = Sentry::getUser();
		$templates = DB::table('email_templates')->where('owner_id', $user->id)->get();
		return json_encode($templates);
	}

	// get email template [API call]
	public function getEmailTemplate($name) {
		$template = DB::table('email_templates')->where('name', $name)->first();
		if(!$template) {
			$result = API::respondError('No email template with that name');
		}
		else {
			$result = API::respondSuccess($template);
		}
		return API::respond($result);
	}

	// get email template [API call]
	public function getEmailTemplateByID($id) {
		$template = DB::table('email_templates')->where('id', $id)->first();
		if(!$template) {
			$result = API::respondError('No email template with that id');
		}
		else {
			$result = API::respondSuccess($template);
		}
		return API::respond($result);
	}

	// get all email templates [API call]
	public function getAllEmailTemplates() {
		$templates = DB::table('email_templates')->get();
		if(!$templates) {
			$result = API::respondError('No email templates found');
		}
		else {
			$result = API::respondSuccess($templates);
		}
		return API::respond($result);
	}

}
