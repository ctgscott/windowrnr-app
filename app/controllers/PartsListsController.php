<?php

class PartsListsController extends BaseController {

	public function create() {
		return PartsListsController::createOrUpdate(Input::get('job_id'), Input::get('estimate_id'));
	}

	public static function createOrUpdate($job_id, $estimate_id) {

		// get the estimate & estimate items
		$estimate = DB::table('estimates')->where('id', $estimate_id)->first();
		$items = DB::table('estimate_items')->where('est_id', $estimate_id)->get();

		// collect the parts list items from the services and service-options in the estimate
		$parts = [];
		foreach ($items as $item) {
			if(!empty($item->parts)) {
				$item->parts = json_decode($item->parts);
				foreach ($item->parts as $itemPart) {
					array_push($parts, $itemPart);
				}
			}
		}
		
//		var_dump($estimate);
//		exit;

		// add estimate level parts
		if($estimate->parts) {
			$estParts = json_decode($estimate->parts);
			foreach ($estParts as $estPart) {
				array_push($parts, $estPart);
			}
		}
		
		$partsListID = DB::table('parts_lists')->where('job_id', '=', $job_id)->where('estimate_id', '=', $estimate_id)->first();
		if($partsListID) {
			
			DB::table('parts_lists')->where('job_id', '=', $job_id)->where('estimate_id', '=', $estimate_id)->update([
				'parts' => json_encode($parts),
				'title' => '',
				// 'status' => 0, // 0 for unfinalized
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]);

			$partsListID = $partsListID->id;
		}
		else {
			$partsListID = DB::table('parts_lists')->insertGetId([
				'job_id' => $job_id,
				'estimate_id' => $estimate_id,
				'parts' => json_encode($parts),
				'title' => '',
				'status' => 0, // 0 for unfinalized
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]);
		}

		return ($partsListID ? $partsListID : 'ERROR: Unable to create/update parts-list');
	}

	public function edit($id) {
		$partsList = DB::table('parts_lists')->where('id', $id)->first();
		$job = DB::table('jobs')->where('id', $partsList->job_id)->first();
		$customer = DB::table('customers')->where('id', $job->customer_id)->first();
		return View::make('parts-lists.edit', ['partsList' => $partsList, 'allParts' => DB::table('parts')->get(), 'job' => $job, 'customer' => $customer, 'print' => (Input::get('print') ? Input::get('print') : false)]);
	}

	public function editSimple($id) {
		$partsList = DB::table('parts_lists')->where('id', $id)->first();
		$job = DB::table('jobs')->where('id', $partsList->job_id)->first();
		$customer = DB::table('customers')->where('id', $job->customer_id)->first();
		return View::make('parts-lists.edit-simple', ['partsList' => $partsList, 'allParts' => DB::table('parts')->get(), 'job' => $job, 'customer' => $customer, 'print' => (Input::get('print') ? Input::get('print') : false)]);
	}

	public function update() {
		DB::table('parts_lists')->where('id', Input::get('id'))->update(['parts' => Input::get('parts'), 'updated_at' => date('Y-m-d H:i:s')]);
		return 'OK';
	}

	public function remove() {
		DB::table('parts_lists')->where('id', Input::get('id'))->delete();
		return 'OK';
	}

	private function addToPartsList($parts, $newPartID) {
		$found = false;
		$newPart = DB::table('parts')->where('id', $newPartID)->first();
		//$newPart->options = json_decode($newPart->options);
		foreach ($parts as $part) {
			if($part->id === $newPartID) {
				$part->expected_qty += $newPart->expected_qty;
				$found = true;
				break;
			}
		}
		if($found === false) {
			array_push($parts, $newPart);
		}
		return $parts;
	}

	private function addToPartsListDirect($parts, $newPart) {
		$found = false;
		/*
		foreach ($parts as $part) {
			if($part->id === $newPartID) {
				$part->expected_qty += $newPart->expected_qty;
				$found = true;
				break;
			}
		}
		*/
		if($found === false) {
			array_push($parts, $newPart);
		}
		return $parts;
	}

	public function finalize() {
		DB::table('parts_lists')->where('id', Input::get('id'))->update(['status' => 1]);

		// send mail to 
		$partsList = DB::table('parts_lists')->where('id', Input::get('id'))->first();
		$job = DB::table('jobs')->where('id', $partsList->job_id)->first();
		$customer = DB::table('customers')->where('id', $job->customer_id)->first();
		$customer_name = '';
		if(!empty($customer->f_name)) $customer_name .= $customer->f_name . ' ';
		if(!empty($customer->l_name)) $customer_name .= $customer->l_name . ' ';
		if(!empty($job->city)) $customer_name .= ', ' . $job->city;

		Mail::send(['html' => 'emails.partslist_finalized'], ['job_id' => $partsList->job_id, 'cust_name' => $customer_name, 'est_id' => $partsList->estimate_id, 'partslist_id' => $partsList->id], function($mail) use($job) {
			$mail->to('orders@windowrnr.com')->subject('Parts List Finalized - Job# ' . $job->id);
		});

		return 'OK';
	}
	public function unfinalize() {
		DB::table('parts_lists')->where('id', Input::get('id'))->update(['status' => 0]);
		return 'OK';
	}
	public function apiFinalize() {
		(Input::get('finalize') == '1' ? $this->finalize() : $this->unfinalize());
		return API::respond(API::respondSuccess('OK')); // no way this can fail, so can return success safely
	}
	public function apiGet($id) {
		$partsList = DB::table('parts_lists')->where('id', $id)->first();
		if($partsList) $partsList->parts = json_decode($partsList->parts);
		return API::respond($partsList ? API::respondSuccess($partsList) : API::respondError('Not Found'));
	}

}