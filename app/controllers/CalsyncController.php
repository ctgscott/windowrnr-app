<?php
/*
error_reporting(E_ALL);
ini_set('display_errors',1);

$events;
$calList;
$client;
$service;*/

class CalsyncController extends BaseController {
    
    /**
	 * Instantiate a new UserController
	 */
	public function __construct()
	{
		parent::__construct();
		//filter to check if user is logged in
		//$this->beforeFilter('auth');		
	}
	
	/**
	 * show a view with form to create calsync
	 */
	public function add() {
		return View::make( 'calsync/new' );
	}
 
	/**
	 * handle data posted by ajax request
	 */
	public function create() {
		
		global $events;
		global $calList;
		global $client;
		global $service;
		
		/*require_once public_path().'/google-api-php-client/src/Google_Client.php';
		require_once public_path().'/google-api-php-client/src/contrib/Google_CalendarService.php';

		require_once public_path().'/FirePHPCore/FirePHP.class.php';	
		ob_start();
		$firephp = FirePHP::getInstance(true);*/
		
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$service = new Google_CalendarService($client); 
		
		//check if its our form
		if ( Session::token() !== Input::get( '_token' ) ) {
			return Response::json( array(
				'msg' => 'Unauthorized attempt to create setting'
			) );
		}
     
		$str1 = "";
		
		if (Session::has('token')) {
			$client->setAccessToken(Session::get('token'));
		}
				
		if ($client->getAccessToken()) {
			$calList = $service->calendarList->listCalendarList();
			$str1 = "<h1>Calendar List</h1><pre>" . print_r($calList, true) . "</pre>";
			$cal_id = $calList['items'][0]['id'];
				  
			$client->setUseObjects(true); 
				 
			$service->events->listEvents($cal_id); 

			/* list of events */  
			$events = $service->events->listEvents($cal_id);

			//echo "<pre>";
			//print_r($events->getItems());
			//exit;

			$str1 = "<pre>".$str1;
			while(true) {
				foreach ($events->getItems() as $event) {
					$str1 = $str1.$event->getSummary().'<br/>';
					$str1 = $str1.$event->getLocation().'<br/>';
					$str1 = $str1.$event->getId().'<br/>';
					$str1 = $str1.$event->getCreated().'<br/>';
					
					$str1 = $str1.$event->getStatus().'<br/>';
					$str1 = $str1.$event->getUpdated().'<br/>';
					$str1 = $str1.$event->gethtmlLink().'<br/>';
					$str1 = $str1.serialize($event->getCreator()).'<br/>';
					$str1 = $str1.serialize($event->getOrganizer()).'<br/>';
					$str1 = $str1.serialize($event->getStart()).'<br/>';
					$str1 = $str1.serialize($event->getEnd()).'<br/>';
					$str1 = $str1.serialize($event->getRecurrence()).'<br/>';
					
					$str1 = $str1.serialize($event->getoriginalStartTime()).'<br/>';
					$str1 = $str1.serialize($event->getattendees()).'<br/>';
					//$str1 = $str1.$event->gethangoutLink().'<br/>';
					$str1 = $str1.$event->getanyoneCanAddSelf().'<br/>';
					$str1 = $str1.$event->getguestsCanInviteOthers().'<br/>';
					$str1 = $str1.$event->getguestsCanModify().'<br/>';
					$str1 = $str1.$event->getguestsCanSeeOtherGuests().'<br/>';
					$str1 = $str1.$event->getprivateCopy().'<br/>';
					$str1 = $str1.$event->getlocked().'<br/>';
					$str1 = $str1.serialize($event->getreminders()).'<br/>';
					//$str1 = $str1.serialize($event->getsource()).'<br/>';
					$str1 = $str1.$event->getendTimeUnspecified().'<br/>';
					$str1 = $str1.$event->getrecurringEventId().'<br/>';
					$str1 = $str1.'***END***<br/><br/>';
				}
				$pageToken = $events->getNextPageToken();
				if ($pageToken) {
					$optParams = array('pageToken' => $pageToken);
					$events = $service->events->listEvents($cal_id, $optParams);
				} else {
					break;
				}
			}
		}
		$str1 = $str1."</pre>";
		
		$upevres = CalsyncController::updateEventsTable($events, $cal_id, 4, $calList);
		
		return $str1."<pre>".$upevres."</pre>";
    }
		
	
	public static function updateEventsTable($events, $calid, $userid, $calList) 
	{
		/*require_once public_path().'/FirePHPCore/FirePHP.class.php';	
		ob_start();
		$firephp = FirePHP::getInstance(true);*/

		/** 
		 * Events found in Google but not found in
		 * the application's calendar.
		 * Update the app events table as appropriate
		 **/
		try {

			foreach ($events->getItems() as $event)
			{
				$rescalid = "";
				$rescalid = DB::table('events')
					->select('events.id')
					->select('events.updated_at')
					->where('events.google_event_id', '=', $event->getId())
					->get();
				
				$avatar = DB::table('profiles')
					->where('google_calendar_id', '=', $event->organizer->email)
					->pluck('avatar');
				if (!isset($avatar)) {
					$avatar = "WinPin2.png";
				}
//				$avatar = "WinPin2.png";

				$creatoremail = $event->creator->email;
				$organizeremail = $event->organizer->email;
					  
				if(isset($event->start->dateTime)) {
//					$startdt = array(date_parse($event->start->dateTime));
					$startdt = date("Y-m-d H:i:s", strtotime($event->start->dateTime));
				} elseif(isset($event->start->date)) {
//					$startdt = array(date_parse($event->start->date));
					$startdt = date("Y-m-d H:i:s", strtotime($event->start->date));
				} else {
					$startdt = "1900-00-00";
				}
					  
				if(isset($event->end->dateTime)) {
//					$enddt = array(date_parse($event->end->dateTime));
					$enddt = date("Y-m-d H:i:s", strtotime($event->end->dateTime));
				} elseif(isset($event->end->date)) {
//					$enddt = array(date_parse($event->end->date));
					$enddt = date("Y-m-d H:i:s", strtotime($event->end->date));
				} else {
					$enddt = "1900-00-00";
				}
	
				$address = urlencode($event->getLocation());
				$latlng = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false"));
				if (isset($latlng->results[0]->geometry->location->lat)) {
					$lat = $latlng->results[0]->geometry->location->lat;
					$lng = $latlng->results[0]->geometry->location->lng;
				} else {
					$lat = null;
					$lng = null;
				}
//				$lat = null;
//				$lng = null;

//				$createdat = array(date_parse($event->created));
				$createdat = date("Y-m-d H:i:s", strtotime($event->created));
//				$updatedat = array(date_parse($event->updated));
				$updatedat = date("Y-m-d H:i:s", strtotime($event->updated));
			
				if(isset($rescalid[0])) {
					echo 'continue';
					
/*					$date11 = explode('.',$rescalid[0]->updated_at);
					$date21 = explode('.',$event->updated);
					
					$date1 = date($date11[0]);
					$date2 = date($date21[0]);
*/					
					$date1 = strtotime($rescalid[0]->updated_at);
					$date2 = strtotime($event->updated);
					
					if($date1 < $date2) {
						 //print '$date1 < $date2  updateDBEvents   ';
						 $updb = CalsyncController::updateDBEvents($events, $calid, 4, $calList, $event);
					} elseif($date1 > $date2) {
						 //print '$date1 > $date2   updateGoogleEvents   ';
						 $upgoogle = CalsyncController::updateGoogleEvents($events, $calid, 4, $calList,$event);
					}
					
					continue;
				} else {  
					echo 'insert';
					
					DB::table('events')->insert(
						array(
							'cust_id' => 0,
							'google_event_id' => $event->id,
							'status' => $event->status,
							'htmlLink' => $event->htmlLink,
							'title' => $event->summary,
							'description' => $event->summary,
							'location' => $event->location,
							'allDay' => 0,
							'event_type' => '',
							'avatar' => $avatar,
							'cal_user_id' => $userid,
							'creatorEmail' => $creatoremail,
							'organizerEmail' => $organizeremail,
							'google_cal_id' => $calList['items'][0]['id'],
							'start' => $startdt,
							'end' => $enddt,
							'lat' => $lat,
							'lng' => $lng,
							'created_at' => $createdat,
							'updated_at' => $updatedat,
						)
					);
				}
			}
			
			$resgoogleinsert = "";
			$resgoogleinsert = DB::table('events')
				->select('events.*')
				->where('events.google_event_id', '=', "")
				->get();
				
			foreach($resgoogleinsert as $googleinsert)
			{
				print '<br/>insertGoogleEvents   ';
				$insgoogle = CalsyncController::insertGoogleEvents(4, $googleinsert);
			}
				
		} catch (Exception $e) {
			Session::flash('error', 'There was a problem: '.$e);
			return $e;
		}
		
		return '1';
	}
	
	public static function insertGoogleEvents($userid, $googleinsert)
	{
		global $client;
		global $service;
		
		$event = new Google_Event();
		$start = new Google_EventDateTime();
		$end = new Google_EventDateTime();
		$attendee1 = new Google_EventAttendee();
		
		if(isset($googleinsert->title) && $googleinsert->title != "")
		{
			$event->setSummary($googleinsert->title);
		}
		else
		{
			$event->setSummary('NA');
		}
		
		if(isset($googleinsert->location) && $googleinsert->location != "")
		{
			$event->setLocation($googleinsert->location);
		}
		else
		{
			$event->setLocation('USA');
		}
		
		
		$d1 = new DateTime($googleinsert->start, new DateTimeZone('America/Denver'));
		$start->setDateTime($d1->format('Y-m-d\Th:i:s.uP'));
		$event->setStart($start);
		
		$d1 = new DateTime($googleinsert->end, new DateTimeZone('America/Denver'));

		$end->setDateTime($d1->format('Y-m-d\Th:i:s.uP'));
		$event->setEnd($end);
		
		$attendee1->setEmail($googleinsert->creatorEmail);
		$attendees = array($attendee1,
						  );
		$event->attendees = $attendees;

		// add job_id property
		if(!empty($googleinsert->job_id)) {
			$extendedProperties = New Google_Service_Calendar_EventExtendedProperties();
			$extendedProperties->setShared(['job_id' => $googleinsert->job_id]);
			$event->setExtendedProperties($extendedProperties);
		}

		$createdEvent = $service->events->insert('primary', $event);
		
		echo $createdEvent->getId().'<br/>';
		
		echo "event created";
		
		DB::table('events')
			->where('events.id', $googleinsert->id)
			->update(array(
				'google_event_id' => $createdEvent->getId(),
			));
	}
	
    public static function updateDBEvents($events, $calid, $userid, $calList, $event)
	{
		  $avatar = "WinPin2.png";
		  $creatoremail = $event->creator->email;
		  $organizeremail = $event->organizer->email;
		  
		  if(isset($event->start->dateTime))
		  {
			   $startdt = array(date_parse($event->start->dateTime));
		  }
		  elseif(isset($event->start->date))
		  {
			  $startdt = array(date_parse($event->start->date));
		  }
		  else
		  {
			  $startdt = "1900-00-00";
		  }
		  
		  if(isset($event->end->dateTime))
		  {
			   $enddt = array(date_parse($event->end->dateTime));
		  }
		  elseif(isset($event->end->date))
		  {
			  $enddt = array(date_parse($event->end->date));
		  }
		  else
		  {
			  $enddt = "1900-00-00";
		  }
		  $createdat = array(date_parse($event->created));
		  $updatedat = array(date_parse($event->updated));
		  $lat = null;
		  $lng = null;
		
		DB::table('events')
			->where('events.google_event_id', $event->getId())
			->update(array(
						  'cust_id' => 0,
						  'google_event_id' => $event->id,
						  'status' => $event->status,
						  'htmlLink' => 'increase size of \"htmlLink\" column in database to 300',
						  'title' => $event->summary,
						  'description' => $event->summary,
						  'location' => $event->location,
						  'allDay' => 0,
						  'event_type' => '',
						  'avatar' => $avatar,
						  'cal_user_id' => $userid,
						  'creatorEmail' => $creatoremail,
						  'organizerEmail' => $organizeremail,
						  'google_cal_id' => $calList['items'][0]['id'],
						  'start' => $startdt[0]['year']."-".$startdt[0]['month']."-".$startdt[0]['day']." ".$startdt[0]['hour'].":".$startdt[0]['minute'].":".$startdt[0]['second'],  //serialize($event->getStart()),
						  'end' => $enddt[0]['year']."-".$enddt[0]['month']."-".$enddt[0]['day']." ".$enddt[0]['hour'].":".$enddt[0]['minute'].":".$enddt[0]['second'],  //serialize($event->getStart()), //$enddt[0][0], //serialize($event->getEnd()),
						  'lat' => $lat,
						  'lng' => $lng,
						  'updated_at' => $updatedat[0]['year']."-".$updatedat[0]['month']."-".$updatedat[0]['day']." ".$updatedat[0]['hour'].":".$updatedat[0]['minute'].":".$updatedat[0]['second'] //$updatedat //serialize($event->getUpdated())
					  )
				);
					
		return 1;
	}
	
	public static function updateGoogleEvents($events, $calid, $userid, $calList, $event)
	{
		global $client;
		global $service;
		
		// First retrieve the event from the API.
		  $event->setSummary('testing shyamal 123456');
		  
		  $updatedEvent = $service->events->update('primary', $event->getId(), $event);
		  
		  // Print the updated date.
		  return  "Google eventupdated at: ".$updatedEvent->getUpdated();

		return 2;
	}

	public function getChannels()
	{
		
	}

	public function makeUpdates()
	{
		$header      = json_decode('{"accept-encoding":["gzip,deflate"],"user-agent":["APIs-Google; (+https://developers.google.com/webmasters/APIs-Google.html)"],"connection":["close"],"content-length":["0"],"x-goog-channel-token":["ya29.QQCgCWXGvGzrMiEAAAACXmttMT-5-WjAqbnFiGL45BVEhS07qRGmFgTcixp6I1i6SIh9t0ZT_zldO7Ep4YU"],"x-goog-resource-uri":["https://www.googleapis.com/calendar/v3/calendars/dev@windowrnr.com/events?key=AIzaSyCgRDHRL8EpXGqfP2GwIRI7DXrVIJ20ODo&alt=json"],"x-goog-resource-id":["rptn32pljMG4X2E0VzWrywg2DkY"],"x-goog-message-number":["1"],"x-goog-resource-state":["sync"],"x-goog-channel-expiration":["Sun, 20 Jul 2014 05:41:43 GMT"],"x-goog-channel-id":["devwindowrnrcom_123"],"accept":["*/*"],"content-type":["application/json; charset=UTF-8"],"host":["app.windowrnr.com"]}', true);
		$resource_id = $header['x-goog-resource-id'][0];

		//set the google client API
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$calendar = new Google_Service_Calendar($client);

		//get the user refresh token based in resource_id
		$res = GoogleCalendarChannels::where('google_resource_id', $resource_id)->get()->first();

		//set the refresh token
		$access_token = $client->refreshToken($res->user->google_refresh_token);
		$client->setAccessToken($client->getAccessToken());

		if ($client->getAccessToken()) 
		{
			try {
				//get the most recent event_token
				$eventToken = GoogleEventToken::orderBy('created_at', 'desc')->first();

				$params = array();
				if($eventToken) {
					$params = array(
						'syncToken' => $eventToken->token,
					);
				}

				//list all events from the latest token created
				$res = $calendar->events->listEvents('primary', $params);

			} catch (Exception $e) {
				$params = array();
				$res = $calendar->events->listEvents('primary', $params);
			}

			echo "<pre>";
			var_dump((Array) $res);
			exit;
			
			if(count($res->getItems()) > 0) 
			{
				$i = 0;
				foreach ($res->getItems() as $event)
				{
				    $localEvent = DB::table('events')->where('google_event_id', $event->id)->get();
				    if($localEvent)
				    {					    	
				    	//update event
				    	Events::updateEventData($event);
				    	$i++;
				    }
				}

				//add the next sync token
				$token = (isset($res->nextSyncToken) && !is_null($res->nextSyncToken)) ? $res->nextSyncToken : $res->nextPageToken;
				GoogleEventToken::create(array('token' => $token));
			}
		}

		exit($i.' events updated.');
	}

	public function listAllEvents()
	{
		/*$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$test = new Google_Http_Request('https://www.googleapis.com/calendar/v3/calendars/dev@windowrnr.com/events?key=AIzaSyCgRDHRL8EpXGqfP2GwIRI7DXrVIJ20ODo&alt=json');

		echo "<pre>";
		var_dump($client->execute($test));
		exit;
*/
		$eventToken = GoogleEventToken::orderBy('created_at', 'desc')->first();

		//try {

			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$calendar = new Google_Service_Calendar($client);
 /*
			if (Session::has('token')) {
				$client->setAccessToken(Session::get('token'));
			}*/

			$client->setAccessToken(Session::get('token'));

			if ($client->getAccessToken()) {

				$params = array(
					'syncToken' => $eventToken->token,					
				);

				//$params = array();

				$res = $calendar->events->listEvents('primary', $params);

				echo "<pre>";
				print_r($res);
				exit;

				if($res->nextSyncToken != $eventToken->token)
				{
					foreach ($res->getItems() as $event)
					{
					    $localEvent = DB::table('events')->where('google_event_id', $event->id)->get();
					    if($localEvent)
					    {					    	
					    	//update event
					    	Events::updateEventData($event);
					    }
					}
					//add the next sync token
					GoogleEventToken::create(array('token' => $res->nextSyncToken));
				}
			}

		/*} catch (Exception $e) {

			echo $e->getMessage();
			exit;

		}	*/	
	}

	public function deleteEvent($id)
	{
		$event_data = Events::find($id);

		try {

			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_CalendarService($client);

			if (Session::has('token')) {
				$client->setAccessToken(Session::get('token'));
			}

			if ($client->getAccessToken()) {

				$res = $cal->events->delete('primary', $event_data->google_event_id);

				echo "<pre>";
				var_dump($res);
				exit;
			}

		} catch (Exception $e) {

			echo $e->getMessage();
			exit;

		}		
	}

	public function updateEvent($id)
	{
		try {

			$event_data = Events::find($id);

			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_CalendarService($client);

			if (Session::has('token')) {
				$client->setAccessToken(Session::get('token'));
			}

			if ($client->getAccessToken()) {

				$event = new Google_Event($cal->events->get('primary', $event_data->google_event_id));

				$event->setLocation('Av. Ferreira Viana, 2886');
				$event->setDescription('updated');
				$event->setSummary('Abc 123');

				$updatedEvent = $cal->events->update('primary', $event->getId(), $event);

				echo "<pre>";
				var_dump($updatedEvent);
				exit;
			}

		} catch (Exception $e) {

			echo $e->getMessage();
			exit;

		}		
	}

	public function getEvent($id)
	{
		try {

			$event_data = Events::find($id);

			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_CalendarService($client);

			if (Session::has('token')) {
				$client->setAccessToken(Session::get('token'));
			}

			echo "<pre>";
			var_dump($client->getAccessToken());
			exit;

			if ($client->getAccessToken()) {

				$event = new Google_Event($cal->events->get('primary', $event_data->google_event_id));

				echo "<pre>";
				var_dump($event);
				exit;
			}

		} catch (Exception $e) {

			echo $e->getMessage();
			exit;

		}		
	}

	public function createWatchChannel()
	{
		try 
		{

			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_Service_Calendar($client);

			if (Session::has('token')) 
			{
				$client->setAccessToken(Session::get('token'));
			}

			$token = (Array) json_decode($client->getAccessToken());

			$calendar   = 'dev@windowrnr.com';

			$channel_id = 'devwindowrnrcom_123';

			$chan = new Google_Service_Calendar_Channel();
			$chan->setId($channel_id);
			$chan->setToken($token['access_token']);
			$chan->setType("web_hook");
			$chan->setAddress("https://app.windowrnr.com/api/calpush");
			//$chan->setExpiration(strtotime("+1 year")*10000);
			$watch = $cal->events->watch($calendar, $chan);

			echo $watch->id . "<br>";
			echo $watch->expiration . "<br>";
			echo $watch->resourceId . "<br>";
			echo $watch->token . "<br>";

			echo "<pre>";
			var_dump($watch);
			exit;

			//$calendarService = new Google_Service_Calendar();
			//$chan->setId('abc123');
			/*$chan = new Google_Service_Calendar_Channel();
			$chan->setId('testing'.time());
			$chan->setToken($token['access_token']);
			$chan->setType("web_hook");
			$chan->setAddress("https://app.windowrnr.com/api/calpush");
			$chan->setExpiration(strtotime("+1 week")*20);
 		    $channels = $cal->settings->watch($chan);*/

 		    $chan = new Google_Service_Calendar_Channel();
			$chan->setId('devwindowrnrcom_123');
			$chan->setResourceId('rptn32pljMG4X2E0VzWrywg2DkY');
			//$chan->setToken('ya29.MwDePg5nmq681SAAAABChTwcfGkPNObc9vPhNHgFVxtSediGj5Ho4K8RrmcB_w');			
 		    $channel = $cal->channels->stop($chan);

			echo "<pre>";
			var_dump($channel);
			exit;


			if ($token['access_token']) {

				$access_token = $token['access_token'];

				$calendar = 'dev@windowrnr.com';
				$url = sprintf("https://www.googleapis.com/calendar/v3/calendars/%s/events/watch", $calendar);

				/* setup the POST parameters */
				$fields = json_encode(array(
				    'id'        => "asdasdasdas",
				    'type'      => "web_hook",
				    'address'   => "https://app.windowrnr.com/api/calpush",
				    ));


				/* setup POST headers */
				$headers[] = 'Content-Type: application/json';
				$headers[] = 'Authorization: Bearer ' . $access_token;

				/* send POST request */
				$channel = curl_init();
				curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($channel, CURLOPT_URL, $url);
				curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($channel, CURLOPT_POST, true);
				curl_setopt($channel, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 2);
				curl_setopt($channel, CURLOPT_TIMEOUT, 3);
				$response = curl_exec($channel);
				curl_close($channel);

				
				echo "<pre>";
				var_dump($response);
				exit;

				//error_log($response);
				
			}

		} catch (Exception $e) {

			echo $e->getMessage();
			exit;

		}		
	}

	public function stopChannel($user_id = null)
	{		
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$cal = new Google_Service_Calendar($client);

		$user_channel = GoogleCalendarChannels::where('user_id', $user_id)->get()->last();

		//echo $user_channel->user->google_refresh_token;
		//exit;

		//set the refresh token
		$access_token = $client->refreshToken($user_channel->user->google_refresh_token);
		$client->setAccessToken($client->getAccessToken());

		$chan = new Google_Service_Calendar_Channel();
		$chan->setId($user_channel->google_channel_id);
		$chan->setResourceId($user_channel->google_resource_id);		
		$channel = $cal->channels->stop($chan);
	}

	/* ------------ */
	/* Updated methods for starting and stopping watching a calendar */
	/* ------------ */

	public function startWatching($calendar) {
		
		$service = $this->getServerAuthenticatedCalendarService();

		// stop watching current channel for this calendar
		$this->stopWatching($calendar, $service);
		
		// start new watch
		$channel = new Google_Service_Calendar_Channel();
		$channel->setId("watch_" . preg_replace("/[^A-Za-z]/", '_', $calendar) . '_' . App::environment() . '_' . time());
		$channel->setToken($service->getClient()->getAccessToken());
		$channel->setType("web_hook");
		$channel->setAddress("https://app.windowrnr.com/calsync/notification");
		$watch = $service->events->watch($calendar, $channel);

		// store resource id for this calendar in the database
		GoogleWatchChannel::insert([
			'channel_id' => $watch->id,
			'calendar_id' => $calendar,
			'resource_id' => $watch->resourceId,
			'expires_at' => date('Y-m-d h:m:s', intval($watch->expiration)/1000),
			'token' => $watch->token
		]);

		return "OK";

	}

	public function stopWatching($calendar, $service) {

		// load channel for this calendar from the db
		$dbChannel = GoogleWatchChannel::where('calendar_id', '=', $calendar)->first();
		if(is_null($dbChannel)) return "OK";

		// stop watching this channel
		$channel = new Google_Service_Calendar_Channel();
		$channel->setId($dbChannel->channel_id);
		$channel->setResourceId($dbChannel->resource_id);
		$result = $service->channels->stop($channel);

		// delete from db
		GoogleWatchChannel::where('calendar_id', '=', $calendar)->delete();

		return "OK";

	}

	/* ------------ */
	/* Updated method for receiving notifications */
	/* ------------ */

	public function onPushNotification() {

		// log notification request in /tmp
		$headers = Request::header();
		$summary = [];
		foreach($headers as $key => $value) $summary[] = "$key: {$value[0]}";
		$filename = "/tmp/push-" . time();
		$contents = implode("\n", $summary);
		file_put_contents($filename, $contents);

		// check if a google calendar push notification
		if(!Request::header('x-goog-resource-id')) return false;

		// load channel for this resource id from the db
		$dbChannel = GoogleWatchChannel::where('resource_id', '=', Request::header('x-goog-resource-id'))->first();
		if(is_null($dbChannel)) return false;

		// update events in the affected calendar from google calendar
		$this->updateEvents($dbChannel->calendar_id);

		return "OK";

	}

	/* ------------ */
	/* Util method for creating a server authenticated calendar instance */
	/* ------------ */

	private function getServerAuthenticatedCalendarService() {

		// NOTE: Using service account auth returns only partial event data
		/*

		// service account (dev) password: notasecret
		
		$client_id = '849469847061-tmbfqf4838p67advj7c50cats3ju9cis.apps.googleusercontent.com'; //Client ID
		$service_account_name = '849469847061-tmbfqf4838p67advj7c50cats3ju9cis@developer.gserviceaccount.com'; //Email Address
		$key_file_location = app_path() . '/auth/service-account-key.p12'; //key.p12
		
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		
		$key = file_get_contents($key_file_location);
		$cred = new Google_Auth_AssertionCredentials(
		    $service_account_name,
		    ['https://www.googleapis.com/auth/calendar'],
		    $key
		);
		$client->setAssertionCredentials($cred);
		$client->setDeveloperKey('AIzaSyCjwp8UdIggfZ0M52XR-Lf0k5tnd7ES6d8');
		if ($client->getAuth()->isAccessTokenExpired()) {
		  $client->getAuth()->refreshTokenWithAssertion($cred);
		}
		$_SESSION['service_token'] = $client->getAccessToken();
		
		*/

		// NOTE: Using dev's access credentials (BAAAD) to create client
		// Make sure all watched calendars are shared with dev
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$token = $client->refreshToken(User::find(2)->google_refresh_token);

		// calendar service
		return new Google_Service_Calendar($client);
	}

	/* ------------ */
	/* Util method for updating one/all calendar(s) from google */
	/* ------------ */

	public function updateEvents($calendar) {

		$service = $this->getServerAuthenticatedCalendarService();

		if($calendar == 'all') {
			$calendarList = $service->calendarList->listCalendarList();
			while(true) {
			    foreach ($calendarList->getItems() as $calendarListEntry) {
			        $this->updateEventsInCalendar($calendarListEntry->id, $service);
			    }
			    $pageToken = $calendarList->getNextPageToken();
			    if($pageToken) {
			        $optParams = array('pageToken' => $pageToken);
			        $calendarList = $service->calendarList->listCalendarList($optParams);
			    }
			    else break;
			}
		}
		else {
			$calendar = urldecode($calendar);
			$this->updateEventsInCalendar($calendar, $service);
		}

		return "OK";
	}

	/* ------------ */
	/* Util method for updating one calendar from google */
	/* ------------ */

	public function updateEventsInCalendar($calendarID, $service) {

		echo "<b>Calendar: " . $calendarID . "</b><br>";

		$userID = 2; // dev, replace with 1 (Scott) after devel
		$user = User::where('email', '=', $calendarID)->first();
		if(!is_null($user)) $userID = $user->id;

		$events = $service->events->listEvents($calendarID, ['showDeleted' => true]);

		while(true) {
		
			foreach ($events->getItems() as $event) {

				if($event->status == 'cancelled') {

					// delete 'cancelled' (deleted/moved) events from the database
					DB::table('events')->where('events.google_event_id', '=', $event->getId())->where('events.google_cal_id', '=', $calendarID)->delete();
				
				}
				else {

					// try loading the event from DB (will be null if it does not exist)
					$dbEvent = DB::table('events')->where('events.google_event_id', '=', $event->getId())->first();

					$lat = null;
					$lng = null;

					if(!is_null($dbEvent) && $dbEvent->location == $event->location) {
						$lat = $dbEvent->lat;
						$lng = $dbEvent->lng;
					}
					else if($event->location != null) {
						$address = urlencode($event->location);
						$latlng = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false"));
						if (isset($latlng->results[0]->geometry->location->lat)) {
							$lat = $latlng->results[0]->geometry->location->lat;
							$lng = $latlng->results[0]->geometry->location->lng;
						}
					}

					// delete if this event if present in any other calendar (in case of move)
					DB::table('events')->where('events.google_event_id', '=', $event->getId())->where('events.google_cal_id', '<>', $calendarID)->delete();

					// if the loaded dbEvent is from another calendar, nullify it to force an insert
					if(!is_null($dbEvent) && $dbEvent->google_cal_id != $calendarID) $dbEvent = null;
					
					$creatoremail = (isset($event->creator) && isset($event->creator->email) ? $event->creator->email : $calendarID);
					$organizeremail = (isset($event->organizer) && isset($event->organizer->email) ? $event->organizer->email : $calendarID);

					if(isset($event->start->dateTime)) $startdt = date("Y-m-d H:i:s", strtotime($event->start->dateTime));
					else $startdt = "NOW()";
						  
					if(isset($event->end->dateTime)) $enddt = date("Y-m-d H:i:s", strtotime($event->end->dateTime));
					else $enddt = "NOW()";

					$createdat = date("Y-m-d H:i:s", strtotime($event->created));
					$updatedat = date("Y-m-d H:i:s", strtotime($event->updated));

					$data = [
						'cust_id' => 0, // parse it from event description
						'google_event_id' => $event->id,
						'status' => $event->status,
						'htmlLink' => (is_null($event->htmlLink) ? '' : $event->htmlLink),
						'title' => (is_null($event->summary) ? '' : $event->summary),
						'description' => (is_null($event->summary) ? '' : $event->summary),
						'location' => $event->location,
						'allDay' => 0,
						'event_type' => '', // parse it from event description
						'avatar' => '',
						'cal_user_id' => $userID,
						'creatorEmail' => $creatoremail,
						'organizerEmail' => $organizeremail,
						'google_cal_id' => $calendarID,
						'start' => $startdt,
						'end' => $enddt,
						'lat' => $lat,
						'lng' => $lng
					];

					if(is_null($dbEvent)) {
						DB::table('events')->insert($data);
					}
					else {
						DB::table('events')->where('events.google_event_id', $event->id)->update($data);
					}

				}

			}

			$pageToken = $events->getNextPageToken();
		    if ($pageToken) {
		        $events = $service->events->listEvents($calendarID, ['showDeleted' => true, 'pageToken' => $pageToken]);
		    } 
		    else {
		        break;
		    }

		}
	}
     
//end of class
}

