<?php

class EstimatesController extends BaseController {

	public function __construct()
	{
		parent::__construct();
	}
 
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('estimates.index');
	}
	
	/**
	 * Show the form for creating a new estimate
	 * given the supplied job id.
	 *
	 * @return Response
	 */
	public function create($jobID)
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/login');
		}
		else
		{
			// User is logged in
			try {

				$user = Sentry::getUser();
				$eJob = DB::table('jobs')->where('id', $jobID)->first();
				$customer = DB::table('customers')->where('id', $eJob->customer_id)->first();

				$results = Job::getJobCustNotes($jobID);
				$rooms = DB::table('rooms')->lists('room', 'id');
				$roomsid = DB::table('rooms')->lists('id', 'id');
				$dir = DB::table('dir')->lists('dir', 'id');

				$winStyle = DB::table('win_style')->lists('winStyle', 'id');
				array_unshift($winStyle, 'Choose ...'); // empty option at start for 'none selected'
				
				//BaseController::firePhp()->addInfo('EstimatesController - Results', array('results' => $results));

				$options = DB::table('options')->get();
				$balancers = DB::table('balancers')->get();
				$winStylesWithBalancers = DB::table('win_style')->where('has_balancer', '=', '1')->lists('id');
				$allParts = DB::table('parts')->get();
			
				return View::make('estimates.create', array('results' => $results, 'rooms' => $rooms, 'dir' => $dir, 'winStyle' => $winStyle, 'user' => $user, 'job' => $eJob, 'customer' => $customer, 'options' => $options, 'balancers' => $balancers, 'winStylesWithBalancers' => $winStylesWithBalancers, 'allParts' => $allParts));

			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
		}
	}

	/**
	 * Save the form data for the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function save()
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/logout');
		}
		else
		{
			// User is logged in
			try {
				$all = Input::all();
				$newEst = array(
					'job_id' => Input::get('job_id'),
					'description' => Input::get('description'),
					'deposit' => Input::get('deposit')
				);
				$estID = Estimate::addEstimate($newEst);

				$items = Input::get('item');
				foreach ($items as $item) 
				{
					$item['est_id'] = $estID;
					unset($item['itemID']);

					if(empty($item['balancer'])) $item['balancer'] = NULL;

					Estimate::addEstimateItems($item);
				}

				// update job status (if needed)
				$job = DB::table('jobs')->where('id', Input::get('job_id'))->first();
				if($job && $job->status < QUOTE_CREATED) {
					DB::table('jobs')
						->where('id', Input::get('job_id'))
						->update(['status' => QUOTE_CREATED]);
				}

				// create/update parts-list
				PartsListsController::createOrUpdate(Input::get('job_id'), $estID);

				Session::flash('success', 'Estimate #'.$estID.' was saved.');

				if(Input::get('sourceref') === 'create') {
					return Redirect::to('/estimates/edit/' . $estID);
				}
				else {
					return Redirect::to('/customers');
				}
			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
		}
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($estID)
	{

		$user = Sentry::getUser();

		$est = Estimate::getEstimateByID($estID);
		$length = count($est['items']);
		
		//BaseController::firePhp()->addInfo('EstimatesController - Est', array('Est' => $est));
		//BaseController::firePhp()->addInfo('EstimatesController - Length', array('Length' => $length));
        
		$job = Job::getJobCustNotes($est['job_id']);
		$rooms = DB::table('rooms')->lists('room', 'id');
		$roomsid = DB::table('rooms')->lists('id', 'id');
		$dir = DB::table('dir')->lists('dir', 'id');
		$winStyle = DB::table('win_style')->lists('winStyle', 'id');
		array_unshift($winStyle, 'Choose ...'); // empty option at start for 'none selected'
		
		$eJob = DB::table('jobs')->where('id', $est['job_id'])->first();
		$customer = DB::table('customers')->where('id', $eJob->customer_id)->first();

		$pdf = DB::table('files')
					->where('owner_type', 'estimate')
					->where('owner_id', $estID)
					->where('title', 'PDF for estimate #' . $estID)
					->where('type', 'PDF')
					->orderBy('uploaded_at', 'desc')
					->first();

		//BaseController::firePhp()->addInfo('EstimatesController - job', array('job' => $job));

		$options = DB::table('options')->get();
		$balancers = DB::table('balancers')->get();
		$winStylesWithBalancers = DB::table('win_style')->where('has_balancer', '=', '1')->lists('id');
		$partsList = DB::table('parts_lists')->where('estimate_id', $estID)->first();

		// ensure parts-list exists
		if(!$partsList) {
			DB::table('parts_lists')->insert([
				'job_id' => $est['job_id'],
				'estimate_id' => $estID,
				'title' => '',
				'parts' => '[]',
				'status' => 0,
				'created_at' => date("Y-m-d H:i:s"),
				'updated_at' => date("Y-m-d H:i:s")
			]);
			$partsList = DB::table('parts_lists')->where('estimate_id', $estID)->first();
		}

		$allParts = DB::table('parts')->get();
		foreach ($allParts as $part) {
			$part->options = json_decode($part->options, JSON_NUMERIC_CHECK);
		}
			
		return View::make('estimates.edit', ['est' => $est, 'job' => $job, 'rooms' => $rooms, 'dir' => $dir, 'winStyle' => $winStyle, 'length' => $length, 'pdf' => $pdf, 'user' => $user, 'customer' => $customer, 'options' => $options, 'balancers' => $balancers, 'winStylesWithBalancers' => $winStylesWithBalancers, 'partsList' => $partsList, 'allParts' => $allParts]);

	}
	
	public function delete($estID)
	{
		$results = Estimate::deleteEstimateByID($estID);
		
		return $results;
	}
	
	/**
	 * Update the form data for the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/logout');
		}
		else
		{
			// User is logged in
			try {
				$all = Input::all();
				$jobID = Input::get('job_id');
				$newEst = array(
					'job_id' => $jobID,
					'cust_id' => Input::get('cust_id'),
					'description' => Input::get('description')
				);
				$estID = Estimate::addEstimate($newEst);

				$items = Input::get('item');


				foreach ($items as $item) 
				{
					$item['est_id'] = $estID;

					Estimate::addEstimateItems($item);
				}
				$oldEstID = Input::get('est_id');
				Estimate::deleteEstimateByID($oldEstID); 
				
				Session::flash('success', 'Estimate #'.$estID.' was saved.');
				return Redirect::to('/job/'.$jobID);
//				return View::make('estimates.test')->with('results', $results);
/*				var_dump($all);
				var_dump($newEst);
				var_dump($estID);
				var_dump($items);
*/			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
		}
	}

	public function update1()
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/logout');
		}
		else
		{
			// User is logged in
			try {
				$all = Input::all();
				$jobID = Input::get('job_id');
				$estimateID = Input::get('est_id');
				$estimate = [];
				$estimate['job_id'] = $jobID;
				$estimate['cust_id'] = Input::get('cust_id');
				$estimate['description'] = Input::get('description');
				$estimate['notes'] = Input::get('estimate_notes');
				$estimate['parts'] = Input::get('parts');

				if(Input::get('deposit')) {
					$estimate['deposit'] = Input::get('deposit');
				}
				else {
					$estimate['deposit'] = null;
				}

				$estResult = Estimate::updateEstimate($estimateID, $estimate);

				if($estResult === 'success') 
				{
					$items = Input::get('item');

					foreach ($items as $item) 
					{
						if($item['itemID'] == 0)
						{
							unset($item['itemID']);
							$item['est_id'] = Input::get('est_id');
							Estimate::addEstimateItems($item);
						} else {
							$itemID = $item['itemID'];
							unset($item['itemID']);
							$item['archive'] = 0;
							Estimate::updateEstimateItems($itemID, $item);
						}
					}
					
					Estimate::deleteArchive($estimateID);

					PartsListsController::createOrUpdate($jobID, $estimateID);
					
					Session::flash('success', 'Estimate #'.$estimateID.' was updated.');
				} else {
					Session::flash('fail', 'Estimate #'.$estimateID.' was NOT updated.');
				}

				return Redirect::to('/job/'.$jobID);

			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
		}
	}

	public function updateAjax()
	{
		if (!Sentry::check()) return 'ERROR: Authentication Error';
		
		// User is logged in
		try {
			$all = Input::all();
			$jobID = Input::get('job_id');
			$estimateID = Input::get('est_id');
			$estimate = [];
			$estimate['job_id'] = $jobID;
			$estimate['cust_id'] = Input::get('cust_id');
			$estimate['description'] = Input::get('description');
			$estimate['notes'] = Input::get('estimate_notes');
			$estimate['parts'] = Input::get('parts');

			if(Input::get('deposit')) {
				$estimate['deposit'] = Input::get('deposit');
			}
			else {
				$estimate['deposit'] = null;
			}

			$estResult = Estimate::updateEstimate($estimateID, $estimate);

			if($estResult === 'success') 
			{
				$items = Input::get('item');

				foreach ($items as $item) 
				{
					if($item['itemID'] == 0)
					{
						unset($item['itemID']);
						$item['est_id'] = Input::get('est_id');
						Estimate::addEstimateItems($item);
					} else {
						$itemID = $item['itemID'];
						unset($item['itemID']);
						$item['archive'] = 0;
						Estimate::updateEstimateItems($itemID, $item);
					}
				}
				
				Estimate::deleteArchive($estimateID);

				PartsListsController::createOrUpdate($jobID, $estimateID);

				return 'OK';
				
			} else {
				return 'ERROR: Estimate #'.$estimateID.' was NOT updated.';
			}

		} catch (Exception $e) {
			return 'ERROR: ' . $e->getMessage();
		}
	}

	/**
	 * Show the form for admin'ing all estimate data.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function admin()
	{
		$rooms = DB::table('rooms')->lists('room', 'id');
		$roomsid = DB::table('rooms')->lists('room', 'id');
		$dir = DB::table('dir')->get();
		$winStyle = DB::table('win_style')->lists('winStyle', 'id');
		$winService = DB::table('win_service')->get();
		$options = DB::table('options')->get();
		return View::make('estimates.admin', array('rooms' => $rooms,'roomsid' => $roomsid, 'dir' => $dir, 'winStyle' => $winStyle, 'winService' => $winService, 'options' => $options));
	}
	
	
	public function adminStyleAddRoom()
	{
			try {
			$all = Input::all();
						
			DB::table('rooms')
			->insert(array(
						  'room' => $all['room_add']
						  )
				     );
	
			Session::flash('success', 'Styles were updated.');
			return Redirect::back();
			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
	}
	
	public function adminStyleAddWinService()
	{
			try {
			$all = Input::all();
					
				//	print_r($all);
				//	die(0);
						
			DB::table('win_service')
			->insert(array(
						  'win_style' => $all['winservicestyleadd'],
						  'title' => $all['winservicetitleadd'],
						  'description' => $all['winservicedescadd'],
						  'price' => $all['winservicepriceadd']
						  )
				     );
	
			Session::flash('success', 'Styles were updated.');
			return Redirect::back();
			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
	}
	
	
	public function adminStyleAddWinServiceOption()
	{
			try {
			$all = Input::all();
					
					//print_r($all);
					//die(0);
						
			DB::table('options')
			->insert(array(
						  'serv_id' => $all['winserviceoptionstyleadd'],
						  'title' => $all['winserviceoptiontitleadd'],
						  'description' => $all['winserviceoptiondescadd'],
						  'price' => $all['winserviceoptionpriceadd']
						  )
				     );
	
			Session::flash('success', 'Styles were updated.');
			return Redirect::back();
			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
	}
	
	
	/**
	 * Show list of Window Service.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function adminListWinService($id)
	{
		try 
			{
				$results['winservice'] = DB::table('win_service')->where('win_service.win_style',$id)->get();
				//BaseController::firePhp()->addInfo('$results', array($results));
				
				return Response::json($results);
			}
			catch (Exception $e) 
			{	
				Session::flash('error', 'There was a problem: '.$e->getMessage());
				return $e;
			}
			
	}

    public function adminListMultiWinService($id)
    {
        try 
            {
                $results1['winserviceoptions'] = DB::table('win_service')->where('win_service.win_style',$id)->get();
                BaseController::firePhp()->addInfo('$results', array($results1));
                return Response::json($results1);
            }
            catch (Exception $e) 
            {    
                Session::flash('error', 'There was a problem: '.$e->getMessage());
                return $e;
            }
    }

    public function adminCheckMultiWinService($id, $stid)
    {
        try 
            {
                $results1['winserviceoptions'] = DB::table('options')->where('options.id',$id)->where('options.serv_id',$stid)->get();
                BaseController::firePhp()->addInfo('$results', array($results1));
                return Response::json($results1);
            }
            catch (Exception $e) 
            {    
                Session::flash('error', 'There was a problem: '.$e->getMessage());
                return $e;
            }
    }
    
    public function adminUpdateMultiWinService($styleid, $servid, $optionid, $ischk)
    {
        try 
            {
                if($ischk == 'true')
                {
                    echo 'add';
                        $results2 = DB::table('options')->where('options.id',$optionid)->get();
                        //BaseController::firePhp()->addInfo('$results2', array($results2));
                        
                        echo $styleid.','.$servid.','.$optionid.','.$ischk;
                        //die(0);
                        print_r($results2);
                        echo $results2[0]->title;
                        echo $results2[0]->description;
                        echo $results2[0]->price;
                        
                        //die(0);
                        
                        try {
                                $temp1=DB::table('options')->where('options.serv_id',$servid)->where('options.title',$results2[0]->title)->where('options.description',$results2[0]->description)->where('options.price',$results2[0]->price)->get();
                                
                                print_r($temp1);
                                //die(0);
                                
                                if($temp1)
                                {
                                    
                                }
                                else
                                {
                                    DB::table('options')->insert(array('serv_id' => $servid,'title' => $results2[0]->title,'description' => $results2[0]->description,'price' => $results2[0]->price));
                        
                                Session::flash('success', 'Styles were updated.');
                                //return Redirect::back();
                                }
                                
                                
                        } catch (Exception $e) {
                            
                            Session::flash('error', 'There was a problem: '.$e);
                            
                            return $e;
                        }
                }
                else if($ischk == 'false')
                {
                    echo 'delete';
                    $results2 = DB::table('options')->where('options.id',$optionid)->get();
                    DB::table('options')->where('options.serv_id',$servid)->where('options.title',$results2[0]->title)->where('options.description',$results2[0]->description)->where('options.price',$results2[0]->price)->delete();
                }
                        
                //$results3['winserviceoptions'] = DB::table('options')->where('options.id',$id)->where('options.serv_id',$stid)->get();
                //BaseController::firePhp()->addInfo('$results', array($results3));
                //return Response::json($results3);
            }
            catch (Exception $e) 
            {    
                Session::flash('error', 'There was a problem: '.$e->getMessage());
                return $e;
            }
    }

	
	/**
	 * Show list of Window Service Options.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function adminListWinServiceOptions($id)
	{
		try 
			{
				$results['winserviceoptions'] = DB::table('options')->where('options.serv_id',$id)->get();
				//BaseController::firePhp()->addInfo('$results', array($results));
				return Response::json($results);
			}
			catch (Exception $e) 
			{	
				Session::flash('error', 'There was a problem: '.$e->getMessage());
				return $e;
			}
	}
	
	/**
	 * Update & Delete rooms.
	 * Called from public->js->admin.js file.
	 */
	public function roomSave()
	{
		$items = Input::get('items');
		
		foreach($items as $item)
		{
			$room = Rooms::find($item['id']);
			if($item['room'] != '')
			{
				$room->room = $item['room'];
				$room->save();
			} else {
				$room->delete();
			}
		}
		return '1';
	}
	
	/**
	 * Update & Delete Window Styles.
	 * Called from public->js->admin.js file.
	 */
	public function winstyleSave()
	{
		$items = Input::get('items');
		
		foreach($items as $item)
		{
			$room = WinStyle::find($item['id']);
			if($item['winstyle'] != '')
			{
				$room->winStyle = $item['winstyle'];
				$room->save();
			} else {
				$room->delete();
			}
		}
		return '1';
	}
	
	/**
	 * Update & Delete Window Service.
	 * Called from public->js->admin.js file.
	 */
	public function winstyleserviceSave()
	{
		$itemsoption = Input::get('itemsoption');
		//print_r($itemsoption);
		//die(0);
			
		foreach($itemsoption as $itemoption)
		{

			if($itemoption['winservicetitle'] != '')
				{
					DB::table('win_service')
					->where('win_service.id', $itemoption['id'])
					->update(array(
							  'title' => $itemoption['winservicetitle'],
							  'description' => $itemoption['winservicedesc'],
							  'price' => $itemoption['winserviceprice']
							  
							  )
					     );
				}
				else
				{
						DB::table('win_service')
					->where('win_service.id', $itemoption['id'])
					->delete();
				}
		}
		return '1';
	}
	
	/**
	 * Update & Delete Window Service Options.
	 * Called from public->js->admin.js file.
	 */
	public function winserviceoptionsSave()
	{
		$itemsoption = Input::get('itemsoption');
		//print_r($itemsoption);
		//die(0);
			
		foreach($itemsoption as $itemoption)
		{

			if($itemoption['winservicetitle'] != '')
				{
					DB::table('options')
					->where('options.id', $itemoption['id'])
					->update(array(
							  'title' => $itemoption['winservicetitle'],
							  'description' => $itemoption['winservicedesc'],
							  'price' => $itemoption['winserviceprice']
							  
							  )
					     );
				}
				else
				{
						DB::table('options')
					->where('options.id', $itemoption['id'])
					->delete();
				}
		}
		return '1';
	}
	
	public static function listEstimatesAdmin()
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/logout');
		}
		else
		{
			// User is logged in
			try 
			{
				$results['winservice'] = DB::table('win_service')->get();
//print_r($results['jobs']);
//die(0);

				//BaseController::firePhp()->addInfo('$results', array($results));
				
				return View::make('estimates.admin')->with('winservice', $results['winservice']);
			}
			catch (Exception $e) 
			{	
				Session::flash('error', 'There was a problem: '.$e->getMessage());
				return $e;
			}
		}
	}
	
	public function adminStyleSaveRoom()
	{
			try {
			$all = Input::all();
			print_r($all);
			//die(0);
			
			$txt = Input::get('rooms');
			
			foreach($all as $a1=>$a2)
			{
				
				//echo $key1.'  '.$val1;
				//print_r( $txt).'  ';
				$expa1 = explode("_",$a1);
				//print_r($expa1);
				print_r($expa1[1].'-'.$a2);
				echo '<br/>';
				
				if($a2 != '')
				{
					DB::table('rooms')
					->where('rooms.id', $expa1[1])
					->update(array(
							  'room' => $a2
							  )
					     );
				}
				else
				{
						DB::table('rooms')
					->where('rooms.id', $expa1[1])
					->delete();
				}
				 
			}
			//die(0);
			
			Session::flash('success', 'Styles were updated.');
//			return Redirect::to('/estimates/admin');
//			EstimatesController::admin();
			return Redirect::back();
			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
	}

	/**
	 * Update the DB with the new Window Style list.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function adminStyleSave()
	{
		try {
			$all = Input::all();
			foreach($all as $key=>$val)
			{
				//print_r( $all);
				//die(0);

				if (is_numeric($key)) {
					$style = WinStyle::find($key+1);
					
					$style->winStyle = $val;
					$style->save();
				}
			}
			Session::flash('success', 'Styles were updated.');
//			return Redirect::to('/estimates/admin');
//			EstimatesController::admin();
			return Redirect::back();
			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
	}
	
	public function adminStyleAddWinStyle()
	{
			try {
			$all = Input::all();
			print_r($all);
			//die(0);
			
			
			DB::table('win_style')
			->insert(array(
						  'winStyle' => $all['winstyle_add']
						  )
				     );
	
			Session::flash('success', 'Styles were updated.');
			return Redirect::back();
			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
	}
	
	public function adminStyleSaveWinstyle()
	{
			try {
			$all = Input::all();
			print_r($all);
			//die(0);
			
			$txt = Input::get('win_style');
			
			foreach($all as $a1=>$a2)
			{
				
				//echo $key1.'  '.$val1;
				//print_r( $txt).'  ';
				$expa1 = explode("_",$a1);
				//print_r($expa1);
				print_r($expa1[1].'-'.$a2);
				echo '<br/>';
				
				if($a2 != '')
				{
					DB::table('win_style')
					->where('win_style.id', $expa1[1])
					->update(array(
							  'winStyle' => $a2
							  )
					     );
				}
				else
				{
						DB::table('win_style')
					->where('win_style.id', $expa1[1])
					->delete();
				}
				 
			}
			//die(0);
			
			Session::flash('success', 'Styles were updated.');
//			return Redirect::to('/estimates/admin');
//			EstimatesController::admin();
			return Redirect::back();
			} catch (Exception $e) {
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
	}
	
	
	public static function shareEst($estID)
	{
		$date = date("m/d/Y", strtotime(Input::get('date')));
		$est = [
			'name' => Input::get('name'),
			'address' => Input::get('address'),
			'addLine2' => Input::get('city'),
			'phone' => Input::get('phone'),
			'email' => Input::get('email'),			
			'date' => $date,			
			'items' => Input::get('items'),
			'total' => Input::get('total'),
			'dep' => Input::get('dep'),
			'balDue' => Input::get('baldue')
		];
		$jobID = Input::get('jobID');
		$filename = public_path('test.pdf');
		if (file_exists($filename)) {
			unlink($filename);
		}
		
		$pdf = PDF::loadView('estimates.share2', $est);
		$pdf->setOrientation('landscape');
		$pdf->setPaper('Letter');
		$pdf->setOption('margin-top', 5);
		$pdf->setOption('margin-bottom', 0);
		$pdf->setOption('margin-right', 0);
		$pdf->setOption('margin-left', 0);
		$pdf->setOption('disable-smart-shrinking', true);
		$pdf->setOption('no-pdf-compression', true);
		$pdf->save($filename);
		
		$data = file_get_contents($filename);
		$meta = [
			'title' => Input::get('description'),
//			'title' => 'bigTest',
			'description' => 'Estimate for job ID #'.$jobID,
			'mimeType' => 'application/pdf'
		];
		$test = Drive::saveFile($jobID, $pdf, $meta);
//		return 'success';
//		return $test;
		return $pdf->stream();

	}
	public static function shareEst2()
	{
		$est = [
			'name' => 'Doe, John',
			'address' => '123 Main Street',
			'addLine2' => 'Los Angeles, CA  90046',
			'phone' => '(310) 123-4567',
			'email' => 'testing@mydomain.com',
			'date' => '8/31/14',
			'items' => 
				array(
					array(
						'room' => 'Living', 
						'dir' => 'West', 
						'style' => 'DH', 
						'serv' => 'really cool service', 
						'dim' => '36x48', 
						'qty' => '1', 
						'item' => '$275', 
						'opt' => '$150', 
						'bal' => 'pulley', 
						'lineTotal' => '$425'
					), array(
						'room' => 'Dining', 
						'dir' => 'North', 
						'style' => 'DH', 
						'serv' => 'extra cool service', 
						'dim' => '48x48', 
						'qty' => '1', 
						'item' => '$275', 
						'opt' => '$150', 
						'bal' => 'pulley', 
						'lineTotal' => '$425'
					)
				),
			'total' => '$850',
			'dep' => '$85',
			'balDue' => '$765'
		];
		$filename = public_path('test.pdf');

		if (file_exists($filename)) {
			unlink($filename);
		}
		
		$pdf = PDF::loadView('estimates.share2', $est);
		$pdf->setOrientation('landscape');
		$pdf->setPaper('Letter');
		$pdf->setOption('margin-top', 5);
		$pdf->setOption('margin-bottom', 0);
		$pdf->setOption('margin-right', 0);
		$pdf->setOption('margin-left', 0);
		$pdf->setOption('disable-smart-shrinking', true);
		$pdf->setOption('no-pdf-compression', true);

		$pdf->save($filename);
		return $pdf->stream($filename);
	}

	public static function shareEst3($estID)
	{
		$date = date("M jS, Y", strtotime(Input::get('date')));
		$est = [
			'name' => Input::get('name'),
			'address' => Input::get('address'),
			'addLine2' => Input::get('city'),
			'phone' => Input::get('phone'),
			'email' => Input::get('email'),			
			'date' => $date,			
			'items' => Input::get('items'),
			'total' => Input::get('total'),
			'dep' => Input::get('dep'),
			'balDue' => Input::get('baldue'),
			'withHead' => false,
			'options' => DB::table('options')->orderBy('id', 'asc')->get(),
			'notes' => Input::get('notes')
		];

		return View::make('estimates.pdfview')->with($est);
	}

	public static function printEstimate() {

		$items = json_decode(Input::get('items'), true);

		$date = date("m/d/Y", strtotime(Input::get('date')));
		$est = [
			'name' => Input::get('name'),
			'address' => Input::get('address'),
			'addLine2' => Input::get('city'),
			'phone' => Input::get('phone'),
			'email' => Input::get('email'),			
			'date' => $date,			
			'items' => $items,
			'total' => Input::get('total'),
			'dep' => Input::get('dep'),
			'balDue' => Input::get('baldue'),
			'withHead' => true,
			'options' => DB::table('options')->orderBy('id', 'asc')->get(),
			'notes' => Input::get('notes'),
			'print' => true
		];

		return View::make('estimates.pdfview')->with($est);
	}

	public function saveEstimatePDF($estID) {

		$date = date("m/d/Y", strtotime(Input::get('date')));
		$est = [
			'name' => Input::get('name'),
			'address' => Input::get('address'),
			'addLine2' => Input::get('city'),
			'phone' => Input::get('phone'),
			'email' => Input::get('email'),			
			'date' => $date,			
			'items' => Input::get('items'),
			'total' => Input::get('total'),
			'dep' => Input::get('dep'),
			'balDue' => Input::get('baldue'),
			'withHead' => true,
			'options' => DB::table('options')->orderBy('id', 'asc')->get(),
			'notes' => Input::get('notes')
		];

		$pdf = PDF::loadView('estimates.pdfview', $est);

		$pdf->setOrientation('Landscape');
		$pdf->setPaper('Letter');
		$pdf->setOption('margin-top', 5);
		$pdf->setOption('margin-bottom', 0);
		$pdf->setOption('margin-right', 0);
		$pdf->setOption('margin-left', 0);
		$pdf->setOption('no-pdf-compression', true);
		
		$filename = public_path().'/uploads/' . str_random(16) . '.pdf';
		$pdf->save($filename);

		$data = file_get_contents($filename);
		$meta = [
			'title' => Input::get('description'),
			'description' => Input::get('description'),
			'mimeType' => 'application/pdf'
		];
		
		$result = Drive::saveFileVerbose(Input::get('jobID'), $data, $meta);
		
		if($result !== FALSE) {
			$result = json_decode($result);
			if($result->status === 'ok') {
				if(isset($result->webContentLink)) { // upload success
					
					$data = [];
					$data['owner_type'] = 'estimate';
					$data['owner_id'] = $estID;
					$data['path'] = $result->webContentLink;
					$data['title'] = 'PDF for estimate #' . $estID;
					$data['type'] = 'PDF';
					$data['uploaded_at'] = date("Y-m-d H:i:s");
					$data['download_url'] = $result->downloadUrl;
					$data['status'] = 1;
					try {
						DB::table('files')->insert($data);
						return json_encode(['status' => 'ok', 'path' => $result->webContentLink, 'download_url' => $result->downloadUrl]);
					} catch (Exception $e) {
						return json_encode(['status' => 'error', 'reason' => 'Exception in DB::table(\'files\')->insert($data)', 'message' => $e->getMessage()]);
					}

				}
				else {
					return json_encode(['status' => 'error', 'reason' => 'if(isset($result->webContentLink)) failed']);
				}
			}
			else {
				return json_encode(['status' => 'error', 'reason' => 'if($result->status === \'ok\') failed', 'saveFile_result' => $result]);
			}
		}

		return json_encode(['status' => 'error', 'reason' => 'Drive::saveFile() returned FALSE']);

	}

	public function emailEstimatePDF() {
		
		// TODO: validate inputs

		// download PDF
		$filename = public_path().'/uploads/downloaded-' . str_random(16) . '.pdf';
		$result = Drive::downloadFile(Input::get('attachment'), $filename);
		if($result !== true) return json_encode(['status' => 'error']);

		// send mail
		Mail::send(['html' =>'emails.empty'], ['body' => Input::get('content')], function($message) use ($filename) {
		    $message->from(Input::get('from'), Input::get('from'));
		    $message->to(Input::get('to'));
		    if(Input::get('cc')) $message->cc(Input::get('cc'));
		    if(Input::get('bcc')) $message->bcc(Input::get('bcc'));
		    $message->subject(Input::get('subject'));
		    $message->attach($filename, array('as' => Input::get('attachment_name') . '.pdf', 'mime' => 'application/pdf')); // attach the file downloaded from drive
		});

		// add 'estimate sent' note to job notes
		$user = Sentry::getUser();
		
		$note = 'Estimate emailed to customer on '.date("n/j/Y (g:ia)", time()).' by: '.$user->first_name;
		$note .= '<br>----------<br>';
		$note .= Input::get('content');

		Note::createNote(Input::get('job_id'), $user->id, $note);
		
		return 'OK';
		
	}

	public function copyEstimate() {
		
		// load estimate
		$est = DB::table('estimates')->where('id', '=', Input::get('id'))->first();

		// create new estimate
		$newEst = [
			'job_id' => $est->job_id,
		 	'description' => Input::get('name'),
		 	'sold' => $est->sold,
		 	'deposit' => $est->deposit,
		 	'notes' => $est->notes,
		 	'parts' => $est->parts
		];
		$newEstID = Estimate::addEstimate($newEst);

		// load estimate items
		$estItems = DB::table('estimate_items')			
			->where('est_id', '=', Input::get('id'))
			->orderBy('item_num')
			->get();
		
		// copy items to new estimate
		foreach ($estItems as $item) {

			// convert to array
			$itemArr = [];
			foreach ($item as $key => $value) $itemArr[$key] = $value;

			unset($itemArr['id']);
			$itemArr['est_id'] = $newEstID;
			Estimate::addEstimateItems($itemArr);
		}

		return json_encode(['status' => 'ok', 'id' => $newEstID, 'name' => Input::get('name'), 'date' => date("Y-m-d H:i:s")]);

	}

	public function markSoldEstimate($id) {
		DB::table('estimates')->where('id', '=', $id)->update(['sold' => '1']);
		return json_encode(['status' => 'ok']);
	}

	public function undoSoldEstimate($id) {
		DB::table('estimates')->where('id', '=', $id)->update(['sold' => '0']);
		return json_encode(['status' => 'ok']);
	}

	public function serviceParts($id) {

		$parts = DB::table('parts')
	        ->leftJoin('win_service_parts', 'parts.id', '=', 'win_service_parts.parts_id')
	        ->where('win_service_parts.win_service_id', $id)
	        ->groupBy('win_service_parts.parts_id')
	        ->get();

	    foreach ($parts as $part) {
	    	$part->options = json_decode($part->options);
	    }

	    return json_encode($parts);

	}
	public function optionParts($id) {
		
		$parts = DB::table('parts')
	        ->leftJoin('options_parts', 'parts.id', '=', 'options_parts.parts_id')
	        ->where('options_parts.options_id', $id)
	        ->groupBy('options_parts.parts_id')
	        ->get();

	    foreach ($parts as $part) {
	    	$part->options = json_decode($part->options);
	    }

	    return json_encode($parts);
	}

	public function testCreate() {
        return View::make('estimates.testcreate');
	}				
}