<?php

class AuthController extends BaseController {

	/* Create Google client, check if access token in Session is expired,
	update if necessary, check if refresh_token is stored in db.
	Update DB if necessary and handles Oauth2 transaction.
	Returns Google Client with proper attributes. 
	---------------------------------------------------------------------*/
	
	public static function makeGoogleClient($path = NULL)
	{	
		/* set up & return Google client
		--------------------------------- */
		$appName = Config::get('app.google_application_name');
		$clientId = Config::get('app.google_client_id');
		$clientSecret = Config::get('app.google_client_secret');
		$redirectUri = Config::get('app.google_redirect_uri');
		$scopes = Config::get('app.google_scopes');
		
		$client = new Google_Client();
		$client->setApplicationName($appName);
		$client->setClientID($clientId);
		$client->setClientSecret($clientSecret);
		$client->setRedirectUri($redirectUri);
		$client->setScopes($scopes);
		$client->setApprovalPrompt("force");
		$client->setAccessType('offline');
		if ($path) {
			$client->setState($path);
		}

		// check to see if access token is expired - refresh as needed
		$token = $client->getAccessToken();
		$expired = $client->isAccessTokenExpired();
		$user = Sentry::getUser();
		if($user->google_refresh_token && $expired == true)
		{
			//set the refresh token
			$refreshToken = json_decode($user->google_refresh_token);
			$client->refreshToken($refreshToken);

			//save the new temp access token into session
			Session::put('token', $client->getAccessToken());
		}
		
		if(!Session::has('token'))
		{
			Session::put('token', $token);
		}

		return $client;
	}
	
	/* No Google Refresh Token for user has been found in DB.  
	*  Initiate Google Oauth2 process.  This will trigger response 
	*  from Google which will be processed by getOauth2 function.
	*--------------------------------------------------------------*/
		public static function makeNewToken($path)
	{
		$client = self::makeGoogleClient($path);
		$user = Sentry::getUser();

		$authUrl = $client->createAuthUrl();		
		echo '<script type="text/javascript"> window.location.href="'.$authUrl.'"; </script>';
	}

	/* Google Oauth2 callback function to receive access code.
	*  Receives code, inserts it in DB in google_refresh_code,
	*  sets access token in Session and redirects user to the page
	*  in the 'state' variable.
	* -------------------------------------------------------------*/
		public static function getOauth2()
	{
		$client = self::makeGoogleClient();

		$code = Input::get('code');
		$path = (Input::get('state') ? Input::get('state') : 'customers');

		$accessToken = $client->authenticate($code);		
		$client->setAccessToken($accessToken);
		Session::put('token', $accessToken);
		$refreshToken = $client->getRefreshToken();

		$user = Sentry::getUser();
		$user->google_refresh_token = json_encode($refreshToken);
		$user->save();

		return Redirect::to($path);
	}
}