<?php

class CustomersController extends BaseController {


	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('auth', array(
			'except' => array(
				'index'
			)
		));
	} 

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{		
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");	

		if (isset($_GET['code'])) 
		{
			$client->authenticate($_GET['code']);

			$googleData = json_decode($client->getAccessToken());

			//Save the refresh_token 
			$user = Sentry::getUser();
			$userData = User::find($user->id);			
			$userData->google_refresh_token = $googleData->refresh_token;
			$userData->save();

			//get a token from refresh_token
			$client->refreshToken($googleData->refresh_token);

			//set the token into session
			Session::put('token', $client->getAccessToken());

			$user_controller = new UserController();
			$user_controller->checkCalendarChannel($user);

			return Redirect::to('/customers');
		}

		$data = Session::all();
		//BaseController::firePhp()->addInfo('$data', $data);

		if (Session::has('token'))
		{
			$client->setAccessToken(Session::get('token'));

			try 
			{

				if(Input::get('archived')) {
					$customers = Job::getArchivedJobs();
					$heading = 'Leads - Archived';
				}
				else {
					$customers = Job::getJobsByStatus((Input::get('status') ? Input::get('status') : LEAD_CREATED), Input::get('scheduled'));
					$heading = 'Leads - New';
					switch(Input::get('status')) {
						 case LEAD_SCHEDULED: $heading = 'Leads - Appointments'; break;
						 case QUOTE_CREATED: $heading = 'Leads - Quote Created'; break;
						 case JOB_ACCEPTED: $heading = (Input::get('scheduled') === '1' ? 'Jobs - Scheduled' : 'Jobs - To be Scheduled'); break;
						 case JOB_COMPLETED: $heading = 'Jobs - Completed'; break;
						 case JOB_ON_STANDBY: $heading = 'Jobs - On Standby'; break;
						 case JOB_ON_SERVICECALL: $heading = 'Jobs - Service Calls'; break;
					}
				}

				return View::make('customers.index')->with(['customers' => $customers, 'heading' => $heading]);
			}
			catch (Exception $e) 
			{
				Session::flash('error', 'There was a problem: '.$e);
				return $e;
			}
		} else {		
			return Redirect::to('/');
		}
	}

	public function leadByJobID($jobID)
	{		
		$results['customers'] = Job::getJobCustomers($jobID, 1, 0);

		//BaseController::firePhp()->addInfo('Results', $results);
			
		return $results;		
	}
	
	public static function leadByCustID($custID)
	{
		//BaseController::firePhp()->addInfo('$id2', array($custID));
		try 
		{
			$results = Customer::getCustomer($custID);
			
			return $results;
		}
		catch (Exception $e) 
		{
			Session::flash('error', 'There was a problem: '.$e);
			return $e;
		}
	}

	public function archive($id)
	{
		DB::table('jobs')
			->where('id', $id)
			->update(array('archive' => '1'));
		
		$user = Sentry::getUser();
		
		Note::createNote($id, $user->id, 'Lead archived on '.date("n/j/Y (g:ia)", time()).' by: '.$user->first_name, 1);

		Session::flash("success", "Job #".$id." archived.");
		return Redirect::to('customers');
	}

	public function getLeads()
	{
		return View::make('customers.leads')
			->with('customers', Customer::all());
	}

	public function getSchedule()
	{		
		$results = array("test", "stuff");
		return View::make('customers.schedule')->with($results);
	}
	
	public static function custDetailPDF($job_id, $appt_id)
	{
		try 
		{
			$results = json_decode(JobsController::jobEventDetail($job_id, $appt_id), $assoc = true);
			if (strpos($results['lead_source'], 'Other') !==false) {
				$results['otherSource'] = explode( 'Other, ', $results['lead_source']);
			}

			//BaseController::firePhp()->addInfo('CustomerController@custDetailPDF - $results', $results);

			$html = View::make('jobs.jobsheet')->with($results);
//return $html;		
			$pdf = App::make('dompdf');
			$pdf->setPaper("letter");
			$pdf->loadHTML($html);

			return $pdf->stream();
		}
		catch (Exception $e) 
		{
			Session::flash('error', 'There was a problem: '.$e);
			return $e;
		}
	}

	public static function iframePDF($job_id, $appt_id)
	{
		$url = array('job_id' => $job_id, 'appt_id' => $appt_id);
		return View::make('jobs.iframe')->with($url);
	}
	
	public static function getDetailID($id)
	{
		$results['customer'] = Customer::getCustomer($id);
		$results['jobs'] = Job::getJobsByCustID($id);
		
		return View::make('customers.edit')->with($results);
	}

	public function typeahead($term)
	{
//		$term = Input::get('term');
		$data = array();

		$query = DB::select("SELECT id, l_name, f_name, billing_address, billing_city, phone, email FROM customers WHERE MATCH(l_name) AGAINST('+".$term."*' IN BOOLEAN MODE) LIMIT 5");

		foreach ($query as $results => $customer) 
		{
			$data[] = array(
				'id' => $customer->id,
				'value' => $customer->l_name,
				'l_name' => $customer->l_name,
				'f_name' => $customer->f_name,
				'address' => $customer->billing_address,
				'city' => $customer->billing_city,
				'phone' => $customer->phone,
				'email' => $customer->email,
			);
		}

		return json_encode($data);
	}

	public function getScheduleID($id)
	{
		$results['googMapApiKey'] = Config::get('app.google_geocode_api_key');
		
		$results['lead'] = Job::getJobCustomers($id);
		$results['leadx'] = Job::getJobCustomer($id);
		$results['title'] = "(".$results['leadx']->job_city.") ".$results['leadx']->customer_lname.", ".$results['leadx']->customer_fname;
		$results['location'] = $results['leadx']->job_address.", ".$results['leadx']->job_city.",  ".$results['leadx']->job_zip;
				
		$ts = strtotime("now");
		$init = (date('w', $ts) == 1) ? strtotime('midnight') : strtotime('last Monday', $ts);
		$monday = date('c', $init);
		$mondayStart = $init;
		$mondayEnd = $mondayStart+86399;
		$tuesdayStart = $mondayEnd+1;
		$tuesdayEnd = $tuesdayStart+86399;
		$wednesdayStart = $tuesdayEnd+1;
		$wednesdayEnd = $wednesdayStart+86399;
		$thursdayStart = $wednesdayEnd+1;
		$thursdayEnd = $thursdayStart+86399;
		$fridayStart = $thursdayEnd+1;
		$fridayEnd = $fridayStart+86399;

		$salesChecks = ProfilesController::getSalesCheckBox();
		
		$results['profiles'] = UserController::getSalesProfiles();

		foreach($results['profiles'] as $profile) {
			$results['calendar'][$profile['google_calendar_id']] = $profile['first_name'];
		}
		$results['notes'] = Note::notesByJobID($id);			

		$results['fullNote'] = EventsController::setDescription($id);

		return View::make('customers.schedule')->with($results);
	}

	public static function EstimateSchedule()
	{
		
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$cal = new Google_CalendarService($client);

		$start = date('c',$_GET['start']);
		$end = date('c',$_GET['end']);
		
		if (Session::has('token')) 
		{
			$client->setAccessToken(Session::get('token'));
		}

		if ($client->getAccessToken()) 
		{
			$rightNow = date('c');
			$params = array('singleEvents' => 'true', 'orderBy' => 'startTime', 'timeMin' => $start, 'timeMax' => $end);
			$calList2 = $cal->events->listEvents('windowrnr.com_g67gtb3doc8ehsdaffpe1idaq4@group.calendar.google.com', $params);

			$events = array();
			foreach ($calList2['items'] as $event)
			{
				if(isset($event['start']['dateTime'])) 
				{
					$eventStart = $event['start']['dateTime'];
					$eventEnd = $event['end']['dateTime'];
					$allDay = false;
				}
				else
				{
					$eventStart = $event['start']['date'];
					$eventEnd = date('Y-m-d', strtotime($event['end']['date'])-1);
					$allDay = true;
				}

				if(!isset($event['description'])) 
				{
					$event['description'] = null;
				}
			
				$events[] = array(
					"title" => $event['summary'],
					"start" => $eventStart,
					"end" => $eventEnd,
					"url" => $event['htmlLink'],
					"allDay" => $allDay,
					"description" => $event['description'],
					"location" => $event['location'],
				);
			}
			//BaseController::firePhp()->addInfo('estimateSchedule($events)', $events);
			return $events;
		}
	}

	public static function EstimateSchedule2()
	{
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$cal = new Google_CalendarService($client);

		$start = date('c',$_GET['start']);
		$end = date('c',$_GET['end']);
		
		if (Session::has('token')) 
		{
			$client->setAccessToken(Session::get('token'));
		}
		
		if ($client->getAccessToken()) 
		{
			$rightNow = date('c');
			$params   = array(
				'singleEvents' => 'true', 
				'orderBy' => 'startTime', 
				'timeMin' => $start, 
				'timeMax' => $end
			);

			$calList = $cal->events->listEvents('windowrnr.com_c7df92ao3vvg02n2kh52b81tn4@group.calendar.google.com', $params);

			//BaseController::firePhp()->addInfo('calList', $calList);

			$events = array();
			foreach ($calList['items'] as $event)
			{
				if(isset($event['start']['dateTime'])) 
				{
					$eventStart = $event['start']['dateTime'];
					$eventEnd = $event['end']['dateTime'];
					$allDay = false;
				}
				else
				{
					$eventStart = $event['start']['date'];
					$eventEnd = date('Y-m-d', strtotime($event['end']['date'])-1);
					$allDay = true;
				}

				if(!isset($event['description'])) 
				{
					$event['description'] = null;
				}
			
				$events[] = array(
					"title" => $event['summary'],
					"start" => $eventStart,
					"end" => $eventEnd,
					"url" => $event['htmlLink'],
					"allDay" => $allDay,
					"description" => $event['description'],
					"location" => $event['location'],
				);
			}
			//BaseController::firePhp()->addInfo('estimateSchedule($events)', $events);
			return $events;
		}
	}
	
	public static function EstSchedByIDByDay($calendar, $monday)
	{
		$client = new Google_Client();
		$client->setApplicationName("Window R & R");
		$cal = new Google_CalendarService($client);

		if (Session::has('token')) 
		{
			$client->setAccessToken(Session::get('token'));
		}
		
		if ($client->getAccessToken()) 
		{
			$ts = strtotime("now");
			$init = (date('w', $ts) == 1) ? $ts : strtotime('last Monday', $ts);
			$start_date = date('c', $init);
			$end_date = date('c', strtotime('this Saturday', $init));			
			$events = array();

			$params = array('singleEvents' => 'true', 'orderBy' => 'startTime', 'timeMin' => $start_date, 'timeMax' => $end_date);

			foreach ($calendar as $calUser) 
			{
				$calList = $cal->events->listEvents($calUser['id'], $params);
				//BaseController::firePhp()->addInfo('EstSchedByIDByDay - calList', $calList);
					
				foreach ($calList['items'] as $event)
				{
					if(isset($event['start']['dateTime'])) 
					{
						$n = date('w', strtotime($event['start']['dateTime']));
						$start = date('w', strtotime($event['start']['dateTime']));
						$end = date('w', strtotime($event['end']['dateTime']));
					}
					else
					{
						$n = date('w', strtotime($event['start']['date']));
						$start = date('w', strtotime($event['start']['date']));
						$end = date('w', strtotime($event['end']['date']));
					}

					if(!isset($event['description'])) 
					{
						$event['description'] = null;
					}
					
					$events[$n][$calUser['name']][] = array(
						"title" => $event['summary'],
						"start" => $start,
						"end" => $end,
						"url" => $event['htmlLink'],
						"allDay" => false,
						"content" => $event['location'],
						"description" => $event['description'],
						"location" => $event['location'],
					);
				}

				if(!isset($events[1])) 
				{
					$events[1][$calUser['name']] = null;
				}

				if(!isset($events[2])) 
				{
					$events[2][$calUser['name']] = null;
				}

				if(!isset($events[3])) 
				{
					$events[3][$calUser['name']] = null;
				}

				if(!isset($events[4])) 
				{
					$events[4][$calUser['name']] = null;
				}

				if(!isset($events[5])) 
				{
					$events[5][$calUser['name']] = null;
				}
			}

			return $events;
		}
	}

	public static function postGoogleInsert($_api = false)
	{
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$cal = new Google_Service_Calendar($client);

		if (Session::has('token')) $client->setAccessToken(Session::get('token'));
		
		if ($client->getAccessToken()) {

			$eventTitles = [EVENT_EST_APPT => 'Estimate Appt.', EVENT_INSTALL_REPAIR => 'Install/Repair Job', EVENT_SERVICE_CALL => 'Service Call.', EVENT_PERSONAL_TIME => 'Personal or Office'];
			$eventTitle = $eventTitles[$_POST['event_type']];
			
			$custEventTitles = [EVENT_EST_APPT => 'WindowRnR - Repair estimate', EVENT_INSTALL_REPAIR => 'WindowRnR - Install/Repair Job', EVENT_SERVICE_CALL => 'WindowRnR - Service Call.', EVENT_PERSONAL_TIME => 'Personal or Office.'];
			$custEventTitle = $custEventTitles[$_POST['event_type']];
			
			$newJobStatus = LEAD_SCHEDULED;
			if($_POST['event_type'] === EVENT_SERVICE_CALL) {
				$newJobStatus = JOB_ON_SERVICECALL;
			}

			$calendarID   = $_POST['calendarID'];

			$event = new Google_Service_Calendar_Event();

			if($_POST['event_type'] == EVENT_PERSONAL_TIME) {
				$event->setSummary('Personal or Office');
				$event->setDescription(Input::get('note'));
			}
			else {
				$summary      = (isset($_POST['summary']) ? $_POST['summary'] : 'Event Summary');
				$location     = (isset($_POST['location']) ? $_POST['location'] : '');
				
				$description  = ($_POST['event_type'] == EVENT_PERSONAL_TIME ? 'Personal Time' : CustomersController::makeGoogleEventDescription());

				$event->setSummary($summary);
				$event->setLocation($location);
				$event->setDescription($description);
			}

			$job_id       = !empty($_POST['job_id']) ? $_POST['job_id'] : 0;
			$email		  = $_POST['email'];
			$cust_id 	  = !empty($_POST['cust_id']) ? $_POST['cust_id'] : 0;

			$start        = substr($_POST['start'], 0, 33);
			$startTime    = strtotime($start);
			$start        = date('c', $startTime);

			$end          = substr($_POST['end'], 0, 33);
			$end          = strtotime($end);
			$end          = date('c', $end);

			$eventStart = new Google_Service_Calendar_EventDateTime();
			$eventStart->setDateTime($start);
			$eventStart->setTimeZone('America/Los_Angeles');
			$event->setStart($eventStart);

			$eventEnd = new Google_Service_Calendar_EventDateTime();
			$eventEnd->setDateTime($end);
			$eventEnd->setTimeZone('America/Los_Angeles');
			$event->setEnd($eventEnd);

			// add the customer as an attendee
/*			if(!empty($email)) {
				$attendee1 = new Google_Service_Calendar_EventAttendee();
				$attendee1->setEmail($email);
				$event->setAttendees([$attendee1]);
				$event->setVisibility('public');
			}
*/
			// add job_id property
			if($_POST['event_type'] != EVENT_PERSONAL_TIME) {
				$extendedProperties = New Google_Service_Calendar_EventExtendedProperties();
				$extendedProperties->setShared(['job_id' => $job_id]);
				$event->setExtendedProperties($extendedProperties);
			}

			// insert the event into google calendar
			$createdEvent = $cal->events->insert($calendarID, $event);

			// Insert the event into the App DB
			$createdEvent->job = $job_id;
			$createdEvent->created_at = date("Y-m-d H:i:s");
			$createdEvent->type = $_POST['event_type'];
			$createdEvent->event_type = $eventTitle;
			$createdEvent->cust_event_type = $custEventTitle;

			$eventID = Events::insertEventData($createdEvent);


			if($_POST['event_type'] != EVENT_PERSONAL_TIME) {
				
				$job = Job::find($job_id);
				$customer = Customer::find($job->customer_id);

				// Create ICS file for event
				$icsData = 'BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//WindowRnR//Scheduled Event//Job ' . $job_id . '//EN
METHOD:PUBLISH
BEGIN:VEVENT
UID:'. $createdEvent->iCalUID . '
SUMMARY:' . $custEventTitle . '
DTSTART:'. date('Ymd\THis', strtotime($createdEvent->getStart()->dateTime)) .'
DTEND:'. date('Ymd\THis', strtotime($createdEvent->getEnd()->dateTime)) .'
LOCATION:'. $createdEvent->location . '
DESCRIPTION:'.$custEventTitle.'\n \n'.'For any questions please call our office: (562) 493-1590.  Thanks!'. '
ORGANIZER:MAILTO:scott@windowrnr.com
' . (!empty($email) ? 'ATTENDEE:MAILTO:' . $email : '') . '
END:VEVENT
END:VCALENDAR';

				// Write to file and attach it to email
				$icsFile = rtrim(sys_get_temp_dir(), '/') . '/' . str_random(16) . '.ics';
				file_put_contents($icsFile, $icsData);
				$user = Sentry::getUser();

				// Use swift mailer to send the email
				if(!empty($email)) {
				$senderName = $user->first_name.' '.$user->last_name;
				$senderEmail = $user->email;
				Mail::send('emails.event', ['createdEvent' => $createdEvent, 'customer' => $customer, 'creator' => $user], function($mail) use($email, $senderEmail, $senderName, $icsFile, $custEventTitle) {
						$mail->from($senderEmail, $senderName);
						$mail->sender($senderEmail, 'WindowRnR');
						$mail->replyTo($senderEmail, 'WindowRnR');
					    $mail->to($email)->subject($custEventTitle);
					    $mail->attach($icsFile, ['as' => 'WindowRnR.ics', 'mime' => 'text/calendar']);
					});
				}

				$apptDate = date('n/j/Y (g:ia)', $startTime);
				if($_POST['event_type'] !== EVENT_PERSONAL_TIME) {
					Note::createNote($job_id, $user->id, "Estimate appt scheduled for: ".$apptDate, 1);
				}

	//			$appt = Appointment::insertData($job_id, $eventID, $calendarID, date('Y-m-d H:i:s', strtotime($_POST['start'])), date('Y-m-d H:i:s', strtotime($_POST['end'])), 1, 'x', $cust_id);

				// FIX for error: invalid date time format for $start & $end
				// convert to mysql datetime format
				$start        = substr($_POST['start'], 0, 33);
				$startTime    = strtotime($start);
				$start        = date('Y-m-d H:i:s', $startTime);
				$end          = substr($_POST['end'], 0, 33);
				$end          = strtotime($end);
				$end          = date('Y-m-d H:i:s', $end);
				if($_POST['event_type'] !== EVENT_PERSONAL_TIME) {
					$appt = Appointment::insertData($job_id, $eventID, $calendarID, $start, $end, 1, 'x', $cust_id);
				}

				// check if we need to change job status
				$changeJobStatus = true;
				switch($newJobStatus) {
					case LEAD_SCHEDULED:
					case QUOTE_CREATED:
						$changeJobStatus = ($job->status < $newJobStatus);
						break;
				}
				$statusResult = ($changeJobStatus && ($_POST['event_type'] !== EVENT_PERSONAL_TIME) ? Job::updateStatus($job_id, $newJobStatus) : 'Success');

			}
var_dump($statusResult);
echo "</br>";

			if($_POST['event_type'] == EVENT_PERSONAL_TIME) {
				if(!$_api) {
					Session::flash('success', 'Event created.');
					return Redirect::to('/calendar');
				}
				else {
					return $eventID;
				}
			}

			if ($appt['status'] === 'Success' && $statusResult === 'Success') {
echo "1";
				if($_api) return $eventID;

				Session::flash('success', 'Lead Scheduled.');

				if($_POST['event_type'] == EVENT_PERSONAL_TIME) {
					return Redirect::to('/calendar');
				}
				else if($_POST['event_type'] == EVENT_INSTALL_REPAIR) {
					$start        = substr($_POST['start'], 0, 33);
					$startTime    = strtotime($start);
					$jobDate      = date('Y-m-d', $startTime);
					return Redirect::to('assign?date='.$jobDate);
				}
				else {
					return Redirect::to('frame/'.$job_id.'/'.$appt['id']);
				}
			}
			else 
			{
echo "2</br>";
var_dump($appt);
echo "</br>";
var_dump($_api);
				if($_api) return false;

				Session::flash("Failure", "Status updated failed with Appointment inserts returning: ".$statusResult[0].", and Job inserts returning: ".$statusResult[1]);
			}
			Session::put('token',$client->getAccessToken());
		}
		else 
		{
			// TODO: Need to handle this case when called via the API
			$authUrl = $client->createAuthUrl();	
		}
	}

	public function getContracts()
	{
		return View::make('customers.contracts')
			->with('customers', Customer::all());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	 
	public function schedule()
	{
		return Redirect::to('customers#tab2');
	}

	public function complete($id) {
		$job = Job::find($id);
		if($job) {
			$job->status = JOB_COMPLETED;
			$job->save();
			Session::flash('success', 'Job marked as completed.');
		}
		return Redirect::to('/customers?status=4&scheduled=1');
	}
	
	public function newLead()
	{		
/*		if (Customer::where('email', '=', Input::get('email'))->exists()) 
		{
			Session::flash('error', 'This e-mail address already exists');
   			return Redirect::back()->withInput();
		}	
*/		
		try 
		{
			$results = CustomersController::store();

			if (is_numeric($results)) 
			{
				Session::flash('success', 'Lead #'.$results.' added.');
				if (isset($_POST['scheduleNewLead'])) 
				{
					return Redirect::to('customers/schedule/'.$results);
				} 
				else 
				{
					return Redirect::to('customers');
				}
			} 
			else 
			{
				Session::flash('error', 'Error occurred: '.$results);
				return Redirect::to('customers');
			}
		}
		catch (Exception $e) 
		{
			Session::flash('error', 'There was a problem: '.$e);

			return Redirect::to('customers');
		}
	}
	
	public static function store()
	{
		//BaseController::firePhp()->addInfo('$_POST', $_POST);

		try {

			$now = date("Y-m-d H:i:s");
		
			$customer = new Customer();
			$customer->f_name = Input::get('f_name');
			$customer->l_name = Input::get('l_name');
			$customer->company_name = (empty(Input::get('company_name')) ? NULL : Input::get('company_name'));
			$customer->phone = Input::get('phone');
			$customer->alt_phone = Input::get('alt_phone');
			$customer->email = Input::get('email');
			$customer->billing_address = Input::get('billing_address');
			$customer->billing_city = Input::get('billing_city');
			$customer->billing_state = Input::get('billing_state');
			$customer->billing_zip = Input::get('billing_zip');
			$customer->created_at = $now;
			$customer->updated_at = $now;
			
			$billingAddress = Input::get('billing_address');
			if($billingAddress === '')
			{
				$customer->billing_address = Input::get('address');
				$customer->billing_city = Input::get('city');
				$customer->billing_state = 'CA';
				$customer->billing_zip = Input::get('zip');
			}
			
			$customer->save();
			
			$job = new Job();
			$job->customer_id = $customer->id;
			$job->status = '1';
			$job->archive = '0';
			$job->address = Input::get('address');
			$job->city = Input::get('city');
			$job->zip = Input::get('zip');
			$job->built = Input::get('built');
			$job->address = Input::get('address');
			$job->created_at = $now;
			$job->updated_at = $now;
			$array = Input::get('lead_source');
			$sourceRef = Input::get('source_referral');
			$job->created_by = 'app.windowrnr.com';
			$job->type = 'Window Repair - Generic';

			if(!empty($sourceRef))
			{
				$referral = $sourceRef;
			}
			if(count($array) > 0) 
			{
				$job->lead_source = $array[0];
				if($array[0] === 'Other' && !empty($referral))
				{
					$job->lead_source = $array[0].', '.$referral;
				}
				$i = 1;
				for($i, $n = count($array); $i < $n; $i++) 
				{
					if($array[$i] === 'Other' && !empty($referral))
					{
						$job->lead_source .= ', '.$array[$i].', '.$referral;
					}
					$job->lead_source .= ', '.$array[$i];
				}
			}
			else {
				$job->lead_source = 'Other';
			}

			$job->save();
			$resultID = $job->id;
			
			$user = Sentry::getUser();
			$timeDate = DB::table('jobs')->where('id', $job->id)->pluck('created_at'); 

			Note::createNote($job->id, $user->id, Input::get('note'));
			
			if (isset($_POST['qty1'])) 
			{
				$pos = array();
				foreach($_POST as $key => $value) 
				{
					if (strpos($key, "qty") !== false) 
					{
						$pos[] = strpos($key , "qty");
					}
				}
				$num = count($pos);

				$i = 0;
				foreach($pos as $qty) 
				{
					$i++;
					if (isset($_POST['qty'.$i])) 
					{
						$style = new Window_total();
						$style->job_id = $job->id;
						$style->qty = Input::get('qty'.$i);
						$style->material = Input::get('material'.$i);
						$style->style = Input::get('style'.$i);
						$style->save();
					}
				}
			}
			return $resultID;
		}
		catch (Exception $e) 
		{
			Session::flash('error', 'There was a problem: '.$e);
			echo $e;
			return $e;
		}
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$results = CustomersController::leadByJobID($id);
		return View::make('customers.edit')->with($results);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{		
		try 
		{
			$now = date("Y-m-d H:i:s");

			$data = Input::all();

			$custID = Input::get('custID');
			$custUpdate = array(
				'f_name'          => Input::get('f_name'),
				'l_name'		  => Input::get('l_name'),
				'company_name'    => (empty(Input::get('company_name')) ? NULL : Input::get('company_name')),
				'phone' 		  => Input::get('phone'),
				'email' 		  => Input::get('email'),
				'alt_phone' 	  => Input::get('alt_phone'),
				'billing_address' => Input::get('billing_address'),
				'billing_city'    => Input::get('billing_city'),
				'billing_state'   => Input::get('billing_state'),
				'billing_zip'     => Input::get('billing_zip'),
				'updated_at'      => $now);
					
			$jobID = Input::get('jobID');
			$jobUpdate = array(
				'address'     => Input::get('address'),
				'city'        => Input::get('city'),
				'state'       => Input::get('state'),
				'zip' 		  => Input::get('zip'),
				'built' 	  => Input::get('built'),
				'lead_source' => Input::get('lead_source'),
				'updated_at'  => $now
			);
			
			// if address changed, geolocate
			$currentJobRecord = DB::table('jobs')->where('id', $jobID)->first();
			if($currentJobRecord->address != Input::get('address') || $currentJobRecord->city != Input::get('city') ||
				$currentJobRecord->state != Input::get('state') || $currentJobRecord->zip != Input::get('zip')) {

				$latlng = Bing::geoLocate(Input::get('address'), Input::get('city'), Input::get('state'), Input::get('zip'));
				if($latlng !== FALSE) {
					$jobUpdate['lat'] = $latlng[0];
					$jobUpdate['lng'] = $latlng[1];
				}
			}
			
			$array = Input::get('lead_source');

			if(count($array) > 0) 
			{
				$jobUpdate['lead_source'] = $array[0];
				$i = 1;
				for($i, $n = count($array); $i < $n; $i++) 
				{
					$jobUpdate['lead_source'] .= ', '.$array[$i];
				}
			}

			// job status - accepted/on-standby/completed
			if(Input::get('status-on-service-call') === 'on') {
				$jobUpdate['status'] = JOB_ON_SERVICECALL;
				$jobUpdate['job_put_on_standby'] = $now;				
			}
			else if(Input::get('status-on-standby') === 'on') {
				$jobUpdate['status'] = JOB_ON_STANDBY;
				$jobUpdate['job_put_on_standby'] = $now;				
			}
			else if(Input::get('status-completed') === 'on') {
				$jobUpdate['status'] = JOB_COMPLETED;
				$jobUpdate['job_completed'] = $now;
			}
			else if(Input::get('status-job-accepted') === 'on') {
				$jobUpdate['status'] = JOB_ACCEPTED;
				$jobUpdate['job_accepted'] = $now;
			}
			else if(Input::get('status') == LEAD_CREATED) {
				$jobUpdate['status'] = LEAD_CREATED;
			}			
			else {
				$jobUpdate['status'] = LEAD_SCHEDULED;
			}

			$pos = array();
			foreach($_POST as $key => $value) 
			{
				if (strpos($key, "qty") !== false) 
				{
					$pos[] = strpos($key , "qty");
				}
			}
			$num = count($pos);

			$i = 0;
			foreach($pos as $qty) 
			{
				$i++;
				$windowTotalID = Input::get('windowTotalID'.$i);

				if (isset($_POST['qty'.$i])) 
				{
					$material = Input::get('material'.$i);
					$style    = Input::get('style'.$i);
					$qty      = Input::get('qty'.$i);

					$windowTotalUpdate = array(
						'qty' 		 => $qty,
						'material'   => $material,
						'style' 	 => $style,
						'updated_at' => $now
					);

					if (isset($_POST['windowTotalID'.$i])) 
					{
						DB::table('window_totals')
							->where('id', $windowTotalID)
							->update($windowTotalUpdate);
					} 
					else 
					{
						DB::table('window_totals')->insert(array(
							'job_id'     => $jobID,
							'qty' 		 => $qty,
							'material'   => $material,
							'style'      => $style,
							'updated_at' => $now,
							'created_at' => $now
						));
					}
				}
			}
			
			$billingAddress = Input::get('billing_address');
			if($billingAddress === '')
			{
				$custUpdate['billing_address'] = Input::get('address');
				$custUpdate['billing_city'] = Input::get('city');
				$custUpdate['billing_state'] = 'CA';
				$custUpdate['billing_zip'] = Input::get('zip');
				DB::table('customers')
					->where('id', $custID)
					->update($custUpdate);
			} else {
				DB::table('customers')
					->where('id', $custID)
					->update($custUpdate);
			}

			DB::table('jobs')
				->where('id', $jobID)
				->update($jobUpdate);
				
			$user = Sentry::getUser();
			$note = "Lead detail updated";
			$noteResult = Note::createNote($jobID, $user->id, $note, 1);
			
			$newNote = Input::get('newNote');
			if ($newNote != '') 
			{
				$noteResult2 = Note::createNote($jobID, $user->id, $newNote, 0);
				if ($noteResult && $noteResult2 == 'Success') 
				{
					Session::flash('success', 'Lead updated successfully.1');
				} 
				else 
				{
					Session::flash('error', 'There was a problem with the lead update.');
				}
			} 
			else 
			{
				if ($noteResult == 'Success') 
				{
					Session::flash('success', 'Job #'.$jobID.' updated successfully.');
				} 
				else 
				{
					Session::flash('error', 'There was a problem with the lead update.');
				}
			}
		}
		catch (Exception $e) 
		{
			Session::flash('error', 'There was a problem: '.$e);
			return $e;
		}

		return Redirect::to('/job/' . Input::get('jobID'));
	}

	public function custMap() 
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/logout');
		}
		else
		{
			$results = array();
			return View::make('customers.map')->with($results);
		}
	}

	public function custMapV2() 
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/logout');
		}
		else
		{
			$results = array();
			return View::make('customers.mapv2')->with($results);
		}
	}

	public static function search($term)
	{
		//BaseController::firePhp()->addInfo('Search term: '.$search);
		
		$results = DB::table('search')
			->where('f_name', 'LIKE', '%'.$term.'%')
			->orWhere('l_name', 'LIKE', '%'.$term.'%')
			->orWhere('billing_address', 'LIKE', '%'.$term.'%')
			->orWhere('address', 'LIKE', '%'.$term.'%')
			->take(10)
			->orderBy('job_created', 'desc')
			->get();
		
		//BaseController::firePhp()->addInfo('Search Results ', $results);
		
		return View::make('customers.search')->with('results', $results);
	}

	public static function searchSolr($term = '')
	{
		$start = Input::get('start');
		if(!$start) $start = 0;

		$results = file_get_contents("http://localhost:8983/solr/windowrnr/select?wt=json&start=$start&rows=20&fl=job_id,cust_id,f_name,l_name,company_name,address,city,billing_address,billing_city,job_created&q=" . urlencode('*' . $term . '*'));

		$results = json_decode($results);

		$next = -1;
		$prev = -1;
		if($start + count($results->response->docs) < $results->response->numFound) {
			$next = $start + count($results->response->docs);
		}
		if($start > 0) {
			$prev = $start - 20;
		}
		
		return View::make('customers.search_solr', ['term'=> $term, 'results' => $results->response, 'prev' => $prev, 'next' => $next, 'start' => $start + 1, 'end' => $start + count($results->response->docs)]);
	}

	public static function calendar()
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/logout');
		}
		else
		{
			$ts = strtotime("now");
			$init = (date('w', $ts) == 1) ? strtotime('midnight') : strtotime('last Monday', $ts);
			$monday = date('c', $init);
			$mondayStart = $init;
			$mondayEnd = $mondayStart+86399;
			$tuesdayStart = $mondayEnd+1;
			$tuesdayEnd = $tuesdayStart+86399;
			$wednesdayStart = $tuesdayEnd+1;
			$wednesdayEnd = $wednesdayStart+86399;
			$thursdayStart = $wednesdayEnd+1;
			$thursdayEnd = $thursdayStart+86399;
			$fridayStart = $thursdayEnd+1;
			$fridayEnd = $fridayStart+86399;

			$salesChecks = ProfilesController::getSalesCheckBox();
			
			$results['profiles'] = UserController::getSalesProfiles();

			foreach($results['profiles'] as $profile) {
				$results['calendar'][$profile['google_calendar_id']] = $profile['first_name'];
			}

			// get customers with jobs with status = 1
			$customers = DB::table('customers')
							->join('jobs', function($join) {
					            $join->on('customers.id', '=', 'jobs.customer_id')->where('jobs.status', '=', 1);
					        })
							->select('customers.id', 'customers.l_name', 'customers.f_name', 'customers.company_name', 'jobs.address', 'jobs.city', 'jobs.id as job_id')
							->orderBy('jobs.created_at', 'DESC')
							->get();
			$status1Customers = [];
			foreach($customers as $customer) {
				$status1Customers[$customer->id] = 'Job# ' . $customer->job_id . ' - ' . implode(', ', [$customer->l_name, $customer->f_name]) . '  -  ' . implode(', ', [$customer->address, $customer->city]);
			}
			$results['customers'] = $status1Customers;
			$results['init_customers'] = $customers;

			//BaseController::firePhp()->addInfo('$results', $results);

			return View::make('customers.calendar')->with($results);
		}
	}

	public static function assign()
	{
		if ( ! Sentry::check())
		{
			// User is not logged in, or is not activated
		    Session::flash('error', 'There was a problem accessing your account.');
			return Redirect::to('/users/logout');
		}
		else
		{
			$ts = strtotime("now");
			$init = (date('w', $ts) == 1) ? strtotime('midnight') : strtotime('last Monday', $ts);
			$monday = date('c', $init);
			$mondayStart = $init;
			$mondayEnd = $mondayStart+86399;
			$tuesdayStart = $mondayEnd+1;
			$tuesdayEnd = $tuesdayStart+86399;
			$wednesdayStart = $tuesdayEnd+1;
			$wednesdayEnd = $wednesdayStart+86399;
			$thursdayStart = $wednesdayEnd+1;
			$thursdayEnd = $thursdayStart+86399;
			$fridayStart = $thursdayEnd+1;
			$fridayEnd = $fridayStart+86399;

			$salesChecks = ProfilesController::getSalesCheckBox();
			
			// list only users in the tech group
			$results['profiles'] = UserController::getTechProfiles();

			foreach($results['profiles'] as $profile) {
				$results['calendar'][$profile['google_calendar_id']] = $profile['first_name'];
			}

			// get customers with jobs with status = 1
			$customers = DB::table('customers')
							->join('jobs', function($join) {
					            $join->on('customers.id', '=', 'jobs.customer_id')->where('jobs.status', '=', 1);
					        })
							->select('customers.id', 'customers.l_name', 'customers.f_name', 'jobs.address', 'jobs.city', 'jobs.id as job_id')
							->get();
			$status1Customers = [];
			foreach($customers as $customer) {
				$status1Customers[$customer->id] = 'Job# ' . $customer->job_id . ' - ' . implode(', ', [$customer->l_name, $customer->f_name]) . '  -  ' . implode(', ', [$customer->address, $customer->city]);
			}
			$results['customers'] = $status1Customers;

			//BaseController::firePhp()->addInfo('$results', $results);

			return View::make('customers.assign')->with($results);
		}
	}

	// make google description using data in $_POST
	public static function makeGoogleEventDescription() {
		
		$desc = [];

		// event type
		if(isset($_POST['event_type'])) {
			$eventTypes = [EVENT_EST_APPT => 'Estimate Appt.', EVENT_INSTALL_REPAIR => 'Install/Repair Job', EVENT_SERVICE_CALL => 'Service Call.'];
			$desc[] = 'Event Type: ' . $eventTypes[$_POST['event_type']];
		}

		// TODO: assigned to
		$desc[] = 'Assigned To: -';

		// sep
		$desc[] = '-----------------';

		// built, phone, email
		if(isset($_POST['job_id'])) {
			$job = DB::table('jobs')->where('id', $_POST['job_id'])->first();
			$customer = DB::table('customers')->where('id', $job->customer_id)->first();
			$desc[] = 'Built: ' . $job->built;
			$desc[] = 'Phone: ' . $customer->phone;
			$desc[] = 'Alt Phone: ' . $customer->alt_phone;
			$desc[] = 'Email: ' . $customer->email;
		}
		else {
			if(isset($_POST['built'])) $desc[] = 'Built: ' . $_POST['built'];
			if(isset($_POST['phone'])) $desc[] = 'Phone: ' . $_POST['phone'];
			if(isset($_POST['alt_phone'])) $desc[] = 'Alt Phone: ' . $_POST['alt_phone'];
			if(isset($_POST['email'])) $desc[] = 'Email: ' . $_POST['email'];
		}

		// lead source
		$leadSource = empty(Input::get('lead_source')) ? '' : implode(Input::get('lead_source'),",");
		if(!empty($leadSource)) $desc[] = 'Lead Source: ' . $leadSource;

		// job id
		if(isset($_POST['job_id'])) $desc[] = 'Job ID: ' . $_POST['job_id'];

		// sep
		$desc[] = '-----------------';

		// notes
		if(Input::get('job_id')) {
			$notes = Note::notesByJobID(Input::get('job_id'));
			if(count($notes)) {
				$desc[] = 'Notes:';
				foreach($notes as $note) {
					$desc[] = $note->f_name.": ".date("g:i a, m/d/Y", strtotime($note->created_at))." ".$note->note;
				}
			}
		}
		
		return implode("\n", $desc);
	}
	
	public static function calNewLead($_api = false)
	{
		try {

			if(Input::get('event_type') != EVENT_PERSONAL_TIME) {
				if(Input::get('customer_type') === 'new') {
					$jobID = CustomersController::store();
					$_POST['job_id'] = $jobID;
					$job = DB::table('jobs')->where('id', $jobID)->first();
					$customer = DB::table('customers')->where('id', $job->customer_id)->first();
					$_POST['cust_id'] = $customer->id;
					$_POST['summary'] = '('.$_POST['city'].') '.$_POST['l_name'].', '.$_POST['f_name'];
					$_POST['location'] = $_POST['address'].', '.$_POST['city'].', CA, '.$_POST['zip'];
					$results['notes'] = Note::notesByJobID($jobID);
					$leadSource = empty(Input::get('lead_source')) ? '' : implode(Input::get('lead_source'),",");
					$_POST['description'] = "Built: ".$_POST['built']."\n"."Phone: ".$_POST['phone']."\n"."Lead Source: ".$leadSource;
				}
				else {
					$jobID = intval(Input::get('job_id'));
					$job = DB::table('jobs')->where('id', $jobID)->first();
					$customer = DB::table('customers')->where('id', $job->customer_id)->first();
					$_POST['job_id'] = $jobID;
					$_POST['cust_id'] = $customer->id;
					$_POST['summary'] = '('.$job->city.') '.$customer->l_name.', '.$customer->f_name;
					$_POST['location'] = $job->address.', '.$job->city.', '.$job->state.', '.$job->zip;
					$_POST['description'] = "Built: ".$job->built."\n"."Phone: ".$customer->phone."\n"."Lead Source: ".$job->lead_source;
					$user = Sentry::getUser();
					Note::createNote($jobID, $user->id, Input::get('note'));
				}
			}

			$results2 = CustomersController::postGoogleInsert($_api);

			if($_POST['event_type'] == EVENT_PERSONAL_TIME) {
				return $results2;
			}

			if (is_numeric($jobID)) 
			{
				Session::flash('success', 'Lead #'.$jobID.' added.');

				if($_api) return $results2;

				if($_POST['event_type'] == EVENT_INSTALL_REPAIR) {
					$jobDate = substr($_POST['start'], 0, 33);
					$jobDate = strtotime($jobDate);
					$jobDate = date('Y-m-d', $jobDate);
					return Redirect::to('assign?date='.$jobDate);
				}
				else
					return $results2; // postGoogleInsert() returns a redirect to the PDF frame, which should be passed along
			} 
			else
			{

				if($_api) return false;

				Session::flash('error', 'Error occurred: '.$results);
				return Redirect::to('customers');
			}
		}
		catch (Exception $e) 
		{

			if($_api) return false;

			Session::flash('error', 'There was a problem: '.$e);
			return Redirect::to('customers');
		}

	}
	
	public static function getTestLogin()
	{
	return View::make('customers.test');
	}

	public function leadCheck() {
		$newEnquiries = DB::table('new_enquiries')->where('checked', '=', 0)->sum('count');
		DB::table('new_enquiries')->where('checked', '=', 0)->update(['checked' => 1]);
		if($newEnquiries > 0) {
			return 	'<div id="alert" class="collapse in" style="display: none">' .
						'<div class="alert alert-success">' .
							'<button type="button" class="close" data-toggle="collapse" data-target="#alert">×</button>' .
							'<h4>Success</h4>' . 
							$newEnquiries . ' new Contact Form leads were added' . 
						'</div>' .
					'</div>';
		}
		return '';
	}

	public function redirectToPartsList($job_id) {
		$finalisedEstimatePartsList = DB::table('parts_lists')->where('job_id', '=', $job_id)->where('status', '=', 1)->first();
		if($finalisedEstimatePartsList) {
			return Redirect::to('/parts-list/'.$finalisedEstimatePartsList->id);
		}
		else {
			//TODO: Show proper error message and redirect to calendar
			echo "Parts list not yet finalized for this job.<br><a href='/calendar'>Return to calendar</a>";
		}
	}
}
