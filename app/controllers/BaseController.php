<?php

class BaseController extends Controller {	

	public function __construct()
	{

	}

	public static function firePhp()
	{	
		$firephp = Log::getMonolog();
		$firephp->pushHandler(new \Monolog\Handler\FirePHPHandler());			
		return $firephp;
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}