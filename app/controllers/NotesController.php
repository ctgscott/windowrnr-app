<?php

class NotesController extends BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('auth');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public static function create($job_id, $user_id, $note)
	{
		try 
		{
			$now = date("Y-m-d H:i:s");
			DB::table('notes')
				->insert(array('job_id' => $job_id, 'user_id' => $user_id, 'note' => $note, 'created_at' => $now));
			return 'Success';
		}
		catch (Exception $e) 
		{
			return $e;
		}		
	}

	public static function notesByJobID($job_id)
	{
		try 
		{
			$results = DB::table('notes_all')
				->select('user_id', 'note', 'created_at', 'f_name', 'l_name')
				->where('job_id', '=', $job_id)
				->get();
			return $results;
		}
		catch (Exception $e) 
		{
			return $e;
		}
	}

	public static function notesByWebPost()
	{
		try
		{
			$results = DB::table('job_note')
				->select('job_id', 'customer_id', 'note', 'created_at')
				->where('user_id', '=', '999')
				->where('archived', '=', '0')
				->where('status', '=', '1')
				->orderBy('created_at', 'desc') // latest note should appear first
				->get();
			return $results;
		}
		catch (Exception $e) 
		{
			return $e;
		}
	}
}