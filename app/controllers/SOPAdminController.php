<?php

class SOPAdminController extends BaseController {

	// public function __construct() {
	// 	parent::__construct();
	// 	$this->beforeFilter('auth');
	// }
	
	// service settings page
	public function index() {
		return View::make('sop-admin.index');
	}

	// getters
	public function getStyles() { return $this->getRecords('win_style'); }
	public function getServices() { return $this->getRecords('win_service'); }
	public function getOptions() { return $this->getRecords('options'); }
	public function getParts() { return $this->getRecords('parts'); }
	public function getServiceParts() { return $this->getRecords('service-parts'); }
	public function getOptionParts() { return $this->getRecords('options-parts'); }

	// adders
	public function addStyle() {
		DB::table('win_style')->insert([
			'winStyle' => Input::get('winStyle'), 
			'has_balancer' => Input::get('hasBalancer')
		]);
		return DB::table('win_style')->get();
	}
	public function addService() {
		DB::table('win_service')->insert([
			'win_style' => Input::get('win_style'), 
			'title' => Input::get('title'),
			'description' => Input::get('description'),
			'price' => Input::get('price')
		]);
		return json_encode(DB::table('win_service')->where('win_style', '=', Input::get('win_style'))->get(), JSON_NUMERIC_CHECK);
	}
	public function addOption() {
		DB::table('options')->insert([
			'serv_id' => Input::get('service'), 
			'title' => Input::get('title'),
			'description' => Input::get('description'),
			'price' => Input::get('price')
		]);
		return json_encode(DB::table('options')->where('serv_id', '=', Input::get('service'))->get(), JSON_NUMERIC_CHECK);
	}
	public function addPartToOption() {

		// insert part record
		$part_id = DB::table('parts')->insertGetId([
			'name' => Input::get('name'),
			'description' => Input::get('description'),
			'price' => Input::get('price')
		]);
		// map the newly created part to the specified option via options_parts
		DB::table('options_parts')->where('options_id', '=', Input::get('option'))->where('parts_id', '=', $part_id)->delete();
		DB::table('options_parts')->insert(['options_id' => Input::get('option'), 'parts_id' => $part_id]);

		return $this->getParts();
	}
	public function addPartToService() {
		
		// insert part record
		$part_id = DB::table('parts')->insertGetId([
			'name' => Input::get('name'),
			'description' => Input::get('description'),
			'price' => Input::get('price')
		]);

		// map the newly created part to the specified service via win_service_parts
		DB::table('win_service_parts')->where('win_service_id', '=', Input::get('service'))->where('parts_id', '=', $part_id)->delete();
		DB::table('win_service_parts')->insert(['win_service_id' => Input::get('service'), 'parts_id' => $part_id]);
		return $this->getServiceParts();
	}

	// updaters
	public function updateStyle() {
		DB::table('win_style')->where('id', '=', Input::get('id'))->update([
			'winStyle' => Input::get('winStyle'), 
			'has_balancer' => Input::get('hasBalancer')
		]);
		return DB::table('win_style')->get();
	}
	public function updateService() {
		DB::table('win_service')->where('id', '=', Input::get('id'))->update([
			'title' => Input::get('title'),
			'description' => Input::get('description'),
			'price' => Input::get('price')
		]);
		return json_encode(DB::table('win_service')->where('win_style', '=', Input::get('style'))->get(), JSON_NUMERIC_CHECK);
	}
	public function updateOption() {
		$return = DB::table('options')->where('id', '=', Input::get('id'))->update([
			'title' => Input::get('title'),
			'description' => Input::get('description'),
			'price' => Input::get('price')
		]);
		return json_encode(DB::table('options')->where('serv_id', '=', Input::get('service'))->get(), JSON_NUMERIC_CHECK);
	}
	public function updateOptionPart() {
		$return=DB::table('parts')->where('id', '=', Input::get('id'))->update([
			'name' => Input::get('name'),
			'description' => Input::get('description'),
			'price' => Input::get('price')
		]);
		return $this->getOptionParts();
	}

	public function updateServicePart() {
		$return=DB::table('parts')->where('id', '=', Input::get('id'))->update([
			'name' => Input::get('name'),
			'description' => Input::get('description'),
			'price' => Input::get('price')
		]);
		return $this->getServiceParts();
	}

	//deleters

	public function deleteStyle() {
		DB::table('win_style')
			->where('id', '=', Input::get('id'))
			->delete();
		DB::table('win_service')
			->where('win_style', '=', Input::get('id'))
			->delete();
		return $this->getStyles(); 
	}
		
	public function deleteService() {
		DB::table('win_service')
			->where('id', '=', Input::get('id'))
			->delete();
		DB::table('options')
			->where('serv_id', '=', Input::get('id'))
			->delete();
		DB::table('win_service_parts')
			->where('win_service_id', '=', Input::get('id'))
			->delete();
		DB::table('win_service_balancers')
			->where('win_service_id', '=', Input::get('id'))
			->delete();
		return $this->getServices();
	}
	public function deleteOption() {
		DB::table('options')
		->where('id', '=', Input::get('id'))
		->delete();
		return $this->getOptions();
	}
	public function deletePart() {
		DB::table('parts')
			->where('id', '=', Input::get('id'))
			->delete();
		DB::table('options_parts')
			->where('parts_id', '=', Input::get('id'))
			->delete();
		DB::table('win_service_parts')
			->where('parts_id', '=', Input::get('id'))
			->delete();
		if(Input::get('option'))
			return $this->getOptionParts();
		else if(Input::get('service'))
			return $this->getServiceParts();

	}

	//Count of services mapped to parts
	public function getServicesToStylesCount() {
		 return  DB::table('win_service')->where('win_style', '=', Input::get('style'))->count();
	}
	public function getOptionsToServiceCount() {
		 return  DB::table('options')->where('serv_id', '=', Input::get('service'))->count();
	}	

	// actual data getter
	private function getRecords($_table) {
		$result = [];
		switch($_table) {
			case 'win_style':
				$result = DB::table($_table)->get();
				break;
			case 'win_service':
				$result = DB::table($_table)->where('win_style', '=', Input::get('style'))->get();
				break;
			case 'options':
				$result = DB::table($_table)->where('serv_id', '=', Input::get('service'))->get();
				break;
			case 'parts':
				$partIDs = DB::table('options_parts')->select('parts_id')->where('options_id', '=', Input::get('option'))->get();
				if($partIDs && count($partIDs)) {
					$partIDs = array_map(function($_item) {
						return $_item->parts_id;
					}, $partIDs);
					$result = DB::table('parts')->whereIn('id', $partIDs)->get();
				}
				break;
			case 'service-parts':
				$partIDs = DB::table('win_service_parts')->select('parts_id')->where('win_service_id', '=', Input::get('service'))->get();
				if($partIDs && count($partIDs)) {
					$partIDs = array_map(function($_item) {
						return $_item->parts_id;
					}, $partIDs);
					$result = DB::table('parts')->whereIn('id', $partIDs)->get();
				}
				break;
			case 'options-parts':
				$partIDs = DB::table('options_parts')->select('parts_id')->where('options_id', '=', Input::get('option'))->get();
				if($partIDs && count($partIDs)) {
					$partIDs = array_map(function($_item) {
						return $_item->parts_id;
					}, $partIDs);
					$result = DB::table('parts')->whereIn('id', $partIDs)->get();
				}
				break;
		}
		return json_encode($result, JSON_NUMERIC_CHECK);
	}


	public function postMapServicesToStyle() {
		$styleID = Input::get('targetStyleID');
		$serviceIDs = json_decode(Input::get('serviceIDs'));

		// get the services
		$services = DB::table('win_service')->whereIN('id', $serviceIDs)->get();
		foreach ($services as $service) {
			DB::table('win_service')->insert([
				'win_style' => $styleID,
				'title' => $service->title,
				'description' => $service->description,
				'price' => $service->price
			]);
		}

	}





















	// generic handlers for all item types
	public function view($type) {
		$result = "View for type: $type";
		switch($type) {
			case 'rooms': 
				$result = $this->viewRooms();
				break;
			case 'window-styles':
				$result = $this->viewWindowStyles();
				break;
			case 'window-services': 
				$result = $this->viewWindowServices();
				break;
			case 'service-options': 
				$result = $this->viewServiceOptions();
				break;
			case 'balancer-types':
				$result = $this->viewBalancerTypes();
				break;
			case 'parts': 
				$result = $this->viewParts();
				break;
		}
		return $result;
	}
	public function add() {
		$type = Input::get('type');
		$value = Input::get('value');
		$result = false;
		switch($type) {
			case 'rooms': 
				$result = $this->addRoom($value);
				break;
			case 'window-styles': 
				$result = $this->addWindowStyle($value);
				break;
			case 'window-services': 
				$style_id = Input::get('style_id');
				$result = $this->addWindowService($value, $style_id);
				break;
			case 'service-options': 
				$service_id = Input::get('service_id');
				$result = $this->addServiceOption($value, $service_id);
				break;
			case 'balancer-types': 
				$result = $this->addBalancerType($value);
				break;
			case 'parts':
				$result = $this->addPart($value);
				break;
			case 'part-to-service': // part to window-service mapping
				$part_id = Input::get('part_id');
				$service_id = Input::get('service_id');
				$result = $this->addPartToService($part_id, $service_id);
				break;
			case 'part-to-option': // part to service-option mapping
				$part_id = Input::get('part_id');
				$option_id = Input::get('option_id');
				$result = $this->addPartToOption($part_id, $option_id);
				break;

		}
		return ($result ? (Input::get('returnid') ? $result : 'OK') : 'ERROR');
	}
	public function update() {
		$type = Input::get('type');
		$id = Input::get('id');
		$value = Input::get('value');
		$field = Input::get('field');
		switch($type) {
			case 'rooms': 
				$result = $this->updateRoom($id, $value);
				break;
			case 'window-styles': 
				$result = $this->updateWindowStyle($id, $value);
				break;
			case 'window-services': 
				$result = $this->updateWindowService($id, $value, $field);
				break;
			case 'service-options': 
				$result = $this->updateServiceOption($id, $value, $field);
				break;
			case 'balancer-types': 
				$result = $this->updateBalancerType($id, $value);
				break;
			case 'parts':
				$result = $this->updatePart($id, $value, $field);
				break;
		}
		return ($result ? 'OK' : 'ERROR');
	}
	public function delete() {
		$type = Input::get('type');
		$id = Input::get('id');
		$result = false;
		switch($type) {
			case 'rooms': 
				$result =  $this->deleteRoom($id);
				break;
			case 'window-styles': 
				$result =  $this->deleteWindowStyle($id);
				break;
			case 'window-services': 
				$result =  $this->deleteWindowService($id);
				break;
			case 'service-options': 
				$result =  $this->deleteServiceOption($id);
				break;
			case 'balancer-types': 
				$result =  $this->deleteBalancerType($id);
				break;
			case 'parts': 
				$result =  $this->deletePart($id);
				break;
			case 'part-to-service': // part to window-service mapping
				$part_id = Input::get('part_id');
				$service_id = Input::get('service_id');
				$result = $this->deletePartToService($part_id, $service_id);
				break;
			case 'part-to-option': // part to service-option mapping
				$part_id = Input::get('part_id');
				$option_id = Input::get('option_id');
				$result = $this->deletePartToOption($part_id, $option_id);
				break;
		}
		return ($result ? 'OK' : 'ERROR');
	}

	// Rooms
	private function addRoom($value) {
		DB::table('rooms')
			->insert(['room' => $value]);
		return true;
	}
	private function viewRooms() {
		return View::make('service-settings.rooms', ['rooms' => DB::table('rooms')->get()]);
	}
	private function updateRoom($id, $value) {
		DB::table('rooms')
			->where('id', '=', $id)
			->update(['room' => $value]);
		return true;
	}
	private function deleteRoom($id) {
		DB::table('rooms')
			->where('id', '=', $id)
			->delete();
		return true;
	}

	// Window Styles
	private function addWindowStyle($value) {
		DB::table('win_style')
			->insert(['winStyle' => $value]);
		return true;
	}
	private function viewWindowStyles() {
		return View::make('service-settings.window-styles', ['windowStyles' => DB::table('win_style')->get()]);
	}
	private function updateWindowStyle($id, $value) {
		DB::table('win_style')
			->where('id', '=', $id)
			->update(['winStyle' => $value]);
		return true;
	}
	private function deleteWindowStyle($id) {
		DB::table('win_style')
			->where('id', '=', $id)
			->delete();
		return true;
	}

	// Window Services
	private function addWindowService($value, $style_id) {
		DB::table('win_service')
			->insert(['title' => $value, 'win_style' => $style_id]);
		return true;
	}
	private function viewWindowServices() {
		return View::make('service-settings.window-services', ['windowStyles' => DB::table('win_style')->get(), 'windowServices' => DB::table('win_service')->get()]);
	}
	private function updateWindowService($id, $value, $field) {
		DB::table('win_service')
			->where('id', '=', $id)
			->update([$field => $value]);
		return true;
	}
	private function deleteWindowService($id) {
		DB::table('win_service')
			->where('id', '=', $id)
			->delete();
		return true;
	}

	// Service Options
	private function addServiceOption($value, $service_id) {
		return DB::table('options')
			->insertGetId(['title' => $value, 'serv_id' => $service_id]);
	}
	private function viewServiceOptions() {
		return View::make('service-settings.service-options', ['windowStyles' => DB::table('win_style')->get(), 'windowServices' => DB::table('win_service')->get(), 'serviceOptions' => DB::table('options')->get(), 'serviceOptionsUnique' => DB::table('options')->select( DB::raw('DISTINCT(title)') )->get()]);
	}
	private function updateServiceOption($id, $value, $field) {
		DB::table('options')
			->where('id', '=', $id)
			->update([$field => $value]);
		return true;
	}
	private function deleteServiceOption($id) {
		DB::table('options')
			->where('id', '=', $id)
			->delete();
		return true;
	}

	// Balancer Types
	private function addBalancerType($value) {
		DB::table('balancers')
			->insert(['name' => $value]);
		return true;
	}
	private function viewBalancerTypes() {
		return View::make('service-settings.balancer-types', ['balancerTypes' => DB::table('balancers')->get()]);
	}
	private function updateBalancerType($id, $value) {
		DB::table('balancers')
			->where('id', '=', $id)
			->update(['name' => $value]);
		return true;
	}
	private function deleteBalancerType($id) {
		DB::table('balancers')
			->where('id', '=', $id)
			->delete();
		return true;
	}

	// parts
	/*
	private function addPart($value) {
		DB::table('parts')->insert(['name' => $value]);
		return true;
	}
	private function viewParts() {
		$serviceOptions = DB::table('options')->get();
		foreach ($serviceOptions as $option) {
			$option->service = DB::table('win_service')->where('id', '=', $option->serv_id)->first()->title;
		}
		return View::make('service-settings.parts', [
			'windowServices' => DB::table('win_service')->get(), 
			'serviceOptions' => $serviceOptions, 
			'parts' => DB::table('parts')->get(),
			'win_service_parts' => DB::table('win_service_parts')->get(),
			'options_parts' => DB::table('options_parts')->get()
		]);
	}
	private function updatePart($id, $value, $field) {
		DB::table('parts')->where('id', '=', $id)->update([$field => $value]);
		return true;
	}
	private function deletePart($id) {
		DB::table('parts')->where('id', '=', $id)->delete();
		return true;
	}
	private function addPartToService($part_id, $service_id) {
		DB::table('win_service_parts')->where('win_service_id', '=', $service_id)->where('parts_id', '=', $part_id)->delete();
		DB::table('win_service_parts')->insert(['win_service_id' => $service_id, 'parts_id' => $part_id]);
		return true;
	}
	private function addPartToOption($part_id, $option_id) {
		DB::table('options_parts')->where('options_id', '=', $option_id)->where('parts_id', '=', $part_id)->delete();
		DB::table('options_parts')->insert(['options_id' => $option_id, 'parts_id' => $part_id]);
		return true;
	}
	private function deletePartToService($part_id, $service_id) {
		DB::table('win_service_parts')->where('win_service_id', '=', $service_id)->where('parts_id', '=', $part_id)->delete();
		return true;
	}
	private function deletePartToOption($part_id, $option_id) {
		DB::table('options_parts')->where('options_id', '=', $option_id)->where('parts_id', '=', $part_id)->delete();
		return true;
	}
	*/
}