<?php

class TwilioController extends BaseController {

	public static function incoming() {
		$newIncoming = new TwilioIncoming();

        $rawFrom = Input::get('From');
        $cleanFrom = preg_replace('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '$1$2$3', $rawFrom);

        $rawTo = Input::get('To');
        $cleanTo = preg_replace('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '$1$2$3', $rawTo);
		
		$newIncoming->api_version = Input::get('ApiVersion');
		$newIncoming->sms_sid = Input::get('SmsSid');
		$newIncoming->sms_status = Input::get('SmsStatus');
		$newIncoming->sms_message_sid = Input::get('SmsMessageSid');
		$newIncoming->num_segments = Input::get('NumSegments');
        $newIncoming->from = $cleanFrom;
		$newIncoming->to_state = Input::get('ToState');
		$newIncoming->message_sid = Input::get('MessageSid');
		$newIncoming->account_sid = Input::get('AccountSid');
		$newIncoming->to_zip = Input::get('ToZip');
		$newIncoming->from_country = Input::get('FromCountry');
		$newIncoming->to_city = Input::get('ToCity');
		$newIncoming->error_url = Input::get('ErrorUrl');
		$newIncoming->from_city = Input::get('FromCity');
		$newIncoming->to = $cleanTo;
		$newIncoming->error_code = Input::get('ErrorCode');
		$newIncoming->from_zip = Input::get('FromZip');
		$newIncoming->body = Input::get('Body');
		$newIncoming->to_country = Input::get('ToCountry');
		$newIncoming->from_state = Input::get('FromState');
		$newIncoming->num_media = Input::get('NumMedia');
		
		$numMedia = Input::get('NumMedia');
		if(!empty($numMedia) && $numMedia == 1) {
//			$fileName = $cleanFrom;
			$url = Input::get('MediaUrl');
			$file = file_get_contents($url);
			$mimeType = mime_content_type($url);
			$meta = [
				'title' => Input::get('Body'),
				'description' => 'Message from: '.$cleanFrom,
				'mimeType' => $mimeType
			];
			$newIncoming->media_content_type = Input::get('MediaContentType');
			$newIncoming->media_url = Input::get('MediaUrl');
//			$test = Drive::saveFile($fileName, $url, $meta);
//			$test = Drive::saveFile('test', $file, $meta);
		}
		
		$newIncoming->save();
		return "<Response></Response>";
	}

	public static function incomingFallback() {

	}

	public static function process() {
		$tasks = TwilioIncoming::where('status', '=', 0)->orderBy('created_at', 'asc')->limit(5)->get();
		return $tasks;
	}

	// warning: long process
	// to be called only via cron
	public static function flattenCustomerNumbers() {

		// get all customers with valid 'phone' value, but empty 'phone_flat'
		$customers = DB::table('customers')->whereNotNull('phone')->where('phone', '<>', '')->whereNull('phone_flat')->get();

		$output = [];
		$updated = 0;
		foreach ($customers as $customer) {
			$flat = preg_replace("/[\D]/", '', $customer->phone);
			array_push($output, '[' . $customer->phone . '] ==> [' . $flat . ']');
			DB::table('customers')->where('id', $customer->id)->update(['phone_flat' => $flat]);
			$updated++;
		}

		echo "Updated (phone): " . $updated;


		// get all customers with valid 'phone' value, but empty 'phone_flat'
		$customers = DB::table('customers')->whereNotNull('alt_phone')->where('alt_phone', '<>', '')->whereNull('alt_phone_flat')->get();

		$output = [];
		$updated = 0;
		foreach ($customers as $customer) {
			$flat = preg_replace("/[\D]/", '', $customer->alt_phone);
			array_push($output, '[' . $customer->alt_phone . '] ==> [' . $flat . ']');
			DB::table('customers')->where('id', $customer->id)->update(['alt_phone_flat' => $flat]);
			$updated++;
		}

		echo "<br>Updated (alt_phone): " . $updated;

		return "";
	}

    public static function main() {
        $contacts = TwilioIncoming::getContacts();
        return View::make('messages.main')->with(['contacts' => $contacts]);
}

    //send Twilio SMS
    public function textOut() {
        $sid = "AC41ea4d78cdd4b4a0b035b4dd1842979c"; // Your Account SID from www.twilio.com/console
        $token = "f6abb54566571ef82de662877d7bd01b"; // Your Auth Token from www.twilio.com/console

        $textBody = Input::get('message');
        $textTo = Input::get('to');
        $textFrom = Input::get('from');
        
        // Create the message and send
        $client = new \Twilio\Rest\Client($sid, $token);
        $message = $client->messages->create(
          $textTo, // Text this number
          array(
            'from' => $textFrom, // From valid Twilio phone #
            'body' => $textBody,
			'StatusCallback' => 'https://app.windowrnr.com/msgs/statusCallback'
          )
        );

		$aSidStart = stripos($message, "accountSid=");
		$sidStart = stripos($message, "sid=");
		$lastChar = strripos($message, "]");
		$apiVer = 
		$accountSid = 
		
		
        $newMsg = new TwilioIncoming();
        $newMsg->to = $textTo;
        $newMsg->from = $textFrom;
        $newMsg->body = $textBody;
		$newMsg->message_sid = $message->sid;
		$newMsg->account_sid = $message->accountSid;
		$newMsg->api_version = $message->apiVersion;
		$newMsg->status = $message->status;
        $newMsg->save();
 
		return $message;
    }
    
    public static function getThread($phone) {
        $thread = TwilioIncoming::getMsgs($phone);
        return $thread;
    }
	
	public static function statusCallback() {
		$msgUpdate['sms_sid'] = Input::get('SmsSid');
		$msgUpdate['sms_status'] = Input::get('SmsStatus');
		$msgUpdate['status'] = Input::get('MessageStatus');
		$msgUpdate['message_sid'] = Input::get('MessageSid');
		$msgUpdate['account_sid'] = Input::get('AccountSid');
		$msgUpdate['api_version'] = Input::get('ApiVersion');
		
		if(!empty(Input::get('ErrorCode') && !empty(Input::get('ErrorMessage'))))
		{
			$msgUpdate['data'] = 'Error code: '.Input::get('ErrorCode').', '.'Error message: '.Input::get('ErrorMessage');
		} elseif (!empty(Input::get('ErrorCode'))) {
			$msgUpdate['data'] = 'Error code: '.Input::get('ErrorCode');
		} elseif (!empty(Input::get('ErrorMessage'))) {
			$msgUpdate['data'] = 'Error message: '.Input::get('ErrorMessage');
		}

		TwilioIncoming::updateStatus($msgUpdate);
	}
}

