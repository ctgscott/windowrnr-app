<?php



class ImageController extends BaseController {

	// upload (with specified owner type and id) and return public path to image
	public static function upload($owner_type, $owner_id) {
		$file = Input::file('file');
		$destinationPath = public_path().'/uploads';
		$title = $file->getClientOriginalName();
		$extension = strtolower($file->getClientOriginalExtension());
		$filename = str_random(16) . '.' . $file->getClientOriginalExtension();

		// is image?
		$isImage = in_array($extension, ['jpg', 'jpeg', 'bmp', 'gif', 'png', 'tiff']);
		
		// upload to local directory
		$mime = $file->getMimeType();
		$upload_success = $file->move($destinationPath, $filename);

		$upload_to_google_success = false;

		// NEW: upload to google drive
		if( $upload_success ) {

			$fileURL = '/uploads/' . $filename;
			$downloadURL = null;

			$upload_to_google_success = false;

			$job_id = 0;
			if($owner_type === 'estimate') {
				$lineItem = DB::table('estimate_items')			
							->where('id', '=', $owner_id)
							->first();
				$estimate = DB::table('estimates')			
							->where('id', '=', $lineItem->est_id)
							->first();
				$job_id = $estimate->job_id;
			}
			else if($owner_type === 'job') {

				$job_id = $owner_id;
			
			}

			if($job_id > 0) {
				$data = file_get_contents($destinationPath . '/' . $filename);
				$meta = [
					//'title' => 'Attachment for ' . $owner_type . ' ID #' . $owner_id,
					'title' => $title,
					'description' => 'Attachment for ' . $owner_type . ' ID #' . $owner_id,
					'mimeType' => $mime
				];
				$result = Drive::saveFile($job_id, $data, $meta);
				
				if($result !== FALSE) {
					$result = json_decode($result);
					if(isset($result->webContentLink)) { // upload success
						$upload_to_google_success = true;
						$fileURL = $result->webContentLink;
						$downloadURL = $result->downloadUrl;
					}
					else {
						return json_encode(['status' => 'error', 'message' => 'No webContentLink in google\'s response']);
					}
				}
				else {
					return json_encode(['status' => 'error', 'message' => 'Drive::saveFile() failed']);
				}
			}
			else {
				return json_encode(['status' => 'error', 'message' => 'Invalid Job ID: ' . $job_id]);
			}

		}
		else {
			return json_encode(['status' => 'error', 'message' => 'Upload to local directory failed']);
		}
		
		if( $upload_to_google_success ) {
			$data = [];
			$data['owner_type'] = $owner_type;
			$data['owner_id'] = $owner_id;
			$data['path'] = $fileURL;
			$data['title'] = $title; //'Attachment for ' . $owner_type . ' ID #' . $owner_id;
			$data['status'] = 1;

			if($isImage === FALSE) {
				$data['type'] = strtoupper(preg_replace("/.+\//", "", $mime));
				$data['download_url'] = $downloadURL;
			}

			try {
				$targetTable = ($isImage === TRUE ? 'images' : 'files');
				DB::table($targetTable)->insert($data);
				return json_encode(['status' => 'ok', 'path' => $fileURL]);
			} catch (Exception $e) {
				return json_encode(['status' => 'error', 'message' => 'Adding record to DB failed']);
			}
		}
		else {
			return json_encode(['status' => 'error', 'message' => 'Unknown error!']);
		}
	}

	public function driveTest() {
		echo Drive::folderExists(Input::get('f'), Input::get('p'));
	}

	// return array of images for specified owner_type and owner_id
	public static function images($owner_type, $owner_id) {
		$images = DB::table('images')
							->where('owner_type', '=', $owner_type)
							->where('owner_id', '=', $owner_id)
							->where('status', '=', '1')
							->get();
		$files = DB::table('files')
							->where('owner_type', '=', $owner_type)
							->where('owner_id', '=', $owner_id)
							->where('status', '=', '1')
							->get();
		return json_encode(array_merge($images, $files));
	}

	// delete an image
	public function delete() {
		if(Input::get('type') === 'image') {
			DB::table('images')->where('id', '=', Input::get('id'))->delete();
		}
		else if(Input::get('type') === 'file') {
			DB::table('files')->where('id', '=', Input::get('id'))->delete();
		}
		return "";
	}

	// save rotation info for image
	public function rotate() {
		$data = ['rotation' => Input::get('angle')];
		DB::table('images')->where('id', Input::get('id'))->update($data);
		return "";
	}

}