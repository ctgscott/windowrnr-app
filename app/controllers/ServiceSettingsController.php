<?php

class ServiceSettingsController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->beforeFilter('auth');
	}
	
	// service settings page
	public function index() {
		return View::make('service-settings.index');
	}

	// generic handlers for all item types
	public function view($type) {
		$result = "View for type: $type";
		switch($type) {
			case 'rooms': 
				$result = $this->viewRooms();
				break;
			case 'window-styles':
				$result = $this->viewWindowStyles();
				break;
			case 'window-services': 
				$result = $this->viewWindowServices();
				break;
			case 'service-options': 
				$result = $this->viewServiceOptions();
				break;
			case 'balancer-types':
				$result = $this->viewBalancerTypes();
				break;
			case 'parts': 
				$result = $this->viewParts();
				break;
		}
		return $result;
	}
	public function add() {
		$type = Input::get('type');
		$value = Input::get('value');
		$result = false;
		switch($type) {
			case 'rooms': 
				$result = $this->addRoom($value);
				break;
			case 'window-styles': 
				$result = $this->addWindowStyle($value);
				break;
			case 'window-services': 
				$style_id = Input::get('style_id');
				$result = $this->addWindowService($value, $style_id);
				break;
			case 'service-options': 
				$service_id = Input::get('service_id');
				$result = $this->addServiceOption($value, $service_id);
				break;
			case 'balancer-types': 
				$service_id = Input::get('service_id');
				$result = $this->addBalancerType($value, $service_id);
				break;
			case 'parts':
				$result = $this->addPart($value);
				break;
			case 'part-to-service': // part to window-service mapping
				$part_id = Input::get('part_id');
				$service_id = Input::get('service_id');
				$result = $this->addPartToService($part_id, $service_id);
				break;
			case 'part-to-option': // part to service-option mapping
				$part_id = Input::get('part_id');
				$option_id = Input::get('option_id');
				$result = $this->addPartToOption($part_id, $option_id);
				break;

		}
		return ($result ? (Input::get('returnid') ? $result : 'OK') : 'ERROR');
	}
	public function update() {
		$type = Input::get('type');
		$id = Input::get('id');
		$value = Input::get('value');
		$field = Input::get('field');
		switch($type) {
			case 'rooms': 
				$result = $this->updateRoom($id, $value);
				break;
			case 'window-styles': 
				$result = $this->updateWindowStyle($id, $value);
				break;
			case 'window-services': 
				$result = $this->updateWindowService($id, $value, $field);
				break;
			case 'service-options': 
				$result = $this->updateServiceOption($id, $value, $field);
				break;
			case 'balancer-types': 
				if($field === 'service_id') {
					$result = $this->updateBalancerTypeService($id, $value);
				}
				else {
					$result = $this->updateBalancerType($id, $value);
				}
				break;
			case 'parts':
				$result = $this->updatePart($id, $value, $field);
				break;
		}
		return ($result ? 'OK' : 'ERROR');
	}
	public function delete() {
		$type = Input::get('type');
		$id = Input::get('id');
		$result = false;
		switch($type) {
			case 'rooms': 
				$result =  $this->deleteRoom($id);
				break;
			case 'window-styles': 
				$result =  $this->deleteWindowStyle($id);
				break;
			case 'window-services': 
				$result =  $this->deleteWindowService($id);
				break;
			case 'service-options': 
				$result =  $this->deleteServiceOption($id);
				break;
			case 'balancer-types': 
				$service_id = Input::get('service_id');
				$result =  $this->deleteBalancerType($id, $service_id);
				break;
			case 'parts': 
				$result =  $this->deletePart($id);
				break;
			case 'part-to-service': // part to window-service mapping
				$part_id = Input::get('part_id');
				$service_id = Input::get('service_id');
				$result = $this->deletePartToService($part_id, $service_id);
				break;
			case 'part-to-option': // part to service-option mapping
				$part_id = Input::get('part_id');
				$option_id = Input::get('option_id');
				$result = $this->deletePartToOption($part_id, $option_id);
				break;
		}
		return ($result ? 'OK' : 'ERROR');
	}

	// Rooms
	private function addRoom($value) {
		DB::table('rooms')
			->insert(['room' => $value]);
		return true;
	}
	private function viewRooms() {
		return View::make('service-settings.rooms', ['rooms' => DB::table('rooms')->get()]);
	}
	private function updateRoom($id, $value) {
		DB::table('rooms')
			->where('id', '=', $id)
			->update(['room' => $value]);
		return true;
	}
	private function deleteRoom($id) {
		DB::table('rooms')
			->where('id', '=', $id)
			->delete();
		return true;
	}

	// Window Styles
	private function addWindowStyle($value) {
		DB::table('win_style')
			->insert(['winStyle' => $value]);
		return true;
	}
	private function viewWindowStyles() {
		return View::make('service-settings.window-styles', ['windowStyles' => DB::table('win_style')->get()]);
	}
	private function updateWindowStyle($id, $value) {
		DB::table('win_style')
			->where('id', '=', $id)
			->update(['winStyle' => $value]);
		return true;
	}
	private function deleteWindowStyle($id) {
		DB::table('win_style')
			->where('id', '=', $id)
			->delete();
		return true;
	}

	// Window Services
	private function addWindowService($value, $style_id) {
		DB::table('win_service')
			->insert(['title' => $value, 'win_style' => $style_id]);
		return true;
	}
	private function viewWindowServices() {
		return View::make('service-settings.window-services', ['windowStyles' => DB::table('win_style')->get(), 'windowServices' => DB::table('win_service')->get()]);
	}
	private function updateWindowService($id, $value, $field) {
		DB::table('win_service')
			->where('id', '=', $id)
			->update([$field => $value]);
		return true;
	}
	private function deleteWindowService($id) {
		DB::table('win_service')
			->where('id', '=', $id)
			->delete();
		return true;
	}

	// Service Options
	private function addServiceOption($value, $service_id) {
		return DB::table('options')
			->insertGetId(['title' => $value, 'serv_id' => $service_id]);
	}
	private function viewServiceOptions() {
		return View::make('service-settings.service-options', ['windowStyles' => DB::table('win_style')->get(), 'windowServices' => DB::table('win_service')->get(), 'serviceOptions' => DB::table('options')->get(), 'serviceOptionsUnique' => DB::table('options')->select( DB::raw('DISTINCT(title)') )->get()]);
	}
	private function updateServiceOption($id, $value, $field) {
		DB::table('options')
			->where('id', '=', $id)
			->update([$field => $value]);
		return true;
	}
	private function deleteServiceOption($id) {
		DB::table('options')
			->where('id', '=', $id)
			->delete();
		return true;
	}

	// Balancer Types
	private function addBalancerType($value, $service_id) {
		$balancerId = DB::table('balancers')
						->insertGetId(['name' => $value]);

		DB::table('win_service_balancers')->insert(['win_service_id' => $service_id, 'balancer_id' => $balancerId]);

		return true;
	}
	private function viewBalancerTypes() {
		$balancerTypes = DB::table('balancers')
						->leftJoin('win_service_balancers', 'balancers.id', '=', 'win_service_balancers.balancer_id')
						->select('balancers.*', 'win_service_balancers.win_service_id')
						->orderBy('balancers.id')
						->get();

		return View::make('service-settings.balancer-types', [
			'balancerTypes' => $balancerTypes,
			'windowServices' => DB::table('win_service')->get()
			]);
	}
	private function updateBalancerType($id, $value) {
		DB::table('balancers')
			->where('id', '=', $id)
			->update(['name' => $value]);
		return true;
	}
	private function updateBalancerTypeService($balancer_id, $service_id) {
		DB::table('win_service_balancers')
			->where('balancer_id', '=', $balancer_id)
			->delete();

		DB::table('win_service_balancers')
			->insert(['balancer_id' => $balancer_id, 'win_service_id' => $service_id]);
			
		return true;
	}
	private function deleteBalancerType($id, $service_id) {
		DB::table('balancers')
			->where('id', '=', $id)
			->delete();

		if($service_id) {
			DB::table('win_service_balancers')
			->where('win_service_id', '=', $service_id)
			->where('balancer_id', '=', $id)
			->delete();			
		}
		
		return true;
	}

	// parts
	private function addPart($value) {
		DB::table('parts')->insert(['name' => $value]);
		return true;
	}
	private function viewParts() {
		$serviceOptions = DB::table('options')->get();
		foreach ($serviceOptions as $option) {
			$option->service = DB::table('win_service')->where('id', '=', $option->serv_id)->first()->title;
		}
		return View::make('service-settings.parts', [
			'windowServices' => DB::table('win_service')->get(), 
			'serviceOptions' => $serviceOptions, 
			'parts' => DB::table('parts')->get(),
			'win_service_parts' => DB::table('win_service_parts')->get(),
			'options_parts' => DB::table('options_parts')->get()
		]);
	}
	private function updatePart($id, $value, $field) {
		DB::table('parts')->where('id', '=', $id)->update([$field => $value]);
		return true;
	}
	private function deletePart($id) {
		DB::table('parts')->where('id', '=', $id)->delete();
		return true;
	}
	private function addPartToService($part_id, $service_id) {
		DB::table('win_service_parts')->where('win_service_id', '=', $service_id)->where('parts_id', '=', $part_id)->delete();
		DB::table('win_service_parts')->insert(['win_service_id' => $service_id, 'parts_id' => $part_id]);
		return true;
	}
	private function addPartToOption($part_id, $option_id) {
		DB::table('options_parts')->where('options_id', '=', $option_id)->where('parts_id', '=', $part_id)->delete();
		DB::table('options_parts')->insert(['options_id' => $option_id, 'parts_id' => $part_id]);
		return true;
	}
	private function deletePartToService($part_id, $service_id) {
		DB::table('win_service_parts')->where('win_service_id', '=', $service_id)->where('parts_id', '=', $part_id)->delete();
		return true;
	}
	private function deletePartToOption($part_id, $option_id) {
		DB::table('options_parts')->where('options_id', '=', $option_id)->where('parts_id', '=', $part_id)->delete();
		return true;
	}
}