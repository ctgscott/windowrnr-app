<?php

class MsgboardController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function listMessages()
	{		
		$messages = NotesController::notesByWebPost();
		if(!empty($messages)) {
			foreach ($messages as $message => $customer) {
				$fullCustomer = Customer::getCustomer($customer->customer_id);
				$customer->phone = $fullCustomer[0]->customer_phone;
			}
		}
		$heading = 'Messages - New';

		return View::make('messages.msgboard')->with(['messages' => $messages, 'heading' => $heading]);
	}
	
	public function getGmail() {
		$response = Gmail::checkLabel();
//		echo $response;
		var_dump($response);
	}
	
	public function getGmail2() {
		$response = Gmail::init();
		var_dump($response);
	}
}