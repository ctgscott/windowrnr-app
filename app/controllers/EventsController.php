<?php

class EventsController extends BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('auth');
	} 

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('events.index');
	}
	
	public static function getGoogleCalEvents($start, $end, $calID)
	{		
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
//		$cal = new Google_CalendarService($client);		
		$cal = new Google_Service_Calendar($client);

		if (Session::has('token')) 
		{			
			$client->setAccessToken(Session::get('token'));
		}

		if ($client->getAccessToken()) 
		{
			$newCal = DB::table('profiles')->where('id', '=', $calID)->pluck('google_calendar_id');
			$avatar = DB::table('profiles')->where('id', '=', $calID)->pluck('avatar');
			$start  = date('c',$start);
			$end    = date('c',$end);		
			$params = array(
				'singleEvents' => 'true', 
				'orderBy' => 'updated', 
				'timeMin' => $start, 
				'timeMax' => $end
			);
			$eventList = $cal->events->listEvents($newCal, $params);

			//BaseController::firePhp()->addInfo('eventList', array($eventList));
			
			dump_r($eventList);

			foreach ($eventList['items'] as $event)
			{
				if (isset($event['location'])) 
				{
					$address = urlencode($event['location']);
				} 
				else 
				{
					$address = null;
					$event['location'] = null;
				}

				set_time_limit(90);
				$latlng = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false"));

				if (isset($latlng->results[0]->geometry->location->lat)) 
				{
					$lat = $latlng->results[0]->geometry->location->lat;
					$lng = $latlng->results[0]->geometry->location->lng;
				} 
				else 
				{
					$lat = null;
					$lng = null;
				}

				$now = date("Y-m-d H:i:s");
				if (!isset($event['description'])) 
				{
					$description = 'None';
				} 
				else 
				{
					$description = $event['description'];
				}

				if (isset($event['start']['date'])) 
				{
					$event['all_day'] = 1;
					$eventStart = strtotime($event['start']['date']);
					$eventEnd = strtotime($event['end']['date']);
				}
				else 
				{
					$event['all_day'] = 0;
					$eventStart = strtotime($event['start']['dateTime']);
					$eventEnd = strtotime($event['end']['dateTime']);
				}

				$count = DB::table('events')
					->select('id')
					->where('google_event_id', '=', $event['id'])
					->count();

				$updated = strtotime($event['updated']);

				if ($count ==1) 
				{
					DB::table('events')
						->where('google_event_id', '=', $event['id'])
						->where('updated_at', '!=', $event['updated'])
						->update(array(
							'google_event_id' => $event['id'],
							'google_cal_id' => $event['organizer']['email'],
							'cal_user_id' => $calID,
							'avatar' => $avatar,
							'start' => $eventStart,
							'end' => $eventEnd,
							'location' => $event['location'],
							'description' => $description,
							'allDay' => $event['all_day'],
							'title' => $event['summary'],
							'created_by' => $event['creator']['email'],
							'created_at' => $now,
							'updated_at' => $event['updated'],
							'lat' => $lat,
							'lng' => $lng
						));
				} 
				else if ($count == 0) 
				{
					$result = DB::table('events')->insert(array(
						'google_event_id' => $event['id'],
						'google_cal_id' => $event['organizer']['email'],
						'cal_user_id' => $calID,
						'avatar' => $avatar,
						'start' => $eventStart,
						'end' => $eventEnd,
						'location' => $event['location'],
						'description' => $description,
						'allDay' => $event['all_day'],
						'title' => $event['summary'],
						'created_by' => $event['creator']['email'],
						'created_at' => $now,
						'updated_at' => $event['updated'],
						'lat' => $lat,
						'lng' => $lng
						)
					);

					echo $result;
				}
			}
		}
	}
	
	public static function updateEvents($start, $end, $gCalID)
	{
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
//		$cal = new Google_CalendarService($client);		
		$cal = new Google_Service_Calendar($client);

		if (Session::has('token')) 
		{
			$client->setAccessToken(Session::get('token'));
		}
		
		if ($client->getAccessToken()) 
		{
			$start = date('c',$start);
			$end = date('c',$end);
			$params = array(
				'singleEvents' => 'true', 		
				'orderBy' => 'startTime', 			
				'timeMin' => $start, 
				'timeMax' => $end
			);
			$eventList = $cal->events->listEvents($gCalID, $params);
			//BaseController::firePhp()->addInfo('eventList', array($eventList));

			if (!Schema::hasTable('temp_events'))
			{			
				try 
				{
					Schema::create('temp_events', function($table) {
						$table->increments('id');
						$table->string('google_event_id')->unique();
						$table->string('status');
						$table->string('htmlLink');
						$table->string('summary')->nullable();
						$table->string('location')->nullable();
						$table->tinyInteger('all_day');
						$table->string('creatorEmail');
						$table->string('organizerEmail');
						$table->integer('cal_user_id')->nullable();
						$table->timestamp('start');
						$table->timestamp('end');
						$table->timestamp('created_at');
						$table->timestamp('updated_at', 6)->index();
					});
				}
				catch (Exception $e) 
				{
					Session::flash('error', 'There was a problem: '.$e);
					return $e;
				}
			}
			
			$totalResult = [];
			set_time_limit(150); 
			
			do {
				try {
					if (isset($eventList['nextPageToken'])) 
					{
						$params2 = array(						
							'pageToken' => $eventList['nextPageToken']
						);


						$eventList= $cal->events->listEvents($gCalID, $params2);
						//BaseController::firePhp()->addInfo('eventList', array($eventList));
						foreach ($eventList['items'] as $event)
						{
							if (!isset($event['location'])) {
								$event['location'] = null;
							}

							if (!isset($event['summary'])) {
								$event['summary'] = null;
							}
							
							if (isset($event['organizer']['email'])) {
								$event['cal_user_id'] =  DB::table('profiles')
									->where('google_calendar_id', $event['organizer']['email'])
									->pluck('user_id');
							}

							if ($event['status'] != 'cancelled') {
								if (isset($event['start']['date'])) {
									$event['all_day'] = 1;
									$eventStart = $event['start']['date'];
									$eventEnd = $event['end']['date'];
								} else {
									$event['all_day'] = 0;
									$eventStart = $event['start']['dateTime'];
									$eventEnd = $event['end']['dateTime'];
								}
								
								$startTime = strtotime($eventStart);
								$endTime = strtotime($eventEnd);
								$eventStart = date("Y-m-d H:i:s", $startTime);
								$eventEnd = date("Y-m-d H:i:s", $endTime);
								
								DB::table('temp_events')->insert(
									array(
										'google_event_id' => $event['id'],
										'status' => $event['status'],
										'htmlLink' => $event['htmlLink'],
										'summary' => $event['summary'],
										'location' => $event['location'],
										'all_day' => $event['all_day'],
										'creatorEmail' => $event['creator']['email'],
										'organizerEmail' => $event['organizer']['email'],
										'cal_user_id' => $event['cal_user_id'],
										'start' => $eventStart,
										'end' => $eventEnd,
										'created_at' => $event['created'],
										'updated_at' => $event['updated']
									)
								);
							} else {
								DB::table('temp_events')->insert(
									array(
										'google_event_id' => $event['id'],
										'status' => $event['status']
									)
								);
							}
						};
					} else {
						foreach ($eventList['items'] as $event)
						{
							if (!isset($event['location'])) {
								$event['location'] = null;
							}

							if (!isset($event['summary'])) {
								$event['summary'] = null;
							}

							if (isset($event['organizer']['email'])) {
								$event['cal_user_id'] =  DB::table('profiles')
									->where('google_calendar_id', $event['organizer']['email'])
									->pluck('user_id');
							}

							if ($event['status'] != 'cancelled') {
								if (isset($event['start']['date'])) {
									$event['all_day'] = 1;
									$eventStart = $event['start']['date'];
									$eventEnd = $event['end']['date'];
								} else {
									$event['all_day'] = 0;
									$eventStart = $event['start']['dateTime'];
									$eventEnd = $event['end']['dateTime'];
								}
								
								$startTime = strtotime($eventStart);
								$endTime = strtotime($eventEnd);
								$eventStart = date("Y-m-d H:i:s", $startTime);
								$eventEnd = date("Y-m-d H:i:s", $endTime);
								
								DB::table('temp_events')->insert(
									array(
										'google_event_id' => $event['id'],
										'status' => $event['status'],
										'htmlLink' => $event['htmlLink'],
										'summary' => $event['summary'],
										'location' => $event['location'],
										'all_day' => $event['all_day'],
										'creatorEmail' => $event['creator']['email'],
										'organizerEmail' => $event['organizer']['email'],
										'cal_user_id' => $event['cal_user_id'],
										'start' => $eventStart,
										'end' => $eventEnd,
										'created_at' => $event['created'],
										'updated_at' => $event['updated']
									)
								);
							} else {
								DB::table('temp_events')->insert(
									array(
										'google_event_id' => $event['id'],
										'status' => $event['status']
									)
								);
							}
						};
					}
				}
				catch (Exception $e) {
					Session::flash('error', 'There was a problem: '.$e);
					return $e;
				}
			} while(isset($eventList['nextPageToken']) && !empty($eventList['nextPageToken']));
		}
	}

	public static function updateEventsTable() 
	{		
		/** 
		 * Events found in Google but not found in
		 * the application's calendar.
		 * Update the app events table as appropriate
		 **/		
		try 
		{
			 $appMissing = DB::table('temp_events')
				->leftJoin('events', 'temp_events.google_event_id', '=', 'events.google_event_id')
				->whereNull('events.google_event_id')			
				->select('temp_events.*')
				->get();			

			foreach ($appMissing as $event)
			{
				if ($event->cal_user_id == null) {
					continue;
				}

				if($event->summary == null) {
					$event->summary = 'x';
				}

				if (isset($event->location)) {
					$address = urlencode($event->location);
				} else {
					$address = null;
					$event->location = null;
				}

				$latlng = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false"));
				if (isset($latlng->results[0]->geometry->location->lat)) 
				{
					$lat = $latlng->results[0]->geometry->location->lat;
					$lng = $latlng->results[0]->geometry->location->lng;
				} 
				else 
				{
					$lat = null;
					$lng = null;
				}
				
				$avatar = DB::table('profiles')->where('google_calendar_id', '=', $event->organizerEmail)->pluck('avatar');
				if ($avatar == null) {
					$avatar = "WinPin2.png";
				}

				DB::table('events')->insert(
					array(
						'google_event_id' => $event->google_event_id,
						'status' => $event->status,
						'htmlLink' => $event->htmlLink,
						'title' => $event->summary,
						'location' => $event->location,
						'allDay' => $event->all_day,
//						'avatar' => $avatar,
						'cal_user_id' => $event->cal_user_id,
						'creatorEmail' => $event->creatorEmail,
						'google_cal_id' => $event->organizerEmail,
						'start' => $event->start,
						'end' => $event->end,
						'lat' => $lat,
						'lng' => $lng,
						'created_at' => $event->created_at,
						'updated_at' => $event->updated_at
					)
				);
			}

			/** 
			 * Events found in Google as 'Canceled' but 'Confirmed'
			 * or 'Tentative' in the application's calendar.
			 * Update the app events table as appropriate
			 **/

			/** 
			 * Events found in the app but not found in
			 * Google's calendar.  Update Google
			 * as appropriate
			 **/

	/*		$googMissing = DB::table('events')
				->leftJoin('temp_events', 'events.google_event_id', '=', 'temp_events.google_event_id')
				->whereNull('temp_events.google_event_id')
				->select('temp_events.google_event_id AS id', 'temp_events.updated_at AS updated')
				->get();
				$firephp->log(count($googMissing), 'googMissing');
	*/
			Schema::dropIfExists('temp_events');
		} 
		catch (Exception $e) 
		{
			Session::flash('error', 'There was a problem: '.$e);
			return $e;
		}
	}
	
	public static function getCalEvents($start = null, $end = null, $calID = 'all')
	{		
		if ($start == 'x')
		{
			$start = new DateTime("-12 months");
		}
		
		if ($end == 'x')
		{
			$end = new DateTime("+12 months");
		}
		
		if ($calID != 'all') {
			$events = DB::table('calendar_' . $calID)
				->where('start', '>=', date('c',$start))
				->where('end', '<=', date('c',$end));
		}
		else {
			if(Input::get('start')) $start = date('c', Input::get('start'));
			if(Input::get('end')) $end = date('c', Input::get('end'));
			$events = DB::table('calendar_all')
				->where('start', '>=', $start)
				->where('end', '<=', $end);
		}

		if(Input::get('assign')) {
			$events = $events->where('event_type', '=', 'Service Call.')->orWhere('event_type', '=', 'Install/Repair Job');
		}

		$events = $events->orderBy('start', 'asc')->get();

		// filter out events of users who are invislble as per events_visibility
		$visibleEvents = [];
		$invisibleUsers = DB::table('events_visibility')->where('session_user_id', Sentry::getUser()->id)->where('visible', '=', 0)->get();
		foreach($events as $event) {
			$invisible = false;
			foreach ($invisibleUsers as $invisibleUser) {
				if($invisibleUser->profile_id === $event->profile_id) {
					$invisible = true;
					break;
				}
			}
			if(!$invisible) $visibleEvents[] = $event;
		}

		//print_r($visibleEvents);

		foreach($visibleEvents as $event) {
			$event->start = date("Y-m-d\TH:i:sO", (strtotime($event->start)));
			$event->end = date("Y-m-d\TH:i:sO", (strtotime($event->end)));
			
			if($event->allDay == 0)
			{
				$event->allDay = false;//($event->allDay == '1' ? true : false); 
			}
			else
			{
				$event->allDay = true; 
			}

			$event->htmlLink = $event->url;
			$event->url = ""; //"/job/" . $event->job_id;
			
		}

		return $visibleEvents;
	}

	// post handler to save assignee information
	public static function setAssignee() {

		// update app database
		$assignees = Input::get('assignee');
		Events::where('id', Input::get('id'))->update(['assignee' => $assignees]);

		// update google calendar - add assignee info to description field
		$event = Events::find(Input::get('id'));
		if(!$event) return 'Error: Event not found!';
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$service = new Google_Service_Calendar($client);
		if (Session::has('token')) $client->setAccessToken(Session::get('token'));
		if ($client->getAccessToken()) {
			
			$calendarEvent = $service->events->get($event->google_cal_id, $event->google_event_id);
			
			//$assignees = empty($assignees) ? '[]' : preg_replace('/\|/i', ', ', $assignees);
			$assigneesText = json_decode($assignees);
			$assigneesText = array_map(function($x) {
				return $x->first_name;
			}, $assigneesText);
			if(count($assigneesText) === 0) $assigneesText = '* NONE *';
			else $assigneesText = implode(", ", $assigneesText);

			$newDescription = $calendarEvent->getDescription();
			if(strpos($newDescription, 'Assigned To:') !== FALSE) {
				$newDescription = preg_replace("/^Assigned To:.*$/im", 'Assigned To: ' . $assigneesText, $newDescription);
			}
			else {
				$newDescription .= "\n" . 'Assigned To: ' . $assigneesText;
			}
			$calendarEvent->setDescription($newDescription);
			$service->events->update('primary', $calendarEvent->getId(), $calendarEvent);
		}

		return 'OK';
	}

	// post handler to save modified event start and end time (after drag/resize in calendar)
	public static function updateEvent() {

		$google_cal_id = Input::get('cal_id');
		$google_event_id = Input::get('event_id');
		$start = Input::get('start');
		$end = Input::get('end');

		$dbStart = strtotime($start);
		$dbStart = date('Y-m-d H:i:s', $dbStart);
		$dbEnd = strtotime($end);
		$dbEnd = date('Y-m-d H:i:s', $dbEnd);

		// load from events
		$event = Events::where('google_cal_id', $google_cal_id)->where('google_event_id', $google_event_id)->first();

		// if not found, look in events_unmapped
		if(!$event) {
			$event = DB::table('events_unmapped')->where('google_cal_id', $google_cal_id)->where('google_event_id', $google_event_id)->first();
		}

		// update google calendar
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$service = new Google_Service_Calendar($client);
		if (Session::has('token')) $client->setAccessToken(Session::get('token'));
		if ($client->getAccessToken()) {
			$calendarEvent = $service->events->get($event->google_cal_id, $event->google_event_id);

			$eventStart = new Google_Service_Calendar_EventDateTime();
			$eventStart->setDateTime(date('c', strtotime(substr($start, 0, 33))));
			$calendarEvent->setStart($eventStart);

			$eventEnd = new Google_Service_Calendar_EventDateTime();
			$eventEnd->setDateTime(date('c', strtotime(substr($end, 0, 33))));
			$calendarEvent->setEnd($eventEnd);

			$service->events->update($google_cal_id, $calendarEvent->getId(), $calendarEvent);

			// update app database
			Events::where('google_cal_id', $google_cal_id)->where('google_event_id', $google_event_id)
				->update(['start' => $dbStart, 'end' => $dbEnd]);
		}

		return 'OK';
	}

	public static function update($id) 
	{
		$eventData = Events::find($id);

		//validate if event exists
		if(!$eventData)
		{
			Session::flash('error', 'Event does not exist.');
			return Redirect::to('/customers');
		}
		
		$jobID = $eventData['job_id'];
		
		$address = (Input::get('location')) ? urlencode(Input::get('location')) : null;

		$latlng = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false"));
		if (isset($latlng->results[0]->geometry->location->lat)) 
		{
			$lat = $latlng->results[0]->geometry->location->lat;
			$lng = $latlng->results[0]->geometry->location->lng;
		}
		 else 
		{
			$lat = null;
			$lng = null;
		}
		
		$avatar = DB::table('profiles')->where('user_id', '=', Input::get('cal_user_id'))->pluck('avatar');
		if ($avatar == null)
		{
			$avatar = "WinPin2.png";
		}
		
		$organizer = $eventData->google_cal_id;
		$newCalID = DB::table('profiles')->where('user_id', '=', Input::get('cal_user_id'))->pluck('google_calendar_id');
		
		$now = date("Y-m-d H:i:s");				
		
		//routine to update the calendar via API
		try {

			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_Service_Calendar($client);

			if (Session::has('token')) {
				$client->setAccessToken(Session::get('token'));
			}

			if ($client->getAccessToken()) {
				
				//update the event
				$event = $cal->events->get($organizer, $eventData->google_event_id);
				$event->setSummary(Input::get('title'));
				$event->setLocation($address);
				$event->setStatus(Input::get('status'));				
				
				// set extendedProperties
				$extendedProperties = New Google_Service_Calendar_EventExtendedProperties();
				$extendedProperties->setShared(['job_id' => $jobID]);
				$event->setExtendedProperties($extendedProperties);

				$cal->events->update($organizer, $event->getId(), $event);

				// move event if cal-id has changed
				if($eventData->google_cal_id != Input::get('cal_user_id')) {					
					$moveResult = $cal->events->move($eventData->google_cal_id, $event->getId(), $newCalID);
				}
			}

		} catch (Exception $e) {
			Session::flash('error', 'An error ocurred: ' . $e->getMessage() . '.');
			echo $e->getMessage();
			exit;
		}		

		$eventData = array(
			'cal_user_id'    => Input::get('cal_user_id'),
			'avatar'         => $avatar,
			'status'         => Input::get('status'),
			'organizerEmail' => $organizer,
			'title'          => Input::get('title'),
			'type'           => intval(Input::get('type')),
			'lat'            => $lat,
			'lng'            => $lng,
			'updated_at'     => $now
		);

		switch(intval(Input::get('type'))) {
			case EVENT_EST_APPT:
				$eventData['event_type'] = 'Estimate Appt.';
				break;
			case EVENT_INSTALL_REPAIR:
				$eventData['event_type'] = 'Install/Repair Job';
				break;
			case EVENT_SERVICE_CALL:
				$eventData['event_type'] = 'Service Call';
				break;
		}

		if(Events::where('id', $id)->update($eventData))
		{
			Session::flash('success', 'Event successfully updated.');
		}
		else
		{
			Session::flash('error', 'An error occurred. Try again later.');
		}

		if(!empty($jobID))
		{
			return Redirect::to('/job/'.$jobID);
		} 
		else
		{
			return Redirect::to('/events/'.$id);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$results = Events::find($id);

		if(!$results)
		{
			Session::flash('error', 'Event does not exist.');
			return Redirect::to('/customers');
		}

		$user = Sentry::findUserById($results->cal_user_id);
		$results->who = $user->first_name;
		
		$results->names = DB::table('users')
			->select('id', 'first_name')
			->get();
		
		$results->notes = Note::notesByJobID($results->job_id);
		
		return View::make('events.edit')->with('results', $results);
	}

	/**
	 * Delete a event into the app and remote calendar
	 *
	 * @return Response
	 */
	public static function delete($id)
	{
		$event = Events::find($id);

		if(!$event)
		{
			DB::table('events_unmapped')->where('id', $id)->delete();
			Session::flash('error', 'Event does not exist.');
			//return "OK"; 
			return Redirect::to('/calendar');
		}
		else {
			$event->delete();
		}

		//delete event in remote calendar
		try {

			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_Service_Calendar($client);

			if (Session::has('token')) 
			{
				$client->setAccessToken(Session::get('token'));
			}

			if ($client->getAccessToken()) 
			{
				$cal->events->delete($event->google_cal_id, $event->google_event_id);
			}

		} catch (Exception $e) {
			Session::flash('error', 'An error ocurred: ' . $e->getMessage() . '.');
			return Redirect::to('/calendar');
		}

		Session::flash('success', 'Event deleted successfully');
		//$result = ['status' => 'success', 'message' => 'Event #'.$id.' deleted successfully', 'eventID' => $id];

		//return $result;
		return Redirect::to('/job/' . $event->job_id);
	}

	// some actions for testing events in google
	public function test() {
		return View::make('events.test');
	}
	public function testGet() {
		try {
			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_Service_Calendar($client);
			$event = NULL;
			echo 'Calendar: <b>' . Input::get('calendar_id') . '</b>&nbsp;&nbsp;Event ID: <b>' . Input::get('event_id') . '</b>';
			echo "<hr>";
			if (Session::has('token')) {
				$client->setAccessToken(Session::get('token'));
			}
			if ($client->getAccessToken()) {
				$event = $cal->events->get(Input::get('calendar_id'), Input::get('event_id'));
			}

			if($event) {
				
				try {
					$eps = $event->getExtendedProperties();
					if($eps) $eps = $eps->getShared();

					if(empty($eps) || !isset($eps['job_id'])) {
						echo "No job_id property found!<hr>";
					}
					else {
						echo "Job ID: {$eps['job_id']} <hr>";
					}
				}
				catch (Exception $e) {
					echo "No job_id property found!<hr>";
				}

				var_dump($event);
			}

			return "";
		}
		catch (Exception $e) {
			Session::flash('error', 'An error ocurred: ' . $e->getMessage() . '.');
			return 'An error ocurred: ' . $e->getMessage() . '.';
		}
	}
	public function testSetProperty() {
		try {
			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_Service_Calendar($client);
			$event = NULL;
			if (Session::has('token')) {
				$client->setAccessToken(Session::get('token'));
			}
			if ($client->getAccessToken()) {
				$event = $cal->events->get(Input::get('calendar_id'), Input::get('event_id'));
			}
			
			if($event && !empty(Input::get('prop_name')) && !empty(Input::get('prop_value'))) {
				echo 'Calendar: <b>' . Input::get('calendar_id') . '</b>&nbsp;&nbsp;Event ID: <b>' . Input::get('event_id') . '</b>';
				echo "<hr>";
				$extendedProperties = New Google_Service_Calendar_EventExtendedProperties();
				$extendedProperties->setShared([Input::get('prop_name') => Input::get('prop_value')]);
				$event->setExtendedProperties($extendedProperties);
				$cal->events->update(Input::get('calendar_id'), Input::get('event_id'), $event);
				echo '<b>Property ' . Input::get('prop_name') . ' set successfully</b>';
				echo "<hr>";
			}

			//var_dump($event);
		}
		catch (Exception $e) {
			Session::flash('error', 'An error ocurred: ' . $e->getMessage() . '.');
			return 'An error ocurred: ' . $e->getMessage() . '.';
		}
	}
	public function testGet_calPush_Log() {
		$files = explode("\n", trim(shell_exec('ls -t /tmp/*_' . App::environment() . '_* | head -25')));
		$date = date("Y-m-d H:i:s");
		$data = '';
		$file = '';
		if(!empty(Input::get('file'))) {
			$file = Input::get('file');
			$data = file_get_contents(Input::get('file'));
		}
		return View::make('events.log', ['files' => $files, 'date' => $date, 'file' => $file, 'data' => $data]);
	}
	public function setProperty() {
		$events = DB::table('events')->where('job_id', '>', 0)->get();
		return View::make('events.set-property', ['events' => $events]);
	}
	public function setSingleProperty() {
		try {
			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_Service_Calendar($client);
			$event = NULL;
			if (Session::has('token')) {
				$client->setAccessToken(Session::get('token'));
			}
			if ($client->getAccessToken()) {
				$event = $cal->events->get(Input::get('calendar_id'), Input::get('event_id'));
			}
			else {
				return "ERROR: Access Denied";
			}

			if(empty(Input::get('prop_name')) || empty(Input::get('prop_value'))) {
				return "ERROR: Invalid Input";
			}
			
			if($event) {
				$extendedProperties = New Google_Service_Calendar_EventExtendedProperties();
				$extendedProperties->setShared([Input::get('prop_name') => Input::get('prop_value')]);
				$event->setExtendedProperties($extendedProperties);
				$cal->events->update(Input::get('calendar_id'), Input::get('event_id'), $event);
				return "OK";
			}
			else {
				return "ERROR: Event Not Found";
			}
		}
		catch (Exception $e) {
			return 'ERROR: ' . $e->getMessage();
		}

		return "ERROR: Unknown";
	}

	// map/unmap event
	public function mapEvent() {

		// get cust id
		$cust_id = DB::table('jobs')->where('id', '=', Input::get('job_id'))->pluck('customer_id');

		// if event not present in events table, copy from events_unmapped
		if(DB::table('events')->where('google_event_id', '=', Input::get('event_id'))->count() === 0 && DB::table('events_unmapped')->where('google_event_id', '=', Input::get('event_id'))->count() > 0) {
			$fields = 'google_event_id, google_cal_id, cust_id, job_id, cal_user_id, avatar, status, creatorEmail, organizerEmail, htmlLink, start, end, location, description, assignee, allDay, title, event_type, type, lat, lng, discarded, created_at, updated_at';
			DB::statement('INSERT INTO events (' . $fields . ') SELECT ' . $fields . ' FROM events_unmapped WHERE google_event_id = :id', ['id' => Input::get('event_id')]);
			DB::table('events_unmapped')->where('google_event_id', '=', Input::get('event_id'))->delete();
		}

		// update job_id and cust_id
		DB::table('events')->where('google_event_id', '=', Input::get('event_id'))->update(['job_id' => Input::get('job_id'), 'cust_id' => $cust_id]);

		return "OK";
	}
	// map/unmap event (to new job)
	public function mapEventNew() {

		// first create job and customer records
		$jobID = CustomersController::store();
		$_POST['job_id'] = $jobID;
		$job = DB::table('jobs')->where('id', $jobID)->first();
		$customer = DB::table('customers')->where('id', $job->customer_id)->first();
		$_POST['cust_id'] = $customer->id;
		$_POST['summary'] = '('.$_POST['city'].') '.$_POST['l_name'].', '.$_POST['f_name'];
		$_POST['location'] = $_POST['address'].', '.$_POST['city'].', CA, '.$_POST['zip'];
		$results['notes'] = Note::notesByJobID($jobID);
		$leadSource = empty(Input::get('lead_source')) ? '' : implode(Input::get('lead_source'),",");
		$_POST['description'] = "Built: ".$_POST['built']."\n"."Phone: ".$_POST['phone']."\n"."Lead Source: ".$leadSource;

		// get cust id
		$cust_id = $job->customer_id;

		// if event not present in events table, copy from events_unmapped
		if(DB::table('events')->where('google_event_id', '=', Input::get('event_id'))->count() === 0 && DB::table('events_unmapped')->where('google_event_id', '=', Input::get('event_id'))->count() > 0) {
			$fields = 'google_event_id, google_cal_id, cust_id, job_id, cal_user_id, avatar, status, creatorEmail, organizerEmail, htmlLink, start, end, location, description, assignee, allDay, title, event_type, type, lat, lng, discarded, created_at, updated_at';
			DB::statement('INSERT INTO events (' . $fields . ') SELECT ' . $fields . ' FROM events_unmapped WHERE google_event_id = :id', ['id' => Input::get('event_id')]);
			DB::table('events_unmapped')->where('google_event_id', '=', Input::get('event_id'))->delete();
		}

		// update job_id and cust_id
		DB::table('events')->where('google_event_id', '=', Input::get('event_id'))->update(['job_id' => $jobID, 'cust_id' => $cust_id]);

		return "OK:$jobID";
	}
	public function unmapEvent() {

		// update event's job_id an dcust_id to 0
		DB::table('events')->where('google_event_id', '=', Input::get('event_id'))->update(['job_id' => 0, 'cust_id' => 0]);

		// delete from events_unmapped (if existing)
		DB::table('events_unmapped')->where('google_event_id', '=', Input::get('event_id'))->delete();

		// copy event to events_unmapped
		$fields = 'google_event_id, google_cal_id, cust_id, job_id, cal_user_id, avatar, status, creatorEmail, organizerEmail, htmlLink, start, end, location, description, assignee, allDay, title, event_type, type, lat, lng, discarded, created_at, updated_at';
		DB::statement('INSERT INTO events_unmapped (' . $fields . ') SELECT ' . $fields . ' FROM events WHERE google_event_id = :id', ['id' => Input::get('event_id')]);

		// delete from events main table
		DB::table('events')->where('google_event_id', '=', Input::get('event_id'))->delete();

		return "OK";
	}
	public function discardEvent() {

		// set discarded to 1 (so it doesn't show in the calendar anymore)
		DB::table('events_unmapped')->where('google_event_id', '=', Input::get('event_id'))->update(['discarded' => 1, 'job_id' => 0]);
		DB::table('events')->where('google_event_id', '=', Input::get('event_id'))->update(['discarded' => 1, 'job_id' => 0]);

		// Delete event from Google Calendar
		self::delete(Input::get('app_event_id'));
		
		return "OK";
	}
	
	public static function setDescription($jobID) {
		$notes = Note::notesByJobID($jobID, 1);			
		$fullNote = '';
		foreach($notes as $note) {
			$fullNote .= $note->f_name.": ".date("g:i a, m/d/Y", strtotime($note->created_at))." ".$note->note."\n\n";
		}
		$job = Job::find($jobID);
		$cust = Customer::find($job->customer_id);
		$desc = "Built: ".$job->built."\n"."Phone: ".$cust->phone."\n"."Alt Phone: ".$cust->alt_phone."\n"."Email: ".$cust->email."\n"."Lead Source: ".$job->lead_source."\n"."Notes: \n".$fullNote;

		return $desc;
	}

	public static function editEventApp() {
		$oldEvent = Events::getEventByID($_POST['eventID']);

		$old_google_cal_id = $oldEvent->google_cal_id;
		$oldEvent->google_cal_id = $_POST['google_cal_id'];
		$oldEvent->cal_user_id = DB::table('profiles')->where('google_calendar_id', $oldEvent->google_cal_id)->pluck('user_id');

		$oldEvent->type = $_POST['type'];
		//Ideally avatar is not required in the events table.
		$oldEvent->avatar = DB::table('profiles')->where('google_calendar_id', $oldEvent->google_cal_id)->pluck('avatar');

		switch(intval($oldEvent->type)) {
			case EVENT_EST_APPT:
				$oldEvent->event_type = 'Estimate Appt.';
				break;
			case EVENT_INSTALL_REPAIR:
				$oldEvent->event_type = 'Install/Repair Job';
				break;
			case EVENT_SERVICE_CALL:
				$oldEvent->event_type = 'Service Call';
				break;
		}

		try {
			DB::table('events')->where('id', $oldEvent->id)->update((array)$oldEvent);
			$result = ['status' => 'success', 'message' => 'Edit event #'.$oldEvent->id.' inserted successfully', 'eventID' => $oldEvent->id, 'old_google_cal_id' => $old_google_cal_id];			
		}
		catch(Exception $e) {
			$result = ['status' => 'fail', 'message' => 'Edit event #'.$oldEvent->id.' failed', 'eventID' => $oldEvent->id];	
		}
		
		return $result;
	}

	public static function editEventGoog() {
		$eventData = Events::getEventByID($_POST['eventID']);
		$new_google_cal_id = $eventData->google_cal_id;
		$old_google_cal_id = $_POST['old_google_cal_id'];
		$new_cal_user_id = DB::table('profiles')->where('google_calendar_id', $new_google_cal_id)->pluck('user_id');
		$old_cal_user_id = DB::table('profiles')->where('google_calendar_id', $old_google_cal_id)->pluck('user_id');

		try {

			$client = new Google_Client();
			$client->setApplicationName("WindowRnR");
			$cal = new Google_Service_Calendar($client);

			if (Session::has('token')) {
				$client->setAccessToken(Session::get('token'));
			}
			
			if ($client->getAccessToken()) {
				//update the event
				$event = $cal->events->get($old_google_cal_id, $eventData->google_event_id);
				
				$description = $event->getDescription();
				$description = explode("\n", $description);
				$eventTypes = [EVENT_EST_APPT => 'Estimate Appt.', EVENT_INSTALL_REPAIR => 'Install/Repair Job', EVENT_SERVICE_CALL => 'Service Call.'];
				$description[0] = 'Event Type: ' . $eventTypes[$_POST['type']];
				$description = implode("\n", $description);
				$event->setDescription($description);

				$cal->events->update($old_google_cal_id, $event->getId(), $event);

				// move event if cal-id has changed
				if($new_cal_user_id != $old_cal_user_id) {					
					$moveResult = $cal->events->move($old_google_cal_id, $event->getId(), $new_google_cal_id);
				}
			}
			$result = ['status' => 'success', 'message' => 'Event inserted in Google Calendar'];
		} catch (Exception $e) {
			$result = ['status' => 'fail', 'message' => 'Failed to insert event in Google Calendar'];
		}

		return $result;
	}
	
	public static function duplicateEventApp() {
		$oldEvent = Events::getEventByID($_POST['eventID']);
		$oldEvent->created_at = date("Y-m-d H:i:s");
		$date = $_POST['date'];
		
		$calendarID   = $oldEvent->google_cal_id;
		$job_id       = $oldEvent->job_id;
		
		//change date to new date from modal
		$start        = $oldEvent->start;
		$newStart	  = explode(" ", $start);
		$newStart[0]  = $date;
		$newStart	  = implode(" ", $newStart);
		$appStart     = date('Y-m-d H:i:s', strtotime($newStart));
	
		$end          = $oldEvent->end;
		$newEnd		  = explode(" ", $end);
		$newEnd[0]	  = $date;
		$newEnd 	  = implode(" ", $newEnd);
		$appEnd       = date('Y-m-d H:i:s', strtotime($newEnd));
	
		// Insert the event into the App DB
		unset($oldEvent->id, $oldEvent->google_event_id);
		$oldEvent->start = $appStart;
		$oldEvent->end = $appEnd;

		$eventID = DB::table('events')->insertGetId((array)$oldEvent);
		$appt = Appointment::insertData($job_id, $eventID, $calendarID, $appStart, $appEnd, 1, 'x', $oldEvent->cust_id);

		$result = ['status' => 'success', 'message' => 'Duplicate event #'.$eventID.' inserted successfully', 'eventID' => $eventID];

		return $result;
	}
	
	public static function duplicateEventGoog() {
		$event = Events::getEventByID($_POST['eventID']);
		$event->created_at = date("Y-m-d H:i:s");
		$date = $_POST['date'];
		
		// instantiate Google Calendar
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$cal = new Google_Service_Calendar($client);

		if (Session::has('token')) $client->setAccessToken(Session::get('token'));
		
		if ($client->getAccessToken()) {
			$calendarID   = $event->google_cal_id;
			$summary      = $event->title;
			$location     = $event->location;
			$job_id       = $event->job_id;

			//change date to new date from modal
			$start        = $event->start;
			$start 		  = date('c', strtotime($start));
		
			$end          = $event->end;
			$end		  = date('c', strtotime($end));
		
			// add event in google
			$googEvent = new Google_Service_Calendar_Event();
            $googEvent->setSummary($summary);
			$googEvent->setLocation($location);
			$googEvent->setDescription($event->description);

			$eventStart = new Google_Service_Calendar_EventDateTime();
			$eventStart->setDateTime($start);
			$googEvent->setStart($eventStart);

			$eventEnd = new Google_Service_Calendar_EventDateTime();
			$eventEnd->setDateTime($end);
			$googEvent->setEnd($eventEnd);
			
			// add job_id property
			$extendedProperties = New Google_Service_Calendar_EventExtendedProperties();
			$extendedProperties->setShared(['job_id' => $job_id]);
			$googEvent->setExtendedProperties($extendedProperties);

			// insert the event into google calendar
			$createdEvent = $cal->events->insert($calendarID, $googEvent);
			
			DB::table('events')
				->where('id', $event->id)
				->update(array('google_event_id' => $createdEvent->id));

				$result = ['status' => 'success', 'message' => 'Event inserted in Google Calendar'];
		}
		return $result;
	}
}
