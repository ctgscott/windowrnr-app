<?php

trait TimeTrait {
	
	public static function findLastUpdatedTime($created_at, $updated_at, $latest_modified_value){
		
		if($created_at) {
			$last_created_at = strtotime($created_at);
			if($latest_modified_value < $last_created_at) {
				$latest_modified_value = $last_created_at;
			}
		}

		if($updated_at){
			$last_updated_at = strtotime($updated_at);
			if($latest_modified_value < $last_updated_at) {
				$latest_modified_value = $last_updated_at;
			}						
		}	

		return $latest_modified_value;
	}
}

?>
