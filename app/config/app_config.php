<?php

// In this file we can set the constants used in the app

return array(
	//here we set the job status instead use a table for this
	'job_status' => array(
		'1' => 'Lead Created',
		'1.1' => 'Web post call back, left msg.',
		'2' => 'Lead Scheduled',
		'3' => 'Quote Created',
		'4' => 'Job Scheduled',
		'5' => 'Sales Receipt Created/Job Complete',
		'6' => 'On Standby',
		'7' => 'Service Call'
	)
);