<?php

Class Drive {

	private static $client = null;
	private static $service = null;
	private static $initialized = false;

	// init client
	public static function init() {

		// create client
		self::$client = AuthController::makeGoogleClient();

//		self::$client = new Google_Client();
//		self::$client->setApplicationName("WindowRnR");
		self::$service = new Google_Service_Drive(self::$client);
		
		//get user data from session
		
		// Wrong: shouldn't use logged user's session
		// $user = Sentry::getUser();

		// Fix: Use scott@windowrnr from the database to authenticate
		$user = User::where('email', '=', 'scott@windowrnr.com')->first();
		
		if($user->google_refresh_token) {
//			self::$client->refreshToken($user->google_refresh_token); //set the refresh token
			$token = json_decode($user->google_refresh_token);
			self::$client->refreshToken($token); //set the refresh token
			
			// Wrong: this token has nothing to do with the user's session
			// Session::put('token', self::$client->getAccessToken());

			// all ok, ready to do stuff
			self::$initialized = true;
			return true; 
		}

		// no logged in user or couldn't refresh token, so fail
		return false;
	}

	// check if specified folder exists inside specified parent
	public static function folderExists($_title, $_parent) {

		if(!self::$initialized) self::init();
		if(!self::$initialized) return FALSE; // error: couldn't init client/service

		// fail if parent folder id not found in database
		$parentFolder = Folder::getID($_parent);
		if(!$parentFolder) return FALSE;

		// query drive
		$qParams = 'mimeType contains "folder" and title = "' . $_title . '" and "' . $parentFolder->folder_id . '" in parents';
		$parameters = ['q' => $qParams];

		$result = self::$service->files->listFiles($parameters);
		$exists = (count($result['items']) != 0);

		// if folder exists in drive, ensure it is there in the database too
		if($exists) {
			$folder = Folder::getID($_title);
			if(!$folder) {
				Folder::insert($result['items'][0]['title'], $result['items'][0]['id'], $parentFolder->folder_id);	
			}
		}

		return ($exists ? 1 : 0);
	}

	// create folder with specified name under specified parent - if it does not already exist
	public static function createNamedFolder($_title, $_description, $_parent) {

		if(!self::$initialized) self::init();
		if(!self::$initialized) return FALSE; // error: couldn't init client/service

		// dont create if it already exists
		$exists = self::folderExists($_title, $_parent);
		if($exists === 1) {
			return TRUE;
		}

		if($exists === FALSE) {
			return FALSE;
		}

		try {
			// get parent id
			$parentFolder = Folder::getID($_parent);

			// fail if parent folder id not found in database
			if(!$parentFolder) return FALSE;

			$parent = new Google_Service_Drive_ParentReference();
			$parent->setId($parentFolder->folder_id);

			// create new folder under parent
			$file = new Google_Service_Drive_DriveFile();
			$file->setTitle($_title);
			$file->setDescription($_description);
			$file->setMimeType('application/vnd.google-apps.folder');
			$file->setParents([$parent]);
			$createdFile = self::$service->files->insert($file);
			$createdFile->folderCreated = Folder::insert($createdFile->title, $createdFile->id, $parentFolder->folder_id);

			if($createdFile === 'Success') return TRUE; // all ok.

			return FALSE; // failed to create row for folder in the database
		}
		catch(Exception $ex) {
			return FALSE; // failed
		}

	}

	// create folder tree (WindowRnR/Y<year>/<jobID>/) used for all job related files
	public static function createJobFolder($_jobId) {

		if(!self::$initialized) self::init();
		if(!self::$initialized) return FALSE; // error: couldn't init client/service

		// ensure that year folder exists
		$year = date('Y');
		if(!self::createNamedFolder('Y' . $year, 'Folder for year ' . $year, 'WindowRnR')) return FALSE;

		// ensure that job folder exists
		if(!self::createNamedFolder($_jobId, 'Folder for job #' . $_jobId, 'Y' . $year)) return FALSE;

		return TRUE;
	}

	// save file under specified jobId
	public static function saveFile($_jobId, $_data, $_meta) {

		if(!self::$initialized) self::init();
		if(!self::$initialized) return FALSE; // error: couldn't init client/service

		// ensure that year folder exists
		$year = date('Y');
		if(!self::createNamedFolder('Y' . $year, 'Folder for year ' . $year, 'WindowRnR')) return FALSE;

		// ensure that job folder exists
		if(!self::createNamedFolder($_jobId, 'Folder for job #' . $_jobId, 'Y' . $year)) return FALSE;

		// create job folder parent reference (to save file in)
		$jobFolder = Folder::getID($_jobId);

		// fail if folder not found in database - shjould never happen
		if(!$jobFolder) return FALSE;

		$parent = new Google_Service_Drive_ParentReference();
		$parent->setId($jobFolder->folder_id);

		// save file to job folder
		$file = new Google_Service_Drive_DriveFile();
		$file->setTitle($_meta['title']);
		$file->setDescription($_meta['description']);
		$file->setMimeType($_meta['mimeType']);
		$file->setParents([$parent]);

		try {
			$createdFile = self::$service->files->insert($file, array(
				'data' => $_data,
				'mimeType' => $_meta['mimeType'],
				'uploadType' => 'multipart'
			));
			return json_encode($createdFile);
		}
		catch(Exception $ex) {
			return FALSE; // failed
		}

	}

	// save file under specified jobId
	public static function saveFileVerbose($_jobId, $_data, $_meta) {

		if(!self::$initialized) self::init();

		// error: couldn't init client/service
		if(!self::$initialized) return json_encode([
			'status' => 'error',
			'reason' => 'Couldn\'t initialize client/service'
		]); 

		// ensure that year folder exists
		$year = date('Y');
		if(!self::createNamedFolder('Y' . $year, 'Folder for year ' . $year, 'WindowRnR')) {
			return json_encode([
				'status' => 'error',
				'reason' => 'Couldn\'t create year folder: "Y' . $year . '"' 
			]);
		}

		// ensure that job folder exists
		if(!self::createNamedFolder($_jobId, 'Folder for job #' . $_jobId, 'Y' . $year)) {
			return json_encode([
				'status' => 'error',
				'reason' => 'Couldn\'t create job folder: "Y' . $year .  ' / ' . $_jobId  . '"'
			]);
		}

		// create job folder parent reference (to save file in)
		$jobFolder = Folder::getID($_jobId);

		// fail if folder not found in database - should never happen
		if(!$jobFolder) {
			return json_encode([
				'status' => 'error',
				'reason' => 'Job folder object not found in database'
			]);
		}

		$parent = new Google_Service_Drive_ParentReference();
		$parent->setId($jobFolder->folder_id);

		// save file to job folder
		$file = new Google_Service_Drive_DriveFile();
		$file->setTitle($_meta['title']);
		$file->setDescription($_meta['description']);
		$file->setMimeType($_meta['mimeType']);
		$file->setParents([$parent]);

		try {
			$createdFile = self::$service->files->insert($file, array(
				'data' => $_data,
				'mimeType' => $_meta['mimeType'],
				'uploadType' => 'multipart'
			));
			$createdFile->status = 'ok';
			return json_encode($createdFile);
		}
		catch(Exception $ex) {
			return json_encode([
				'status' => 'error',
				'reason' => 'Exception in $service->files->insert(...)',
				'message' => $ex->getMessage()
			]);
		}

	}

	// download file with specified download_url from drive
	public static function downloadFile($_downloadUrl, $_destinationFile) {

		if(!self::$initialized) self::init();
		if(!self::$initialized) return FALSE; // error: couldn't init client/service

		$request = new Google_Http_Request($_downloadUrl, 'GET', null, null);
	    $result = self::$client->getAuth()->authenticatedRequest($request);
	    if ($result->getResponseHttpCode() == 200) {
	    	file_put_contents($_destinationFile, $result->getResponseBody());
	    	return true;
	    }

	    return false;
	}

/*

// Info: Return format from drive API

{
	"alternateLink":"https://docs.google.com/file/d/0B_GIjPf9cQulR0FNSTdrTnJIZWs/edit?usp=drivesdk",
	"appDataContents":false,"copyable":true,
	"createdDate":"2014-11-10T11:40:31.112Z",
	"defaultOpenWithLink":null,
	"description":"Image attachment for estimate ID #46",
	"downloadUrl":"https://doc-0k-5c-docs.googleusercontent.com/docs/securesc/ai4epng3eg9d40sq0e0io7160f8odrck/3va2jop2gpaqbuan9v4vimr5q8u2q4of/1415613600000/13058876669334088843/08762063339820583705/0B_GIjPf9cQulR0FNSTdrTnJIZWs?e=download&gd=true",
	"editable":true,
	"embedLink":null,
	"etag":"\"M4l5RvCt2StP2jOGfgyJPGdTZTc/MTQxNTYxOTYzMDY4Nw\"",
	"explicitlyTrashed":null,
	"exportLinks":null,
	"fileExtension":"",
	"fileSize":"51563",
	"headRevisionId":"0B_GIjPf9cQuldzNva0ttQXNoREd6bmhFN0J2Q2lkZm54dzc4PQ",
	"iconLink":"https://ssl.gstatic.com/docs/doclist/images/icon_11_image_list.png",
	"id":"0B_GIjPf9cQulR0FNSTdrTnJIZWs",
	"kind":"drive#file",
	"lastModifyingUserName":"Vijayakrishnan Krishnan",
	"lastViewedByMeDate":"2014-11-10T11:40:30.687Z",
	"markedViewedByMeDate":"1970-01-01T00:00:00.000Z",
	"md5Checksum":"1948b6fbc44ef60df08a789aa132ad34",
	"mimeType":"image/png",
	"modifiedByMeDate":"2014-11-10T11:40:30.687Z",
	"modifiedDate":"2014-11-10T11:40:30.687Z",
	"openWithLinks":null,
	"originalFilename":"Image attachment for estimate ID #46",
	"ownerNames":["Vijayakrishnan Krishnan"],
	"quotaBytesUsed":"51563",
	"selfLink":"https://www.googleapis.com/drive/v2/files/0B_GIjPf9cQulR0FNSTdrTnJIZWs",
	"shared":false,
	"sharedWithMeDate":null,
	"thumbnailLink":"https://lh3.googleusercontent.com/4zFBrIC13-5kDh57j4zQlkRByWsnGRUuaulJ014TE0k3vdKPFN4PWUb0eKijisEEzFvJww=s220",
	"title":"Image attachment for estimate ID #46",
	"version":"5150",
	"webContentLink":"https://docs.google.com/uc?id=0B_GIjPf9cQulR0FNSTdrTnJIZWs&export=download", // <-- use this
	"webViewLink":null,
	"writersCanShare":true
}

*/

}
