<?php

Class Gmail {

	private static $client = null;
	private static $service = null;
	private static $initialized = false;
	private static $GmailRO = 'https://www.googleapis.com/auth/gmail.readonly';
	private static $GmailLabels = 'https://www.googleapis.com/auth/gmail.labels	';

	// init client
	public static function init($scope = null) {

		// create client
//		self::$client = AuthController::makeGoogleClient();
		$client = AuthController::makeGoogleClient();
		
		// change the scope as needed
/*		if ($scope != null) {
			$client->setScopes($scope);
		}
*///		self::$service = new Google_Service_Gmail(self::$client);
		$service = new Google_Service_Gmail($client);
//		return $service;
		
		$user = User::where('email', '=', 'scott@windowrnr.com')->first();
		
		if($user->google_refresh_token) {
			$token = json_decode($user->google_refresh_token);
			$client->refreshToken($token); //set the refresh token
			
			// all ok, ready to do stuff
			$initialized = true;
			return true; 
		}

		// no logged in user or couldn't refresh token, so fail
		return false;
	}
	
	// check Gmail account to see if there are any files with 
	// the labell 'VoiceMail'.  If so, retrieve them and hand them back 
	// with msg id and attachments.
	public static function checkLabel() {
//		if(!self::$initialized) self::init('https://www.googleapis.com/auth/gmail.readonly');
//		if(!self::$initialized) self::init();
//		if(!self::$initialized) return FALSE; // error: couldn't init client/service
	
		$client = AuthController::makeGoogleClient();
		$service = new Google_Service_Gmail($client);
		return $client;
		$userId = 'me';
		
		$pageToken = NULL;
		$messages = array();
		$opt_param = array();
		do {
			try {
				if ($pageToken) {
				$opt_param['pageToken'] = $pageToken;
//				$opt_param['q'] = 'voicemail';
				}
//				$messagesResponse = $service->users_messages->listUsersMessages($userId, $opt_param);
//				$messagesResponse = $service->users_messages->listUsersMessages($userId);
				$labels = $service->users_labels->listUsersLabels($userId);
				return $labels;
				if ($messagesResponse->getMessages()) {
					$messages = array_merge($messages, $messagesResponse->getMessages());
					$pageToken = $messagesResponse->getNextPageToken();
				}
			} catch (Exception $e) {
				print 'An error occurred: ' . $e->getMessage();
			}
		} while ($pageToken);

		foreach ($messages as $message) {
			print 'Message with ID: ' . $message->getId() . '<br/>';
		}

		return $messages;
	}

	
	function listMessages($service, $userId) {
	  $pageToken = NULL;
	  $messages = array();
	  $opt_param = array();
	  do {
		try {
		  if ($pageToken) {
			$opt_param['pageToken'] = $pageToken;
		  }
		  $messagesResponse = $service->users_messages->listUsersMessages($userId, $opt_param);
		  if ($messagesResponse->getMessages()) {
			$messages = array_merge($messages, $messagesResponse->getMessages());
			$pageToken = $messagesResponse->getNextPageToken();
		  }
		} catch (Exception $e) {
		  print 'An error occurred: ' . $e->getMessage();
		}
	  } while ($pageToken);

	  foreach ($messages as $message) {
		print 'Message with ID: ' . $message->getId() . '<br/>';
	  }

	  return $messages;
}
	
	// remove label ($label), from msg id ($id)
	public static function removeLabel($label, $msgId) {
		if(!self::$initialized) self::init();
		if(!self::$initialized) return FALSE; // error: couldn't init client/service
		
	}

	// download file with specified download_url from drive
	public static function downloadFile($_downloadUrl, $_destinationFile) {

		if(!self::$initialized) self::init();
		if(!self::$initialized) return FALSE; // error: couldn't init client/service

		$request = new Google_Http_Request($_downloadUrl, 'GET', null, null);
	    $result = self::$client->getAuth()->authenticatedRequest($request);
	    if ($result->getResponseHttpCode() == 200) {
	    	file_put_contents($_destinationFile, $result->getResponseBody());
	    	return true;
	    }

	    return false;
	}
}
