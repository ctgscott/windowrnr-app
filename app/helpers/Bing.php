<?php

/* Helper class to geolocate addresses using Bing spatial */
Class Bing {

	public static function geoLocate($_address, $_city, $_state, $_zip) {

		$recID = rand(1001, 9999);
		$input = "$recID,en-US,,$_address,$_state,,,,$_city,$_zip,,,,,,,,,,,,,,,,,,,,,,,,";

		$key = 'An-iht2ueb6XDeq9k1vob5u089navkKIVrt2QOfZ2Fzz7h-JpaZfQdaJOo6oMMOC';
		$url = "http://spatial.virtualearth.net/REST/v1/Dataflows/Geocode?description=MyJob&input=csv&key=$key";

		// create job
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/csv'));
		$result = curl_exec($ch);
		curl_close($ch);
		$result = json_decode($result);

		// if creation successful, wait for completion
		if($result->statusDescription === 'Created') {
			$jobId = $result->resourceSets[0]->resources[0]->id;
			$jobStatus = $result->resourceSets[0]->resources[0]->status;
			//echo "Job created: " . $jobId . "<br>";

			// Call the API to determine the status of all geocode jobs associated with a Bing Maps key
			//echo "Waiting for geocode job completion ...<br>";
			while ($jobStatus != "Completed")
			{
				// Wait 3 seconds, then check the job’s status
				sleep(3);

				// Construct the URL to check the job status, including the jobId
				$checkUrl = "http://spatial.virtualearth.net/REST/v1/Dataflows/Geocode/".$jobId."?output=xml&key=".$key;
				$checkResponse = file_get_contents($checkUrl);
				$responseBody = new SimpleXMLElement($checkResponse);

				// Get and print the description and current status of the job  
				$jobDesc = $responseBody->ResourceSets->ResourceSet->Resources->DataflowJob->Description;
				$jobStatus = $responseBody->ResourceSets->ResourceSet->Resources->DataflowJob->Status;

				echo $jobDesc." - ".$jobStatus."<br>";

				//var_dump($responseBody);
			}

			//echo "Downloading results...<br>";

			$successUrl = '';
			$links = $responseBody->ResourceSets->ResourceSet->Resources->DataflowJob->Link;
			foreach ($links as $link) {
				if ($link['name'] == "succeeded") { 
					$successUrl = $link; 
					break; 
				}
			}

			if($successUrl != '') {
				$successUrl .= "?output=xml&key=".$key;
				$successResponse = file_get_contents($successUrl);
				//file_put_contents("response-$_start-$_end.csv", $successResponse);
				$parsed = str_getcsv($successResponse);

				return [$parsed[20], $parsed[21]];
			}
		}
		// else {
		// 	echo "Job could not be created: " . $result->statusDescription . "<br>";
		// }

		return FALSE;

	}
}
