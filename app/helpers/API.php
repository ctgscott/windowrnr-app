<?php

Class API {	
	public static function respondSuccess($data)
	{
		$result = [];
		if (empty($data))
		{
			$result['status'] = 'fail';
			$result['data'] = 'Request returned NULL.';
		}
		else
		{
			$result['status'] = 'success';
			$result['data'] = $data;
		}
		return $result;
	}

	public static function respondFail($data)
	{
		$result = [];
		$result['status'] = 'fail';
		$result['data'] = $data;
		return $result;
	}

	public static function respondError($data)
	{
		$result = [];
		$result['status'] = 'error';
		$result['data'] = $data;
		return $result;
	}

	public static function respondFailJSend($title, $message, $code = 0)
	{
		$result = [];
		$result['status'] = 'fail';
		$result['data'] = ['title' => $title, 'message' => $message, 'code' => $code];
		return $result;
	}

	public static function respond($data) {
		return Response::make(json_encode($data, JSON_NUMERIC_CHECK), 200, ['Content-Type' => 'application/json; charset=utf-8']);
	}
}