<?php

/*
-- Nov 29
-- Add column status to estimate_items
ALTER TABLE `estimate_items` ADD `status` INT(3)  NULL  DEFAULT NULL  AFTER `options_price`;

-- Add column 'sold' to estimates
ALTER TABLE `estimates` ADD `sold` INT(1)  NULL  DEFAULT '0'  AFTER `description`;

*/

class Estimate extends Eloquent 
{
	use TimeTrait;
	protected $guarded = array();
	public static $rules = array();
	
	public static function getEstimateByID($estID)
	{
		$results = (array) DB::table('estimates')
			->where('id', '=', $estID)
			->first();
			
		$results['items'] = DB::table('estimate_items')			
			->where('est_id', '=', $estID)
			->orderBy('item_num')
			->get();
			
		DB::table('estimate_items')
			->where('est_id', '=', $estID)
			->update(array('archive' => '1'));
		
		return $results;
	}

	public static function getEstimatesByJobID($job_ID)
	{

		$results = DB::table('estimates')			
			->where('job_id', '=', $job_ID)
			->orderBy('created_at')
			->get();

		return $results;
	}
	
	public static function deleteEstimateByID($estID)
	{
		$results['items'] = DB::table('estimate_items')
			->where('est_id', '=', $estID)
			->delete();
			
		$results['est'] = DB::table('estimates')
			->where('id', '=', $estID)
			->delete();
			
		return $results;
	}

	/**
	 * Function to save new job estimate
	 *
	 * @param array $data 
	 */
	public static function addEstimate($data)
	{
		$now = date("Y-m-d H:i:s");
		$data['created_at'] = $now;
		$data['updated_at'] = $now;
		$data['cust_id'] = Job::getCustomerByJobID($data['job_id']);
		
		$id = DB::table('estimates')->insertGetId($data);

		return $id;	
	}

	/**
	 * Function to save new job estimate items
	 *
	 * @param int $job_id 
	 * @param int $user_id
	 * @param string $note
	 */
	public static function addEstimateItems($data)
	{
		$now = date("Y-m-d H:i:s");
		$data['created_at'] = $now;

		DB::table('estimate_items')->insert($data);

		return 'Success';	
	}
	
	/**
	 * Function to update a job estimate
	 *
	 * @param array $data 
	 */
	public static function updateEstimate($id, $data)
	{
		try {
			$now = date("Y-m-d H:i:s");
			$data['updated_at'] = $now;
			
			DB::table('estimates')
				->where('id', $id)
				->update($data);
			
			return 'success';
		} catch (Exception $e) {
			return 'fail';
		}
	}

	/**
	 * Function to sync estimate data with iPad
	 *
	 */
	public static function syncData()
	{
		try {
			$results['rooms'] = DB::table('rooms')
				->get();
			
			$results['dir'] = DB::table('dir')
				->get();
			
			$results['win_style'] = DB::table('win_style')
				->get();

			foreach ($results['win_style'] as $win_style) {
				$win_style->has_balancer = !!$win_style->has_balancer;
			}
			
			$results['win_service'] = DB::table('win_service')
				->get();

			$results['balancers'] = DB::table('balancers')
										->leftJoin('win_service_balancers', 'balancers.id', '=', 'win_service_balancers.balancer_id')
										->select('balancers.*', 'win_service_balancers.win_service_id as service_id')
										->get();
			
			$results['options'] = DB::table('options')
				->get();
			
			$results['parts'] = DB::table('parts')
				->get();

			foreach ($results['parts'] as $part) {
				$part->options = json_decode($part->options);

				$part->win_services = [];
				$win_service_parts = DB::table('win_service_parts')->where('parts_id', '=', $part->id)->select('win_service_id')->get();
				foreach ($win_service_parts as $win_service_part) {
					$part->win_services[] = $win_service_part->win_service_id;
				}

				$part->service_options = [];
				$options_parts = DB::table('options_parts')->where('parts_id', '=', $part->id)->select('options_id')->get();
				foreach ($options_parts as $options_part) {
					$part->service_options[] = $options_part->options_id;
				}
				
			}

			// new additions: calendars and event types
			$results['calendars'] = DB::table('profiles')
				->select('id', 'user_id', 'google_calendar_id', 'sales_check', 'created_at', 'updated_at')
				->get();

			$latest_modified_value = 0;

			foreach ($results as $key => $value) {
				foreach ($value as $akey => $avalue) {
					$latest_modified_value = self::findLastUpdatedTime(property_exists($avalue, 'created_at') ? $avalue->created_at : NULL , property_exists($avalue, 'updated_at') ? $avalue->updated_at : NULL, $latest_modified_value);					
				}
			}

			$results['event_types'] = [
				['id' => 1, 'title' => 'Lead Created'],
				['id' => 2, 'title' => 'Lead Scheduled'],
				['id' => 3, 'title' => 'Quote Created'],
				['id' => 4, 'title' => 'Job Scheduled'],
				['id' => 5, 'title' => 'Sales Receipt Created/Job Complete'],
				['id' => 6, 'title' => 'On Standby'],
				['id' => 7, 'title' => 'Service Call'],
				['id' => 9, 'title' => 'Personal or Office']
			];

			$results['last_updated_at'] = $latest_modified_value;

			return $results;
		} catch (Exception $e) {
			return 'fail';
		}
	}

	/**
	 * Function to update job estimate items
	 *
	 * @param int $job_id 
	 * @param int $user_id
	 * @param string $note
	 */
	public static function updateEstimateItems($id, $data)
	{
		try {
			$now = date("Y-m-d H:i:s");
			$data['updated_at'] = $now;

			DB::table('estimate_items')
				->where('id', $id)
				->update($data);

			return 'Success';	
		} catch (Exception $e) {
		//	return 'fail';
			return $e;
		}
	}
	
	public static function deleteArchive($id)
	{
		DB::table('estimate_items')
			->where('est_id', $id)
			->where('archive', 1)
			->delete();
	}
}