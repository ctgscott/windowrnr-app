<?php

/*

-- Jan 12, 2015

CREATE TABLE `google_watch_channels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` text NOT NULL,
  `calendar_id` text NOT NULL,
  `resource_id` text NOT NULL,
  `expires_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `token` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

*/

class GoogleWatchChannel extends \Eloquent {

	protected $table = 'google_watch_channels';

}