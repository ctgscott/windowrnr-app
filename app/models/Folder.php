<?php

class Folder extends Eloquent 
{
	protected $guarded = array();
	public static $rules = array();
	
	public static function getID($job_id)
	{
		try {
			$folder = DB::table('folders')
				->where('folder_name', $job_id)
				->first();
			return $folder;	
		} catch (Exception $e) {
			return 'fail, error = '.$e;
		}
	}
	
	public static function insert($job_id, $folder_id, $parent_id)
	{
		$now = date("Y-m-d H:i:s");
		$data = [];
		$data['created_at'] = $now;
		$data['folder_name'] = $job_id;
		$data['folder_id'] = $folder_id;
		$data['parent_id'] = $parent_id;

		// Vj: fix for error (while inserting): field `updated_at` doesn't have a default value
		$data['updated_at'] = $now;
		
		try {
			DB::table('folders')->insert($data);
			return 'Success';	
		} catch (Exception $e) {
			return 'fail, error = '.$e;
		}
	}
}