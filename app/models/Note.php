<?php

/*

-- Dec 06

-- Add field 'is_system' to table: notes

ALTER TABLE `notes` ADD `is_system` INT(2)  NULL  DEFAULT NULL  AFTER `note`;

-- Set known system notes as is_system = 1

UPDATE notes SET is_system = 1 
WHERE 
	note LIKE 'Lead detail updated%' OR
	note LIKE 'Lead status changed to:%' OR
	note LIKE 'Estimate appt scheduled for:%' OR
	note LIKE 'Estimate emailed to customer on%' OR
	note LIKE 'Lead created.' OR
	note LIKE 'Lead archived on%' OR
	note LIKE 'Lead archived with Note:%';

-- Add field 'is_system' to view: notes_all

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `notes_all` AS
    select 
        `a`.`id` AS `id`,
        `a`.`job_id` AS `job_id`,
        `a`.`user_id` AS `user_id`,
        `a`.`note` AS `note`,
        `a`.`created_at` AS `created_at`,
        `b`.`first_name` AS `f_name`,
        `b`.`last_name` AS `l_name`,
        `a`.`is_system` AS `is_system`
    from
        (`notes` `a`
        left join `users` `b` ON ((`a`.`user_id` = `b`.`id`)))

*/

class Note extends Eloquent 
{
	protected $guarded = array();
	public static $rules = array();

	/**
	 * Function to create new note
	 *
	 * @param int $job_id 
	 * @param int $user_id
	 * @param string $note
	 */
	public static function createNote($job_id, $user_id, $note, $is_system = 0)
	{
		$now = date("Y-m-d H:i:s");

		$data = array(
			'job_id'  => $job_id, 
			'user_id' => $user_id, 
			'note'    => $note,
			'is_system' => $is_system,
			'created_at' => $now,
			'updated_at' => $now
		);

		DB::table('notes')->insert($data);

		return 'Success';	
	}

	public static function notesByJobID($job_id, $noSystem = null)
	{
		if(!isset($noSystem)) {
			$results = DB::table('notes_all')
				->select('user_id', 'note', 'created_at', 'f_name', 'l_name', 'is_system')
				->where('job_id', '=', $job_id)
				->orderBy('created_at', 'asc') // latest note should appear last
				->get();
		} else {
			$results = DB::table('notes_all')
				->select('user_id', 'note', 'created_at', 'f_name', 'l_name', 'is_system')
				->where('job_id', '=', $job_id)
				->where('is_system', '=', 0)
				->orderBy('created_at', 'asc') // latest note should appear last
				->get();
		}
		
		return $results;
	}

	public static function notesByWebPost()
	{
		$results = DB::table('notes_all')
			->select('job_id', 'note', 'created_at', 'f_name')
			->where('user_id', '=', '999')
			->orderBy('created_at', 'desc') // latest note should appear first
			->get();

		return $results;
	}
}
