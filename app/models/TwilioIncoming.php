<?php

class TwilioIncoming extends Eloquent {
	protected $table = 'twilio_incoming';
    public $timestamps = true;


    public static function getContacts()
    {
        $contact = DB::table('twilio_incoming')
            ->distinct()
            ->orderBy('created_at', 'desc')
            ->get(['from']); 

        return $contact;
    }
	
	public static function getContactsMasked()
	{
        $contact = DB::table('twilio_incoming')
            ->distinct()
            ->orderBy('created_at', 'desc')
            ->get(['from']); 

		foreach ($contact as $from) 
		{
			$areaCode = substr($from->from,0,3);
			$prefix = substr($from->from,3,3);
			$lastFour = substr($from->from,6,4);
			$from->from = '('.$areaCode.') '.$prefix.'-'.$lastFour;
		}
		
        return $contact;
	}
    
    public static function getMsgs($phone) 
    {
        $msgs = DB::table('twilio_incoming')
            ->where('from', '=', $phone)
            ->orWhere('to', '=', $phone)
			->orderBy('created_at', 'desc')
            ->get(['body', 'created_at', 'from', 'to']);
            
        return $msgs;
    }
	
	public static function updateStatus($msgUpdate)
	{
//		var_dump($msgUpdate);
		DB::table('twilio_incoming')
			->where('message_sid', $msgUpdate['message_sid'])
			->update($msgUpdate);
	}
}