<?php

class GoogleCalendarChannels extends \Eloquent {

	protected $table = 'google_calendar_channels';

	protected $fillable = [];

	
    public function user()
    {
        return $this->belongsTo('User');
    }
}