<?php

/*

-- 18-Nov 2014

-- Make state nullable
ALTER TABLE `jobs` CHANGE `state` `state` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_unicode_ci  NULL  DEFAULT '';

-- Add timestamps for when a job is accepted or put on standby
ALTER TABLE `jobs` ADD `job_accepted` TIMESTAMP  NULL  DEFAULT NULL  AFTER `updated_at`;
ALTER TABLE `jobs` ADD `job_put_on_standby` TIMESTAMP  NULL  DEFAULT NULL  AFTER `job_accepted`;

-- Add accepted and put-on-standby timestamps to the view: job_cust
CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `job_cust` AS
    select distinct
        `a`.`id` AS `customer_id`,
        `b`.`id` AS `job_id`,
        `a`.`l_name` AS `customer_lname`,
        `a`.`f_name` AS `customer_fname`,
        `a`.`phone` AS `customer_phone`,
        `a`.`alt_phone` AS `customer_altphone`,
        `a`.`email` AS `customer_email`,
        `a`.`billing_address` AS `billing_address`,
        `a`.`billing_city` AS `billing_city`,
        `a`.`billing_state` AS `billing_state`,
        `a`.`billing_zip` AS `billing_zip`,
        `b`.`created_at` AS `job_created_at`,
        `b`.`created_by` AS `job_created_by`,
        `b`.`address` AS `job_address`,
        `b`.`city` AS `job_city`,
        `b`.`zip` AS `job_zip`,
        `b`.`built` AS `built`,
        `b`.`lead_source` AS `lead_source`,
        `b`.`status` AS `job_status`,
        `b`.`archive` AS `job_archive`,
        `b`.`job_accepted` AS `job_accepted_at`,
        `b`.`job_put_on_standby` AS `job_put_on_standby_at`
    from
        (`customers` `a`
        join `jobs` `b` ON ((`a`.`id` = `b`.`customer_id`)))
    order by `b`.`id`


-- 20-Nov 2014

-- Add completed timestamp to view: job_cust
CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `job_cust` AS
    select distinct
        `a`.`id` AS `customer_id`,
        `b`.`id` AS `job_id`,
        `a`.`l_name` AS `customer_lname`,
        `a`.`f_name` AS `customer_fname`,
        `a`.`phone` AS `customer_phone`,
        `a`.`alt_phone` AS `customer_altphone`,
        `a`.`email` AS `customer_email`,
        `a`.`billing_address` AS `billing_address`,
        `a`.`billing_city` AS `billing_city`,
        `a`.`billing_state` AS `billing_state`,
        `a`.`billing_zip` AS `billing_zip`,
        `b`.`created_at` AS `job_created_at`,
        `b`.`created_by` AS `job_created_by`,
        `b`.`address` AS `job_address`,
        `b`.`city` AS `job_city`,
        `b`.`zip` AS `job_zip`,
        `b`.`built` AS `built`,
        `b`.`lead_source` AS `lead_source`,
        `b`.`status` AS `job_status`,
        `b`.`archive` AS `job_archive`,
        `b`.`job_accepted` AS `job_accepted_at`,
        `b`.`job_put_on_standby` AS `job_put_on_standby_at`,
        `b`.`job_completed` AS `job_completed_at`
    from
        (`customers` `a`
        join `jobs` `b` ON ((`a`.`id` = `b`.`customer_id`)))
    order by `b`.`id`


-- 2-Dec 2014

-- Update definition of view: cust_map (for use inside the marker bubble via custMap.json)
CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `cust_map` AS
    select 
        `a`.`id` AS `id`,
        `a`.`l_name` AS `l_name`,
        `a`.`f_name` AS `f_name`,
        `b`.`id` AS `job_id`,
        `b`.`lat` AS `lat`,
        `b`.`lng` AS `lng`,
        `b`.`address` AS `address`,
        `b`.`city` AS `city`,
        `b`.`zip` AS `zip`,
        `b`.`status` AS `job_status`,
        `b`.`job_accepted` AS `job_accepted`,
        `b`.`job_put_on_standby` AS `job_put_on_standby`,
        `b`.`job_scheduled` AS `job_scheduled`,
        `b`.`job_completed` AS `job_completed`
    from
        (`customers` `a`
        left join `jobs` `b` ON ((`a`.`id` = `b`.`customer_id`)))

*/

class Job extends Eloquent 
{
	protected $guarded = array();
	public static $rules = array();
	
	public function customer()
	{
		return $this->belongsTo('Customer');
	}
	
	public function notes()
	{
		return $this->hasMany('Note');
	}
	
	public function window_total()
	{
		return $this->hasOne('Window_total');
	}
	
	public function windows()
	{
		return $this->hasMany('Window');
	}

	public function appointments()
	{
		return $this->hasMany('Appointment');
	}

	public static function getJobCustNotes($jobId)
	{
		$results = DB::table('job_cust')
			->where('job_cust.job_id', '=', $jobId)
			->first();
		
		$results->notes = (object)Note::notesByJobID($jobId);
//		$results->notes = (isset ($notes) ? $notes : '');
		
//		DB::table('notes_all')->where('job_id', '=', $jobId)->get();
		
		return $results;
	}
	public static function getJobCustomers($jobId = null, $status = null, $archive = null)
	{
		$res = DB::table('job_cust')
			->orderby('job_id');
		
		if(!is_null($status))
		{
			$res->where('job_cust.job_status', '=', $status);
		}
			
		if(!is_null($archive))	
		{			
			$res->where('job_cust.job_archive', '=', $archive);
		}

		if(!is_null($jobId))
		{
			$res->where('job_cust.job_id', '=', $jobId);
		}

		return $res->get();
	}

	public static function getJobsByStatus($status, $scheduled = null)
	{
		if($status == 0) {
			$res = DB::table('job_cust')->orderby('job_id', 'desc')->where('job_cust.job_archive', '<>', 1)->get();
		}
		else {
			$res = DB::table('job_cust')->orderby('job_id', 'desc')->where('job_cust.job_status', '=', $status)->where('job_cust.job_archive', '<>', 1)->get();
		}
		
		if($status == JOB_ACCEPTED) {
			if($scheduled === '1') { // job status is JOB_ACCEPTED and has one or more event(s) of type "Install/Repair Job"
				$res = array_filter($res, function($_model) {
					return DB::table('events')->where('job_id', '=', $_model->job_id)->where('type', '=', EVENT_INSTALL_REPAIR)->count() > 0;
				});
			}
			else {
				$res = array_filter($res, function($_model) {
					return DB::table('events')->where('job_id', '=', $_model->job_id)->where('type', '=', EVENT_INSTALL_REPAIR)->count() === 0;
				});
			}
		}
			
		return $res;
	}

	public static function getArchivedJobs()
	{
		return DB::table('job_cust')->orderby('job_id')->where('job_cust.job_archive', '=', 1)->get();
	}

	public static function getJobCustomer($jobId)
	{
		$res = DB::table('job_cust')->where('job_id', '=', $jobId)->first();

		return $res;
	}
	/*
	 * Update status of the job. The status should be 
	 * 1 = Lead created
	 * 1.1 = web post, we called back, left VM
	 * 2 = Lead Scheduled
	 * 3 = Quote created
	 * 4 = Job scheduled
	 * 5 = Sales receipt created/Job complete
	 *
	 * @param int $id
	 * @param int $status
	 *
	 */	
	public static function updateStatus($id, $status)
	{		
		DB::table('jobs')
			->where('id', $id)
			->update(array('status' => $status));
			
		$job_status = Config::get('app_config.job_status');		
			$result = Note::createNote($id, Sentry::getUser()->id, 'Lead status changed to: '.$job_status[$status], 1);
			return $result;
	}

	public static function getCustomerByJobID($jobID)
	{
		$cust = DB::table('jobs')
			->where('id', $jobID)
			->pluck('customer_id');
			
		return $cust;
	}

	public static function getJobsByCustID($custID)
	{
		$jobs = DB::table('jobs')
			->where('customer_id', $custID)
			->get();
			
		return $jobs;
	}
}