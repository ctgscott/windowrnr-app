<?php


class Customer extends Eloquent {

	protected $guarded = array();

	public static $rules = array();
	
	public function jobs()
	{
		return $this->hasMany('Job');
	}

	public static function getCustomer($id = null)
	{
		$res = DB::table('customers')
				->select(
					'customers.id as customer_id', 
					'customers.l_name as customer_lname', 
					'customers.f_name as customer_fname', 
					'customers.company_name as company_name', 
					'customers.phone as customer_phone', 
					'customers.alt_phone as customer_altphone', 
					'customers.email as customer_email', 
					'customers.billing_address as billing_address',
					'customers.billing_city as billing_city',
					'customers.billing_state as billing_state',
					'customers.billing_zip as billing_zip'
				);

		if(!is_null($id))
		{
			$res->where('customers.id', '=', $id);
		}
				
		return $res->get();
	}
}