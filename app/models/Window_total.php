<?php

class Window_total extends Eloquent {

	protected $table = 'window_totals';

	protected $guarded   = array();
	public static $rules = array();

	public function job()
	{
		return $this->belongsTo('Job');
	}

	public static function totalsByJobID($job_id)
	{
		$results = DB::table('window_totals')
			->select('id', 'qty', 'material', 'style')
			->where('job_id', '=', $job_id)
			->get();

		return $results;
	}
}