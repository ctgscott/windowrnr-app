<?php

class Option extends Eloquent 
{
	protected $guarded = array();
	public static $rules = array();

	/**
	 * Function to create new service option
	 *
	 * @param int $job_id 
	 * @param int $user_id
	 * @param string $note
	 */
	public static function createOption($serv_id, $title, $description, $price)
	{
		$data = array(
			'serv_id'  => $serv_id, 
			'title' => $title, 
			'description'    => $description,
			'price' => $price
		);

		DB::table('options')->insert($data);

		return 'Success';	
	}

	public static function optionsByServID($serv_id)
	{
		$results = DB::table('options')->where('serv_id', '=', $serv_id)->get();
		
		return $results;
	}
}