<?php

/*
-- 21-Nov 2014

-- Change of length for htmlLink
ALTER TABLE `events` CHANGE `htmlLink` `htmlLink` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_unicode_ci  NOT NULL  DEFAULT '';

-- 26-Dec 2014

-- Add assignee field to event table
ALTER TABLE `events` ADD `assignee` VARCHAR(100)  NULL  DEFAULT NULL  AFTER `description`;

-- Add assignee field to calendar_all view
CREATE ALGORITHM=UNDEFINED DEFINER=`windowcustomer`@`%` SQL SECURITY DEFINER VIEW `calendar_all`
AS SELECT
   distinct `a`.`id` AS `id`,
   `a`.`google_event_id` AS `google_event_id`,
   `a`.`google_cal_id` AS `google_cal_id`,
   `a`.`cust_id` AS `cust_id`,
   `a`.`job_id` AS `job_id`,
   `a`.`cal_user_id` AS `cal_user_id`,
   `a`.`cal_user_id` AS `className`,
   `a`.`avatar` AS `avatar`,
   `a`.`start` AS `start`,
   `a`.`end` AS `end`,
   `a`.`location` AS `location`,
   `a`.`allDay` AS `allDay`,
   `a`.`title` AS `title`,
   `a`.`htmlLink` AS `url`,
   `a`.`assignee` AS `assignee`,
   `a`.`event_type` AS `event_type`,
   `a`.`lat` AS `lat`,
   `a`.`lng` AS `lng`,
   `a`.`created_at` AS `created_at`,
   `a`.`updated_at` AS `updated_at`,
   `b`.`sales_check` AS `sales_check`
FROM (`events` `a` left join `profiles` `b` on((`a`.`cal_user_id` = `b`.`user_id`))) where (`b`.`sales_check` = 1);

-- Enlarge description field in events table
ALTER TABLE `events` CHANGE `description` `description` LONGTEXT  CHARACTER SET utf8  COLLATE utf8_unicode_ci  NOT NULL;

-- 22-Jan 2015

-- Add type (int) field 
ALTER TABLE `events` ADD `type` INT(2)  NULL  DEFAULT NULL  AFTER `event_type`;
UPDATE events SET type = 1 where event_Type = 'Estimate Appt.';
UPDATE events SET type = 2 where event_Type = 'Install/Repair Job';
UPDATE events SET type = 3 where event_Type = 'Service Call';

*/

//use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Events extends Eloquent 
{
	//used for softdelete
	//use SoftDeletingTrait;
    //protected $dates = ['deleted_at'];

	protected $guarded = array();
	public static $rules = array();

	private $eventData = array();

	private static function setEventData($event)
	{
		if(isset($event['job']))
		{
			$event['cust_id'] = DB::table('jobs')->where('id', $event['job'])->pluck('customer_id');
		}

		if (isset($event['organizer']) && isset($event['organizer']['email'])) 
		{
			$event['cal_user_id'] = DB::table('profiles')->where('google_calendar_id', $event['organizer']['email'])->pluck('user_id');
		}

		if($event['summary'] == null) 
		{
			$event['summary'] = 'x';
		}

		if (isset($event['location'])) 
		{
			$address = urlencode($event['location']);
		} 
		else 
		{
			$address = null;
			$event['location'] = null;
		}

		if (isset($event['start']['date'])) 
		{
			$event['all_day'] = 1;
			$eventStart = strtotime($event['start']['date']);
			$eventEnd = strtotime($event['end']['date']);
		}
		else 
		{
			$event['all_day'] = 0;
			$eventStart = strtotime($event['start']['dateTime']);
			$eventEnd = strtotime($event['end']['dateTime']);
		}
		
		if(!empty($address)) {
			$geocode_key = Config::get('app.google_geocode_api_key');
			$latlng = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=".$geocode_key));
		}
		
		if (isset($latlng->results[0]->geometry->location->lat)) 
		{
			$lat = $latlng->results[0]->geometry->location->lat;
			$lng = $latlng->results[0]->geometry->location->lng;
		}
		else 
		{
			$lat = null;
			$lng = null;
		}
		
		$avatar = DB::table('profiles')->where('user_id', '=', $event['cal_user_id'])->pluck('avatar');
		
		if ($avatar == null) 
		{
			$avatar = "WinPin2.png";
		}

		$event_data = array(
			'google_event_id' => $event['id'],
			'google_cal_id'   => isset($event['organizer']['email']) ? $event['organizer']['email'] : '',
			'cust_id' 		  => isset($event['cust_id']) ? $event['cust_id'] : 0,
			'job_id' 		  => isset($event['job']) ? $event['job'] : 0,
			'cal_user_id' 	  => $event['cal_user_id'],
			'avatar' 		  => $avatar,
			'status' 		  => $event['status'],
			'creatorEmail'    => $event['creator']['email'],
			'organizerEmail'  => isset($event['organizer']['email']) ? $event['organizer']['email'] : '',
			'htmlLink' 		  => $event['htmlLink'],
			'start' 		  => date("Y-m-d H:i:s", $eventStart),
			'end' 			  => date("Y-m-d H:i:s", $eventEnd),
			'location' 		  => isset($event['location']) ? $event['location'] : '',
			'description'     => isset($event['description']) ? $event['description'] : '',
			'allDay' 		  => $event['all_day'],
			'title' 		  => $event['summary'],
			'type'			  => isset($event['type']) ? $event['type'] : '',
			'event_type'	  => isset($event['event_type']) ? $event['event_type'] : '',
			'lat' 			  => $lat,
			'lng' 			  => $lng,
			'updated_at'	  => date("Y-m-d H:i:s"),
		);

		if (isset($event['created_at'])) 
		{
			$event_data['created_at'] = $event['created_at'];
		}
		
		return $event_data;
	}

	public static function insertEventData($event, $table = 'events')
	{
		$data = self::setEventData($event);

		$id = DB::table($table)->insertGetId($data);

		return $id;
	}

	public static function updateEventData($event, $table = 'events', $setjob = false)
	{
		if($event->status == 'cancelled')
		{
			$data = array('status' => $event->status);
		} 
		else
		{
			$data = self::setEventData($event);
			//unset variables
			if(!$setjob)
				unset($data['job_id'], $data['cust_id'], $data['cal_user_id'], $data['google_cal_id']);
		}

		$res = DB::table($table)->where('google_event_id', $event->id)->update($data);

		return $res;
	}

	public static function updateEventDataWithCalendar($event, $table = 'events', $setjob = false)
	{
		if($event->status == 'cancelled')
		{
			$data = array('status' => $event->status);
		} 
		else
		{
			$data = self::setEventData($event);
			//unset variables
			if(!$setjob)
				unset($data['job_id'], $data['cust_id']);
		}

		$res = DB::table($table)->where('google_event_id', $event->id)->update($data);

		return $res;
	}
	
	public static function getEventsByJob($jobID)
	{
		$events = DB::table('events')->where('job_id', $jobID)->get();
		
		return $events;
	}

	public static function getEventsByID($id)
	{
		$events = DB::table('events')
			->where('id', $id)
			->get();
				
		return $events;
	}

	public static function getEventByID($id)
	{
		$events = DB::table('events')
			->where('id', $id)
			->first();
				
		return $events;
	}

	public static function getEventByGoogID($id)
	{
		$event = DB::table('events')
			->where('google_event_id', $id)
			->first();
				
		return $event;
	}
}
