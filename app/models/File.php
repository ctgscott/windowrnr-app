<?php

// create table SQL
/*
CREATE TABLE `files` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
*/

/*
-- 23 Nov, 2014

-- Add column 'uploaded_at'
ALTER TABLE `files` ADD `uploaded_at` TIMESTAMP  NULL  DEFAULT NULL  AFTER `type`;

-- Add column 'download_url'
ALTER TABLE `files` ADD `download_url` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `path`;

*/

class File extends Eloquent {

}