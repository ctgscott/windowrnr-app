<?php

class Appointment extends Eloquent 
{
	protected $guarded = array();
	public static $rules = array();
	
	public function job()
	{
		return $this->belongsTo('Job');
	}

	/**
	* $cust_id is optional
	* $allDay is optional, if absent defaults to 'no'
	*/
	public static function insertData($job_id, $event_id, $user, $start, $end, $allDay, $url, $cust_id=null)
	{
		$id = DB::table('appointments')->insertGetId(
			array(
				'job_id'     => $job_id, 
				'cust_id'    => $cust_id, 
				'event_id'   => $event_id,
				'user'       => $user,
				'appt_start' => $start, 
				'appt_end'   => $end, 
				'all_day'    => $allDay, 
				'appt_url'   => $url, 
				'created_at' => date("Y-m-d H:i:s")
			)
		);
		$result = array('status' => 'Success', 'id' => $id);
		return $result;
	}
	
	public static function getApptByEventID($event_id)
	{
		$appt = DB::table('appointments')->where('event_id', $event_id)->first();
		
		return $appt;
	}
	
	public static function getAppt($appt_id)
	{
		$appt = DB::table('appointments')->where('id', $appt_id)->first();
		
		return $appt;
	}
}
