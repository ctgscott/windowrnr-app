<?php

// create table SQL
/*
CREATE TABLE `images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
*/

// 'rotation' field addition SQL
/*
ALTER TABLE `images` ADD `rotation` INT(4)  NULL  DEFAULT 0  AFTER `title`;
UPDATE `images` SET `rotation` = '0';
*/

class Image extends Eloquent {

}