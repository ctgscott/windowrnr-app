<?php

class GoogleEventToken extends Eloquent {

	protected $table = 'google_event_token';

	protected $guarded = array('id');

	protected $fillable = [];
 
	/*
	 *	Function to get the latest token 
	 *
	 * @return Db object
	 */
	public function getToken()
	{
		$res = DB::table('google_event_token')->orderby('created_at', 'desc')->get();

		return $res;
	}

}