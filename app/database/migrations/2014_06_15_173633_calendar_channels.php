<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CalendarChannels extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('google_calendar_channels', function($table)
		{
		    $table->increments('id');
		    $table->integer('user_id');
		    $table->string('google_channel_id');
		    $table->string('google_resource_id');
		    $table->text('resource_uri');
		    $table->integer('expiration');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('google_calendar_channels');
	}

}
