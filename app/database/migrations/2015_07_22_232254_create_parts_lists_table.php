<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartsListsTable extends Migration {

	public function up() {
		Schema::create('parts_lists', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('job_id');
			$table->integer('estimate_id');
			$table->string('title');
			$table->longText('parts');
			$table->tinyInteger('status');
			$table->timestamps();
		});
	}

	public function down() {
		Schema::drop('parts_lists');
	}

}
