<!DOCTYPE html>
<html lang="en" ng-app="sopAdminApp">
	<head>
		<meta charset="utf-8" />
		<title>Services / Options / Parts</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
		<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
		<script src="/js/ext/ui-bootstrap-1.0.3.min.js"></script>
		<script src="/js/ext/ui-bootstrap-tpls-1.0.3.js"></script>
		<script src="/sopadmin/controller.js"></script>

		<link href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

		<link href="{{ asset('css/nav.css') }}" rel="stylesheet">

		<link href="/css/service-settings.css" rel="stylesheet">
		<link href="/sopadmin/index.css" rel="stylesheet">

		<style>
			body {
				padding-top: 60px;
			}
		</style>
	</head>

	<body class="wrnr-ng-body" ng-controller="SOPAdminCtrl">
		
		<!-- Navbar -->
		@include('layouts/nav')

		<div class="container-full">
			<div class="title">
				<h3>Services / Options / Parts</h3>
			</div>

			<table class="columns">
				<tr>
					<!-- <td class="column column-styles" ng-if="styles && styles.length"> -->
						<td class="column column-styles">
						<div class="title">
							Styles
							<div class="buttons">
								<button ng-if="!stylesExpanded" class="btn btn-xs btn-default" ng-click="expand('styles', true)"><i class="glyphicon glyphicon-resize-full"></i></button>
								<button ng-if="stylesExpanded" class="btn btn-xs btn-default" ng-click="expand('styles', false)"><i class="glyphicon glyphicon-resize-small"></i></button>
								<button class="btn btn-xs btn-success add" ng-click="addStyle()"><i class="glyphicon glyphicon-plus"></i></button>
							</div>
							<div class="sizer"></div>
						</div>
						<ul ng-if="!stylesExpanded && styles && styles.length">
							<li ng-repeat="style in styles " data-id="[: style.id :]">
								<button class="btn btn-sm [: style.id === currentStyleID ? 'btn-primary' : 'btn-default btn-dimmed' :]" ng-click="loadServices(style.id)">
									[: style.winStyle :]
									<div class="buttons">
										<a class="edit" ng-click="editStyle(style); $event.stopPropagation();"><i class="glyphicon glyphicon-pencil"></i></a
										><a class="delete"ng-click="deleteStyle(style); $event.stopPropagation();"><i class="glyphicon glyphicon-remove"></i></a>
									</div>
								</button>
							</li>
						</ul>
						<table ng-if="stylesExpanded && styles && styles.length" class="expanded-view">
							<thead>
								<tr>
									<th class="enarrow ecenter">#<div class="sizer"></div></th>
									<th class="eleft">Style<div class="sizer"></div></th>
									<th class="eleft">Balancer?<div class="sizer"></div></th>
									<th class="eleft">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="style in styles" data-id="[: style.id :]" ng-click="loadServices(style.id)" class="[: style.id === currentStyleID ? 'current' : '' :]">
									<td class="enarrow ecenter">[: style.id :]</td>
									<td class="eleft"><span class="enowrap" title="[: style.winStyle :]">[: style.winStyle :]</span></td>
									<td class="eleft">[: !!style.has_balancer :]</td>
									<td class="eleft enopadding">
										<div class="buttons enowrap">
											<a class="edit" ng-click="editStyle(style); $event.stopPropagation();"><i class="glyphicon glyphicon-pencil"></i></a
											><a class="delete"ng-click="deleteStyle(style);$event.stopPropagation();"><i class="glyphicon glyphicon-remove"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="no-items" ng-if="!styles || !styles.length">
							No styles present. To add one, click <a ng-click="addStyle()">here</a>
						</div>				
					</td>
					<td class="column column-services" ng-if="currentStyleID">
						<div class="title">
							Services 
							<div class="buttons">
								<button ng-if="!servicesExpanded" class="btn btn-xs btn-default" ng-click="expand('services', true)"><i class="glyphicon glyphicon-resize-full"></i></button>
								<button ng-if="servicesExpanded" class="btn btn-xs btn-default" ng-click="expand('services', false)"><i class="glyphicon glyphicon-resize-small"></i></button>
								<button class="btn btn-xs btn-success add" ng-click="addService()"><i class="glyphicon glyphicon-plus"></i></button>
								<button class="btn btn-xs btn-success" ng-click="mapStyles()"><i class="glyphicon glyphicon-retweet"></i></button>
							</div>
							<div class="sizer"></div>
						</div>
						<ul ng-if="!servicesExpanded && services && services.length">
							<form name="serviceForm">
								<li ng-repeat="service in services">
									<button class="btn btn-sm [: service.id === currentServiceID ? 'btn-primary' : 'btn-default btn-dimmed' :]" ng-click="loadOptions(service.id)">
										<input type="checkbox" class="servCheckbox" value="[: {id: service.id, title: service.title} :]">
										[: service.title :]
										<div class="buttons enowrap">
											<a class="show-parts" ng-click="loadServiceParts(service.id); $event.stopPropagation();"><i class="glyphicon glyphicon-magnet"></i></a
											><a class="edit" ng-click="editService(service); $event.stopPropagation();"><i class="glyphicon glyphicon-pencil"></i></a
											><a class="delete"ng-click="deleteService(service);$event.stopPropagation();"><i class="glyphicon glyphicon-remove"></i></a>
										</div>
									</button>
								</li>
							</form>
						</ul>
						<table ng-if="servicesExpanded && services && services.length" class="expanded-view">
							<thead>
								<tr>
									<th class="enarrow ecenter">#<div class="sizer"></div></th>
									<th class="eleft">Title<div class="sizer"></div></th>
									<th class="eleft">Description<div class="sizer"></div></th>
									<th class="ecenter">Price<div class="sizer"></div></th>
									<th class="eleft">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="service in services" data-id="[: service.id :]" ng-click="loadOptions(service.id)" class="[: service.id === currentServiceID ? 'current' : '' :]">
									<td class="enarrow ecenter">[: service.id :]</td>
									<td class="eleft"><span class="enowrap" title="[: service.title :]">[: service.title :]</span></td>
									<td class="eleft"><span class="enowrap" title="[: service.description :]">[: service.description :]</span></td>
									<td class="ecenter">[: service.price :]</td>
									<td class="eleft enopadding">
										<div class="buttons enowrap">
											<a class="show-parts" ng-click="loadServiceParts(service.id); $event.stopPropagation();"><i class="glyphicon glyphicon-magnet"></i></a
											><a class="edit" ng-click="editService(service); $event.stopPropagation();"><i class="glyphicon glyphicon-pencil"></i></a
											><a class="delete"ng-click="deleteService(service);$event.stopPropagation();"><i class="glyphicon glyphicon-remove"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="no-items" ng-if="!services || !services.length">
							No services mapped to the selected window style. To add one, click <a ng-click="addService()">here</a>
						</div>
					</td>
					<td class="column column-options" ng-if="currentServiceID && !serviceParts">
						<div class="title">
							Options
							<div class="buttons">
								<button ng-if="!optionsExpanded" class="btn btn-xs btn-default" ng-click="expand('options', true)"><i class="glyphicon glyphicon-resize-full"></i></button>
								<button ng-if="optionsExpanded" class="btn btn-xs btn-default" ng-click="expand('options', false)"><i class="glyphicon glyphicon-resize-small"></i></button>
								<button class="btn btn-xs btn-success add" ng-click="addOption()"><i class="glyphicon glyphicon-plus"></i></button>
							</div>
							<div class="sizer"></div>
						</div>
						<ul ng-if="!optionsExpanded && options && options.length">
							<li ng-repeat="option in options">
								<button class="btn btn-sm [: option.id === currentOptionID ? 'btn-primary' : 'btn-default btn-dimmed' :]" ng-click="loadParts(option.id)">
									[: option.title :]
									<div class="buttons">
										<a class="edit" ng-click="editOption(option); $event.stopPropagation();"><i class="glyphicon glyphicon-pencil"></i></a
										><a class="delete"ng-click="deleteOption(option);$event.stopPropagation();"><i class="glyphicon glyphicon-remove"></i></a>
									</div>
								</button>
							</li>
						</ul>
						<table ng-if="optionsExpanded && options && options.length" class="expanded-view">
							<thead>
								<tr>
									<th class="enarrow ecenter">#<div class="sizer"></div></th>
									<th class="eleft">Title<div class="sizer"></div></th>
									<th class="eleft">Description<div class="sizer"></div><div class="sizer"></div></th>
									<th class="ecenter">Price<div class="sizer"></div></th>
									<th class="eleft">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="option in options" data-id="[: option.id :]" ng-click="loadParts(option.id)">
									<td class="enarrow ecenter">[: option.id :]</td>
									<td class="eleft"><span class="enowrap" title="[: option.title :]">[: option.title :]</span></td>
									<td class="eleft"><span class="enowrap" title="[: option.description :]">[: option.description :]</span></td>
									<td class="ecenter">[: option.price :]</td>
									<td class="eleft enopadding">
										<div class="buttons enowrap">
											<a class="edit" ng-click="editOption(option); $event.stopPropagation();"><i class="glyphicon glyphicon-pencil"></i></a
											><a class="delete"ng-click="deleteOption(option);$event.stopPropagation();"><i class="glyphicon glyphicon-remove"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="no-items" ng-if="!options || !options.length">
							No service options mapped to the selected window service. To add one, click <a ng-click="addOption()">here</a>
						</div>
					</td>
					<td class="column column- " ng-if="currentOptionID || serviceParts">
						<div class="title">
							Parts
							<div class="buttons">
								<button ng-if="!partsExpanded" class="btn btn-xs btn-default" ng-click="expand('parts', true)"><i class="glyphicon glyphicon-resize-full"></i></button>
								<button ng-if="partsExpanded" class="btn btn-xs btn-default" ng-click="expand('parts', false)"><i class="glyphicon glyphicon-resize-small"></i></button>
								<button class="btn btn-xs btn-success add" ng-click="addPart()" ><i class="glyphicon glyphicon-plus"></i></button>
							</div>
							<div class="sizer"></div>
						</div>
						<ul ng-if="!partsExpanded && parts && parts.length">
							<li ng-repeat="part in parts">
								<button class="btn btn-sm [: part.id === currentpartID ? 'btn-primary' : 'btn-default btn-dimmed' :]">
									[: part.name :]
									<div class="buttons">
										<a class="edit" ng-click="editPart(part); $event.stopPropagation();"><i class="glyphicon glyphicon-pencil"></i></a
										><a class="delete"ng-click="deletePart(part);$event.stopPropagation();"><i class="glyphicon glyphicon-remove"></i></a>
									</div>
								</button>
							</li>
						</ul>
						<table ng-if="partsExpanded && parts && parts.length" class="expanded-view">
							<thead>
								<tr>
									<th class="enarrow ecenter">#<div class="sizer"></div></th>
									<th class="eleft">Title<div class="sizer"></div></th>
									<th class="eleft">Description<div class="sizer"></div><div class="sizer"></div></th>
									<th class="ecenter">Price<div class="sizer"></div></th>
									<th class="eleft">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="part in parts" data-id="[: part.id :]" class="[: part.id === currentPartID ? 'current' : '' :]">
									<td class="enarrow ecenter">[: part.id :]</td>
									<td class="eleft"><span class="enowrap" title="[: part.name :]">[: part.name :]</span></td>
									<td class="eleft"><span class="enowrap" title="[: part.description :]">[: part.description :]</span></td>
									<td class="ecenter">[: part.price :]</td>
									<td class="eleft enopadding">
										<div class="buttons enowrap">
											<a class="edit" ng-click="editPart(part); $event.stopPropagation();"><i class="glyphicon glyphicon-pencil"></i></a
											><a class="delete"ng-click="deletePart(part);$event.stopPropagation();"><i class="glyphicon glyphicon-remove"></i></a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>						
						<div class="no-items" ng-if="!parts || !parts.length">
							No parts mapped to the selected service option. To add one, click <a ng-click="addPart()">here</a>
						</div>
					</td>
				</tr>
			</table>
		</div>

		<!-- Javascripts -->

		<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

		<script type='text/javascript' src="{{ asset('js/main.js') }}"></script>

		<script type='text/javascript' src="/sopadmin/column-sizer.js"></script>
		<!-- ./ Javascripts -->

	</body>
</html>
