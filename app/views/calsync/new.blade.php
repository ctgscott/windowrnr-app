@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
SYNC
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
<h4>SYNC</h4>
<form id="form-add-setting" class="form-inline" action="/calsync" method="POST">
{{ Form::token() }}
<div>
		<h4><em>Sync</em></h4>
<!--<input placeholder="Enter Setting Name" id="setting_name" class="input-small typeahead tt-query" name="setting_name" type="text" value="" 
autocomplete="off" required>

		<input placeholder="Enter Setting Value" id="setting_value" class="input-small" name="setting_value" type="text" value="" required>-->
		<input class="btn-primary btn" type="submit" id="btn-add-setting">
	</div>
	<div id="res">
    
    </div>
</form>
@stop

@section('scripts')
<script type='text/javascript' src="{{ asset('js/calsync/new.js') }}"></script>
@stop
