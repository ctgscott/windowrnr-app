@extends('layouts.fullwidth')

{{-- Web site Title --}}
@section('title')
Schedule Appointments
@stop

@section('styles')
<meta name="googMapApiKey" content={{$googMapApiKey}}>
<link rel='stylesheet' type='text/css' href="{{ asset('js/ext/fullcalendar/fullcalendar.css') }}" />
<link rel='stylesheet' type='text/css' href="{{ asset('css/leaflet.css') }}" />
<link rel='stylesheet' type='text/css' href="{{ asset('css/jquery-ui-1.10.3.custom.min.css') }}" />
<link rel='stylesheet' type='text/css' href="{{ asset('js/ext/jquery.validationEngine/validationEngine.jquery.css') }}" />
<link rel='stylesheet' type='text/css' href="{{ asset('css/customer.css') }}" >
<link rel='stylesheet' type='text/css' href="{{ asset('css/schedule.css') }}" >
<link rel='stylesheet' type='text/css' href="{{ asset('css/bootstrap-datepicker3.css') }}" >
@stop

{{-- Content --}}
@section('content')
@foreach($profiles as $profile) 
	<input type="hidden" id="start-location-uid-{{ $profile['id'] }}" value="{{ $profile['start_location'] }}">
	<input type="hidden" id="color-uid-{{ $profile['id'] }}" value="{{ $profile['color'] }}">
@endforeach
<h4>Estimates Appointments</h4>
<div class="avatarList">
	@foreach ($profiles as $seller)
		<img src="{{ '/img/'.$seller['avatar'] }}" />
		<input type="checkbox" id="salescheckbox{{ $seller['id'] }}" value="{{ $seller['id'] }}" name="salesTeam[]" @if ($seller['sales'] == '1') checked @endif > {{ $seller['first_name'] }}
	@endforeach
	<button id="reset_page" class="goog-buttonset-action cool-button" onclick="" tabindex="1" type="submit">Refresh Page</button>
	<button id="rerender" class="goog-buttonset-action cool-button" onclick="" tabindex="2" type="submit">Rerender Events</button>
</div>

<div id="map_container" class="container-fluid mapContainer">
</div>

<div class="container-fluid">
	<div id="calendar">
	</div>
	<div id="map_day_container" class="container-fluid mapDayContainer">
		<span class="map_day" id="map_day"></span>
	</div>
</div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
		<form id="scheduleformID" action="/customers/postGoogleInsert" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Calendar Event for Job ID #{{ $lead['0']->job_id }}</h4>
			</div>
			<div class="modal-body">
				<DIV class="row"><b>Starting:&nbsp;</b><span id="startTimeSpan"></span></div>
				<DIV class="row"><b>Ending:&nbsp;</b><span id="endTimeSpan"></span><br/><br/></div>
				<DIV class="row">
					<div class="span2 calendar">
						{{ Form::label('calendarID', 'Calendar:', array('class' => 'scheduleLabel control-label')) }}
						{{ Form::text('date', null, array('class' => 'form-control', 'id' => 'multiDatePick')) }}
						{{ Form::select('calendarID', $calendar, '', array('class' => 'form-control')) }}
						{{ Form::button('Multi-day?', array('class' => 'btn btn-primary', 'id' => 'btnMultiDay')) }}
						{{ Form::hidden('job_id', $lead['0']->job_id , array('id' => 'job_id')) }}
						{{ Form::hidden('email', $lead['0']->customer_email) }}
						{{ Form::hidden('start', '', array('id' => 'start')) }}
						{{ Form::hidden('end', '', array('id' => 'end')) }}
					</div>
					<div class="span3 event_type">
						{{ Form::label('event_type', 'Event Type:', array('class' => 'scheduleLabel control-label')) }}
						{{ Form::select('event_type', [EVENT_EST_APPT => 'Estimate Appt.', EVENT_INSTALL_REPAIR => 'Install/Repair Job', EVENT_SERVICE_CALL => 'Service Call'], (empty(Input::get('type')) ? '' : Input::get('type')), array('class' => 'form-control')) }}
					</div>
				</DIV>
				<div class="row">
					<div class="form-group form-group-sm">
						{{ Form::label('summary', 'Title:', array('class' => 'scheduleLabel control-label')) }}
						{{ Form::text('summary', $title, array('class' => 'form-control', 'id' => 'summary', 'readOnly' => 'true')) }}
					</div>
				</div>
				<div class="row">
					<div class="form-group form-group-sm">
						{{ Form::label('location', 'Location:', array('class' => 'scheduleLabel control-label')) }}
						{{ Form::text('location', $location, array('class' => 'form-control', 'id' => 'location', 'readOnly' => 'true')) }}
					</div>
				</div>
				<DIV class="rowElem"><br/><br/>
					{{ Form::label('notes', 'Notes:', array('class' => 'scheduleLabel control-label')) }}
					{{ Form::textarea('description', $fullNote, array('class' => 'form-control', 'id' => 'notes', 'readOnly' => 'true', 'rows' => '7')) }}
				</DIV>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<a class="btn btn-success" id="btnEditJob" target="_blank">Edit Job</a>
				{{ Form::submit('Schedule Event', array('class' => 'btn btn-primary btnSchedule')) }}
			</div>
		</form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<DIV style="display: none" id='schedulebox' title="Schedule Appointment">
	<FORM id="scheduleformID" method="post">
		<DIV class="sysdesc">&nbsp;</DIV>
		<DIV class="row" id="timeSelected">Time Selected for Lead id #<span id="job_id" name="job_id" value="{{ $lead['0']->job_id }}"><?php echo $lead['0']->job_id ?></span>&nbsp;</div> 
		<DIV class="row">Starting:&nbsp;<span id="startTime"></span></div>
		<DIV class="row">Ending:&nbsp;<span id="endTime"></span><br/><br/></div>
		<DIV class="row">

			<div class="span2 calendar">
<!--				<LABEL class="scheduleLabel">Calendar</LABEL> -->
				{{ Form::label('calendarName', 'Calendar:', array('class' => 'scheduleLabel control-label')) }}
<!--				<SELECT id="calendarName" name="calendarName"> 
				@foreach ($profiles as $seller)
					<option value="{{ $seller['google_calendar_id'] }}">{{ $seller['first_name'] }}</option>
				@endforeach				
				</SELECT> -->
				{{ Form::select('calendarName', $calendar, '', array('class' => 'form-control')) }}
			</div>
			<div class="span3 event_type">
<!--				<LABEL class="scheduleLabel">Event Type:</LABEL>  -->
				{{ Form::label('event_type', 'Event Type:', array('class' => 'scheduleLabel control-label')) }}
<!--				<SELECT id="event_type" name="event_type"> 
					<option value="Estimate Appt.">Estimate Appt.</option>
					<option value="Service">Service</option>
				</SELECT> -->
				{{ Form::select('event_type', [EVENT_EST_APPT => 'Estimate Appt.', EVENT_INSTALL_REPAIR => 'Install/Repair Job', EVENT_SERVICE_CALL => 'Service Call'], '', array('class' => 'form-control')) }}
			</div>
		</DIV>
		<div class="row">
			<div class="form-group form-group-sm">
<!--				<LABEL class="scheduleLabel">Title:</LABEL> -->
				{{ Form::label('summary', 'Title:', array('class' => 'scheduleLabel control-label')) }}
<!--				<TEXTAREA id="summary" rows="1" cols="43" name="summary" disabled>({//{ $lead[0]->job_city }})&nbsp;{//{ $lead[0]->customer_lname }}, {//{ $lead[0]->customer_fname }}</TEXTAREA> 
				<input type="text" class="input-sm form-control" id="summary" name="summary" disabled value="({{ $lead[0]->job_city }})&nbsp;{{ $lead[0]->customer_lname }}, {{ $lead[0]->customer_fname }}"></input> -->
<!--				<div class="col-xs-6"> -->
					{{ Form::text('summary', $title, array('class' => 'form-control', 'id' => 'summary', 'disabled' => 'disabled')) }}
<!--				</div> -->
			</div>
		</div>
		<div class="row">
			<div class="form-group form-group-sm">
<!--				<LABEL class="scheduleLabel">Location:</LABEL> -->
				{{ Form::label('location', 'Location:', array('class' => 'scheduleLabel control-label')) }}
<!--				<TEXTAREA id="location" rows=1 cols=43 name="location" disabled>{{ $lead[0]->job_address }}, {{ $lead[0]->job_city }}, {{ $lead[0]->job_zip }}</TEXTAREA> -->
<!--				<div class="col-xs-6"> -->
<!--				<input id="location" class="input-sm form-control" name="location" disabled value="{{ $lead[0]->job_address }}, {{ $lead[0]->job_city }}, {{ $lead[0]->job_zip }}"></input> -->
					{{ Form::text('location', $location, array('class' => 'form-control', 'id' => 'location', 'disabled' => 'disabled')) }}
<!--				</div> -->
			</div>
		</div>
		<DIV class="rowElem"><br/><br/>
<!--			<LABEL class="scheduleLabel">Notes:</LABEL> -->
			{{ Form::label('notes', 'Notes:', array('class' => 'scheduleLabel control-label')) }}
<!--			<TEXTAREA class="form-control" id="notes" rows="7" cols=43 name="notes" disabled>@foreach ($notes as $note)[{{ $note->f_name }}: {{ date("g:i a, m/d/Y", strtotime($note->created_at)) }}] {{ $note->note }}{{ "\n\n" }}@endforeachBuilt: {{ $lead[0]->built."\n" }}Phone: {{$lead[0]->customer_phone."\n" }}Lead Source: {{ $lead[0]->lead_source }}</TEXTAREA> -->
			{{ Form::textarea('notes', $fullNote, array('class' => 'form-control', 'id' => 'notes', 'name' => 'notes', 'disabled' => 'disabled', 'rows' => '7')) }}
		</DIV>
	</FORM>
</DIV>
@stop

@section('scripts')
<script type='text/javascript' src="{{ asset('js/ext/bootstrap-datepicker.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/ext/fullcalendar/fullcalendar.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/maps.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/schedule.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/ext/moment.min.js') }}"></script>
<script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?v=3.exp&key={{Config::get('app.google_javascript_api_key')}}"></script>
<script type='text/javascript' src="{{ asset('js/ext/jquery.ui.map.full.min.js') }}"></script>
@stop
