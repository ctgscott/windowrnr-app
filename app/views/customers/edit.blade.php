@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Lead Details
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')

<div class="well well-small">
	
	{{ Form::open(array('action' => 'CustomersController@update', 'class' => 'form-inline')) }}		
	<div>
		<h4><em>Customer & Billing Information:</em></h4>
		<input class="input-small form-control" name="cust" type="text" value="Cust. ID: {{ $customer[0]->customer_id }}" disabled/>
		<input class="input-small form-control" name="custID" type="hidden" value="{{ $customer[0]->customer_id }}" id="custID"/><br>
		
		<div class="top5">
			<input placeholder="Last Name" class="input-small form-control" name="l_name" type="text" value="{{ $customer[0]->customer_lname }}" required>
			<input placeholder="First Name" class="input-small form-control" name="f_name" type="text" value="{{ $customer[0]->customer_fname }}" required>
			<input placeholder="Company" class="input-small form-control" name="company_name" type="text" value="{{ $customer[0]->company_name }}"><br>
		</div>

		<div class="top5">
			<?php echo Form::text('phone', $customer[0]->customer_phone, array('placeholder' => 'Phone', 'class' => 'input-small form-control phone', 'id' => 'phone' )); ?>
			<?php echo Form::text('alt_phone', $customer[0]->customer_altphone, array('placeholder' => 'Alt. Phone', 'class' => 'input-small form-control phone', 'id' => 'phone2')); ?>
			<?php echo Form::text('email', $customer[0]->customer_email, array('placeholder' => 'email', 'class' => 'input-small form-control')); ?>
		</div>
	</div>
	<div class="top5">
		<input placeholder="Billing Address" class="input-small address form-control" type="text" name="billing_address" value="{{ $customer[0]->billing_address }}" >
		<input placeholder="City" class="input-small city form-control" name="billing_city" type="text" value="{{ $customer[0]->billing_city }}" >
		<input placeholder="State" class="input-small form-control state" style="width: 60px" name="state" type="text" value="{{ $customer[0]->billing_state }}" >
		<input placeholder="Zip" class="input-small form-control zip" style="width: 80px" name="billing_zip" type="text" value="{{ $customer[0]->billing_zip }}" >
	</div>
	<hr>
	<h4><em>Job & Jobsite Information:</em></h4>
	@foreach ($jobs as $job)
		<div class="row-fluid top5">
			<div class="span12">
				<a class="btn btn-small btn-info" href="/job/{{ $job->id }}">Job ID: {{ $job->id }}</a>
				<input class="input-small form-control" name="jobID" type="hidden" value="{{ $job->id }}" id="jobID"/>
				<input placeholder="Jobsite Address" class="input-small form-control address" name="address" type="text" value="{{ $job->address }}" disabled/>
				<?php echo Form::text('city', $job->city, array('placeholder' => 'City', 'class' => 'input-small form-control city', 'disabled' => 'disabled')); ?>
				<input placeholder="Zip" class="input-small form-control zip" name="zip" style="width: 80px" type="text" id="zip" value="{{ $job->zip }}" disabled/>
				<?php echo Form::text('built', $job->built, array('placeholder' => 'Year Built', 'class' => 'input-small form-control built', 'id' => 'built', 'disabled' => 'disabled', 'style' => 'width: 80px;')); ?>
			</div>
		</div>
	@endforeach
	<div class="editButtons">
		<?php echo Form::submit('Update', ['class' => 'btn btn-small btn-primary', 'style' => 'margin-top: 15px;']);?>
	</div>
</div>

@stop

@section('scripts')
	<script src="{{ asset('js/ext/jquery.maskedinput.js') }}"></script>
	<script src="{{ asset('js/maskedinput.js') }}"></script>
@stop

