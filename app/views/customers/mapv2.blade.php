@extends('layouts.fullwidth')

{{-- Web site Title --}}
@section('title')
Customer Map
@stop

@section('styles')
<link href="{{ asset('css/map.css') }}" rel="stylesheet">
<script type='text/javascript' src="{{ asset('custMap.json') }}"></script>

@stop

{{-- Content --}}
@section('content')
	<style>
		body {
			padding-top: 51px;
		}
		.gmnoprint, .gm-style-cc {
		    visibility: hidden !important;
		}
	</style>
	<div id="custMap" class="custMap" ></div>
	<div class="custmap-filters">
		<div class="city-filter">City <select class="cities"></select></div>
	</div>
	<ul class="job-status-legend">
		<li class="head">Job Status</li>
		<li><label><input type="checkbox" data-status="1" checked><span class="legend lead-created"></span><span class="legend-label">Lead Created</span></span></label></li>
		<li><label><input type="checkbox" data-status="2" checked><span class="legend lead-scheduled"></span><span class="legend-label">Lead Scheduled</span></label></li>
		<li><label><input type="checkbox" data-status="3" checked><span class="legend quote-created"></span><span class="legend-label">Quote Created</span></label></li>
		<li><label><input type="checkbox" data-status="4" checked><span class="legend job-scheduled"></span><span class="legend-label">Job Scheduled</span></label></li>
		<li><label><input type="checkbox" data-status="5" checked><span class="legend job-complete"></span><span class="legend-label">Job Complete</span></label></li>
		<li><label><input type="checkbox" data-status="6" checked><span class="legend on-standby"></span><span class="legend-label">On Standby</span></label></li>
	</ul>
@stop

@section('scripts')
<!--	<script type='text/javascript' src="{{ asset('js/ext/date.js') }}"></script> -->
<!--	<script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script> -->
	<script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDdkZOwIZHlT9PnIQbhI4e6dwIjNqfz0vw"></script>
	<script type='text/javascript' src="{{ asset('js/custMapV2.js') }}"></script>
@stop
