@extends('layouts.fullwidth')

{{-- Web site Title --}}
@section('title')
Assign Techs
@stop

@section('styles')
<link rel='stylesheet' type='text/css' href="{{ asset('js/ext/fullcalendar/fullcalendar.css') }}" />
<link rel='stylesheet' type='text/css' href="{{ asset('css/calendar.css') }}" >
<link rel='stylesheet' type='text/css' href="{{ asset('css/schedule.css') }}" >
<link rel='stylesheet' type='text/css' href="{{ asset('css/map-suggest-inline.css') }}" >

<link rel='stylesheet' type='text/css' href="{{ asset('css/assign.css') }}" >

@stop

{{-- Content --}}
@section('content')

@if(!empty(Input::get('date')))
<input type="hidden" id="forDate" value="{{ Input::get('date') }}">
@endif

<div class="container-fluid assign">
	<div class="col-md-6">

		<div class="tech_team_members" style="display: none">
			@foreach($profiles as $profile)
				<div class="member" data-profile='{{ json_encode($profile) }}'><i class="glyphicon glyphicon-user"></i>{{ $profile['first_name'] }}</div>
			@endforeach
		</div>

		<!--
		<div class="tech_team_members" style="display: none">
			<div class="member"><i class="glyphicon glyphicon-user"></i>Brian</div>
			<div class="member"><i class="glyphicon glyphicon-user"></i>Nick</div>
			<div class="member"><i class="glyphicon glyphicon-user"></i>Ceasar</div>
			<div class="member"><i class="glyphicon glyphicon-user"></i>Esteban</div>
			<div class="member"><i class="glyphicon glyphicon-user"></i>Jeff</div>
			<div class="member"><i class="glyphicon glyphicon-user"></i>Jackie</div>
			<div class="member"><i class="glyphicon glyphicon-user"></i>Scotty</div>
			<div class="member"><i class="glyphicon glyphicon-user"></i>Diego</div>
			<div class="member"><i class="glyphicon glyphicon-user"></i>Michael</div>
			<div class="member"><i class="glyphicon glyphicon-user"></i>Eric</div>
		</div>
		-->
		
		<div id="calendar"></div>
	</div>
	<div class="col-md-6">
		<div id="map_day_container" class="container-fluid mapDayContainer">
			<span class="map_day" id="map_day"></span>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script type='text/javascript' src="{{ asset('js/ext/fullcalendar/fullcalendar.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/maps.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/schedule.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/ext/typeahead/typeahead.bundle.js') }}"></script> 
<script type='text/javascript' src="{{ asset('js/custTypeAhead.js') }}"></script> 
<script type='text/javascript' src="{{ asset('js/ext/handlebars/handlebars-v1.3.0.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/ext/moment.min.js') }}"></script>
<!--<script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&sensor=false"></script>-->
<script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdkZOwIZHlT9PnIQbhI4e6dwIjNqfz0vw"></script>
<script type='text/javascript' src="{{ asset('js/ext/jquery.ui.map.full.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/ext/jquery.maskedinput.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/maskedinput.js') }}"></script>

<!-- address lookup -->
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>-->
<script type='text/javascript' src="{{ asset('js/map-suggest-inline.js') }}"></script>
@stop
