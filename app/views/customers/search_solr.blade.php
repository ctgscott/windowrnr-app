@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Customer Search
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
<div class="panel panel-default search-results" id="leads">

	@if($results->numFound == 0)
		<h3>No results matching '{{ $term }}'</h3>
	@else
    	<h3>Search results for '{{ $term }}'</h3>

	  	<?php
		$pages = [];
		$pages[] = ($prev != -1) ? "<a href='/search/$term?start=$prev'>◀ Previous</a>" : "<a class='disabled'>◀ Previous</a>";
		$pages[] = ($next != -1) ? "<a href='/search/$term?start=$next'>Next ▶</a>" : "<a class='disabled'>Next ▶</a>";
		echo "<div class='pages'>" . implode('&nbsp;&nbsp;|&nbsp;&nbsp;', $pages) . "<span class='page-info'>$start to $end of {$results->numFound} results</span></div>" ;
		?>

	  	<div class="panel-body">
	  		<table class="table table-striped table-bordered">
				<thead>
					<th>Last Name, First</th>			
					<th>Job Address</th>
					<th>Billing Address</th>
					<th>Job Created</th>
				</thead>
				<tbody class="existing">
					@foreach ($results->docs as $result)
						<?php
					  		$f_name = isset($result->f_name) ? $result->f_name[0] : '';
					  		$l_name = isset($result->l_name) ? $result->l_name[0] : '';
					  		$company_name = isset($result->company_name) ? $result->company_name[0] : '';
					  		$job_id = isset($result->job_id) ? $result->job_id[0] : '';
					  		$cust_id = isset($result->cust_id) ? $result->cust_id[0] : '';
					  		$address = isset($result->address) ? $result->address[0] : '';
					  		$city = isset($result->city) ? $result->city[0] : '';
					  		$billing_address = isset($result->billing_address) ? $result->billing_address[0] : '';
					  		$billing_city = isset($result->billing_city) ? $result->billing_city[0] : '';
					  		$job_created = isset($result->job_created) ? $result->job_created[0] : '';

					  		$name = [];
					  		if($l_name != '') array_push($name, $l_name);
					  		if($f_name != '') array_push($name, $f_name);
					  		$name = implode(', ', $name);

					  		$add = [];
					  		if($address != '') array_push($add, $address);
					  		if($city != '') array_push($add, $city);
					  		$add = implode(', ', $add);

					  		$bill_add = [];
					  		if($billing_address != '') array_push($bill_add, $billing_address);
					  		if($billing_city != '') array_push($bill_add, $billing_city);
					  		$bill_add = implode(', ', $bill_add);
				  		?>
						<tr>									
							<td id="lNameField"><a href="/job/{{ $job_id }}">@if(!empty($company_name))<b>{{ $company_name }}</b><br>@endif{{ $name }}</a></td>					
							<td id="addressField" class="addressField">{{ $add }}</td>
							@if ($billing_address != null)<td id="billingAddress" class="addressField">{{ $bill_add }}</td>
							@else <td id="billingAddress"></td>
							@endif
							<td id="jobCreatedField" class="jobCreatedField">{{ $job_created != '' ? date('n/j/Y - g:i a', strtotime($job_created)) : '' }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<?php
		$pages = [];
		$pages[] = ($prev != -1) ? "<a href='/search/$term?start=$prev'>◀ Previous</a>" : "<a class='disabled'>◀ Previous</a>";
		$pages[] = ($next != -1) ? "<a href='/search/$term?start=$next'>Next ▶</a>" : "<a class='disabled'>Next ▶</a>";
		echo "<div class='pages'>" . implode('&nbsp;&nbsp;|&nbsp;&nbsp;', $pages) . "<span class='page-info'>$start to $end of {$results->numFound} results</span></div>" ;
		?>

	@endif

</div>
@stop

