@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Leads
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
<link href="{{ asset('css/map-suggest-inline.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')

<div class="well well-sm" id="addCustomerForm">
	<form id="newLeadForm" class="form-inline" action="/customers/newLead" method="POST">
		<div>
			<h4><em>Customer Information:</em></h4>
			<input placeholder="Last Name" id="l_name" class="input-small typeahead form-control" name="l_name" value="" autocomplete="off" required="" type="text">
			<input placeholder="First Name" id="f_name" class="input-small form-control" name="f_name" value="" required="" type="text">
			<input placeholder="Company Name" class="input-small form-control input-wide" name="company_name" value="" type="text">
			<?php echo Form::text('phone', '', array('placeholder' => 'Phone', 'class' => 'input-small form-control', 'id' => 'phone')); ?>
			<?php echo Form::text('alt_phone', '', array('placeholder' => 'Alt. Phone', 'class' => 'input-small form-control', 'id' => 'phone2')); ?>
			<?php echo Form::text('email', '', array('placeholder' => 'EMail', 'class' => 'input-small form-control', 'id' => 'email')); ?>
		</div>
		<div id="jobList" class="accordion-body collapse">
			<h4><em>Prior Jobs</em></h4>
			<table class="table table-condensed table-striped table-leads">
				<thead>
					<tr><th>&nbsp;&nbsp;</th>
					<th>Job ID</th>
					<th>Address</th>
					<th>City</th>
					<th>Est. Scheduled</th>
					<th>Job Scheduled</th>
					<th>Job Completed</th>
				</tr></thead>
				<tbody id="jobs_table">
				</tbody>
			</table>
			<button class="btn btn-primary btn-sm" type="button" onclick="copyButton()" id="copyButton">Copy info to new job</button>
		</div>
		<hr>
		<h4><em>Job &amp; Jobsite Information:</em></h4>
		<div class="row">

			<div class="jobsite" data-address-group="jobsite" data-open-in-gmap>
				<input placeholder="Jobsite Address" id="job_address" class="input-small address form-control address map-suggest-address" name="address" value="" required="" type="text">
				<?php echo Form::text('city', '', array('placeholder' => 'City', 'id' => 'job_city', 'class' => 'input-small form-control city map-suggest-city')); ?>
				<input placeholder="Zip" class="input-small zip form-control map-suggest-zip" name="zip" id="zip" value="" type="text" style="width:90px;">
				<?php echo Form::text('built', '', array('placeholder' => 'Year Built', 'class' => 'input-small form-control built', 'id' => 'built', 'style' => 'width:100px;')); ?>
				{{ Form::button('Billing Detail', ['class' => 'btn btn-small btn-success', 'id' => 'btnBillDetail']) }}
			</div>

			<div id="divBillDetail" class="collapse" data-address-group="billing">
				{{ Form::text('billing_address', '', array('placeholder' => 'Billing Address', 'class' => 'form-control input-small map-suggest-address')) }}
				{{ Form::text('billing_city', '', array('placeholder' => 'Billing City', 'class' => 'input-small city form-control map-suggest-city')) }}
				{{ Form::text('billing_state', '', array('placeholder' => 'State', 'class' => 'input-small state form-control map-suggest-state', 'id' => 'billing_state')) }}
				{{ Form::text('billing_zip', '', array('placeholder' => 'Billing Zip', 'class' => 'input-small form-control map-suggest-zip', 'id' => 'billing_zip')) }}
			</div>
			
			<div class="well well-sm source" name="source">
				<div class="sourceTitle">
					<em>Lead Source:</em>
				</div>
				<div id="lead_source">
					<input id="sourceCheckbox1" value="Angies" name="lead_source[]" type="checkbox"> Angies&nbsp;&nbsp;
					<input id="sourceCheckbox2" value="Yelp" name="lead_source[]" type="checkbox"> Yelp&nbsp;&nbsp;
					<input id="sourceCheckbox3" value="Google" name="lead_source[]" type="checkbox"> Google&nbsp;&nbsp;
					<input id="sourceCheckbox4" value="LA Conservancy" name="lead_source[]" type="checkbox"> LA Conserv.&nbsp;&nbsp;
					<input id="sourceCheckbox5" value="Other" name="lead_source[]" type="checkbox"> Referral or Other:
					<?php echo Form::text('source_referral', '', array('placeholder' => 'Note Referral or Other', 'class' => 'input-large form-control', 'name' => 'source_referral', 'id' => 'source_referral', 'disabled' => 'disabled', 'style' => 'width:400px')); ?>        
				</div>
			</div>

		</div>
		<div>
			<textarea placeholder="Notes" id="note" value="" class="textarea form-control" rows="6" name="note"></textarea>
		</div>
		<div class="buttons">
			<?php echo Form::submit('Save', ['class' => 'btn btn-primary form-control', 'name' => 'saveNewLead', 'id' => 'saveNewLead']);?>
			<?php echo Form::submit('Schedule Appt.', ['class' => 'btn btn-success form-control', 'name' => 'scheduleNewLead', 'id' => 'scheduleNewLead', 'style' => 'width:150px']);?>
			<button class="btn btn-default" id="btnCloseNewLeadForm">Close</button>
		</div>
	</form>
</div>

<div id="leads_outer">
	<h3 class="leads-heading">{{ $heading }}<button class="btn btn-primary btn-sm" id="btnAddNewLead">Add New Lead</button></h3>
	<div class="well well-sm" id="leads">
		
		<table class="table table-condensed table-striped table-leads">
			<thead>
				<th>Job#</th>
				<th>Last Name</th>
				<th>Address</th>
				<th>City</th>
				<th>Phone</th>
				<th>&nbsp;</th>
			</thead>
			<tbody class="existing {{ Input::get('status') == QUOTE_CREATED ? 'quotes' : '' }}">
			@foreach ($customers as $customer)
				<tr>
					<td>{{ $customer->job_id }}</td>
					<td class="cust_name"><a href="/job/{{ $customer->job_id }}">@if(!empty($customer->company_name))<b>{{ $customer->company_name }}</b><br>@endif{{ $customer->customer_lname }}</a></td>
					<td>{{ $customer->job_address }}</td>
					<td>{{ $customer->job_city }}</td>
					<td>{{ $customer->customer_phone }}</td>
					<td>
						@if(!Input::get('archived'))
							@if(Input::get('status') == LEAD_CREATED || !Input::get('status'))
								<button title="Schedule" id="scheduleBtn" class="btn btn-info action_confirm radius btn-xs" onclick="location.href='{{ URL::to('customers/schedule') }}/{{ $customer->job_id }}?type={{ EVENT_EST_APPT }}'">Schedule</button>
							@elseif(Input::get('status') == LEAD_SCHEDULED)
								<button title="Create Quote" id="createQuoteBtn" class="btn btn-success action_confirm radius btn-xs" onclick="location.href='/estimates/create/{{ $customer->job_id }}'">Create Quote</button>
							@elseif(Input::get('status') == QUOTE_CREATED)
								<button title="Schedule" id="scheduleBtn" class="btn btn-info action_confirm radius btn-xs" onclick="location.href='{{ URL::to('customers/schedule') }}/{{ $customer->job_id }}?type={{ EVENT_INSTALL_REPAIR }}'">Schedule</button>
							@elseif(Input::get('status') == JOB_ACCEPTED)
								@if(Input::get('scheduled'))
									<button title="Schedule" id="completeBtn" class="btn btn-success action_confirm radius btn-xs" data-id="{{ $customer->job_id }}">Done</button>
									<button title="Schedule" id="scheduleBtn" class="btn btn-info action_confirm radius btn-xs" onclick="location.href='{{ URL::to('customers/schedule') }}/{{ $customer->job_id }}?type={{ EVENT_INSTALL_REPAIR }}'">Reschedule</button>
								@else
									<button title="Schedule" id="scheduleBtn" class="btn btn-info action_confirm radius btn-xs" onclick="location.href='{{ URL::to('customers/schedule') }}/{{ $customer->job_id }}?type={{ EVENT_INSTALL_REPAIR }}'">Schedule</button>
								@endif
							@elseif(Input::get('status') == JOB_ON_STANDBY)
								<button title="Schedule" id="scheduleBtn" class="btn btn-info action_confirm radius btn-xs" onclick="location.href='{{ URL::to('customers/schedule') }}/{{ $customer->job_id }}?type={{ EVENT_INSTALL_REPAIR }}'">Schedule</button>
							@endif
							<button title="Archive" id="btnArchive{{$customer->job_id }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button> 
						@else
							<button title="Unarchive" id="btnUnarchive{{$customer->job_id }}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-hand-up"></span></button> 
						@endif
					</td>
				</tr>
				@if (Input::get('status') == QUOTE_CREATED)
					<?php
					$estimates = Estimate::where('job_id', '=', $customer->job_id)->get();
					?>
					<tr class="quotes-row">
					<td>&nbsp;</td>
					<td colspan="8" class="quotes-cell">
						<ul>
						@foreach ($estimates as $estimate)
							<li>
								<span class="quote-id">Est #{{ $estimate->id }}</span>
								<a class="quote-name" href="/estimates/edit/{{ $estimate->id }}">{{ $estimate->description }}</a>
							</li>
						@endforeach
						</ul>
					</td>
					</tr>
				@endif
			@endforeach
			</tbody>
		</table>
	</div>
</div>


@include('archiveNote')

@stop

@section('scripts')
	<script type='text/javascript' src="{{ asset('js/fieldAdd.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/ext/moment.min.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/ext/handlebars/handlebars-v1.3.0.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/ext/typeahead/typeahead.bundle.js') }}"></script> 
	<script type='text/javascript' src="{{ asset('js/ext/jquery.maskedinput.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/maskedinput.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/customer.js') }}"></script>

	<!-- address lookup -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyDdkZOwIZHlT9PnIQbhI4e6dwIjNqfz0vw"></script>
	<script type='text/javascript' src="{{ asset('js/map-suggest-inline.js') }}"></script>
@stop
