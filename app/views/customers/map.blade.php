@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Customer Map
@stop

@section('styles')
<link href="{{ asset('css/map.css') }}" rel="stylesheet">
<!--<script type='text/javascript' src="{{ asset('data.json') }}"></script>-->
<script type='text/javascript' src="{{ asset('custMap.json') }}"></script>

@stop

{{-- Content --}}
@section('content')
	<style>
		body {
			padding-top: 40px;
		}
	</style>
	<div id="custMap" class="custMap" >
	</div>
@stop

@section('scripts')
	<script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
	<script type='text/javascript' src="{{ asset('js/ext/markerclusterer.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/custMap.js') }}"></script>
@stop
