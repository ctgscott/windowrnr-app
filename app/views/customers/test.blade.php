@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Big Test
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')

<form id='login' action='/api/login' method='post' accept-charset='UTF-8'>
<fieldset >
<legend>Login</legend>
<!--<input type='hidden' name='submitted' id='submitted' value='1'/> -->
 
<label for='email' >Email*:</label>
<input type='text' name='email' id='email'  maxlength="50" />
 
<label for='password' >Password*:</label>
<input type='password' name='password' id='password' maxlength="50" />

<label for='rememberMe' >Remember Me*:</label>
<input type='checkbox' name='rememberMe' id='rememberMe'/>
 
<input type='submit' name='Submit' value='Submit' />
 
</fieldset>
</form>
@stop