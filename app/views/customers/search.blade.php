@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Customer Search
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
<div class="panel panel-default" id="leads">

	<div class="panel-heading">
    	<h4 class="panel-title">Existing leads</h3>
  	</div>

	<div class="panel-body">
		<table class="table table-striped table-bordered">
			<thead>
				<th>Last Name, First</th>			
				<th>Job Address</th>
				<th>Billing Address</th>
				<th>Job Created</th>
			</thead>
			<tbody class="existing">
				@foreach ($results as $result)
					<tr>									
						<td id="lNameField"><a href="/job/{{ $result->job_id }}">{{ $result->l_name }}, {{ $result->f_name }}</a></td>					
						<td id="addressField" class="addressField">{{ $result->address.", ".$result->city }}</td>
						@if ($result->billing_address != null)<td id="billingAddress" class="addressField">{{ $result->billing_address.", ".$result->billing_city }}</td>
						@else <td id="billingAddress"></td>
						@endif
						<td id="jobCreatedField" class="jobCreatedField">{{ date('n/j/Y - g:i a', strtotime($result->job_created)) }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

</div>
@stop

@section('scripts')
	<script type='text/javascript' src="{{ asset('js/fieldAdd.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/ext/moment.min.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/ext/typeahead/typeahead.min.js') }}"></script> 
	<script type='text/javascript' src="{{ asset('js/customer.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/ext/hogan-2.0.0.js') }}"></script>
@stop
