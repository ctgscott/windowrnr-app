<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/home/content/05/6712605/html/dev/windowrnr/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/home/content/05/6712605/html/dev/windowrnr/bootstrap/css/signin.css" rel="stylesheet">
	
  </head>

  <body>
  
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="/home/content/05/6712605/html/dev/windowrnr/bootstrap/js/bootstrap.js"></script>

    <div class="container">

    <form class="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>
		
		<!-- Large button group -->
		<div class="btn-group">
			<button class="btn btn-default btn-large dropdown-toggle login-btn" type="button" data-toggle="dropdown">
				Employee &nbsp; <span class="caret"></span></button>
			<ul class="dropdown-menu" name=>
				<li>Batya</li>
				<li>Birgit</li>
				<li>Brian</li>
				<li>Carlos</li>
				<li>Cesar</li>
				<li>Ed</li>
				<li>Esteban</li>
				<li>Jeff</li>
				<li>Nick</li>
				<li>Norm</li>
				<li>Scott</li>
				</ul>
		</div>
		<div class="btn-toolbar" style="margin: 0;">

        <input type="password" class="input-block-level" placeholder="Password">
        <button class="btn btn-large btn-primary btn-block" type="submit">Sign in</button>
		
		<label class="checkbox">
			<input type="checkbox" value="remember-me"> Remember me
        </label>
	</form>

    </div> <!-- /container -->

  </body>
</html>