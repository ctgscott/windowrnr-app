@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
PDF Frame
@stop

@section('styles')
@stop

{{-- Content --}}
@section('content')

<div class="frame-outer">
<iframe name="inlineframe" src="/pdf/jobs/{{ $job_id.'/'.$appt_id }}" frameborder="5" scrolling="no" width="855" height="880" marginwidth="5" marginheight="5" seamless ></iframe>
<a class="btn btn-primary" href="/job/{{ $job_id }}">Edit Job</a>
</div>

<style>
div.frame-outer {
	overflow: hidden;
	margin-bottom: 30px;
}
div.frame-outer > * {
	float: left;
}
div.frame-outer a.btn {
	margin-left: 20px;
	padding: 6px 20px;
}
</style>
@stop

