@extends('layouts.fullwidth')

{{-- Web site Title --}}
@section('title')
Edit Lead Details
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
<link href="{{ asset('css/job.css') }}" rel="stylesheet">

<!-- address lookup -->
<link href="{{ asset('css/map-suggest-inline.css') }}" rel="stylesheet">

<!-- image uploader and viewer -->
<link href="{{ asset('css/image-upload.css') }}" rel="stylesheet">
<link href="{{ asset('js/ext/lightbox/css/lightbox.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')


@if($results->job_archive)
	<div id="static-notifications">
		<div class="alert alert-danger">
			This job is currently archived&nbsp;&nbsp;<a class="btn btn-primary btn-sm" href="/job/unarchiveDirect/{{ $results->job_id }}">Unarchive</a>
		</div>
	</div>
@endif

<div class="well well-sm cells-outer">
	<h3>Job # {{ $results->job_id }} - Details</h3>
	{{ Form::open(array('action' => 'CustomersController@update', 'class' => 'form-inline')) }}		
	{{ Form::hidden('jobID', 'Job #'.$results->job_id, array('class' => 'form-control input-small labelBox', 'disabled' => 'disabled')) }}
	{{ Form::hidden('status', $results->job_status)}}
	
	<div class="cells left">

		<div style="overflow: hidden; float: none; clear: both;">

			<!-- customer info -->
			<div class="cell">
				<h4><em>Customer Information:</em></h4>
				<input class="input-small form-control" name="custID" value="{{ $results->customer_id }}" id="custID" type="hidden">
				<input placeholder="Last Name" id="l_name" class="input-small form-control" name="l_name" value="{{ $results->customer_lname }}" required="" type="text">
				<input placeholder="First Name" id="f_name" class="input-small form-control" name="f_name" value="{{ $results->customer_fname }}" required="" type="text">
				<br>
				<input placeholder="Company Name (Optional)" class="input-small form-control input-wide" id="company" name="company_name" value="{{ $results->company_name }}" type="text">
				<br/>
				{{ Form::text('phone', $results->customer_phone, array('placeholder' => 'Phone', 'class' => 'phone input-small form-control', 'id' => 'phone' )) }}
				{{ Form::text('alt_phone', $results->customer_altphone, array('placeholder' => 'Alt. Phone', 'class' => 'phone input-small form-control', 'id' => 'phone2')) }}
				<br/>
				{{ Form::text('email', $results->customer_email, array('placeholder' => 'email', 'class' => 'input-small form-control', 'id' => 'email')) }}
				<a href="" id="btnMailTo" disabled><span class="btn btn-small btn-success" id="mailToSpan">Email</span></a>
			</div>

			<!-- job info -->
			<div class="cell">
				<h4><em>Jobsite Information:</em></h4>
				<div class="" data-address-group="jobsite" data-open-in-gmap>
					<input class="input-small form-control" name="jobID" value="{{ $results->job_id }}" id="jobID" type="hidden">
					{{ Form::text('address', $results->job_address, array('class' => 'form-control input-small map-suggest-address input-wide', 'id' => 'job_address')) }}
					<br/>
					{{ Form::text('city', $results->job_city, array('placeholder' => 'City', 'id' => 'job_city', 'class' => 'input-small city form-control map-suggest-city')) }}
					{{ Form::text('zip', $results->job_zip, array('placeholder' => 'Zip', 'class' => 'input-small form-control map-suggest-zip', 'style' => 'width: 80px;', 'id' => 'zip')) }}
					<br/>
					{{ Form::text('built', $results->built, array('placeholder' => 'Yr. Built', 'class' => 'input-small built form-control not-last', 'id' => 'built', 'style' => 'width: 90px')) }}
				</div>
			</div>

			<!-- billing info -->
			<div class="cell">
				<h4><em>Billing Address:</em></h4>
				<div id="divBillDetail" class="{{{ $results->billing_address === $results->job_address ? '' : 'in' }}}" data-address-group="billing">
					{{ Form::text('billing_address', $results->billing_address, array('placeholder' => 'Billing Address', 'id' => 'billing_address', 'class' => 'form-control input-small map-suggest-address', 'style' => 'width: 250px;')) }}
					<br/>
					{{ Form::text('billing_city', $results->billing_city, array('placeholder' => 'Billing City', 'id' => 'billing_city', 'class' => 'input-small city form-control map-suggest-city')) }}
					{{ Form::text('billing_state', $results->billing_state, array('placeholder' => 'State', 'class' => 'input-small state form-control map-suggest-state', 'id' => 'billing_state')) }}
					<br/>
					{{ Form::text('billing_zip', $results->billing_zip, array('placeholder' => 'Zip', 'class' => 'input-small form-control map-suggest-zip', 'id' => 'billing_zip', 'style' => 'width: 90px')) }}
				</div>
			</div>

			<!-- source -->
			<div class="cell source-cell">
				<h4><em>Lead Source:</em></h4>
				<div id="source">
					<?php BaseController::firePhp()->addInfo('$results->lead_source', array($results->lead_source)); ?>

					<label class="lead-source-label"><input name="lead_source[]" type="checkbox" value="Angies" {{ isset($results->lead_source) && strpos($results->lead_source, 'Angies') !== FALSE ? 'checked' : '' }}>Angies</label>
					<label class="lead-source-label"><input name="lead_source[]" type="checkbox" value="Yelp" {{ isset($results->lead_source) && strpos($results->lead_source, 'Yelp') !== FALSE ? 'checked' : '' }}>Yelp</label>
					<label class="lead-source-label"><input name="lead_source[]" type="checkbox" value="Google" {{ isset($results->lead_source) && strpos($results->lead_source, 'Google') !== FALSE ? 'checked' : '' }}>Google</label>
					<label class="lead-source-label"><input name="lead_source[]" type="checkbox" value="LA" {{ isset($results->lead_source) && strpos($results->lead_source, 'LA') !== FALSE ? 'checked' : '' }}>LA Conserv.</label>

					<?php
						if (isset($results->lead_source)) {
							if (strpos($results->lead_source, 'Other') !==false) {
								$other = explode( 'Other, ', $results->lead_source);
								echo '<br/><input id="sourceCheckbox5" value="Other" name="lead_source[]" checked="checked" type="checkbox"> Referral or Other:&nbsp;';
								if(!empty($other[1])) {
									echo '<br/>' . Form::text('source_referral', $other[1], array('placeholder' => 'Referral or Other', 'class' => 'input-large form-control wide', 'name' => 'lead_source[]', 'id' => 'source_referral'));
								} else {
									echo '<br/>' . Form::text('source_referral', '', array('placeholder' => 'Referral or Other', 'class' => 'input-large form-control wide', 'name' => 'lead_source[]', 'id' => 'source_referral'));
								}
							} else {
								echo '<br/><input id="sourceCheckbox5" value="Other" name="lead_source[]" type="checkbox"> Referral or Other: ';
								echo '<br/>' . Form::text('source_referral', '', array(
									'placeholder' => 'Referral or Other', 
									'class' => 'input-large form-control wide', 
									'name' => 'lead_source[]', 
									'id' => 'source_referral', 
									'disabled' => 'disabled'));
							}
						}
					?>
				</div>
			</div>

			<!-- job status -->
			<div class="cell status-cell">
				<h4><em>Job Status:</em></h4>
				@if($results->job_status >= JOB_ACCEPTED)
					<label class="line-label">
						<input type="checkbox" name="status-job-accepted" checked>Job Accepted (and deposit received)
						<span class="sub-note">{{ $results->job_accepted_at ? date('M j, Y - g:i A', strtotime($results->job_accepted_at)) : '' }}</span>
						
						@if($results->job_status == JOB_ACCEPTED)

							<?php
							$jobEvents = false;
							$jobEventDate = '';
							?>

							@foreach ($results->events as $event)
								@if($event->type === EVENT_INSTALL_REPAIR) 
									<?php 
									$jobEvents = true;
									$jobEventDate = explode(' ', $event->start)[0];
									?>
								@endif
							@endforeach

							@if($jobEvents === true)
								<a class="btn btn-primary btn-sm btn-assign-techs" href="/assign?date={{ $jobEventDate }}">Assign Techs</a>
							@else
								<a class="btn btn-primary btn-sm btn-assign-techs" href="/customers/schedule/{{ $results->job_id }}?type=Install/Repair Job">Schedule Job</a>
							@endif

						@endif
						
					</label>
					<label class="line-label">
						<input type="checkbox" name="status-on-standby" {{ ($results->job_status == JOB_ON_STANDBY ? "checked" : "") }}>On Standby
						<span class="sub-note">{{ $results->job_status == JOB_ON_STANDBY && $results->job_put_on_standby_at ? date('M j, Y - g:i A', strtotime($results->job_put_on_standby_at)) : '' }}</span>
					</label>
					<label class="line-label {{ ($results->job_status < JOB_ACCEPTED ? "disabled" : "") }}">
						<input type="checkbox" name="status-completed" {{ ($results->job_status == JOB_COMPLETED ? "checked" : "") }} {{ ($results->job_status < JOB_ACCEPTED ? "disabled" : "") }}>Completed
						<span class="sub-note">{{ $results->job_status == JOB_COMPLETED && $results->job_completed_at ? date('M j, Y - g:i A', strtotime($results->job_completed_at)) : '' }}</span>
					</label>
					<label class="line-label">
						<input type="checkbox" name="status-on-service-call" {{ ($results->job_status == JOB_ON_SERVICECALL ? "checked" : "") }}>Service Call
					</label>
				@else
					<label class="line-label">
						<input type="checkbox" name="status-job-accepted">Job Accepted (Deposit recv'd)
					</label>
					<label class="line-label disabled">
						<input type="checkbox" name="status-on-standby" disabled>On Standby
					</label>
					<label class="line-label disabled">
						<input type="checkbox" name="status-completed" disabled>Completed
					</label>
				@endif
					<label class="line-label">
						<a class="btn btn-info" id="btn_printLabel">Label</a>
				@if(!$results->job_archive)
						<a class="btn btn-danger btn-archive-job" id="btn_archive" data-job-id="{{ $results->job_id }}" href="/job/archive/{{ $results->job_id }}">Archive</a>
				@endif
					</label>
			</div>

		</div>

		<!-- events -->
		<div class="cell non-standard events-cell">
			<h4><em>Events:</em></h4>
			{{ Form::button('Schedule Appt.', ['class' => 'btn btn-small btn-success btn-schedule-appointment', 'id' => 'btnSchedule']) }}
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th class="idField">Event ID</th>
						<th class="eventType" >Event Type</th>
						<th class="calendarName">Who</th>
						<th class="eventStart">Scheduled</th>
						<th class="printEvent"></th>
					</tr>
				</thead>
				<tbody class="existing">
					@foreach ($results->events as $event)
						<tr>
							<td class="idField" id="idField"><a class="btn btn-info btn-xs active eventID" role="button" href="/events/{{ $event->id }}">#{{ $event->id }}</a></td>
							<td class="eventType" id="eventType" >{{ $event->event_type }}</td>
							<td class="calendarName" id="calendarName"   class="">{{ $event->who }}</td>
							<td class="eventStart" id="eventStart"  class="">{{ date('m/d/Y - g:i a', strtotime($event->start)) }}</td>
							<?php if(!is_null($event->appt)) { ?>
								<td class="printEvent">{{ Form::button('Print', array('type' => 'button', 'class' => 'btn btn-success btn-xs btnPrintEvent', 'onClick' => 'printSheet('.$event->job_id.', '.$event->appt->id.')')) }}</td>
							<?php } ?>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<!-- estimates -->
		<div class="cell non-standard events-cell">
			<h4>
				<em>Estimates:</em>
				<a title="Create Estimate" target="_blank" href="/estimates/create/{{ $results->job_id }}" class="btn btn-success btn-quick-add-estimate" data-id="{{ $results->job_id }}">Create Estimate</a>
				@if($results->finalisedPartsListID)
					<a title="Parts List" target="_blank" href="/parts-list/{{ $results->finalisedPartsListID }}" class="btn btn-success btn-quick-add-estimate" data-id="">Parts List</a>	
				@endif
			</h4>
			<table class="table table-condensed table-striped job_estimates">
				<thead>
					<tr>
						<th class="estDesc">Description</th>
						<th>Estimate Date</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody class="existing">
					@foreach ($results->estimates as $estimate)
						<tr class="{{ $estimate->sold == '1' && $results->job_status == 4 ? 'sold' : '' }}">
							<td id="estDescription"><a class="btn btn-info btn-xs active btnEst" role="button" href="/estimates/edit/{{ $estimate->id }}" title="{{ $estimate->description }}">{{ $estimate->description }}</a></td>
							<td id="date" class="estDate">{{ date("m/d/Y - g:i a", strtotime($estimate->created_at)) }}</td>
							<td id="estButtons" class="estButtons">
								@if($results->job_status == 4)
									@if($estimate->sold == '1')
										<button title="Undo Mark as Sold" type="button" class="btn btn-default btn-sm resetSoldEst resel-sold" id="resetSoldEst{{ $estimate->id }}" data-id="{{ $estimate->id }}"><i class="glyphicon glyphicon-refresh"></i></button>
										{{ Form::button('Sold', array('type' => 'button', 'class' => 'btn btn-primary btn-sm soldEst sold', 'id' => 'soldEst'.$estimate->id, 'data-id' => $estimate->id, 'disabled' => 'disabled' )) }}
									@else
										{{ Form::button('Sold', array('type' => 'button', 'class' => 'btn btn-primary btn-sm soldEst', 'id' => 'soldEst'.$estimate->id, 'data-id' => $estimate->id, 'title' => 'Mark as Sold' )) }}
									@endif
								@endif
								@if($estimate->partsListID)
									<a href="/parts-list/{{ $estimate->partsListID }}" class="btn btn-info btn-sm">Parts</a>
								@endif
								{{ Form::button('Print', array('type' => 'button', 'class' => 'btn btn-success btn-sm printEst', 'id' => 'printEst'.$estimate->id, 'data-id' => $estimate->id )) }}
								{{ Form::button('Copy', array('type' => 'button', 'class' => 'btn btn-success btn-sm copyEst', 'id' => 'copyEst'.$estimate->id)) }}
								{{ Form::button('<i class="glyphicon glyphicon-remove"></i>', array('type' => 'button', 'class' => 'btn btn-danger btn-sm deleteEst', 'id' => 'deleteEst'.$estimate->id)) }}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<!-- files -->
		<div class="cell files-cell">
			<h4><em>Files:</em></h4>
			<div class="images" data-allow-upload="true" data-image-owner-type="job" data-image-owner-id="{{ $results->job_id }}"></div>
		</div>


	</div> <!-- end of cells left -->

	<div class="cells right">

		<!-- add note -->
		<div class="cell">
			<h4><em>Add Note:</em></h4>
			<textarea placeholder="New Notes" id="newNote" value="" class="textarea form-control notes wide" rows="2" name="newNote"></textarea>
		</div>

		<!-- notes -->
		<div class="cell notes-cell">
			<h4>
				<em>File Notes:</em>
				<label class="show-system-notes">
					<input type="checkbox" class="chk-show-system-notes" checked> Show System Notes
				</label>
			</h4>
			<ol class="job-notes">
				@foreach($results->notes as $note)
					<li class="oldNote {{ $note->is_system ? 'system' : '' }}" style="margin-top:8px;"><em>{{ date('M. d, Y \(g:ia\)', strtotime($note->created_at)) }}:</em> <p class="note-item">{{ $note->note }}</p></li>
				@endforeach
			</ol>
		</div>

		<!-- buttons -->
		<div class="editButtons cell buttons-cell">
			<?php echo Form::submit('Apply', ['class' => 'btn btn-small btn-primary']);?>&nbsp;&nbsp;
		</div>

	</div>
	
</div>

{{ Form::close() }}

@include('archiveNote')

@stop

@section('scripts')
	<script src="{{ asset('js/fieldAdd.js') }}"></script>
	<script src="{{ asset('js/job.js') }}"></script>
	<script src="{{ asset('js/ext/ckeditor/ckeditor.js') }}"></script> 
	<script src="{{ asset('js/ext/jquery.maskedinput.js') }}"></script>
	<script src="{{ asset('js/maskedinput.js') }}"></script>
	<script src="{{ asset('js/leadDetail.js') }}"></script>

	<!-- address lookup -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{Config::get('app.google_javascript_api_key')}}"></script>
	<script type='text/javascript' src="{{ asset('js/map-suggest-inline.js') }}"></script>

	<!-- image upload -->
	<script type='text/javascript' src="{{ asset('js/ext/dmuploader.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/ext/lightbox/js/lightbox.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/image-upload.js') }}"></script>
	
	<!-- DYMO Label printer -->
	<script type="text/javascript" src="{{ asset('js/ext/dymo/DYMO.Label.Framework_2.0Beta2.js') }} "charset="UTF-8"></script>

@stop

