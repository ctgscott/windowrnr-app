@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Estimate Appointments List
@stop

{{-- Content --}}
@section('content')
<div class="well well-small" id="leads">
	<h4><em>Estimates To Do</em></h4>
	<table class="table table-condensed table-striped">
		<thead>
<!--			<th>Job#</th> -->
			<th>Created</th>
<!--			<th>Cust #</th> -->
			<th>Last Name</th>			
			<th>Address</th>
			<th>City</th>
			<th>Built&nbsp;</th>
			<th>Sales Person</th>
			<th>Appt Scheduled</th>
		</thead>
		<tbody class="existing">
			@foreach ($estimates as $est)
				<tr>
<!--					<td id="idField"><a href="/job/{{ $est->id }}">{{ $est->id }}</a></td>  -->
					<td id="createdField">{{ date("n/j/y", strtotime($est->created_at)) }}</td>
<!--					<td id="custNumField" class="cust_num"><a href="/customers/{{ $est->customer_id }}">{{ $est->customer_id }}</a></td> -->
					<td id="lNameField" class="cust_name"><a href="/job/{{ $est->id }}">
						@if(!empty($est->company_name))
							<b>{{ $est->company_name }}</b><br>
						@endif
						{{ $est->l_name.', '.$est->f_name }}
					</a></td>
					<td id="addressField">{{ $est->address }}</a></td>
					<td id="cityField">{{ $est->city }}</td>
					<td id="builtField">{{ $est->built }}</td>
					<td id="salesPerson">{{ $est->first_name }}</td>
					<td id="estSchedule">{{ $est->start }}</td>
					<td id="buttonField">
						<button class="btn btn-xs btn-danger" id="btnArchive{{$est->id }}">&nbsp;&nbsp;&nbsp;<span class="nav-glyph glyphicon glyphicon-trash"></span>&nbsp;</button> 
						<button id="quoteBtn" class="btn btn-xs btn-success action_confirm radius" onClick="location.href='{{ URL::to('estimates/create') }}/{{ $est->id}}'">Quote</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

@include('archiveNote')

@stop

@section('scripts')
	<script type='text/javascript' src="{{ asset('js/estimateList.js') }}"></script>
@stop
