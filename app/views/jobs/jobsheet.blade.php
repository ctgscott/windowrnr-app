<!DOCTYPE html>

<html>
<head>
    <title>WindowRnR - Lead Sheet</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <link href="{{ asset('css/pdf.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="register">
        <div class="row1" >
            <h1>Lead Sheet</h1>
        </div>
		<div class="rowx">&nbsp;
            <label class="bigCheck" id="label1">Stand by <img class="checkImg" src="{{ asset('img/unchecked.png') }}"></img></label>
            <label class="bigCheck" id="label2">Service Call <img class="checkImg" src="{{ asset('img/unchecked.png') }}"></img></label>
		</div>
		<hr >
        <div class="row2" >
            <label class="left">Date&nbsp;</label><input id="datepicker" type="text" class="short" value="{{ date('n/j/Y', time()) }}">
            <label id="phoneLabel">Phone&nbsp;</label><input id="phone" class="medium" type="text" value="{{ $customer_phone }}">
			<label id="altPhoneLabel">Alt. Phone&nbsp;</label><input id="altPhone" class="medium right" type="text" value="{{ $customer_altphone }}">
		</div>
		@if(!empty($company_name))
			<div class="row3">
				<label class="left">Company&nbsp;</label><input class="long" type="text" value="{{ $company_name }}">
			</div>
		@endif
		<div class="row3">
			<label class="left">Name&nbsp;</label><input class="long" type="text" value="{{ $customer_lname.', '.$customer_fname }}">
			<label id="emailLabel">Email&nbsp;</label><input id="email" class="medium right" type="text" value="{{ $customer_email }}">
		</div>
		<div class="row4">
            <label class="left">Job Addr.&nbsp;</label><input class="longer" type="text" value="{{ $job_address.', '.$job_city.', '.$job_zip }}">
        </div>
        <div class="row5">
<!--            <label class="left">Own. Addr.&nbsp;</label><input class="longer" type="text" value="{//{ $custDetail['billing_address'].', '.$custDetail['billing_city'].', '.$custDetail['billing_state'].', '.$custDetail['billing_zip'] }}"> -->
			<label class="left">Own. Addr.&nbsp;</label><input class="longer" type="text" value="{{{ ($billing_address != '') ? $billing_address.', '.$billing_city.', '.$billing_state.', '.$billing_zip : '' }}}">
            <label id="ageLabel">Year Built&nbsp;</label><input id="built" class="short right" type="text" value="{{ $built }}">
		</div>
<!--		<div class="row6">
			<label class="left">Symptoms&nbsp;</label><input class="longer" type="text">
		</div>
-->		<div class="row7 align">
			<input id="check3" class="checkBox left" type="checkbox" value="" <?php if(is_numeric(strpos($lead_source, 'Angies'))) echo "checked" ?>><span class="checkLabel">Angies</span>
			<input id="check4" class="checkBox" type="checkbox" value="" <?php if (is_numeric(strpos($lead_source, 'LA'))) echo "checked" ?>><span class="checkLabel">LA Cons.</span>
			<input id="check5" class="checkBox" type="checkbox" value="" <?php if (is_numeric(strpos($lead_source, 'Google'))) echo "checked" ?>><span class="checkLabel">Google&nbsp;</span>
			<input id="check6" class="checkBox" type="checkbox" value="" <?php if (is_numeric(strpos($lead_source, 'Yelp'))) echo "checked" ?>><span class="checkLabel">Yelp&nbsp;</span>
<!--			<span class="glyphicon glyphicon-{//{ (is_numeric(strpos($lead_source, 'Other')) ? 'check' : 'unchecked') }}"></span>-->
			<input id="check7" class="checkBox" type="checkbox" value="" {{ (is_numeric(strpos($lead_source, 'Other')) ? 'checked' : '') }}> <span class="otherLabel checkLabel">Other/Ref:&nbsp;</span><input class="" id="otherText" type="text" value="{{ (isset($otherSource[1]) ? $otherSource[1] : '') }}">
		</div>
		<hr >
        <div class="infobox">
            <h4>Notes</h4>
            <div class="notes">
				@foreach ($notes as $note)
					@if($note['is_system'] != 1)
						{{ '<br/>'.date('n/j/Y - g:i a', strtotime($note['created_at'])).'('.$note['f_name'].' '.$note['l_name'][0].'.):&nbsp;&nbsp;'.$note['note'].'<br />' }}
					@endif
				@endforeach
				<br />
			</div>
        </div>
		<hr>
		<div class="align1">
			<label class="left">Appt. Date&nbsp;</label><input class="medium wide-input" type="text" value="{{ date('n/j/Y,g:i a', strtotime($appt['appt_start'])) }}{{ " - ".date('g:i a', strtotime($appt['appt_end'])) }}">
			<label id="salesLabel">Sales Person&nbsp;</label><input class="medium wide-input right" type="text"value="{{ $sales }}">
		</div>
	</div>
	<div class="tablePage">
		<table class="table">
			<tr>
				<th>#</th>
				<th>Room</th>
				<th>Dir.</th>
				<th>Notes</th>
			</tr>
			<tr>
				<td class="num">1</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">2</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">3</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">4</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">5</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">6</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">7</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">8</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">9</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">10</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">11</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">12</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">13</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">14</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">15</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">16</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">17</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">18</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">19</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">20</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">21</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">22</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">23</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
			<tr>
				<td class="num">24</td>
				<td class="dir"><input type="text" class="cell" value=""></td>
				<td class="room"><input type="text" class="cell" value=""></td>
				<td class="notes"><input type="text" class="cell" value=""></td>
			</tr>
		</table>
	</div>
</body>
</html>