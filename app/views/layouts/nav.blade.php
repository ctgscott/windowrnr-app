<a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	<span class="glyphicon glyphicon-bar"></span>
	<span class="glyphicon glyphicon-bar"></span>
	<span class="glyphicon glyphicon-bar"></span>
</a>
<div class="navbar navbar-inverse navbar-fixed-top navbar-default"> 
	<div class="container">
		<div class="navbar-collapse" style="margin:0px 0px 0px 0px;">
			<form class="navbar-form row {{ Sentry::check() ? 'loggedin' : '' }}" role="search" action="/search" method="get"  id="search-form" name="search-form" style="margin:0px 0px 0px 0px;height:20px;" onsubmit="return doSearch()">
				<ul class="nav navbar-nav pull-left col-md-9" style="">
					@if (Sentry::check())
						<li class=""><a href="{{ URL::to('/msgboard') }}" >Msg Board</a></li>
						<li {{ (Request::is('calendar*') ? 'class="active"' : '') }} ><a href="{{ URL::to('/calendar') }}" >Calendar</a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="">Jobs<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/customers?status={{ LEAD_CREATED }}"><span class="nav-glyph glyphicon glyphicon-phone green"></span>Leads - New</a></li>
								<li><a href="/customers?status={{ LEAD_SCHEDULED }}"><span class="nav-glyph glyphicon glyphicon-list-alt blue"></span>Leads - Apptmt Sched.</a></li>
								<li><a href="/customers?status={{ QUOTE_CREATED }}"><span class="nav-glyph glyphicon glyphicon-list-alt red"></span>Leads - Quote Created</a></li>
								<li class="divider"></li>
								<li><a href="/customers?status={{ JOB_ACCEPTED }}&scheduled=0"><span class="nav-glyph glyphicon glyphicon-time orange"></span>Jobs - To be Scheduled</a></li>
								<li><a href="/customers?status={{ JOB_ACCEPTED }}&scheduled=1"><span class="nav-glyph glyphicon glyphicon-time green"></span>Jobs - Scheduled</a></li>
								<li><a href="/customers?status={{ JOB_ON_STANDBY }}"><span class="nav-glyph glyphicon glyphicon-warning-sign orange"></span>Jobs - On Standby</a></li>
								<li><a href="/customers?status={{ JOB_ON_SERVICECALL }}"><span class="nav-glyph glyphicon glyphicon-earphone blue"></span>Jobs - Service Calls</a></li>
								<li class="divider"></li>
								<li><a href="/customers?status={{ JOB_COMPLETED }}"><span class="nav-glyph glyphicon glyphicon-thumbs-up green"></span>Jobs - Completed</a></li>
								<li><a href="/customers?archived=1"><span class="nav-glyph glyphicon glyphicon-trash gray"></span>Jobs - Archived</a></li>
								<li class="divider"></li>
								<li><a href="/customers?status={{ LEAD_CREATED }}&add=1"><span class="nav-glyph glyphicon glyphicon-plus"></span>Add New Lead</a></li>
								<!--<li><a href="/estimates/todo"><span class="nav-glyph glyphicon glyphicon-calendar"></span>Estimate Appointments</a></li>-->
							</ul>
						</li>
						<li title="Add New Lead"><span class="btn btn-primary navbar-btn glyphicon glyphicon-plus" onclick="window.location.href = '/customers?status={{ LEAD_CREATED }}&add=1'; "></span></li>
						<li {{ (Request::is('assign*') ? 'class="active"' : '') }} ><a href="{{ URL::to('/assign') }}" >Assign Techs</a></li>
						<li id="mapLink" {{ (Request::is('custMap') ? 'class="active"' : '') }} style=""><a href="{{ URL::to('/custMap') }}" style="">Customer Map</a></li>
						<li class="nav-search">
							<input type="text" class="form-control" placeholder="Customer or Street Name " id="query" name="term" value="{{ isset($term) ? $term : '' }}" />
							<button class="btn btn-success search-btn navbar-btn glyphicon glyphicon-search" type="submit"></button>
						</li>
					@endif
				</ul>
				<ul class="nav pull-right navbar-nav col-md-3" style="">
					@if (Sentry::check())
						<li class="dropdown gear" style="top:-10px;"  >	<a class="gear dropdown-toggle" data-toggle="dropdown" href="{{ URL::to('admin') }}" >
							<i class="glyphicon glyphicon-cog navCog"></i><b class="caret"></b></a>
							<ul class="dropdown-menu" style="margin:0px 0px 0px 0px;">
								@if (Sentry::check())
									<li {{ (Request::is('users/show/' . Sentry::getUser()->id) ? 'class="active"' : '') }}><a href="{{ URL::to('/users/show/'.Sentry::getUser()->id) }}"><i class="glyphicon glyphicon-cog" ></i>&nbsp;&nbsp;Account</a></li>
								@endif 
								@if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
									<li><a href="/users"><i class="glyphicon glyphicon-user" ></i>&nbsp;&nbsp;Users</a></li>
									<li><a href="/groups"><i class="glyphicon glyphicon-user" style="font-size:10px;" ></i><i class="glyphicon glyphicon-user"  style="font-size:10px;left:-4px;" ></i>Groups</a></li>
									<li><a href="/service-settings"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp;Service Settings</a></li>
									<li><a href="/admin/refreshMap"><i class="glyphicon glyphicon-map-marker" ></i>&nbsp;&nbsp;Update Customer Map</a></li>
									<li><a href="/email-templates"><i class="glyphicon glyphicon-envelope" ></i>&nbsp;&nbsp;Email Templates</a></li>
									<li><a href="/admin/reindex"><i class="glyphicon glyphicon-search" ></i>&nbsp;&nbsp;Reindex Solr</a></li>
								@endif
								@if (Sentry::check())
									<li><a href="/util/sash-weight/"><i class="glyphicon glyphicon-dashboard" ></i>&nbsp;&nbsp;Sash Weight Calculator</a></li>
								@endif 
							</ul>
						</li>
						<li  style="" ><a href="{{ URL::to('users/logout') }}">Logout</a></li>
						<li class="navbar-text">Hi {{ Sentry::getUser()->first_name }}!</li>
					@else
						<li {{ (Request::is('users/register') ? 'class="active"' : '') }}><a href="{{ URL::to('users/register') }}">Register</a></li>
						<li {{ (Request::is('users/login') ? 'class="active"' : '') }}><a href="{{ URL::to('users/login') }}">Login</a></li>
					@endif
					
				</ul>
			</form>
		</div>
	</div>
	<div class="env-label">Env: {{ App::environment() }} - {{ gethostname() }}</div>
</div>
<script>
function doSearch() {
	window.location.href = '/search/' + escape($('#query').val());
	return false;
}
</script>
<style>
button.btn[type="submit"] {
	padding: 4px 8px;
	margin-left: 4px;
}
</style>