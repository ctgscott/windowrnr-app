<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="google-site-verification" content="IRoKQXC2L-kbOSVqpMV75Ac3wWST5mnQtkCTaOimpW4" />
		<meta charset="utf-8" />
		<title>
		@section('title')
		@show
		</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

		<link href="{{ asset('css/nav.css') }}" rel="stylesheet">

		@section('styles')
		@show
		<style>
			body {
				padding-top: 60px;
			}
		</style>
	</head>

	<body>
		
		<!-- Navbar -->
		@include('layouts/nav')

		<!-- Container -->
		<div class="container mapContainer" id="mapContainer">
			<div id="notifications">
				<!-- Notifications -->
				@include('notifications')
				<!-- ./ notifications -->
			</div>
			<!-- Content -->
			@yield('content')
			<!-- ./ content -->
		</div>
		<!-- ./ container -->

		<!-- Javascripts -->
		<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

		<script type='text/javascript' src="{{ asset('js/main.js') }}"></script>
		<!-- ./ Javascripts -->

		@section('scripts')
		@show

	</body>
</html>
