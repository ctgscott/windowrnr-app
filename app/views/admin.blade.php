@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
Admin Page
@stop

@section('styles')
<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
<script type='text/javascript' src="{{ asset('js/admin.js') }}"></script>
@stop

{{-- Content --}}
@section('content')

<h4>Update Customer Map</h4>
<div class="well">
	<?php echo Form::button('Rebuid JSON file', ['class' => 'btn btn-small btn-primary btn-info', 'id' => 'mapFile']);?>
</div>

<h4>Users</h4>
<div class="well">
	<?php echo Form::button('Users', ['class' => 'btn btn-small btn-primary btn-info', 'id' => 'users']);?>
</div>


<h4>Groups</h4>
<div class="well">
	<?php echo Form::button('Groups', ['class' => 'btn btn-small btn-primary btn-info', 'id' => 'groups']);?>
</div>
@stop

@section('scripts')
@stop