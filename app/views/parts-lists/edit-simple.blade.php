@extends('layouts.default')

@section('title')
Parts List
@stop

@section('styles')
<link href="{{ asset('css/parts-list-simple.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')

	<input type="hidden" id="partslist_id" value="{{ $partsList->id }}">

	<div class="well well-small" id="printable">
		<div class="title">
			<h3>Parts List</h3>
		</div>
		<div>
			Customer: <b>{{ implode(', ', [$customer->l_name ? $customer->l_name : '', $customer->f_name ? $customer->f_name : '']) . ($customer->company_name ? ' [' . $customer->company_name . ']' : '')}}</b>
			&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
			<a href="/job/{{ $partsList->job_id }}">Job# {{ $partsList->job_id }}</a> - <a href="/estimates/edit/{{ $partsList->estimate_id }}">Est# {{ $partsList->estimate_id }}</a>
			&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
			Updated: {{ $partsList->updated_at }}
		</div>

		<div class="status-outer {{ $partsList->status == 1 ? 'finalized' : '' }}"><b>This parts list is {{ $partsList->status == 0 ? 'Not finalized' : 'Finalized' }}</b>&nbsp;&nbsp;
			@if($partsList->status == 0)
				<button class="btn btn-xs btn-success" id="btnFinalize">Finalize</button>
			@else
				<button class="btn btn-xs btn-warning" id="btnUnfinalize">Un-Finalize</button>
			@endif
		</div>

		<?php 
		$parts = json_decode($partsList->parts);
		foreach ($allParts as $allPart) {
			$allPart->options = json_decode($allPart->options);
		}
		$total = 0;
		?>

		<script>
		var parts = {{ $partsList->parts }},
			allParts = {{ json_encode($allParts) }};
		</script>

		<table class="parts">
			<thead>
				<tr>
					<td><span class="sub">Picked</span></td>
					<td>#</td>
					<td align="center">Qty</td>
					<td>Options</td>
					<td>Description</td>
				</tr>
			</thead>
			<tbody>
				<tr class="noprint" style="display: none">
					<td colspan="5" style="text-align: left">
						<div class="btn-group">
							<button type="button" class="btn btn-xs btn-primary dropdown-toggle btn-add-part" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Add Part&nbsp;&nbsp;<span class="caret"></span>
							</button>
							<ul class="dropdown-menu new-part-chooser">
								@foreach($allParts as $part)
									<li><a data-part-id="{{ $part->id }}">{{ $part->name }}</a></li>
								@endforeach
							</ul>
						</div>
					</td>
				</tr>
			</tbody>
		</table>

		<hr class="hline">

		<div class="buttons">
			<a class="btn btn-primary" href="/estimates/edit/{{ $partsList->estimate_id }}">Back to Estimate</a>
			<button class="btn btn-success" id="btnPrint">Print</button>
		</div>

		<div class="add-option-elements" style="display:none">
			<div class="btn-group">
				<button type="button" class="btn btn-xs btn-default dropdown-toggle btn-add-option" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Add
				</button>
				<ul class="dropdown-menu new-option-menu">
					<li><a data-option-type="color">Color</a></li>
					<li><a data-option-type="material">Material</a></li>
					<li><a data-option-type="size">Size</a></li>
					<li class="divider"></li>
					<li><a data-option-type="other">Other</a></li>
				</ul>
			</div><div class="btn-group new-option-value-group" style="display: none">
				<button type="button" class="btn btn-xs btn-default dropdown-toggle btn-add-option-value" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					&gt;choose&lt;
				</button>
				<ul class="dropdown-menu new-option-value-menu"></ul>
			</div>
		</div>
	</div>

@stop

@section('scripts')
@if(isset($print) && $print)
<script>
window.onload = window.print;
</script>
@endif
<script type='text/javascript' src="{{ asset('js/parts-list-simple.js') }}"></script>
@stop