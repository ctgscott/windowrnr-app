@extends('layouts.default')

@section('title')
Parts List
@stop

@section('styles')
<link href="{{ asset('css/parts-list.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')

	<input type="hidden" id="partslist_id" value="{{ $partsList->id }}">

	<div class="well well-small" id="printable">
		<div class="title">
			<h3>Parts List</h3>
		</div>
		<div>
			Customer: <b>{{ implode(', ', [$customer->l_name ? $customer->l_name : '', $customer->f_name ? $customer->f_name : '']) . ($customer->company_name ? ' [' . $customer->company_name . ']' : '')}}</b>
			&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
			<a href="/job/{{ $partsList->job_id }}">Job# {{ $partsList->job_id }}</a> - <a href="/estimates/edit/{{ $partsList->estimate_id }}">Est# {{ $partsList->estimate_id }}</a>
			&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
			Updated: {{ $partsList->updated_at }}
		</div>
		<hr class="hline">

		<?php 
		$parts = json_decode($partsList->parts);
		foreach ($allParts as $allPart) {
			$allPart->options = json_decode($allPart->options);
		}
		$total = 0;
		?>

		<script>
		var parts = {{ $partsList->parts }},
			allParts = {{ json_encode($allParts) }};
		</script>

		<table class="parts">
			<thead>
				<tr>
					<td><span class="sub">Picked</span></td>
					<td>#</td>
					<td>Part Name</td>
					<td>Options</td>
					<td align="right">Unit Price</td>
					<td align="right">Quantity</td>
					<td align="right">Price</td>
					<td align="center">&nbsp;</td>
				</tr>
			</thead>
			<tbody>
				<tr class="noprint">
					<td></td>
					<td></td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-xs btn-default dropdown-toggle btn-add-part" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Add Part&nbsp;&nbsp;<span class="caret"></span>
							</button>
							<ul class="dropdown-menu new-part-chooser">
								@foreach($allParts as $part)
									<li><a href="#" data-part-id="{{ $part->id }}">{{ $part->name }}</a></li>
								@endforeach
							</ul>
						</div>
					</td>
					<td></td>
					<td align="right"></td>
					<td align="right"></td>
					<td align="right"></td>
					<td align="center"></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td align="right"></td>
					<td align="right"><b>Total</b></td>
					<td align="right" class="grand-total">{{ '$ ' . money_format("%!.0i", $total) }}</td>
					<td align="center"></td>
				</tr>
			</tbody>
		</table>

		<hr class="hline">

		<div class="buttons">
			<button class="btn btn-primary" id="btnSave">Save</button>
			<button class="btn btn-success" id="btnPrint">Print</button>
		</div>
	</div>

@stop

@section('scripts')
@if(isset($print) && $print)
<script>
window.onload = window.print;
</script>
@endif
<script type='text/javascript' src="{{ asset('js/parts-list.js') }}"></script>
@stop