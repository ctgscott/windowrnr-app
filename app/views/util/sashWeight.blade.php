@extends('layouts.default')

@section('title')
Sash Weight Calculator
@stop

@section('styles')
<link href="{{ asset('css/sash-weight.css') }}" rel="stylesheet">
@stop

@section('content')
	<div class="well well-small">
		<div class="title">
			<h3>Sash Weight Calculator</h3>
		</div>
		<div class="well well-small float-container" style="display: block">
			<form id="sash_form" class="form-horizontal">
				<div class="control-group">
					<label class="control-label">Dimensions:</label>
					<input type="text" id="width" placeholder="Width" class="numeric">&nbsp;&nbsp;x&nbsp;&nbsp;<input type="text" id="height" placeholder="Height" class="numeric"><span class="hint">inches</span>
				</div>
				<div class="control-group">
					<label class="control-label">Glass:</label>
					<input type="text" id="glass" placeholder='N' class="numeric"><span class="hint"># of 1/8" sheets of glass</span>
				</div>
				<div class="control-group">
					<label class="control-label">Material:</label>
					<div class="btn-group" data-toggle="buttons">
			            <label class="btn btn-default btn-sm btn-radio"><input type="radio" name="material" autocomplete="off" value="1">Douglas Fir</label>
			            <label class="btn btn-default btn-sm btn-radio"><input type="radio" name="material" autocomplete="off" value="2">Pine</label>
			            <label class="btn btn-default btn-sm btn-radio"><input type="radio" name="material" autocomplete="off" value="3">Other</label>
			        </div>
				</div>
				<div class="control-group">
					<label class="control-label">Thickness:</label>
					<div class="btn-group" data-toggle="buttons">
			            <label class="btn btn-default btn-sm btn-radio"><input type="radio" name="thickness" autocomplete="off" value="1">1 3/8"</label>
			            <label class="btn btn-default btn-sm btn-radio"><input type="radio" name="thickness" autocomplete="off" value="2">1 3/4"</label>
			        </div>
				</div>
				<div class="control-group">
					<label class="control-label">&nbsp;</label>
					<button type="submit" class="btn btn-primary" id="btnCalculate" >Calculate</button>
					<button type="reset" class="btn btn-default" id="btnReset" >Clear</button>
				</div>
			</form>
			<div id="results"></div>
		</div>
	</div>
@stop

@section('scripts')
	<script type='text/javascript' src="{{ asset('js/sash-weight.js') }}"></script>
@stop