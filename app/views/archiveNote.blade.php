<?php	
	$url = Request::segment(1);
?>
<div class="modal fade" id="archiveNote">
	{{ Form::open(array('url' => 'job/archiveNote', 'id' => 'archiveNoteForm')) }}
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Archive lead with note</h4>
				</div>
				<div class="modal-body">
					{{ Form::hidden('status', Input::get('status')) }}

					{{ Form::hidden('returnURL', $url) }}
					{{ Form::hidden('scheduled', Input::get('scheduled')) }}
					{{ Form::hidden('job_id', '', array('id' => 'modalJobID')) }}
					{{ Form::text('archiveNote', '', array('placeholder' => 'Add note here...', 'class' => 'archiveNote form-control')) }}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					{{ Form::submit('Archive', ['class' => 'btn btn-primary', 'onclick' => "$('#archiveNoteForm').submit(); return false;"]) }}
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	{{ Form::close() }}
</div><!-- /.modal -->