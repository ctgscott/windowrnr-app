@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Messages
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
<link href="{{ asset('css/map-suggest-inline.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')

<div id="leads_outer">
	<h3 class="leads-heading">{{ $heading }}</h3>
	<div class="well well-sm" id="leads">
		<table class="msg-table table table-condensed table-striped table-leads">
			<thead>
				<th>Job#</th>
				<th style="width: 60px">Msg Type</th>
				<th style="width: 75px">Phone</th>
				<th style="width: 650px">Note</th>
				<th style="width: 150px">Created</th>
				<th>&nbsp;</th>
			</thead>
			<tbody class="existing {{ Input::get('status') == QUOTE_CREATED ? 'quotes' : '' }}">
			@foreach ($messages as $message)
				<tr class="msg_row">
					<td><a href="/job/{{ $message->job_id }}">{{ $message->job_id }}</a></td>
					<td style="width: 75px" class="msg_type"><b>Web Post</b></td>
					<td style="width: 85px" class="msg_type phone"><b>{{ $message->phone }}</b></td>
					<td class="note_body">{{ $message->note }}</td>
					<td class="note_created">{{ $message->created_at }}</td>
					<td>
						<button title="Schedule" id="scheduleBtn" class="btn btn-info action_confirm radius btn-xs" onclick="location.href='{{ URL::to('customers/schedule') }}/{{ $message->job_id }}?type={{ EVENT_EST_APPT }}'">Schedule</button>
						<button title="Left Msg" id="btnVM{{$message->job_id }}" class="btn btn-success btn-xs" onclick="location.href='{{ URL::to('job/updateStatus') }}/{{ $message->job_id }}/1.1'"><span class="glyphicon glyphicon-phone">Left-Msg</span></button> 
						<button title="Archive" id="btnArchive{{$message->job_id }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button> 
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>


@include('archiveNote')

@stop

@section('scripts')
<!--	<script type='text/javascript' src="{{ asset('js/fieldAdd.js') }}"></script>
-->	<script type='text/javascript' src="{{ asset('js/ext/moment.min.js') }}"></script>
<!--<script type='text/javascript' src="{{ asset('js/ext/handlebars/handlebars-v1.3.0.js') }}"></script>
-->	<script type='text/javascript' src="{{ asset('js/ext/typeahead/typeahead.bundle.js') }}"></script> 
<!--<script type='text/javascript' src="{{ asset('js/ext/jquery.maskedinput.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/maskedinput.js') }}"></script>
-->	<script type='text/javascript' src="{{ asset('js/customer.js') }}"></script>

	<!-- address lookup -->
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
	<script type='text/javascript' src="{{ asset('js/map-suggest-inline.js') }}"></script>
-->
@stop
