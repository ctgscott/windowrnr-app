@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Lead Details
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
<link href="{{ asset('css/msgs.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')

<div class="well well-small" id="mainCtn" style="overflow: hidden" >
	
	{{ Form::open(array('action' => 'TwilioController@textOut', 'class' => 'form-inline')) }}		
	<div class="layout well" id="contacts">
		<div class="head" id="contactHead">
			{{ Form::hidden('initThread', "", array('id' => 'hiddenContact')) }}  
			{{ Form::button('New Contact?', array('type' => 'button', 'id' => 'btnNewContact', 'class' => 'tabable btn btn-primary btn-small', 'onClick' => 'createThread()')) }}
		</div>
		@foreach ($contacts as $key=>$contact)
			<div class="input-small form-control contact" style="margin-bottom: 15px;" id="contact{{$key}}">
				<?php 
					$areaCode = substr($contact->from,0,3);
					$prefix = substr($contact->from,3,3);
					$lastFour = substr($contact->from,6,4);
					$from = '('.$areaCode.') '.$prefix.'-'.$lastFour;
				?>
				{{ Form::button($from, array('type' => 'button', 'id' => 'contactBtn' . $key, 'class' => 'btn btn-success btn-small', 'onClick' => 'getThread('.$contact->from.')')) }}
			</div>
		@endforeach
	</div>
	<div class="layout well" id="threadWell">
		<div class="head" id="threadHead">
			<h4 class="msg" id="contactTitle">Message History</h4>
			<?php echo Form::text('', '', array('placeholder' => '(123) 456-7890', 'class' => 'input-small form-control', 'id' => 'inputNewNum', 'onkeyup' => 'enableThreadBtn()')); ?>
			<?php echo Form::button('Start Conversation', ['class' => 'btn btn-small btn-primary', 'id' => 'btnNewNum', 'onClick' => 'startThread()', 'disabled' => 'disabled']);?>
			<div class="newMsg">
				<?php echo Form::button('Send Msg', ['class' => 'tabable btn btn-small btn-primary sendBtn', 'onClick' => 'sendMsg()', 'id' => 'btnSend', 'disabled' => 'disabled', 'tabindex' => '2']);?>
				<?php echo Form::textarea('message', '', array('placeholder' => 'Message', 'class' => 'input-small form-control message', 'id' => 'inputMsg', 'disabled' => 'disabled', 'tabindex' => '1')); ?>
			</div>
		</div>
		<div class="layout" id="threadDiv">
		</div>
	</div>
</div>

@stop

@section('scripts')
	<script src="{{ asset('js/ext/jquery.maskedinput.js') }}"></script>
	<script src="{{ asset('js/maskedinput.js') }}"></script>
	<script src="{{ asset('js/msgs.js') }}"></script>
	<script src="{{ asset('js/ext/moment.js') }}"></script>
@stop

