@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Event Details
@stop

@section('styles')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
<div class="well">
	<h3>Event Detail</h3>
	{{ Form::open(array('action' => array('EventsController@update', $results->id), 'class' => 'form-inline')) }}		
	<div>
		<label>{{ Form::text('eventID', 'Event #'.$results->id, array('class' => 'form-control labelBox', 'disabled' => 'disabled', 'style' => 'width: 125px;')) }}</label>
		<a class="btn btn-info btn-sm active" role="button"  style="margin-top:0px; margin:10px;"  href="/job/{{ $results->job_id }}">Job ID: {{ $results->job_id }}</a>
	</div>
	<div class="well well-sm wellNotes">
		<div class="btm10">
			<label class="event-label">Event Title: 
				{{ Form::text('title', $results->title, array('class' => 'form-control input-large', 'id' => 'inputTitle')) }}
			</label>
			
			<label class="event-label">Location: 
				{{ Form::text('location', $results->location, array('class' => 'form-control input-large', 'id' => 'inputLocation')) }}
			</label>
		</div>
		@foreach ($results->names as $name)
			<?php 
				$idName[$name->id] = $name->first_name;
			?>
		@endforeach
		<div class="btm10">
			<label class="event-label">Calendar: {{ Form::select('cal_user_id', $idName, $results->cal_user_id, array('class' => 'formSelect form-control')) }}</label>
			<label class="event-label">Status: {{ Form::select('status', array('confirmed' => 'confirmed', 'tentative' => 'tentative', 'cancelled' => 'cancelled'), $results->status, array('class' => 'formSelect form-control')) }}</label>
			<label class="event-label">Scheduled: {{ Form::text('scheduled', date('g:i', strtotime($results->start)).' - '.date('g:i a, n/j/Y', strtotime($results->end)), array('class' => 'form-control', 'disabled' => 'disabled')) }}</label>
			<label class="event-label">Event Type: 
				{{ Form::select('type', [EVENT_EST_APPT => 'Estimate Appt.', EVENT_INSTALL_REPAIR => 'Install/Repair Job', EVENT_SERVICE_CALL => 'Service Call'], $results->type, array('class' => 'formSelect form-control')) }}
			</label>
		</div>
		<div>
			<label>Created at: {{ Form::text('createdAt', date('g:i a, n/j/Y', strtotime($results->created_at)), array('class' => 'form-control', 'disabled' => 'disabled')) }}</label>
			<label>Created by: {{ Form::text('createdBy', $results->creatorEmail, array('class' => 'form-control', 'disabled' => 'disabled')) }}</label>
		</div>
	</div>
	<div class="editButtons">
		{{ Form::submit('Update', ['class' => 'btn btn-small btn-primary']) }}
		{{ link_to_route('event.delete', 'Delete', $results->id, ['data-method'=>'delete', 'class' => 'btn btn-small btn-danger']) }}
	{{ Form::close() }}
	</div>

</div>

@stop

@section('scripts')
@stop

