@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
calPush() Log
@stop

@section('styles')
<style type="text/css">
#output {
	width: 840px;
}
#output pre {
	border: 0;
	border-radius: 0;
}
#files {
	position: absolute;
	right: 0;
	top: 0;
	border: 1px solid #ddd;
	width: 270px;
	background-color: #f7f7f7;
}
#files > h5 {
	padding-left: 10px;
}
#files > a {
	display: block;
	padding: 2px 10px;
}
</style>
@stop

{{-- Content --}}
@section('content')

<h4>Log Viewer</h4><hr>

<div style="position: relative">

<div id="files">
	<h5>Current Time: {{ $date }}</h5>
	@foreach($files as $cfile)
		<?php
		$displayName = $cfile;
		$displayName = preg_replace('/^\/tmp\/devwindowrnrcom_production_\d+\-/', 'DEV - ', $displayName);
		$displayName = preg_replace('/^\/tmp\/scottwindowrnrcom_production_\d+\-/', 'SCOTT - ', $displayName);
		$displayName = preg_replace('/\-\d+$/', '', $displayName);
		?>
		<a href="/event-test-log?file={{ urlencode($cfile) }}" style="{{ $file === $cfile ? 'background-color: #848484; color: #fff' : '' }}">{{ $displayName }}</a>
	@endforeach
</div>

<div id="output">
	<h5>{{ $file }}</h5>
	<pre>{{ $data }}</pre>
</div>

</div>


@stop


