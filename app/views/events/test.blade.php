@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Events Test
@stop

@section('styles')
<style type="text/css">
.form {
    padding: 10px;
    border: 1px solid #ccc;
    background-color: #ddd;
}
.form label {
    display: block;
    font-weight: normal;
    margin-bottom: 10px;
}
.form label span {
	display: inline-block;
	width: 90px;
}
.form label input {
    width: 300px;
    padding: 2px 6px;
}
.form hr {
	margin: 10px 0;
	border-top: 1px solid #bbb;
}
#output {
	padding-top: 20px;
}
</style>
@stop

{{-- Content --}}
@section('content')

<div class="form">
<label><span>Calendar ID</span><input type="text" id="calendar_id" placeholder="Enter Calendar ID" value="{{ Sentry::getUser()->email }}"></label>
<label><span>Event ID</span><input type="text" id="event_id" placeholder="Enter Event ID" value="qquae0tj5u5gi8kcerol9b612k"></label>
<hr>
<button id="btn_fetch" class="btn btn-primary btn-sm">Get Event</button>&nbsp;
<button id="btn_setprop" class="btn btn-primary btn-sm">Set Job ID</button>
</div>

<div id="output"></div>

@stop

@section('scripts')
<script>
$(document).ready(function() {
	$('#btn_fetch').click(function() {
		getEvent($('#calendar_id').val(), $('#event_id').val());
		return false;
	});
	$('#btn_setprop').click(function() {
		var name = 'job_id', value = prompt('Job ID:');
		if(name && value) {
			setProp($('#calendar_id').val(), $('#event_id').val(), name, value);
		}
	});
});
function getEvent(_calendar_id, _event_id) {
	$('#output').html('Please wait ...');
	$.get('/event-test-get?calendar_id=' + _calendar_id + '&event_id=' + _event_id, function(_data) {
		$('#output').html(_data);
	});
}
function setProp(_calendar_id, _event_id, _name, _value) {
	$('#output').html('Please wait ...');
	$.post('/event-test-set-property', {calendar_id: _calendar_id, event_id: _event_id, prop_name: _name, prop_value: _value}, function(_data) {
		$('#output').html(_data);
	});
}
</script>
@stop

