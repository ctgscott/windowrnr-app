@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Set job_id property
@stop

@section('styles')
<style type="text/css">
#events {
    border-top: 1px solid #eee;
    margin-top: 20px;
}
#events > div {
    border-bottom: 1px solid #eee;
    padding: 3px 5px;
    position: relative;
}
#events > div.doing {
    background-color: #FFE2AD;
}
#events > div.done {
	background-color: #E7FFE8;
}
#events > div.error {
	background-color: #E8E8E8;
}
#events > div::after {
    position: absolute;
    right: 10px;
    color: #000;
}
#events > div.doing::after {
	content: 'Please wait...';
}
#events > div.done::after {
	content: 'OK';
	color: green;
	font-weight: bold;
}
#events > div.error::after {
	content: attr(ertext);
	color: red;
}
h4 {
	position: relative;
}
h4 button {
	position: absolute;
	right: 0;
	top: 0;
}
#btn_stop {
	display: none;
}
div[data-id] .btn {
	padding: 0 5px;
	position: absolute;
	left: 350px;
	top: 2px;
}
</style>
@stop

{{-- Content --}}
@section('content')

<h4>Set job_id property in Google Calendar [{{ count($events) }} events]<button id="btn_start" class="btn btn-sm btn-primary">Start</button><button id="btn_stop" class="btn btn-sm btn-default">Stop</button></h4>

<div id="events">
	@foreach($events as $event)
		<div data-id="{{ $event->google_event_id }}" data-calendar-id="{{ $event->google_cal_id }}" data-job-id="{{ $event->job_id }}" ertext="ERROR">
			{{ $event->title }} - Job# {{ $event->job_id }}
			<button class="btn_set_single btn btn-sm btn-primary">Set</button>
		</div>
	@endforeach
</div>

@stop

@section('scripts')
<script>
var stopSignal = false;
$(document).ready(function() {
	$('#btn_start').click(function() {
		stopSignal = false;
		startProcessing($('div[data-id]').first());
		$(this).hide();
		$('#btn_stop').show();
	});
	$('#btn_stop').click(function() {
		stopSignal = true;
		$(this).hide();
		$('#btn_start').show();
	});
	$('.btn_set_single').click(function() {
		stopSignal = false;
		startProcessing($(this).parent(), true);
	});
});
function startProcessing(_div, _single) {
	if(stopSignal) return;
	if(!_div.length) return;
	var event_id = _div.data('id'), i = 0;
	_div.removeClass().addClass('doing');
	_div.find('.btn').hide();
	$.post('/events-set-single-property', {calendar_id: _div.data('calendar-id'), event_id: _div.data('id'), prop_name: 'job_id', prop_value: _div.data('job-id')}, function(_data) {
		if(_data === 'OK') {
			_div.removeClass().addClass('done');
		}
		else {
			if(_data.indexOf('Not Found') !== -1) {
				_div.attr('ertext', 'ERROR: Not Found');
			}
			else {
				_div.attr('ertext', _data);
			}
			_div.removeClass().addClass('error');
		}
		_div.find('.btn').show();
		if(!_single) startProcessing(_div.next());
	});
}
</script>
@stop
