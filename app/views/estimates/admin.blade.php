@extends('layouts.default') 
 
{{-- Web site Title --}}
@section('title')
Admin Estimate Data
@stop

@section('styles')
<link href="{{ asset('css/estAdmin.css') }}" rel="stylesheet">
<style type="text/css">
#roomid:hover
{
	border-width:0px;
	border-color:#000000;	
}

.roomid:hover
{
	border-width:0px;
	border-color:#000000;	
}

input.form-control,input.form-control:focus {
    border:none;
/*    box-shadow: none;*/
   -webkit-box-shadow: none;
   -moz-box-shadow: none;
   -moz-transition: none;
   -webkit-transition: none;
}

button.no-border {
	border: none;
	background-color: #fff;
}
</style>
@stop

{{-- Content --}}
@section('content')

<div class="well well-small" id="items">
	<div class="panel-group" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
						<button id="expandItem1" class="btn btn-success expandItem" type="button"><i id="expandGlyph1" class="glyphicon glyphicon-chevron-down"></i></button>
						Rooms
					</a>
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse">
            <h5 style="padding:8px;">To delete any room, just make room name textbox blank and click on update button</h5>
				<div class="panel-body">
                {{ Form::open(array('url' => 'estimates/admin/style/addroom')) }}
                		<div style="float:right;">Add Room : 
							{{ Form::text('room_add', '', array('class' => '', 'id' => 'roomidadd')) }}
                            {{ Form::submit('Add', ['class' => 'btn btn-primary', 'id' => 'btnAddRoom']) }}
						</div>
                
                {{ Form::close() }}                    

				
                {{ Form::open() }}                
                    <div style="width:65%;">                    
                    @foreach ($roomsid as $key=>$val)
						<div id="divRoom{{ $key }}" class="form-control" style="float:left; width:220px; height:38px; margin-bottom:10px; margin-right:10px; border:1px solid #428bca;"> 
                        {{-- Form::checkbox('roomchk_'.$key, $val, false,  array('class' => 'bg-info', 'id' => 'roomidchk', 'style' => ' border:0px solid transparent; padding:8px;')) --}}
							{{Form::button('<i class="glyphicon glyphicon-remove" style="color: red; margin-top: 5px"></i>', array('id' => 'btnCloseRoom'.$key, 'class' => 'no-border', 'style' => 'float: right;'))}}
							<!--<a class="glyphicon glyphicon-remove" style="color: red; float: right;"></a>-->
							{{ Form::text('room_'.$key, $val, array('class' => '', 'id' => 'roomid'.$key, 'style' => 'height: 25px; border:0px solid transparent; width:160px; padding:4px;  ')) }}
                            {{-- Form::submit('X', ['class' => '', 'id' => 'btnDeleteRoom']) --}}
                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 						                      
					@endforeach                     
                    {{ Form::button('Update', ['class' => 'btn btn-primary', 'id' => 'btnUpdateRoom']) }}
                    </div>                   
                {{ Form::close() }}
                    
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
						<button id="expandItem1" class="btn btn-success expandItem" type="button"><i id="expandGlyph1" class="glyphicon glyphicon-chevron-down"></i></button>
						Window Styles
					</a>
				</h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse">
            <h5 style="padding:8px;">To delete any window style, just make window style name textbox blank and click on update button</h5>
				<div class="panel-body">
                
                {{ Form::open(array('url' => 'estimates/admin/style/addwinstyle')) }}
                		<div style="float:right;">Add Window Style : 
							{{ Form::text('winstyle_add', '', array('class' => '', 'id' => 'winstyleidadd')) }}
                            {{ Form::submit('Add', ['class' => 'btn btn-primary', 'id' => 'btnAddwinstyle']) }}
						</div>
                
                {{ Form::close() }}    
                
					{{ Form::open() }}
                    <div style="width:65%;">
						@foreach ($winStyle as $key=>$val)
                        <div id="divWinStyle{{ $key }}" class="form-control" style="float:left; width:220px; height:38px; margin-bottom:10px; margin-right:10px; border:1px solid #428bca;"> 
								{{Form::button('<i class="glyphicon glyphicon-remove" style="color: red; margin-top: 5px"></i>', array('id' => 'btnCloseWinStyle'.$key, 'class' => 'no-border', 'style' => 'float: right;'))}}
								{{ Form::text('winstyle_'.$key, $val, array('class' => '', 'id' => 'winstyleid'.$key, 'style' => 'height: 25px; border:0px solid transparent; width:160px; padding:4px;  ')) }}
							</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 	
						@endforeach
					{{ Form::button('Update', ['class' => 'btn btn-primary', 'id' => 'btnUpdateWinStyle']) }}
                     </div>   
					{{ Form::close() }}
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						<button id="expandItem1" class="btn btn-success expandItem" type="button"><i id="expandGlyph1" class="glyphicon glyphicon-chevron-down"></i></button>
						Window Services
					</a>
					
				</h4>
			</div>
			<div id="collapseThree" class="panel-collapse collapse">
				<div class="panel-body">
					<div class="well-small" id="leads">
                        <div style="float:left;">Window Style: {{ Form::select('ddwinService', array('default' => 'Please select a window style') + $winStyle, 'test', array('class' => 'winService form-control', 'id' => 'ddwinService', 'style' => 'height: 25px; padding: 2px 12px;')) }}</div>
                         <!-- <button type="button" class="btn btn-link" id="btnwinserviceadd" style="float:right;">Add Window Service</button> -->
                        <br/>
                        <table class="table table-condensed table-striped" id="my-containing-data" style="margin:2px;"><thead><th style="width:29%;">Title</th><th style="width:56%;">Description</th><th style="width:90px;">Price</th><th>&nbsp;Delete</th></thead><tbody class=\"existing\"></table>
                        
                        {{ Form::open(array('url' => 'estimates/admin/style/addwinservice')) }}
                            {{ Form::hidden('winservicestyleadd', '0', array('class' => '', 'id' => 'winservicestyleadd')) }}
                           
                            <div style="float:left;" id="divwinserviceadd"> 
                            	
                                {{ Form::text('winservicetitleadd', '', array('class' => 'form-control', 'placeholder' => 'Title for new service', 'id' => 'winservicetitleadd', 'style' => 'width:250px;border:1px solid #428bca; float:left; margin-right:53px;margin-left:7px; height: 25px; padding: 2px 6px;')) }}
                                
                                {{ Form::text('winservicedescadd', '', array('class' => 'form-control', 'placeholder' => 'Description for new service', 'id' => 'winservicedescadd', 'style' => 'width:500px;border:1px solid #428bca; float:left;margin-right:87px;height: 25px; padding: 2px 6px;')) }}
                                
                                {{ Form::text('winservicepriceadd', '', array('class' => 'form-control', 'placeholder' => 'Price', 'id' => 'winservicepriceadd', 'style' => 'width:70px;border:1px solid #428bca; float:left;margin-right:20px;height: 25px; padding: 2px 6px;')) }}
                               
                                {{ Form::submit('Add', ['class' => 'btn btn-primary', 'id' => 'btnAddWinService', 'disabled' => 'disabled', 'style' => 'width: 55px;height: 25px; padding: 2px 6px;']) }}
                           
                            </div> <br/><br/>               
                		{{ Form::close() }}  
                		
                        <div id='divwinserviceupdatebtn'></div>
                    </div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
						<button id="expandItem1" class="btn btn-success expandItem" type="button"><i id="expandGlyph1" class="glyphicon glyphicon-chevron-down"></i></button>
						Service Options
					</a>
				</h4>
			</div>
			<div id="collapseFour" class="panel-collapse collapse">
				<div class="panel-body">
					<div class="well-small" id="leads">
                        <div style="float:left;">
	                        Window Style: {{ Form::select('ddwinStyleServiceOption', array('default' => 'Please select a window style') + $winStyle, 'test', array('class' => 'winStyle form-control', 'id' => 'ddwinStyleServiceOption')) }}
                            &nbsp;&nbsp;&nbsp;<p id="divDDWinServiceOption" style="float:right;"></p>
							<!-- {{ Form::select('ddwinServiceOption', array('default' => 'Select Service'), 'test', array('class' => 'winService', 'id' => 'selectWinService2')) }} -->
                        </div>
                        <div id="loading" style="float:right;display:none;">Loading...</div><br/><br/>
                        <table class="table table-condensed table-striped" id="dataDDwinStyleServiceOption" style="margin:2px;"><!-- <thead><th style="width:29%;">Title</th><th style="width:48%;">Description</th><th style="width:90px;">Price</th><th style="width:100px;">Services</th><th>&nbsp;Delete</th></thead><tbody class=\"existing\"> -->
                        </table>
                        {{ Form::open(array('url' => 'estimates/admin/style/addwinserviceoption')) }}
                        {{ Form::hidden('winserviceoptionstyleadd', '0', array('class' => '', 'id' => 'winserviceoptionstyleadd')) }}
                            
						<div style="display:none;float:left;" id="divserviceoptionsadd">
							
							{{ Form::text('winserviceoptiontitleadd', '', array('class' => 'form-control winServiceAdd', 'placeholder' => 'Title for new service option', 'id' => 'winserviceoptiontitleadd', 'style' => 'width:250px;border:1px solid #428bca; float:left; margin-right:53px;margin-left:7px;')) }}
							
							{{ Form::text('winserviceoptiondescadd', '', array('class' => 'form-control winServiceAdd', 'placeholder' => 'Description for new service option', 'id' => 'winserviceoptiondescadd', 'style' => 'width:450px;border:1px solid #428bca; float:left;margin-right:53px;')) }}
						   
							{{ Form::text('winserviceoptionpriceadd', '', array('class' => 'form-control winServiceAdd', 'placeholder' => 'Price', 'id' => 'winserviceoptionpriceadd', 'style' => 'width:70px;border:1px solid #428bca; float:left;margin-right:25px;')) }}
						   
							
							<!-- Button trigger modal -->
							<button type="button" class="btn btn-link btn-sm" data-toggle="modal" data-target="#myModal">
							  View
							</button>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<!-- Modal -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									<h4 class="modal-title" id="myModalLabel">Select Services</h4>
								  </div>
								  <div class="modal-body" id="modalbody1">
								  {{-- Form::select('123winStyleServiceOption', array('default' => 'Please select a window style') + $winStyle, 'test', array('class' => 'winStyle', 'id' => '123winStyleServiceOption')) --}}
								  {{--  
									 @foreach($winStyle as $service) 
									 <div class="pull-left col-lg-6 service-container">
									  <div class="checkbox pull-left">
									   <label>
										<input type="checkbox" name="services[]" value=""  /><span>{{ $service }}</span>
									   </label>
									  </div>
									  
									 </div>
									 @endforeach --}}
								  </div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
								  </div>
								</div>
							  </div>
							</div>
					
								{{ Form::submit('Add', ['class' => 'btn btn-primary winServiceAdd', 'id' => 'btnAddWinServiceOption', 'disabled' => 'disabled', 'style' => 'margin-left: 10px;']) }}
						</div><br/><br/>
                        
                            {{ Form::close() }}
                            
                        <div id='divwinserviceoptionupdatebtn'></div>
                    </div>
				</div>
			</div>
		</div>
	</div>		
</div>
	

{{-- var_dump($rooms) --}}
{{-- var_dump($dir) --}}
{{-- var_dump($winStyle) --}}
{{-- var_dump($winService) --}}
{{-- var_dump($options) --}}

@stop

@section('scripts')
	<script type='text/javascript' src="{{ asset('js/admin.js') }}"></script>
@stop

