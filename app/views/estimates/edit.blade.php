@extends('layouts.fullwidth')

{{-- Web site Title --}}
@section('title')
Edit Estimate
@stop

@section('styles')
<link href="{{ asset('css/estimate.css') }}" rel="stylesheet">

<!-- image uploader and viewer -->
<link href="{{ asset('css/image-upload.css') }}" rel="stylesheet">
<link href="{{ asset('js/ext/lightbox/css/lightbox.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
	<div class="well well-small" id="estimate">
		<div class="title">
			<h3>Estimate Editor</h3>
		</div>
		@if($job->company_name)
			<h4>{{ $job->company_name }}</h4>
		@endif
		<h5 class="bold" id="custName">{{ $job->customer_lname.", ".$job->customer_fname }}</h5>
		<span id="jobAddress">{{ $job->job_address }},</span>
		<span id="jobCity">{{ $job->job_city.", CA  ".$job->job_zip }}</span><br/>
		<span id="jobPhone">{{ $job->customer_phone }}</span>
		<span id="jobEmail">{{ $job->customer_email }}</span>
		<div><a class="btn btn-primary back-to-job-button" href="{{ '/job/'.$job->job_id }}">{{ "Back to Job #".$job->job_id }}</a></div>
	</div>

	@if($est['sold'] == '1')
	<?php $sold = true; ?>
	<div class="well well-small sold-no-edits">
	This estimate is sold and cannot be edited further.
	</div>
	<script>
		window.estimateIsSold = true;
	</script>
	@endif

	<div class="well well-small" id="items">
		{{ Form::open(array('url' => 'estimate/update1', 'class' => 'estimate-form')) }}
			<h4 class="estTitle">Title: </h4>
			{{ Form::text('description', $est['description'], array('id' => 'description', 'class' => 'descriptionBox form-control')) }}
			<h5 class="estDate" id="estDate">{{ date("m/d/Y - g:i a", strtotime($est['updated_at'])) }}</h5>
			<div class="titleContainer">
				<h4 class="title">Items</h4>
				{{ Form::button('<i class="glyphicon glyphicon-plus"></i>', array('type' => 'button', 'class' => 'btn btn-primary addItem')) }}
				<div class="heading bold">
					<span class="itemNum">#</span
					><span class="room">Room</span
					><span class="dir">Dir.</span
					><span class="style">Style</span
					><span class="titleService">Service Type</span
					><span class="titleBalancer">Balancer</span
					><span class="titleDimension">Dimension"</span
					><span class="titleParts">Parts</span
					><span class="titleStatus">Status</span
					><span class="titlePrice">Price</span>
				</div>
			</div>
			{{ Form::hidden('est_id', $est['id']) }}
			{{ Form::hidden('job_id', $est['job_id'], array('id' => 'job_id')) }}
			{{ Form::hidden('cust_id', $est['cust_id']) }}
			{{ Form::hidden('updated_at', $est['updated_at'], array('id' => 'updated_at')) }}
			<div id="parent">
				@for ($i = 1; $i <= $length; $i++)
					<div id="item_group_{{ $i }}" class="well well-small clonedSection" data-id="{{ $i }}">
						<div class="estimate-drag-handle"><i class="glyphicon glyphicon-move"></i></div
>{{ Form::hidden('item['.$i.'][itemID]', $est['items'][$i-1]->id, array('id' => 'itemID'.$i)) 
}}{{ Form::text('item'.$i, $i, array('readOnly' => 'true', 'class' => 'itemNum', 'id' => 'item'.$i))
}}{{ Form::hidden('item['.$i.'][item_num]', $i, array('id' => 'itemHidden'.$i))
}}{{ Form::select('item['.$i.'][room]', $rooms, $est['items'][$i-1]->room, array('class' => 'room', 'id' => 'room'.$i))
}}{{ Form::select('item['.$i.'][dir]', $dir, $est['items'][$i-1]->dir, array('class' => 'dir', 'id' => 'dir'.$i))
}}{{ Form::hidden('item['.$i.'][win_num]', $est['items'][$i-1]->win_num, array('class' => 'winNum', 'id' => 'winNum'.$i))
}}{{ Form::select('item['.$i.'][win_style]', $winStyle, $est['items'][$i-1]->win_style, array('class' => 'winStyle', 'id' => 'winStyle'.$i))
}}{{ Form::select('item['.$i.'][win_serv]', array('1' => 'Service'), '1', array('disabled' => 'disabled', 'class' => 'service', 'id' => 'shortService'.$i, 'data-loaded-value' => $est['items'][$i-1]->win_serv))
}}{{ Form::select('item['.$i.'][balancer]', array('1' => 'Service'), '1', array('disabled' => 'disabled', 'class' => 'balancer', 'id' => 'balancer'.$i, 'data-loaded-value' => $est['items'][$i-1]->balancer))
}}<div class="dimension_column"
>{{ Form::text('item['.$i.'][sash_width]', $est['items'][$i-1]->sash_width, array('class' => 'dimWidth dim', 'id' => 'dimWidth'.$i, 'placeholder' => 'W')) 
}}<span class="sub">x</span
>{{ Form::text('item['.$i.'][sash_height]', $est['items'][$i-1]->sash_height, array('class' => 'dimHeight dim', 'id' => 'dimHeight'.$i, 'placeholder' => 'H')) 
}}</div><div class="parts_column">N/A</div>{{ Form::select('item['.$i.'][status]', array('1' => 'Pending', '2' => 'On Hold', '3' => 'Done'), $est['items'][$i-1]->status, array('class' => 'status', 'id' => 'status'.$i))
}}<div class="rt_buttons">{{ Form::button('<i class="glyphicon glyphicon-chevron-down" id="expandGlyph'.$i.'"></i>', array('type' => 'button', 'class' => 'btn btn-success expandItem', 'id' => 'expandItem'.$i, 'title' => 'Expand'))
}}{{ Form::button('<i class="glyphicon glyphicon-book" id="copyGlyph'.$i.'"></i>', array('type' => 'button', 'class' => 'btn btn-info cloneItem', 'id' => 'cloneItem'.$i, 'title' => 'Clone'))
}}{{ Form::button('<i class="glyphicon glyphicon-remove"></i>', array('type' => 'button', 'class' => 'btn btn-danger closeItem', 'id' => 'closeItem'.$i, 'title' => 'Remove'))
}}</div>{{ Form::text('price'.$i, '', array('placeholder' => '$', 'class' => 'price', 'id' => 'price'.$i, 'disabled')) }}
						<div class="subGroup collapse" id="subGroup{{ $i }}" style="">
							<div class="row2">
							{{ Form::text('item['.$i.'][serv_desc]',  $est['items'][$i-1]->serv_desc, array('placeholder' => 'Service description', 'class' => 'description', 'id' => 'description'.$i)) }}{{ Form::text('item['.$i.'][serv_price]', $est['items'][$i-1]->serv_price, array('placeholder' => '$', 'class' => 'servPrice', 'id' => 'servPrice'.$i)) }}
							</div>
							<div class="row3">
								<span class="bold left25">Sash Thickness: </span>
								<div class="input-group sashThick">
									<span class="input-group-addon sashThick">
										{{ Form::radio('item['.$i.'][sash_thick]', '1', true, ['class' => 'thickness', 'id' => 'sashThin'.$i]) }} 1 3/8"
									</span>
									<span class="input-group-addon sashThick">
										{{ Form::radio('item['.$i.'][sash_thick]', '2', false, ['class' => 'thickness', 'id' => 'sashThick'.$i]) }} 1 3/4"
									</span>
								</div>
							</div>
							<div class="row">
								{{ Form::hidden('item['.$i.'][serv_options]', $est['items'][$i-1]->serv_options, array('id' => 'servOptions'.$i, 'class' => 'serv_options_value')) }}
								<fieldset class="custom-fieldset" id="options{{ $i }}">
	    							<legend>Options</legend>
	    							<div class="service-options">
	    							<span class="none">No options chosen</span>
	    							</div>
	    							<div class="new-service-option">
		    							<select class="option-chooser">
		    								<option value="0" selected>Choose an option...</option>
		    								@foreach($options as $option)
		    									<option value="{{ $option->id }}" data-service="{{ $option->serv_id }}" data-price="{{ $option->price }}">{{ $option->title }}</option>
		    								@endforeach
		    							</select
		    							><input class="option-price optionsPrice" value="" readonly="readonly"
		    							><button class="btn btn-primary btn-sm btn-add-option">Add</button>
	    							</div>
								</fieldset>
							</div>
							<div class="row">
								{{ Form::hidden('item['.$i.'][parts]', $est['items'][$i-1]->parts, ['id' => 'itemparts'.$i, 'class' => 'loaded-parts', 'data-item-num' => $i]) }}
							</div>
							<div class="images" data-allow-upload="true" data-image-owner-type="estimate" data-image-owner-id="{{ $est['items'][$i-1]->id }}"></div>
						</div>
					</div>
				@endfor
			</div>
			<div class="row rowTotal">
				{{ Form::text('totalPrice', '$', array('readOnly' => 'true', 'class' => 'totalPrice form-control total', 'id' => 'totalPrice')) }}
				{{ Form::label('totalPrice', 'Total&nbsp;', array('class' => 'lblTotal')) }}
				<textarea class="estimate-notes" name="estimate_notes" placeholder="Estimate Notes (optional)">{{ $est['notes'] }}</textarea>
			</div>
			<div class="row rowTotal">
				<button class="btn btn-sm btn-info" id="btnResetDeposit" title="Reset to Default"><i class="glyphicon glyphicon-remove"></i></button>
				@if(!empty($est['deposit']) || $est['deposit'] == 0)
					{{ Form::text('deposit', $est['deposit'], array('class' => 'deposit form-control total', 'id' => 'deposit', 'data-modified' => '1')) }}
				@else
					{{ Form::text('deposit', '$', array('class' => 'deposit form-control total', 'id' => 'deposit')) }}
				@endif
				{{ Form::label('deposit', 'Deposit&nbsp;', array('class' => 'lblTotal')) }}
			</div>
			<div class="row rowTotal">
				{{ Form::text('balance', '$', array('readOnly' => 'true', 'class' => 'balance form-control total', 'id' => 'balance')) }}
				{{ Form::label('balance', 'Balance Due&nbsp;', array('class' => 'lblTotal')) }}
			</div>
			{{ Form::button('Update&nbsp;&nbsp;<span class="glyphicon glyphicon-check"></span>', ['type' => 'submit', 'class' => 'btn btn-primary form-control', 'id' => 'saveEstimate']) }}
			{{ Form::button('Share&nbsp;&nbsp;<span class="glyphicon glyphicon-share"></span>', ['class' => 'btn btn-success form-control', 'id' => 'btnShareEstimate']) }}
			{{ Form::button('Print&nbsp;&nbsp;<span class="glyphicon glyphicon-print"></span>', ['class' => 'btn btn-success form-control', 'id' => 'btnPrintEstimatePDF']) }}
			{{ Form::button('Parts', ['class' => 'btn btn-info form-control', 'id' => 'btnShowAllParts']) }}
			@if(Input::get('print'))
				{{ Form::button('Back&nbsp;&nbsp;<span class="glyphicon glyphicon-arrow-left"></span>', ['class' => 'btn btn-info form-control', 'data-id' => $est['job_id'], 'id' => 'btnBackToJob']) }}
			@endif
		{{ Form::close() }}
	</div>

	<div class="modal fade" tabindex="-1" id="modalShare">
		<div class="modal-dialog" id="pdfModal">
			<div class="modal-content">
				<div class="modal-header email-header">
					<h4 class="modal-title">Email Estimate</h4>
				</div>
				<div class="modal-body" id="pdfModalBody">
					<div class="input-group" id="pdfBox"></div><!-- /input-group -->
					<div class="input-group" id="emailBox">
						<label class="field">
							<span>To:</span>
							<input type="text" value="{{ !empty($customer) ? $customer->email : ''}}" placeholder="Customer's Email" id="email_to">
						</label>
						<label class="field">
							<span>Cc:</span>
							<input type="text" id="email_cc">
						</label>
						<label class="field">
							<span>Bcc:</span>
							<input type="text" id="email_bcc">
						</label>
						<label class="field">
							<span>From:</span>
							<input type="text" readonly value="{{ $user->email }}" id="email_from">
						</label>
						<label class="field">
							<span>Subject:</span>
							<input type="text" value="WindowRnR - Window Repair Estimate" id="email_subject">
						</label>
						<label class="field">
							<span>Template:</span>
							<div class="select-holder"><select id="email_template"></select></div>
							<a href="/email-templates?src=est" target="_blank">Manage</a>
							<a href="#" id="refreshTemplates">Refresh</a>

							<!-- values used for variable replacement -->
							<input type="hidden" id="vr_customer_lname" value="{{ trim($customer->l_name) }}">
							<input type="hidden" id="vr_customer_fname" value="{{ trim($customer->f_name) }}">
							<input type="hidden" id="vr_signature" 
								value="<a href='http://www.windowrestorationandrepair.com/'><img src='http://app.windowrnr.com/img/logo.png' width='225'></a><br/><div style='font-family: Calibri; font-size: 12;'>3377 Cerritos Ave., Los Alamitos, CA 90720<br/>(562) 493-1590 &nbsp; &#124; &nbsp; <a href='www.WindowRnR.com'>www.WindowRnR.com</a></div>">
						</label>

<!-- <textarea id="email_content">Dear {{ trim($customer->f_name . ' ' . $customer->l_name) }},

Please find the estimate attached.

Sincerely,
Scott Campbell
WindowRnR</textarea> -->

<div contenteditable="true" id="email_content">
Dear {{ trim($customer->f_name . ' ' . $customer->l_name) }},<br><br>
Please find the estimate attached.<br><br>
Sincerely,<br>
Scott Campbell<br>
WindowRnR
</div>

<pre id="email_default_content" class="hidden">Dear {{ trim($customer->f_name . ' ' . $customer->l_name) }},<br><br>
Please find the estimate attached.<br><br>
Sincerely,<br>
Scott Campbell<br>
WindowRnR</pre>

						<label class="field">
							<span>Attachment:</span>
							<span class="file-label">{{ $est['description'] }}.pdf</span>
							<input type="hidden" id="email_attachment" value="">
						</label>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pdfFooter">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="btnSavePDF">Save PDF</button>
						<button type="button" class="btn btn-primary" id="btnPrintPDF">Print PDF</button>
						<button type="button" class="btn btn-success" id="btnEmailPDF">Email</button>
						<div class="status-line pdf-status-line"></div>
						<div class="saved-pdf hidden">
							@if(!empty($pdf))
								<?php
									$savedAt = strtotime($pdf->uploaded_at);
									if(date('Y', $savedAt) === date('Y'))
										$savedAt = date('M j - g:i A', $savedAt);
									else
										$savedAt = date('M j, Y - g:i A', $savedAt);
									$pdfPath = str_replace('&export=download', '', $pdf->path);
								?>
								<div class="done">PDF <span class="sub">(Created: {{ $savedAt }})</span>: <a href="{{ $pdfPath }}" target="_blank">{{ $pdfPath }}</a></div>
							@endif
						</div>
						<input type="hidden" id="pdf_download_url" value="{{ !empty($pdf) ? $pdf->download_url : '' }}">
					</div>
					<div class="email-footer">
						<button type="button" class="btn btn-default" id="btnCloseEmail">Close</button>
						<button type="button" class="btn btn-primary" id="btnSendEmail">Send</button>
						<div class="status-line email-status-line"></div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- markup for cloned items -->
	<div id="new_lineitem_markup">
		<div id="item_group_NEWITEMID" class="well well-small clonedSection newlyAddedSection markupTemplate" data-id="NEWITEMID">
			<div class="estimate-drag-handle"><i class="glyphicon glyphicon-move"></i></div
>{{ Form::hidden('item[NEWITEMID][itemID]', 0, array('id' => 'itemIDNEWITEMID'))
}}{{ Form::text('itemNEWITEMID', 'NEWITEMID', array('class' => 'itemNum', 'id' => 'itemNEWITEMID', 'disabled' => 'disabled', 'readonly' => 'readonly'))
}}{{ Form::hidden('item[NEWITEMID][item_num]', 'NEWITEMID', array('id' => 'itemHiddenNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][room]', $rooms, 0, array('class' => 'room', 'id' => 'roomNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][dir]', $dir, 0, array('class' => 'dir', 'id' => 'dirNEWITEMID'))
}}{{ Form::hidden('item[NEWITEMID][win_num]', 1, array('class' => 'winNum', 'id' => 'winNumNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][win_style]', $winStyle, 0, array('class' => 'winStyle', 'id' => 'winStyleNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][win_serv]', [], '1', array('disabled' => 'disabled', 'class' => 'service', 'id' => 'shortServiceNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][balancer]', array('0' => 'N/A', '1' => 'B1'), '0', array('disabled' => 'disabled', 'class' => 'balancer', 'id' => 'balancerNEWITEMID'))
}}<div class="dimension_column"
>{{ Form::text('item[NEWITEMID][sash_width]', '', array('class' => 'dimWidth dim', 'id' => 'dimWidthNEWITEMID', 'placeholder' => 'W')) 
}}<span class="sub">x</span
>{{ Form::text('item[NEWITEMID][sash_height]', '', array('class' => 'dimHeight dim', 'id' => 'dimHeightNEWITEMID', 'placeholder' => 'H')) 
}}</div><div class="parts_column">N/A</div>{{ Form::select('item[NEWITEMID][status]', array('1' => 'Pending', '2' => 'On Hold', '3' => 'Done'), '1', array('class' => 'status', 'id' => 'statusNEWITEMID'))
}}<div class="rt_buttons">{{ Form::button('<i class="glyphicon glyphicon-chevron-down" id="expandGlyphNEWITEMID'.'"></i>', array('type' => 'button', 'class' => 'btn btn-success expandItem', 'id' => 'expandItemNEWITEMID', 'title' => 'Expand'))
}}{{ Form::button('<i class="glyphicon glyphicon-book" id="copyGlyphNEWITEMID"></i>', array('type' => 'button', 'class' => 'btn btn-info cloneItem', 'id' => 'cloneItemNEWITEMID', 'title' => 'Clone'))
}}{{ Form::button('<i class="glyphicon glyphicon-remove"></i>', array('type' => 'button', 'class' => 'btn btn-danger closeItem', 'id' => 'closeItemNEWITEMID', 'title' => 'Remove'))
}}</div>{{ Form::text('priceNEWITEMID', '', array('placeholder' => '$', 'class' => 'price', 'id' => 'priceNEWITEMID', 'disabled')) }}
			<div class="subGroup collapse" id="subGroupNEWITEMID" style="">
				<div class="row2">
				{{ Form::text('item[NEWITEMID][serv_desc]', '', array('placeholder' => 'Service description', 'class' => 'description', 'id' => 'descriptionNEWITEMID')) }}{{ Form::text('item[NEWITEMID][serv_price]', '', array('placeholder' => '$', 'class' => 'servPrice', 'id' => 'servPriceNEWITEMID')) }}
				</div>
				<div class="row3">
					<span class="bold left25">Sash Thickness: </span>
					<div class="input-group sashThick">
						<span class="input-group-addon sashThick">
							{{ Form::radio('item[NEWITEMID][sash_thick]', '1', true, ['class' => 'thickness', 'id' => 'sashThinNEWITEMID']) }} 1 3/8"
						</span>
						<span class="input-group-addon sashThick">
							{{ Form::radio('item[NEWITEMID][sash_thick]', '2', false, ['class' => 'thickness', 'id' => 'sashThickNEWITEMID']) }} 1 3/4"
						</span>
					</div>
				</div>
				<div class="row">
					{{ Form::hidden('item[NEWITEMID][serv_options]', '', array('id' => 'servOptionsNEWITEMID', 'class' => 'serv_options_value')) }}
					<fieldset class="custom-fieldset" id="optionsNEWITEMID">
						<legend>Options</legend>
						<div class="service-options">
						<span class="none">No options chosen</span>
						</div>
						<div class="new-service-option">
							<select class="option-chooser">
								<option value="0" selected>Choose an option...</option>
								@foreach($options as $option)
									<option value="{{ $option->id }}" data-service="{{ $option->serv_id }}" data-price="{{ $option->price }}" style="display: none">{{ $option->title }}</option>
								@endforeach
							</select
							><input class="option-price optionsPrice" value="" readonly="readonly"
							><button class="btn btn-primary btn-sm btn-add-option">Add</button>
						</div>
					</fieldset>
				</div>
				<div class="row">
					{{ Form::hidden('item[NEWITEMID][parts]', '[]', ['id' => 'itempartsNEWITEMID', 'class' => 'loaded-parts', 'data-item-num' => 'NEWITEMID']) }}
				</div>
				<?php /* <div class="images" data-allow-upload="true" data-image-owner-type="estimate" data-image-owner-id="{{ $est['items'][$i-1]->id }}"></div> */ ?>
			</div>
		</div>
	</div>

	<?php /*  modal for parts details */ ?>
	<div class="modal" tabindex="-1" id="modalParts">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Parts</h4>
				</div>
				<div class="modal-body" id="partsModalBody">
					Hey There!
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<?php /* modal for inputting part-option value */ ?>
	<div class="modal" tabindex="-1" id="modalPartOptionValue">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Parts</h4>
				</div>
				<div class="modal-body">
					<h5 class="modal-heading">Hehehe</h5>
					<ul class="choosable-options"></ul>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" onclick="return partOptionValueSelected.call(this, '')">Close</button>
				</div>
			</div>
		</div>
	</div>

	<iframe name="printIFrame"></iframe>

@stop

@section('scripts')
	<script type='text/javascript' src="{{ asset('js/itemAdd.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/estimate.js') }}"></script> 
	<script type='text/javascript' src="{{ asset('js/estimateEdit.js') }}"></script> 
	<script type="text/javascript">
		var rooms = <?php echo json_encode($rooms, JSON_NUMERIC_CHECK); ?>;
		var dir = <?php echo json_encode($dir, JSON_NUMERIC_CHECK); ?>;
		var winStyle = <?php echo json_encode($winStyle, JSON_NUMERIC_CHECK); ?>;
		var balancers = <?php echo json_encode($balancers, JSON_NUMERIC_CHECK); ?>;
		var winStylesWithBalancer = <?php echo json_encode($winStylesWithBalancers, JSON_NUMERIC_CHECK); ?>;
		var allParts = <?php echo json_encode($allParts, JSON_NUMERIC_CHECK); ?>;
		var editPage = true;
		var estimateParts = <?php echo $est['parts'] ? $est['parts'] : '[]' ?>;
		var estimatePartsList = <?php echo json_encode($partsList, JSON_NUMERIC_CHECK); ?>;
		estimatePartsList.parts = JSON.parse(estimatePartsList.parts);

		var showAP = {{ Input::get('ap') ? Input::get('ap') : 'false' }};
	</script>

	<!-- image upload -->
	<script type='text/javascript' src="{{ asset('js/ext/dmuploader.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/ext/lightbox/js/lightbox.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/image-upload.js') }}"></script>

	@if(Input::get('print') == 1)
	<script>
		var printMe = true;
	</script>
	@endif
@stop

