<?php if($withHead === true) { ?><!DOCTYPE html>
<html lang="en" style="width: 11.5in">
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
</head>
<body><?php } ?>

<style>
@page {
	size: landscape;
}

<?php if($withHead === true) { ?>
body {
	padding: 10px;
}
<?php } ?>

#container {
	padding: 10px;
	color:windowtext;
	font-size: 13.33px;
	font-weight: 400;
	font-style: normal;
	text-decoration: none;
	font-family: sans-serif;
	vertical-align:bottom;
	white-space:nowrap;
	width: 11in;
}

#head_1 {
	overflow: hidden;
	padding-bottom: 5px;
	border-bottom: 1px solid #eee;
	position: relative;
	height: 75px;
}
#head_1 > * {
	position: absolute;
	bottom: 5px;
}
#head_1 > img {
	left: 0;
}
#head_1 > .caption {
	left: 310px;
	bottom: 3px;
	font-size: 12px;
	font-weight: bold;
}
#head_1 > .caption-right {
	right: 0;
	color: #D9D9D9;
	font-size:26.66px;
	font-weight:400;
	bottom: 3px;
}
#head_1 > .caption-right .date {
	font-size: 16px;
	color: #333;
	text-align: right;
}

#head_2 {
	overflow: hidden;
	position: relative;
	min-height: 118px;
	color:windowtext;
	font-size: 10.66px;
	font-weight: 400;
	font-style: normal;
	text-decoration: none;
	font-family: sans-serif;
	vertical-align:bottom;
	white-space:nowrap;
}
#head_2 > * {
	position: absolute;
	bottom: 5px;
}
#head_2 > .block-1 {
	left: 0;
	width: 296px;
}
#head_2 > .block-1 > *:not(:last-child) {
	padding-bottom: 8px;
}
#head_2 > .block-1 .lic {
	font-weight: bold;
}
#head_2 > .block-2 {
	left: 297px;
	width: 374px;
	text-align: center;
	font-weight: bold;
	padding-bottom: 40px;
}
#head_2 > .block-3 {
	right: 0;
	text-align: right;
	padding-bottom: 15px;
}
#head_2 > .block-3 .name {
	font-size: 16px;
	font-weight: bold;
	color: #555;
	padding-bottom: 4px;
}

#container table {
	border-collapse: collapse;
	table-layout: fixed;
	width: 100%;
	padding:0px;
	color:windowtext;
	font-size: 10.66px;
	font-weight: 400;
	font-style: normal;
	text-decoration: none;
	font-family: sans-serif;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
}
#container table tr.head td {
	border-top: 1px solid #999;
	background-color: #ddd;
	font-size: 11.5px;
	vertical-align: middle;
}
#container table tr.empty {
	background-color: #f7f7f7;
}
#container table tr td {
	border-left: 1px solid #999;
	border-bottom: 1px solid #999;
	vertical-align: top;
	padding: 2px 5px;
}
#container table tr td:nth-child(5) {
	white-space: pre-wrap;
}
#container table tr td:nth-child(5).options {
	white-space: nowrap;
}
#container table tr td:last-child {
	border-right: 1px solid #999;
}

#container table tr td.acenter {
	text-align: center;
}
#container table tr td.aleft {
	text-align: left;
}
#container table tr td.aright {
	text-align: right;
}
#container table tr td.nobottomborder {
	border-bottom-width: 0;
}
#container table tr td.options {
    overflow: hidden;
}
#container table tr td.options > div {
    float: left;
    width: 32.5%;
}

#foot {
	font-size: 10.66px;
	font-family: sans-serif;
	position: relative;
	margin-top: 5px;
}
#foot .line {
	width: 800px;
}
#foot .abbr {
    overflow: hidden;
    margin-top: 8px;
}
#foot .abbr > div.lbl {
	float: left;
    margin-right: 10px;
}
#foot .abbr > div:not(.lbl) {
    float: left;
    margin-right: 20px;
    min-width: 150px;
}
#foot .block-2 {
	position: absolute;
	right: 0;
	bottom: 42px;
	font-size: 13.33px;
	font-family: sans-serif;
	text-align: right;
}
#foot .sign {
	text-align: left;
	padding-top: 20px;
}
-->
</style>

<div id="container">

<div id="head_1">
	{{ HTML::image('img/logo.png') }}
	<div class="caption">&quot;...repairing windows is ALL we do.&quot;</div>
	<div class="caption-right">
		Quote/Order
		<div class="date">{{ $date }}</div>
	</div>
</div>

<div id="head_2">
	<div class="block-1">
		<div class="lic">Lic. #987212</div>
		<div class="address">
			<div>3377 Cerritos Ave</div>
			<div>Los Alamitos, CA 90720</div>
		</div>
		<div class="contact">
			<div>Office: 562-493-1590</div>
			<div>Web: www.WindowRnR.com</div>
			<div>Email: info@WindowRnR.com</div>
		</div>
	</div>
	<div class="block-2">
		<div><i>What we do <u>NOT</u> do...</i></div>
		<div>1. Paint&nbsp;&nbsp;&nbsp;2. Re-Screen&nbsp;&nbsp;&nbsp;3. Glazing putty&nbsp;&nbsp;&nbsp;4. Strip window entirely</div>
	</div>
	<div class="block-3">
		<div class="name">{{ $name }}</div>
		<div class="address">
			<div>{{ $address }}</div>
			<div>{{ $addLine2 }}</div>
			<div>{{ $phone }}</div>
			<div>{{ $email }}</div>
		</div>
	</div>
</div>

<?php 
setlocale(LC_MONETARY, 'en_US');
$totalWindows = 0;
?>

<table border=0 cellpadding=2 cellspacing=0>

	<tr class="head">
		<td class="acenter" style="width: 20px">#</td>
		<td style="width: 50px">&nbsp;Rm</td>
		<td style="width: 50px">&nbsp;Dirc</td>
		<td style="width: 50px">&nbsp;Style</td>
		<td>&nbsp;Service Description</td>
		<td class="acenter" style="width: 60px">Dim.</td>
		<td class="acenter" style="width: 40px">Qty</td>
		<td class="acenter" style="width: 60px">Item</td>
		<td class="acenter" style="width: 60px">Serv Optns</td>
		<td class="acenter" style="width: 50px">Opt. $</td>
		<td class="acenter" style="width: 50px">Balancer</td>
		<td class="acenter" style="width: 70px">Total</td>
	</tr>

	<?php
	$hasOptions = false;
	$optionIDs = [];
	?>
 	@foreach ($items as $key => $item)
		<?php
		if(!$hasOptions) $hasOptions = !($item['opt'] == 0);
		
		if(is_numeric($item['item'])) {
			$item['item'] = intval($item['item']);
		}
		if(is_numeric($item['opt'])) {
			$item['opt'] = intval($item['opt']);
		}
		if(is_numeric($item['lineTotal'])) {
			$item['lineTotal'] = floatval($item['lineTotal']);
		}
		?>
	<tr class="item">
		<td class="acenter">{{ $key+1 }}</td>
		<td>{{ $item['room'] }}</td>
		<td>{{ $item['dir'] }}</td>
		<td>{{ $item['style'] }}</td>
		<td>{{ $item['serv'] }}</td>
		<td class="acenter">{{ $item['dim'] }}</td>
		<td class="acenter">{{ $item['qty'] }}</td>
		<td class="aright">{{ '$ '.money_format("%!.0i", $item['item']) }}</td>
		<?php
		if($item['opt'] !== 0) {
			$itemOpt = str_replace("[", "", $item['opts']);
			$itemOpt = str_replace("]", "", $itemOpt);
			$itemOptionIDs = explode(", ", $itemOpt);
			$itemOptionIDs = array_map(function($_item) {
				return intval($_item);
			}, $itemOptionIDs);
			foreach ($itemOptionIDs as $itemOptionID) {
				if(!in_array($itemOptionID, $optionIDs)) {
					$optionIDs[] = $itemOptionID;
				}
			}
		}
		?>
		<td class="acenter">{{ $item['opt'] == 0 ? 'None' : $item['opts'] }}</td>
		<td class="aright">{{ '$ '.money_format("%!.0i", $item['opt']) }}</td>
		<td class="acenter">{{ $item['bal'] }}</td>
		<td class="aright">{{ '$ '.money_format("%!.0i", $item['lineTotal']) }}</td>
	</tr>
	<?php
	$totalWindows += intval($item['qty']);
	?>
 	@endforeach

 	<?php emptyRow(); ?>
	
	<!-- estimate-notes -->
 	@if(!empty($notes))
	 	<tr>
			<td class="acenter">-</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="acenter">Notes:</td>
			<td>{{ nl2br($notes) }}</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<?php emptyRow(); ?>
 	@endif

 	<!-- options -->
 	@if($hasOptions)
		<tr>
			<td class="acenter">-</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="acenter">Options:</td>
			<td class="options">
				@foreach($options as $option)
					@if(in_array($option->id, $optionIDs))
						<div>{{ $option->id }} - {{ $option->title }}</div>
					@endif
				@endforeach
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	@endif
	<?php 
		if(is_numeric($total)) {
			$total = floatval($total);
		}
		if(is_numeric($dep)) {
			$dep = floatval($dep);
		}
		if(is_numeric($balDue)) {
			$balDue = floatval($balDue);
		}
	?>
	<tr>
		<td colspan=6 class="aright nobottomborder">Total Windows&nbsp;</td>
		<td class="acenter">{{ $totalWindows }}</td>
		<td colspan=4 class="aright nobottomborder">Total&nbsp;</td>
		<td class="aright">{{ '$ '.money_format("%!.0i", $total) }}&nbsp;</td>
	</tr>
	<tr>
		<td colspan=11 class="aright nobottomborder">Deposit&nbsp;</td>
		<td class="aright">{{ is_numeric($dep) ? '$ '.money_format("%!.0i", $dep) : '' }}&nbsp;</td>
	</tr>
	<tr>
		<td colspan=11 class="aright">Balance&nbsp;</td>
		<td class="aright">{{ '$ '.money_format("%!.0i", $balDue) }}&nbsp;</td>
	</tr>

</table>

<div id="foot">
	<div class="block-1">
		<div class="line">* Paint prep, priming &amp; painting will be necessary after our work. We don't apply glazing putty unless explicitly listed in quote. Painters/glazier references available upon request
		</div>
		<div class="line">* Price subject to visible conditions, <u>additional unforeseen damage may incur extra costs</u>; add $5 per 1/2 lb for steel counterweights as needed
		</div>
		<div class="line">* Installation starts @ 7:30am. We will need 3 feet of working space in front of each window (interior). Install date contingent on weather conditions
		</div>
		<div class="line abbr">
			<div>dh-double hung</div>
			<div>pix-picture</div>
			<div>ws-weather-strip</div>
			<div>is-inswing</div>
			<div>os-outswing</div>
			<div>bmp-bump</div>
			<div>rbt-rabbit</div>
			<div>sc-single casement</div>
			<div>xo-glider</div>
			<div>sb-slide bolt</div>
			<div>lck-lock</div>
			<div>stc-stucco mould</div>
			<div>bs-blind stop</div>
			<div>nl-night lock</div>
			<div>dc-double casement</div>
			<div>awn-awning</div>
			<div>R&amp;R-Remove &amp; Replace</div>
			<div>NEW-Install New sash with glass</div>
			<div>ph-parliament hinges</div>
		</div>
		<div class="sign">
		Sales Rep:____________________&nbsp;Customer Acceptance:___________________________________&nbsp;Date:_________________________
		</div>
	</div>
	<div class="block-2">
		<div>Balance Due upon Completion</div>
		<div>TWO YR WARRANTY!</div>
	</div>
</div>

</div>

<?php if(isset($print) && $print) { ?>
<script>
window.onload = function() {
	window.print();
}
</script>
<?php } ?>

<?php if($withHead === true) { ?></body></html><?php } ?>

<?php
function emptyRow() {
?>
	<tr class="empty">
		<td class="acenter">-</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
<?php
}
?>
