@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Estimate PDF Frame
@stop

@section('styles')
<link rel='stylesheet' type='text/css' href="{{ asset('css/estFrame.css') }}" >
@stop

{{-- Content --}}
@section('content')

<div align="center">
<iframe name="inlineframe" src="/pdf/estimate/{{ $est_id }}" frameborder="5" scrolling="no" width="1056" height="816" marginwidth="5" marginheight="5" seamless ></iframe>
</div>
@stop

