@extends('layouts.fullwidth')

@section('title')
Test Estimate
@stop

@section('styles')
<!-- <link href="{{ asset('css/estimate.css') }}" rel="stylesheet"> -->
@stop

{{-- Content --}}
@section('content')
	<div class="well well-small" id="estimate">
		<div class="title">
			<h3>Test Estimate Builder</h3>
		</div>
	</div>

	<div class="well well-small" id="items">
		{{ Form::open(array('url' => 'api/estimate/save')) }}
			<div>
				{{ Form::label('job_id', 'Job ID') }}
				{{ Form::text('job_id', '3085') }}
			</div>
			<div>
				{{ Form::label('description', 'Description') }}
				{{ Form::text('description', 'Test Estimate') }}
			</div>
			<div>
				{{ Form::label('cust_id', 'Cust ID') }}
				{{ Form::text('cust_id', '4045') }}
			</div>
			<div>
				{{ Form::label('estimate_notes', 'Estimate Notes') }}
				{{ Form::text('estimate_notes', 'Estimate Notes (optional)') }}
			</div>
			<div>
				{{ Form::label('item[0][dir]', 'Direction') }}
				{{ Form::text('item[0][dir]', '2', array('id' => 'dir')) }}
			</div>
			<div>
				{{ Form::label('item[0][serv_price]', 'Service Price') }}
				{{ Form::text('item[0][serv_price]', '285', array('id' => 'serv_price')) }}
			</div>
			<div>
				{{ Form::label('item[0][serv_desc]', 'Service Description') }}
				{{ Form::text('item[0][serv_desc]', 'RnR WDH - Twinds (single)', array('id' => 'serv_desc')) }}
			</div>
			<div>
				{{ Form::label('item[0][win_style]', 'Window Style') }}
				{{ Form::text('item[0][win_style]', '1', array('id' => 'win_style')) }}
			</div>
			<div>
				{{ Form::label('item[0][win_serv]', 'Service') }}
				{{ Form::text('item[0][win_serv]', '1', array('id' => 'win_serv')) }}
			</div>
			<div>
				{{ Form::label('item[0][serv_options]', 'Service Options') }}
				{{ Form::text('item[0][serv_options]', '1-85', array('id' => 'serv_options')) }}
			</div>
			<div>
				{{ Form::label('item[0][room]', 'Room') }}
				{{ Form::text('item[0][room]', '3', array('id' => 'room')) }}
			</div>
			<div>
				{{ Form::label('item[0][sash_width]', 'Sash Width') }}
				{{ Form::text('item[0][sash_width]', '12.5', array('id' => 'sash_width')) }}
			</div>
			<div>
				{{ Form::label('item[0][parts]', 'Parts') }}
				{{ Form::text('item[0][parts]', '', array('id' => 'parts')) }}
			</div>
			<div>
				{{ Form::label('item[0][item_num]', 'Item Number') }}
				{{ Form::text('item[0][item_num]', '1', array('id' => 'item_num')) }}
			</div>
			<div>
				{{ Form::label('item[0][sash_thick]', 'Sash Thickness') }}
				{{ Form::text('item[0][sash_thick]', '1', array('id' => 'sash_thick')) }}
			</div>
			<div>
				{{ Form::label('item[0][balancer]', 'Balancer') }}
				{{ Form::text('item[0][balancer]', '1', array('id' => 'balancer')) }}
			</div>
			<div>
				{{ Form::label('item[0][status]', 'Status') }}
				{{ Form::text('item[0][status]', '1', array('id' => 'status')) }}
			</div>
			<div>
				{{ Form::label('item[0][sash_height]', 'Sash Height') }}
				{{ Form::text('item[0][sash_height]', '10', array('id' => 'sash_height')) }}
			</div>
			{{ Form::button('update', ['type' => 'button', 'id' => 'updateEstimate']) }}
			<textarea id="estIDText" placeholder="Estimate ID# goes here for update"></textarea>
			</br></br>
			<div>
			{{ Form::submit('Submit', ['type' => 'submit', 'id' => 'saveEstimate']) }}
			</div>
	</div>
	  
@stop

@section('scripts')
	<script>
		$('#updateEstimate').click(function() {
			var data = {};
			data.estID = $("#estIDText").val();
			data.job_id = $("input[name=job_id]").val();
			data.description = $("input[name=description]").val();
			data.cust_id = $("input[name=cust_id]").val();
			data.estimate_notes = $("input[name=estimate_notes]").val();
			var items = [];
			items[0][dir] = $("#dir").val();
			items[0].serv_price = $("#serv_price").val();
//alert(items);
			items[0].itemID = '306';
			items[0].serv_desc = $("#serv_desc").val();
			items[0].win_style = $("#win_style").val();
			items[0].win_serv = $("#win_serv").val();
			items[0].serv_options = $("#serv_options").val();
			items[0].room = $("#room").val();
			items[0].sash_width = $("#sash_width").val();
			items[0].parts = $("#parts").val();
			items[0].item_num = $("#item_num").val();
			items[0].sash_thick = $("#sash_thick").val();
			items[0].balancer = $("#balancer").val();
			items[0].status = $("#status").val();
			items[0].sash_height = $("#sash_height").val();
			data.item = items;
			
			$.ajax({
				  type: "POST",
				  url: '/api/estimate/update',
				  data: data,
				  done: (function( data ) {
					alert( "Data Loaded: " + data );
				  })
			});
		});
	</script>
@stop
