<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<!-- <meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">-->
<!--<link rel=File-List href="blank2_files/filelist.xml"> -->
<style id="_blank.proposal_12284_Styles">
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
.font512284
	{color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;}
.font612284
	{color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:underline;
	text-underline-style:single;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;}
.font712284
	{color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
.font812284
	{color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
.font912284
	{color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
.xl6312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6412284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl6612284
	{color:windowtext;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;
	padding-left:12px;
	mso-char-indent-count:1;}
.xl6712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl6812284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.5pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6912284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:"Short Date";
	text-align:left;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl7012284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl7112284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7212284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:14.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:"Bodoni MT Black", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl7312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl7412284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl7512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:"Short Date";
	text-align:general;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl7612284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.5pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Book Antiqua", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"m\/d\/yy\;\@";
	text-align:left;
	vertical-align:bottom;
	border:1.0pt solid windowtext;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl7812284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"m\/d\/yy\;\@";
	text-align:left;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl7912284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:0;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl8012284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:Fixed;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl8112284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl8212284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0\.00_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\.00\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl8312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:0;
	text-align:left;
	vertical-align:middle;
	border:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl8412284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0\.00_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\.00\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl8512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:Fixed;
	text-align:left;
	vertical-align:middle;
	border:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl8612284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.5pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8812284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.5pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8912284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border:1.0pt solid windowtext;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl9012284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl9112284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9212284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl9312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:"m\/d\/yy\;\@";
	text-align:general;
	vertical-align:middle;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl9412284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:Fixed;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:none;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl9512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:.5pt solid #BFBFBF;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:#F3F3F3;
	mso-pattern:black none;
	white-space:nowrap;}
.xl9612284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:Fixed;
	text-align:left;
	vertical-align:middle;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl9712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #BFBFBF;
	border-right:none;
	border-bottom:none;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:normal;}
.xl9812284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:Fixed;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid #BFBFBF;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl9912284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:Fixed;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #BFBFBF;
	border-right:none;
	border-bottom:none;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl10012284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl10112284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #BFBFBF;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl10212284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid #BFBFBF;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl10312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0\.00_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\.00\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid #BFBFBF;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl10412284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:0;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid #BFBFBF;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #BFBFBF;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl10512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl10612284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl10812284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl10912284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11012284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11112284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11212284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.5pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11412284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11612284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl11712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(* \#\,\#\#0_\)\;_\(* \\\(\#\,\#\#0\\\)\;_\(* \0022-\0022??_\)\;_\(\@_\)";
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl11812284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(* \#\,\#\#0_\)\;_\(* \\\(\#\,\#\#0\\\)\;_\(* \0022-\0022??_\)\;_\(\@_\)";
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:none;
	border-bottom:.5pt solid #BFBFBF;
	border-left:.5pt solid #BFBFBF;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl11912284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:.5pt solid #D9D9D9;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12012284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:.5pt solid #D9D9D9;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:#F3F3F3;
	mso-pattern:black none;
	white-space:nowrap;}
.xl12112284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(\0022$\0022* \#\,\#\#0_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid #D9D9D9;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:#F3F3F3;
	mso-pattern:black none;
	white-space:nowrap;}
.xl12212284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl12312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl12412284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #BFBFBF;
	background:white;
	mso-pattern:black none;
	white-space:normal;}
.xl12512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	background:white;
	mso-pattern:black none;
	white-space:normal;}
.xl12612284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #BFBFBF;
	border-bottom:none;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:normal;}
.xl12712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #BFBFBF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12812284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12912284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #BFBFBF;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl13012284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #BFBFBF;
	border-left:.5pt solid #BFBFBF;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl13112284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid #BFBFBF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl13212284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:.5pt solid #BFBFBF;
	border-bottom:.5pt solid #BFBFBF;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl13312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:none;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl13412284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl13512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:.5pt solid #BFBFBF;
	border-right:.5pt solid silver;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl13612284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"_\(* \#\,\#\#0\.00_\)\;_\(* \\\(\#\,\#\#0\.00\\\)\;_\(* \0022-\0022??_\)\;_\(\@_\)";
	text-align:general;
	vertical-align:middle;
	border-top:.5pt solid #BFBFBF;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #BFBFBF;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl13712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid #BFBFBF;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl13812284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid silver;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl13912284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:none;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl14012284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:none;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl14112284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.5pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"Short Date";
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl14212284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:none;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl14312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:.5pt solid #BFBFBF;
	border-bottom:.5pt solid silver;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl14412284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl14512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid silver;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl14612284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid silver;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl14712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid silver;
	border-bottom:.5pt solid silver;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl14812284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid silver;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl14912284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid silver;
	border-right:.5pt solid silver;
	border-bottom:none;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl15012284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:.5pt solid #BFBFBF;
	border-right:none;
	border-bottom:none;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:normal;}
.xl15112284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:.5pt solid #BFBFBF;
	border-right:.5pt solid #BFBFBF;
	border-bottom:none;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:normal;}
.xl15212284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid #BFBFBF;
	border-bottom:.5pt solid silver;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl15312284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl15412284
	{padding:0px;
	mso-ignore:padding;
	color:#D9D9D9;
	font-size:20.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl15512284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl15612284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
.xl15712284
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Century Gothic", sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	background:white;
	mso-pattern:black none;
	white-space:nowrap;}
-->
</style>
</head>

<body>
<div id="_blank.proposal_12284" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=957 class=xl6312284
 style='border-collapse:collapse;table-layout:fixed;width:720pt'>
 <col class=xl6312284 width=18 style='mso-width-source:userset;mso-width-alt:
 658;width:14pt'>
 <col class=xl6312284 width=45 style='mso-width-source:userset;mso-width-alt:
 1645;width:34pt'>
 <col class=xl6312284 width=32 style='mso-width-source:userset;mso-width-alt:
 1170;width:24pt'>
 <col class=xl6312284 width=37 style='mso-width-source:userset;mso-width-alt:
 1353;width:28pt'>
 <col class=xl6312284 width=40 style='mso-width-source:userset;mso-width-alt:
 1462;width:30pt'>
 <col class=xl6312284 width=462 style='mso-width-source:userset;mso-width-alt:
 16896;width:347pt'>
 <col class=xl6312284 width=42 style='mso-width-source:userset;mso-width-alt:
 1536;width:32pt'>
 <col class=xl6312284 width=27 style='mso-width-source:userset;mso-width-alt:
 987;width:20pt'>
 <col class=xl6312284 width=39 style='mso-width-source:userset;mso-width-alt:
 1426;width:29pt'>
 <col class=xl6312284 width=57 style='mso-width-source:userset;mso-width-alt:
 2084;width:43pt'>
 <col class=xl6312284 width=42 style='mso-width-source:userset;mso-width-alt:
 1536;width:32pt'>
 <col class=xl6312284 width=59 style='mso-width-source:userset;mso-width-alt:
 2157;width:44pt'>
 <col class=xl6312284 width=57 style='mso-width-source:userset;mso-width-alt:
 2084;width:43pt'>
 <tr height=30 style='mso-height-source:userset;height:22.5pt'>
  <td height=30 width=18 style='height:22.5pt;width:14pt' align=left
  valign=top>
  <![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:0px;margin-top:0px;width:302px;
  height:71px'>{{ HTML::image('img/logo.png') }}
  </span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=30 class=xl6612284 width=18 style='height:22.5pt;width:14pt'><a
    name="RANGE!A1:M42">&nbsp;</a></td>
   </tr>
  </table>
  </span></td>
  <td class=xl6612284 width=45 style='width:34pt'>&nbsp;</td>
  <td class=xl6612284 width=32 style='width:24pt'>&nbsp;</td>
  <td colspan=10 class=xl15412284 width=862 style='width:648pt'>Quote/Order</td>
 </tr>
 <tr class=xl6412284 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td colspan=5 height=20 class=xl15512284 style='height:15.0pt'>&nbsp;</td>
  <td class=xl6412284></td>
  <td class=xl6512284>&nbsp;</td>
  <td class=xl7012284>&nbsp;</td>
  <td class=xl7012284>&nbsp;</td>
  <td class=xl7012284>&nbsp;</td>
  <td class=xl6412284></td>
  <td class=xl6712284>&nbsp;</td>
  <td class=xl6912284 style="text-align:right">{{ $date }}&nbsp;</td>
 </tr>
 <tr class=xl7112284 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl7212284 style='height:17.25pt'>&nbsp;</td>
  <td class=xl7012284>&nbsp;</td>
  <td class=xl7012284>&nbsp;</td>
  <td class=xl7012284>&nbsp;</td>
  <td class=xl7012284>&nbsp;</td>
  <td class=xl10712284><span style='mso-spacerun:yes'>�����������������
  </span>&quot;�repairing windows is ALL we do.&quot;</td>
  <td colspan=5 class=xl15712284>{{ $name }}</td>
  <td class=xl9212284></td>
  <td class=xl9312284>&nbsp;</td>
 </tr>
 <tr class=xl6412284 height=18 style='mso-height-source:userset;height:14.1pt'>
  <td height=18 class=xl7312284 colspan=4 style='height:14.1pt'>3377 Cerritos
  Ave</td>
  <td class=xl6712284>&nbsp;</td>
  <td class=xl10912284>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lic. #987212</td>
  <td colspan=6 class=xl6712284>{{ $address }}</td>
  <td class=xl6512284>&nbsp;</td>
 </tr>
 <tr class=xl6412284 height=18 style='mso-height-source:userset;height:14.1pt'>
  <td height=18 class=xl7312284 colspan=4 style='height:14.1pt'>Los Alamitos,
  CA 90720</td>
  <td class=xl6712284>&nbsp;</td>
  <td class=xl10712284>&nbsp;</td>
  <td colspan=6 class=xl6712284>{{ $addLine2 }}</td>
  <td class=xl6412284></td>
 </tr>
 <tr class=xl6412284 height=18 style='mso-height-source:userset;height:14.1pt'>
  <td colspan=4 height=18 class=xl10512284 style='height:14.1pt'>office:
  562-493-1590</td>
  <td class=xl6712284>&nbsp;</td>
  <td class=xl10812284><font class="font512284">What we do </font><font
  class="font612284">NOT</font><font class="font512284"> do�</font></td>
  <td colspan=5 class=xl6712284>{{ $phone }}</td>
<!--  <td class=xl12312284>Level:<span style='mso-spacerun:yes'>�</span></td> -->
  <td class=xl7412284>&nbsp;</td>
 </tr>
 <tr class=xl6412284 height=18 style='mso-height-source:userset;height:14.1pt'>
  <td height=18 class=xl10912284 style='height:14.1pt'>Web: www.WindowRnR.com<span
  style='mso-spacerun:yes'>������������������������������������������</span></span></td>
  <td class=xl6512284>&nbsp;</td>
  <td class=xl6512284>&nbsp;</td>
  <td class=xl6712284>&nbsp;</td>
  <td class=xl6712284>&nbsp;</td>
  <td class=xl11112284><span style='mso-spacerun:yes'>�</span>1. Paint<span
  style='mso-spacerun:yes'>��� </span>2. Re-Screen<span
  style='mso-spacerun:yes'>�� </span>3. Glazing putty<span
  style='mso-spacerun:yes'>� </span>4. Strip window entirely</td>
  <td colspan=6 class=xl15312284>{{ $email }}</td>
  <td class=xl6412284></td>
 </tr>
 <tr class=xl6412284 height=18 style='mso-height-source:userset;height:14.1pt'>
  <td height=18 class=xl11012284 colspan=5 style='height:14.1pt'>Email:  info@WindowRnR.com</td>
  <td class=xl6412284></td>
  <td class=xl6553512284></td>
  <td class=xl6553512284></td>
  <td class=xl6553512284></td>
  <td class=xl6412284></td>
  <td class=xl10612284></td>
  <td class=xl6553512284></td>
  <td class=xl7512284>&nbsp;</td>
 </tr>
 <tr class=xl6812284 height=3 style='mso-height-source:userset;height:2.0pt'>
  <td height=3 class=xl6812284 style='height:2.0pt'></td>
  <td class=xl10512284></td>
  <td class=xl10512284></td>
  <td class=xl10512284></td>
  <td class=xl6412284></td>
  <td class=xl6812284></td>
  <td class=xl7612284></td>
  <td class=xl6812284></td>
  <td class=xl6812284></td>
  <td class=xl6812284></td>
  <td class=xl6812284></td>
  <td class=xl6812284></td>
  <td class=xl6812284></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl7712284 style='height:15.0pt; text-align:center'>#</td>
  <td class=xl7812284>&nbsp;Rm</td>
  <td class=xl7712284>&nbsp;Dirc</td>
  <td class=xl7712284 style='border-left:none'>&nbsp;Style</td>
  <td colspan=2 class=xl15612284>&nbsp;Service Description</td>
  <td class=xl8912284>Dim.</td>
  <td class=xl8912284 style='border-left:none'>Qty</td>
  <td class=xl8912284 style='border-left:none'>Item</td>
  <td class=xl9012284>Serv Optns</td>
  <td class=xl8912284>Opt. $</td>
  <td class=xl8912284 style='border-left:none'>Balancer</td>
  <td class=xl8912284 style='border-left:none'>Line Total</td>
 </tr>

 @foreach ($items as $key => $item)
	 <tr class=xl6412284 height=18 style='height:13.5pt'>
	  <td height=18 class=xl7912284 style='height:13.5pt; text-align:center'>{{ $key+1 }}</td>
	  <td class=xl8012284 style='border-left:none'>&nbsp;{{ $item['room'] }}</td>
	  <td class=xl8012284 style='border-left:none'>&nbsp;{{ $item['dir'] }}</td>
	  <td class=xl12212284 style='border-left:none'>&nbsp;{{ $item['style'] }}</td>
	  <td colspan=2 class=xl14212284 style='border-right:.5pt solid #BFBFBF;
	  border-left:none'>&nbsp;{{ $item['serv'] }}</td>
	  <td class=xl13812284>&nbsp;{{ $item['dim'] }}</td>
	  <td class=xl11712284 style='border-left:none'>{{ $item['qty'] }}</td>
	  <td class=xl8112284 style='border-left:none; text-align:right'>{{ '$'.$item['item'] }}&nbsp;</td>
	  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
	  <td class=xl8112284 style='border-left:none; text-align:right'>{{ $item['opt'] }}&nbsp;</td>
	  <td class=xl8212284 style='border-left:none'>&nbsp;{{ $item['bal'] }}</td>
	  <td class=xl9512284 style='border-left:none; text-align:right'>{{ '$'.$item['lineTotal'] }}&nbsp;</td>
	 </tr>
 @endforeach

<!-- This is where the extra rows are created 
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>2</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14612284 style='border-right:.5pt solid #BFBFBF;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8212284 style='border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>3</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14612284 style='border-right:.5pt solid #BFBFBF;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8212284 style='border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>4</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14612284 style='border-right:.5pt solid #BFBFBF;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8212284 style='border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>5</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14612284 style='border-right:.5pt solid #BFBFBF;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8212284 style='border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>6</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14612284 style='border-right:.5pt solid #BFBFBF;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8212284 style='border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>7</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8212284 style='border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>8</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8212284 style='border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>9</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=19 style='height:14.25pt'>
  <td height=19 class=xl8312284 style='height:14.25pt;border-top:none'>10</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>11</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>12</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>13</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>14</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>15</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14612284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>16</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14612284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>17</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>18</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14612284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>19</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14612284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>20</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>21</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8412284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl8312284 style='height:13.5pt;border-top:none'>22</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8512284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8212284 style='border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=18 style='height:13.5pt'>
  <td height=18 class=xl7912284 style='height:13.5pt'>23</td>
  <td class=xl8012284 style='border-left:none'>&nbsp;</td>
  <td class=xl8512284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl12212284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14212284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl13812284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8112284 style='border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl8212284 style='border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 <tr class=xl6412284 height=19 style='height:14.25pt'>
  <td height=19 class=xl8312284 style='height:14.25pt;border-top:none'>24</td>
  <td class=xl9412284 style='border-left:none'>&nbsp;</td>
  <td class=xl9812284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl13912284 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl14812284 style='border-right:.5pt solid silver;
  border-left:none'>&nbsp;</td>
  <td class=xl14012284>&nbsp;</td>
  <td class=xl11712284 style='border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>�������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl10212284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl8112284 style='border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span style='mso-spacerun:yes'>���������
  </span>-<span style='mso-spacerun:yes'>�� </span></td>
  <td class=xl10312284 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl9512284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>�</span>$<span
  style='mso-spacerun:yes'>���������������� </span>-<span
  style='mso-spacerun:yes'>�� </span></td>
 </tr>
 -->
 
 <tr class=xl6412284 height=19 style='height:14.25pt'>
  <td height=19 class=xl10412284 style='height:14.25pt'>&nbsp;</td>
  <td class=xl9912284>&nbsp;</td>
  <td class=xl9612284>&nbsp;</td>
  <td class=xl9712284 width=37 style='width:28pt'>&nbsp;</td>
  <td colspan=3 class=xl15012284 width=544 style='border-right:.5pt solid #BFBFBF;
  width:409pt'>Total Windows<span style='mso-spacerun:yes'>��</span></td>
  <td class=xl11812284 style='border-top:none;border-left:none'><span
  style='mso-spacerun:yes'>����� </span>-<span style='mso-spacerun:yes'>��
  </span></td>
  <td class=xl10112284>&nbsp;</td>
  <td class=xl10012284>&nbsp;</td>
  <td class=xl13612284>&nbsp;</td>
  <td class=xl13512284 style='border-top:none'>Total</td>
  <td class=xl12112284 style='border-left:none; text-align:right'>{{ $total }}&nbsp;</td>
 </tr>
 <tr class=xl6412284 height=19 style='height:14.25pt'>
  <td height=19 class=xl12412284 width=18 style='height:14.25pt;width:14pt'>&nbsp;</td>
  <td class=xl12512284 width=45 style='width:34pt'>&nbsp;</td>
  <td class=xl12512284 width=32 style='width:24pt'>&nbsp;</td>
  <td class=xl12512284 width=37 style='width:28pt'>&nbsp;</td>
  <td class=xl12512284 width=40 style='width:30pt'>&nbsp;</td>
  <td class=xl12512284 width=462 style='width:347pt'>&nbsp;</td>
  <td class=xl12512284 width=42 style='width:32pt'>&nbsp;</td>
  <td class=xl12512284 width=27 style='width:20pt'>&nbsp;</td>
  <td class=xl12512284 width=39 style='width:29pt'>&nbsp;</td>
  <td class=xl12612284 width=57 style='width:43pt'>&nbsp;</td>
  <td class=xl12512284 width=42 style='width:32pt'>&nbsp;</td>
  <td class=xl13312284>Deposit</td>
  <td class=xl11912284 style='border-top:none;border-left:none; text-align:right'>{{ $dep }}&nbsp;</td>
 </tr>
 <tr class=xl6412284 height=19 style='mso-height-source:userset;height:14.25pt'>
  <td height=19 class=xl13712284 colspan=10 style='height:14.25pt;border-right:
  .5pt solid #BFBFBF'>*Paint prep, priming &amp; painting will be necessary
  after our work. We don't apply glazing putty unless explicitly listed in
  quote. Painters/glazier references available upon re<span style='display:
  none'>quest.<span style='mso-spacerun:yes'>�</span></span></td>
  <td class=xl12512284 width=42 style='width:32pt'>&nbsp;</td>
  <td class=xl11412284>Balance</td>
  <td class=xl12012284 style='border-top:none; text-align:right'>{{ $balDue }}&nbsp;</td>
 </tr>
 <tr class=xl6412284 height=19 style='height:14.25pt'>
  <td height=19 class=xl12712284 colspan=7 style='height:14.25pt'>*Price
  subject to visible conditions, <font class="font912284">additional unforeseen
  damage may incur extra costs</font><font class="font712284">; </font><font
  class="font812284">add $5 per lb</font><font class="font712284"> for lead
  counterweights as needed.<span style='mso-spacerun:yes'>�</span></font></td>
  <td class=xl12812284></td>
  <td class=xl12812284></td>
  <td class=xl12912284>&nbsp;</td>
  <td class=xl12812284></td>
  <td class=xl13412284 width=59 style='width:44pt'></td>
  <td class=xl6312284></td>
 </tr>
 <tr height=18 style='height:13.5pt'>
  <td height=18 class=xl13012284 colspan=9 style='height:13.5pt'>*installation
  starts @ 7:30am.<span style='mso-spacerun:yes'>� </span>We will need 3 feet
  of working space in front of each window (interior).<span
  style='mso-spacerun:yes'>� </span>Install date contingent on weather
  conditions.<span style='mso-spacerun:yes'>�</span></td>
  <td class=xl13212284>&nbsp;</td>
  <td class=xl9112284></td>
  <td class=xl6312284></td>
  <td class=xl6312284></td>
 </tr>
 <tr height=18 style='height:13.5pt'>
  <td height=18 class=xl8712284 colspan=3 style='height:13.5pt'>dh-double hung</td>
  <td class=xl8712284 colspan=2>pix-picture</td>
  <td class=xl8712284 colspan=5><font class="font912284">R&amp;R</font><font
  class="font712284"> - Remove &amp; Replace<span
  style='mso-spacerun:yes'>����� </span>ws-weather-strip<span
  style='mso-spacerun:yes'>����� </span>is-inswing<span
  style='mso-spacerun:yes'>������ </span>os-outswing<span
  style='mso-spacerun:yes'>��� </span>bmp-bump<span
  style='mso-spacerun:yes'>����� </span>rbt-rabbit<span
  style='mso-spacerun:yes'>���� </span>ph-parliament hinges</font></td>
  <td class=xl11312284></td>
  <td class=xl6312284></td>
  <td class=xl11412284></td>
 </tr>
 <tr height=18 style='height:13.5pt'>
  <td height=18 class=xl8712284 colspan=3 style='height:13.5pt'>sc single
  casement</td>
  <td class=xl8712284 colspan=2>xo-glider</td>
  <td class=xl8712284 colspan=3><font class="font912284">NEW</font><font
  class="font712284">- Install New sash with glass<span
  style='mso-spacerun:yes'>��� </span>sb- slide bolt<span
  style='mso-spacerun:yes'>��������� </span>lck- lock<span
  style='mso-spacerun:yes'>����� </span>stc- stucco mould<span
  style='mso-spacerun:yes'>���� </span>bs- blind stop<span
  style='mso-spacerun:yes'>��� </span>nl- night lock</font></td>
  <td class=xl9112284></td>
  <td colspan=4 class=xl11412284>Balance Due upon Completion</td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td height=19 class=xl8712284 colspan=3 style='height:14.25pt'>dc-double
  casemnt</td>
  <td class=xl8712284 colspan=2>awn-awning</td>
  <td class=xl8712284><font class="font812284">Sales Rep</font><font
  class="font712284">:____________________ </font><font class="font812284">Customer
  Acceptance: ___________________________________Date:_________________________</font></span></td>
  <td class=xl14112284 align=center></td>
  <td class=xl11612284></td>
  <td class=xl6412284></td>
  <td class=xl6312284></td>
  <td colspan=3 class=xl14412284>TWO YR WARRANTY!</td>
 </tr>

<!--
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=18 style='width:14pt'></td>
  <td width=45 style='width:34pt'></td>
  <td width=32 style='width:24pt'></td>
  <td width=37 style='width:28pt'></td>
  <td width=40 style='width:30pt'></td>
  <td width=462 style='width:347pt'></td>
  <td width=42 style='width:32pt'></td>
  <td width=27 style='width:20pt'></td>
  <td width=39 style='width:29pt'></td>
  <td width=57 style='width:43pt'></td>
  <td width=42 style='width:32pt'></td>
  <td width=59 style='width:44pt'></td>
  <td width=57 style='width:43pt'></td>
 </tr>
 <![endif]>
 
 -->
</table>

</div>
</body>

</html>
