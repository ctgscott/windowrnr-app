@extends('layouts.fullwidth')

{{-- Web site Title --}}
@section('title')
Create Estimate
@stop

@section('styles')
<link href="{{ asset('css/estimate.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
	<div class="well well-small" id="estimate">
		<div class="title">
			<h3>Estimate Builder</h3>
		</div>
		@if($results->company_name)
			<h4>{{ $results->company_name }}</h4>
		@endif
		<h5 class="bold" id="custName">{{ $results->customer_lname.", ".$results->customer_fname }}</h5>
		<span id="jobAddress">{{ $results->job_address }},</span>
		<span id="jobCity">{{ $results->job_city.", CA  ".$results->job_zip }}</span><br/>
		<span id="jobPhone">{{ $results->customer_phone }}</span>
		<span id="jobEmail">{{ $results->customer_email }}</span>
		<h5 class="bold"><a href="{{ '/job/'.$results->job_id }}">{{ "Job #".$results->job_id }}</a></h5>
	</div>

	<div class="well well-small" id="items">
		{{ Form::open(array('url' => 'estimate/save')) }}
		{{ Form::hidden('sourceref', 'create') }}
		<h4 class="estTitle">Title: </h4>
		{{ Form::text('description', 'Initial Estimate', array('id' => 'description', 'class' => 'descriptionBox form-control')) }}
		<h5 class="estDate" id="estDate">{{ date("m/d/Y - g:i a") }}</h5>
		<div class="titleContainer">
			<h4 class="title">Items</h4>
			{{ Form::button('<i class="glyphicon glyphicon-plus"></i>', array('type' => 'button', 'class' => 'btn btn-primary addItem')) }}
			<div class="heading bold">
				<span class="itemNum">#</span
				><span class="room">Room</span
				><span class="dir">Dir.</span
				><span class="style">Style</span
				><span class="titleService">Service Type</span
				><span class="titleBalancer">Balancer</span
				><span class="titleDimension">Dimension"</span
				><span class="titleParts">Parts</span
				><span class="titleStatus">Status</span
				><span class="titlePrice">Price</span>
			</div>
		</div>
		{{ Form::hidden('job_id', $results->job_id, array('id' => 'job_id')) }}
		{{ Form::hidden('cust_id', $results->customer_id) }}
		<div id="parent"></div>
		<div class="row rowTotal">
			{{ Form::text('totalPrice', '$', array('readOnly' => 'true', 'class' => 'totalPrice form-control total', 'id' => 'totalPrice')) }}
			{{ Form::label('totalPrice', 'Total&nbsp;', array('class' => 'lblTotal')) }}
		</div>
		<div class="row rowTotal">
			<button class="btn btn-sm btn-info" id="btnResetDeposit" title="Reset to Default"><i class="glyphicon glyphicon-remove"></i></button>
			{{ Form::text('deposit', '$', array('class' => 'deposit form-control total', 'id' => 'deposit')) }}
			{{ Form::label('deposit', 'Deposit&nbsp;', array('class' => 'lblTotal')) }}
		</div>
		<div class="row rowTotal">
			{{ Form::text('balance', '$', array('readOnly' => 'true', 'class' => 'balance form-control total', 'id' => 'balance')) }}
			{{ Form::label('balance', 'Balance Due&nbsp;', array('class' => 'lblTotal')) }}
		</div>
		{{ Form::button('Save&nbsp;&nbsp;<span class="glyphicon glyphicon-check"></span>', ['type' => 'submit', 'class' => 'btn btn-primary form-control', 'id' => 'saveEstimate']) }}
		<span class="note">Once saved, the estimate can be shared and/or printed as needed.</span> 
		{{ Form::close() }}
	</div>
	
	<div class="modal fade" tabindex="-1" id="modalShare">
		<div class="modal-dialog" id="pdfModal">
			<div class="modal-content">
				<div class="modal-header email-header">
					<h4 class="modal-title">Email Estimate</h4>
				</div>
				<div class="modal-body" id="pdfModalBody">
					<div class="input-group" id="pdfBox"></div><!-- /input-group -->
					<div class="input-group" id="emailBox">
						<label class="field">
							<span>To:</span>
							<input type="text" value="{{ !empty($customer) ? $customer->email : ''}}" placeholder="Customer's Email" id="email_to">
						</label>
						<label class="field">
							<span>Cc:</span>
							<input type="text" id="email_cc">
						</label>
						<label class="field">
							<span>Bcc:</span>
							<input type="text" id="email_bcc">
						</label>
						<label class="field">
							<span>From:</span>
							<input type="text" readonly value="{{ $user->email }}" id="email_from">
						</label>
						<label class="field">
							<span>Subject:</span>
							<input type="text" value="WindowRnR - Window Repair Estimate" id="email_subject">
						</label>
						<textarea id="email_content">
Dear {{ trim($customer->f_name . ' ' . $customer->l_name) }},

Please find the estimate attached.

Sincerely,
Scott Campbell
WindowRnR
						</textarea>
						<label class="field">
							<span>Attachment:</span>
							<span class="file-label">Job {{ $job->id }} Initial Estimate.pdf</span>
							<input type="hidden" id="email_attachment" value="">
						</label>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pdfFooter">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="btnSavePDF">Save PDF</button>
						<button type="button" class="btn btn-primary" id="btnPrintPDF" onClick="window.print()">Print PDF</button>
						<button type="button" class="btn btn-success" id="btnEmailPDF">Email</button>
						<div class="status-line pdf-status-line"></div>
						<div class="saved-pdf hidden">
							@if(!empty($pdf))
								<?php
									$savedAt = strtotime($pdf->uploaded_at);
									if(date('Y', $savedAt) === date('Y'))
										$savedAt = date('M j - g:i A', $savedAt);
									else
										$savedAt = date('M j, Y - g:i A', $savedAt);
									$pdfPath = str_replace('&export=download', '', $pdf->path);
								?>
								<div class="done">PDF <span class="sub">(Created: {{ $savedAt }})</span>: <a href="{{ $pdfPath }}" target="_blank">{{ $pdfPath }}</a></div>
							@endif
						</div>
						<input type="hidden" id="pdf_download_url" value="{{ !empty($pdf) ? $pdf->download_url : '' }}">
					</div>
					<div class="email-footer">
						<button type="button" class="btn btn-default" id="btnCloseEmail">Close</button>
						<button type="button" class="btn btn-primary" id="btnSendEmail">Send</button>
						<div class="status-line email-status-line"></div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- markup for cloned items -->
	<div id="new_lineitem_markup">
		<div id="item_group_NEWITEMID" class="well well-small clonedSection newlyAddedSection markupTemplate" data-id="NEWITEMID">
			<div class="estimate-drag-handle"><i class="glyphicon glyphicon-move"></i></div
>{{ Form::hidden('item[NEWITEMID][itemID]', 0, array('id' => 'itemIDNEWITEMID'))
}}{{ Form::text('itemNEWITEMID', 'NEWITEMID', array('class' => 'itemNum', 'id' => 'itemNEWITEMID', 'disabled' => 'disabled', 'readonly' => 'readonly'))
}}{{ Form::hidden('item[NEWITEMID][item_num]', 'NEWITEMID', array('id' => 'itemHiddenNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][room]', $rooms, 0, array('class' => 'room', 'id' => 'roomNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][dir]', $dir, 0, array('class' => 'dir', 'id' => 'dirNEWITEMID'))
}}{{ Form::hidden('item[NEWITEMID][win_num]', 1, array('class' => 'winNum', 'id' => 'winNumNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][win_style]', $winStyle, 0, array('class' => 'winStyle', 'id' => 'winStyleNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][win_serv]', [], '1', array('disabled' => 'disabled', 'class' => 'service', 'id' => 'shortServiceNEWITEMID'))
}}{{ Form::select('item[NEWITEMID][balancer]', array('0' => 'N/A', '1' => 'B1'), '0', array('disabled' => 'disabled', 'class' => 'balancer', 'id' => 'balancerNEWITEMID'))
}}<div class="dimension_column"
>{{ Form::text('item[NEWITEMID][sash_width]', '', array('class' => 'dimWidth dim', 'id' => 'dimWidthNEWITEMID', 'placeholder' => 'W')) 
}}<span class="sub">x</span
>{{ Form::text('item[NEWITEMID][sash_height]', '', array('class' => 'dimHeight dim', 'id' => 'dimHeightNEWITEMID', 'placeholder' => 'H')) 
}}</div><div class="parts_column">N/A</div>{{ Form::select('item[NEWITEMID][status]', array('1' => 'Pending', '2' => 'On Hold', '3' => 'Done'), '1', array('class' => 'status', 'id' => 'statusNEWITEMID'))
}}<div class="rt_buttons">{{ Form::button('<i class="glyphicon glyphicon-chevron-down" id="expandGlyphNEWITEMID'.'"></i>', array('type' => 'button', 'class' => 'btn btn-success expandItem', 'id' => 'expandItemNEWITEMID', 'title' => 'Expand'))
}}{{ Form::button('<i class="glyphicon glyphicon-book" id="copyGlyphNEWITEMID"></i>', array('type' => 'button', 'class' => 'btn btn-info cloneItem', 'id' => 'cloneItemNEWITEMID', 'title' => 'Clone'))
}}{{ Form::button('<i class="glyphicon glyphicon-remove"></i>', array('type' => 'button', 'class' => 'btn btn-danger closeItem', 'id' => 'closeItemNEWITEMID', 'title' => 'Remove'))
}}</div>{{ Form::text('priceNEWITEMID', '', array('placeholder' => '$', 'class' => 'price', 'id' => 'priceNEWITEMID', 'disabled')) }}
			<div class="subGroup collapse" id="subGroupNEWITEMID" style="">
				<div class="row2">
				{{ Form::text('item[NEWITEMID][serv_desc]', '', array('placeholder' => 'Service description', 'class' => 'description', 'id' => 'descriptionNEWITEMID')) }}{{ Form::text('item[NEWITEMID][serv_price]', '', array('placeholder' => '$', 'class' => 'servPrice', 'id' => 'servPriceNEWITEMID')) }}
				</div>
				<div class="row3">
					<span class="bold left25">Sash Thickness: </span>
					<div class="input-group sashThick">
						<span class="input-group-addon sashThick">
							{{ Form::radio('item[NEWITEMID][sash_thick]', '1', true, ['class' => 'thickness', 'id' => 'sashThinNEWITEMID']) }} 1 3/8"
						</span>
						<span class="input-group-addon sashThick">
							{{ Form::radio('item[NEWITEMID][sash_thick]', '2', false, ['class' => 'thickness', 'id' => 'sashThickNEWITEMID']) }} 1 3/4"
						</span>
					</div>
				</div>
				<div class="row">
					{{ Form::hidden('item[NEWITEMID][serv_options]', '', array('id' => 'servOptionsNEWITEMID', 'class' => 'serv_options_value')) }}
					<fieldset class="custom-fieldset" id="optionsNEWITEMID">
						<legend>Options</legend>
						<div class="service-options">
						<span class="none">No options chosen</span>
						</div>
						<div class="new-service-option">
							<select class="option-chooser">
								<option value="0" selected>Choose an option...</option>
								@foreach($options as $option)
									<option value="{{ $option->id }}" data-service="{{ $option->serv_id }}" data-price="{{ $option->price }}">{{ $option->title }}</option>
								@endforeach
							</select
							><input class="option-price optionsPrice" value="" readonly="readonly"
							><button class="btn btn-primary btn-sm btn-add-option">Add</button>
						</div>
					</fieldset>
				</div>
				<div class="row">
					{{ Form::hidden('item[NEWITEMID][parts]', '[]', ['id' => 'itempartsNEWITEMID', 'class' => 'loaded-parts', 'data-item-num' => 'NEWITEMID']) }}
				</div>
				<?php /* <div class="images" data-allow-upload="true" data-image-owner-type="estimate" data-image-owner-id="{{ $est['items'][$i-1]->id }}"></div> */ ?>
			</div>
		</div>
	</div>

	<?php /*  modal for parts details */ ?>
	<div class="modal" tabindex="-1" id="modalParts">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Parts</h4>
				</div>
				<div class="modal-body" id="partsModalBody">
					Hey There!
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<?php /* modal for inputting part-option value */ ?>
	<div class="modal" tabindex="-1" id="modalPartOptionValue">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Parts</h4>
				</div>
				<div class="modal-body">
					<h5 class="modal-heading">Hehehe</h5>
					<ul class="choosable-options"></ul>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" onclick="return partOptionValueSelected.call(this, '')">Close</button>
				</div>
			</div>
		</div>
	</div>

	<iframe name="printIFrame"></iframe>

@stop

@section('scripts')
	<script type='text/javascript' src="{{ asset('js/itemAdd.js') }}"></script>
	<script type='text/javascript' src="{{ asset('js/estimate.js') }}"></script>
	<script type="text/javascript">
		var rooms = <?php echo json_encode($rooms, JSON_NUMERIC_CHECK); ?>;
		var dir = <?php echo json_encode($dir, JSON_NUMERIC_CHECK); ?>;
		var winStyle = <?php echo json_encode($winStyle, JSON_NUMERIC_CHECK); ?>;
		var balancers = <?php echo json_encode($balancers, JSON_NUMERIC_CHECK); ?>;
		var winStylesWithBalancer = <?php echo json_encode($winStylesWithBalancers, JSON_NUMERIC_CHECK); ?>;
		var allParts = <?php echo json_encode($allParts, JSON_NUMERIC_CHECK); ?>;
		var editPage = false;
		var estimateParts = [];
		addItem();
		winStyleChange($('.winStyle').first()[0], false, true);

		var showAP = {{ Input::get('ap') ? Input::get('ap') : 'false' }};
	</script>
@stop

