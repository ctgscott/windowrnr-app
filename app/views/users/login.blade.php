@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Log In
@stop

@section('styles')
    <link href="{{ asset('css/user.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
<div class="form-signin">

    <h4>Login</h4>

    <div class="well">
    	<form class="form-horizontal" action="{{ URL::to('users/login') }}" method="post" role="form">   
            {{ Form::token(); }}

            <div class="{{ ($errors->has('email')) ? 'has-error' : '' }} form-group" for="email">
                
                <div class="controls col-md-12">
                <label class="control-label" for="email">Email</label>
                
                    <input name="email" id="email" value="{{ Request::old('email') }}" class="form-control" placeholder="Email" type="text" >
                    <span class="{{ ($errors->has('email')) ? 'has-error glyphicon glyphicon-remove form-control-feedback' : '' }}"></span>
                    {{ ($errors->has('email') ? $errors->first('email') : '') }}
                   
                </div>
            </div>
        
           <div class="{{ $errors->has('password') ? 'has-error' : '' }} form-group" for="password">
                
                <div class="controls col-md-12">
                <label class="control-label" for="password">Password</label>
                    <input name="password" id="password" value="" class="input-xsmall form-control" placeholder="Password" type="password" >
                    <span class="{{ ($errors->has('email')) ? 'has-error glyphicon glyphicon-remove form-control-feedback' : '' }}"></span>
                    {{ ($errors->has('password') ?  $errors->first('password') : '') }}
                </div>
            </div>

            <div class="form-group" for="rememberme">
                <div class="controls col-sm-12">
                    <label class="checkbox-inline" for="rememberme">
                        <input type="checkbox" id="rememberme" name="rememberMe" value="1"> Remember Me
                    </label>
                </div>
            </div>
        
            <div class="form-actions">
                <input class="btn btn-primary btn-default form-control" value="Log In" type="submit" style="width:100px;"> 
                <a href="{{ URL::to('users/resetpassword') }}" class="btn btn-link btn-default">Forgot Password?</a>
            </div>
      </form>

    </div>

    <div style="text-align: center; color: #ccc; font-size: 90%;">Environment: {{ App::environment() }}</div>

</div>
@stop