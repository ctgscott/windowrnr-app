@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
Reset Password
@stop

@section('styles')
<link href="{{ asset('css/user.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
<div class="form-signin">
    <h4>Reset Password</h4>
    <div class="well">
    	<form class="form-horizontal" action="{{ URL::to('users/resetpassword') }}" method="post">   
        	{{ Form::token() }}
        	
    		<div class="{{ ($errors->has('email')) ? 'error' : '' }} form-group" for="email">
                <div class="controls col-md-12">
                    <label class="control-label" for="email">E-mail</label>
                    <input name="email" id="email" value="{{ Request::old('email') }}" type="text" class="input-xlarge form-control" placeholder="E-mail">
                    {{ ($errors->has('email') ? $errors->first('email') : '') }}
                </div>
            </div>

        	<div class="form-actions">
        		<button class="btn btn-primary" type="submit">Reset Password</button>
        	</div>
      </form>
    </div>
</div>
@stop