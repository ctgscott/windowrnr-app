@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
Register
@stop

@section('styles')
<link href="{{ asset('css/user.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
<div class="form-signin">
    <h4>Register New Account</h4>
    <div class="well">
    	<form class="form-horizontal" action="{{ URL::to('users/register') }}" method="post">
            {{ Form::token() }}
            
            <div class="{{ ($errors->has('fname')) ? 'has-error' : '' }} form-group" for="fname">
                
                <div class="controls col-md-12">
                <label class="control-label" for="email">First Name</label>
                    <input name="fname" id="fname" value="{{ Request::old('fname') }}" class="input-xlarge form-control" placeholder="First Name" type="text">
                    {{ ($errors->has('fname') ? $errors->first('fname') : '') }}
                </div>
            </div>	

            <div class="{{ ($errors->has('lname')) ? 'has-error' : '' }} form-group" for="lname">
                
                <div class="controls col-md-12">
                <label class="control-label" for="email">Last Name</label>
                    <input name="lname" id="lname" value="{{ Request::old('lname') }}" class="input-xlarge form-control" placeholder="Last Name" type="text">
                    {{ ($errors->has('lname') ? $errors->first('lname') : '') }}
                </div>
            </div>	

            <div class="{{ ($errors->has('email')) ? 'has-error' : '' }} form-group" for="email">
                
                <div class="controls col-md-12">
                <label class="control-label" for="email">E-mail</label>
                    <input name="email" id="email" value="{{ Request::old('email') }}" class="input-xlarge form-control" placeholder="E-mail" type="text">
                    {{ ($errors->has('email') ? $errors->first('email') : '') }}
                </div>
            </div>	

    		<div class="{{ $errors->has('password') ? 'has-error' : '' }} form-group" for="password">
            	
        		<div class="controls col-md-12">
                <label class="control-label" for="password">Password</label>
    				<input name="password" id="password" value="" class="input-xlarge form-control" placeholder="Password" type="password">
        			{{ ($errors->has('password') ?  $errors->first('password') : '') }}
        		</div>
        	</div>

        	<div class="{{ $errors->has('password_confirmation') ? 'has-error' : '' }} form-group" for="password_confirmation">
            	
        		<div class="controls col-md-12">
                <label class="control-label" for="password_confirmation">Confirm Password</label>
    				<input name="password_confirmation" id="password_confirmation" value="" class="input-xlarge form-control" placeholder="Password Again" type="password">
        			{{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}
        		</div>
        	</div>

    		<div class="form-actions">
    	    	<input class="btn btn-primary form-control" value="Register" type="submit" style="width:100px;"> &nbsp; &nbsp; &nbsp; 
    	    	<input class="btn form-control btn-default" value="Reset" type="reset" style="width:100px;">
    	    </div>	
    	</form>
    </div>
</div>
@stop