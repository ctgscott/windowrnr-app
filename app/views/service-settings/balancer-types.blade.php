<h2 class="view-heading">Balancer Types<button class="btn btn-default btn-sm btn-primary btn-add-new" data-item-type="balancer-types" data-add-simple data-type-name="Balancer Type">Add New</button></h2>
<div id="services" style="display: none">
@foreach($windowServices as $windowService)
{{ $windowService->id }}: {{ $windowService->title }}<br>
@endforeach
</div>
<ul class="view-container floating-list">
	@foreach($balancerTypes as $balancerType)
		<li data-id="{{ $balancerType->id }}" data-service-id="{{ $balancerType->win_service_id }}" data-item-type="balancer-types" data-can-delete data-can-edit><span>{{ $balancerType->name }}</span><a href="#" data-id="{{ $balancerType->id }}" data-service-id="{{ $balancerType->win_service_id }}" data-item-type="balancer-types" data-can-edit data-edit-field="service_id"><span>({{ $balancerType->win_service_id }})</span></a></li>
	@endforeach
</ul>
