<h2 class="view-heading">Window Styles<button class="btn btn-default btn-sm btn-primary btn-add-new" data-item-type="window-styles" data-add-simple data-type-name="Window Style">Add New</button></h2>
<ul class="view-container floating-list">
	@foreach($windowStyles as $windowStyle)
		<li data-id="{{ $windowStyle->id }}" data-item-type="window-styles" data-can-delete data-can-edit><span>{{ $windowStyle->winStyle }}</span></li>
	@endforeach
</ul>