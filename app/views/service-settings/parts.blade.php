<style>
.parts-options-menu {
	position: absolute;
	display: none;
	background-color: #fff;
	border: 1px solid #777;
	font-size: 90%;
}
.parts-options-menu > a {
	display: block;
	padding: 0 5px;
	line-height: 22px;
	vertical-align: middle;
	text-decoration: none;
	color: #000;
}
.parts-options-menu > a:hover {
	background-color: #888;
	color: #fff;
}
.active-options {
	outline: 1px dotted #428bca;
}
span.part-option {
	display: block;
	border: 1px solid #ccc;
	margin-bottom: 3px;
	padding: 2px 6px;
	position: relative;
}
span.part-option:hover {
	border: 1px solid #0077AA;
}
span.delete-option {
	display: none;
	color: #DD6666;
	font-size: 90%;
	margin-left: 10px;
}
span.part-option:hover span.delete-option {
	display: inline-block;
}
.option-button {
	position: absolute;
	padding: 1px 3px;
	cursor: pointer;
	color: #428BCA;
	font-size: 90%;
	display: none;
}
span.part-option:hover .option-button {
	display: block;
}
.add-new-option {
	right: 24px;
	top: 2px;
}
.delete-option {
	right: 2px;
	top: 2px;
	width: 20px;
	color: #943E3E;
}
.part-option-name {
	font-weight: bold;
	padding: 3px 0;
}
div.part-option-values {
    padding-top: 3px;
}
.part-option-value {
	background-color: #e7e7d7;
	padding: 0 4px;
	margin-right: 4px;
	margin-bottom: 3px;
	display: inline-block;
	border: 1px solid transparent;
}
.part-option-value.default-value {
	background-color: #C1E2FF;
}
.part-option-value i {
    color: #903A3A;
    font-weight: normal;
    font-weight: 300;
    padding-left: 5px;
}
.part-option-value.btn-add-value {
	text-align: center;
	padding: 0 3px;
	text-decoration: none;
	background-color: #fff;
	border: 0;
	font-size: 90%;
}
.part-option-value.btn-add-value:first-child {
    padding-left: 0;
}
.part-option-value.btn-add-value:hover {
	text-decoration: underline;
}
.part-option-value.btn-add-value i {
	color: #2C6A98;
	padding: 0;
}
.selected-item .item-view [data-can-edit] > span {
	width: 100%;
}
span.part-option.invalid {
    border: 0 !important;
}
span.part-option.invalid .add-new-option {
    right: auto;
    display: block;
    top: -5px;
    left: 0;
}
</style>
<div class="parts-list">
	<h2 class="view-heading">Parts<div class="top-actions"
		><button class="btn btn-default btn-sm btn-primary btn-add-new" data-item-type="parts" data-add-simple data-callback-function="parts_onaddnew_callback" data-type-name="Part">Add New</button
		><button class="btn btn-default btn-sm btn-primary btn-copy" onclick="parts_manage()">Manage</button></div>
	</h2>
	<ul class="view-container tabular-list">
		<li class="head">
			<span class="p20">Title</span
			><span class="p20">Description</span
			><span class="p8">Exp-Qty</span
			><span class="">Mandatory Options</span>
		</li>
		@foreach($parts as $part)
			<li data-id="{{ $part->id }}" data-item-type="parts" data-can-delete>
				<span class="p20" data-id="{{ $part->id }}" data-item-type="parts" data-can-edit data-edit-field="name"><span>{{ $part->name }}</span></span
				><span class="p20" data-id="{{ $part->id }}" data-item-type="parts" data-can-edit data-edit-field="description"><span>{{ $part->description }}</span></span
				><span class="p8" data-id="{{ $part->id }}" data-item-type="parts" data-can-edit data-edit-field="expected_qty"><span>{{ $part->expected_qty }}</span></span
				><span style="width: 370px" data-id="{{ $part->id }}" data-item-type="parts" data-can-edit data-edit-field="options" data-custom-edit-function="parts_onedit" data-options-json='{{ $part->options }}'><span></span></span>
			</li>
		@endforeach
	</ul>
</div>
<div class="parts-manage dnd-view" data-hidden-at-first>
	<h2 class="view-heading">Parts <span class="small">▶</span> Manage<button class="btn btn-default btn-sm btn-primary btn-copy" onclick="parts_list()">Back</button></h2>
	<div class="subheading">All Parts<span class="small">&nbsp;&nbsp;&nbsp;&nbsp;You can drag and drop the options to their respective services or service-options</div>
	<div class="sources parts-dnd">
		@foreach($parts as $part)
			<div data-id="{{ $part->id }}">{{ $part->name }}</div>
		@endforeach
	</div>
	<div class="subheading">All Window Services</div>
	<div class="targets parts-dnd parts-dnd-services">
		@foreach($windowServices as $windowService)
			<div data-id="{{ $windowService->id }}">
				<b>{{ $windowService->title }}</b><hr>
				@foreach($win_service_parts as $win_service_part)
					@if($win_service_part->win_service_id == $windowService->id)
						<div class="already-added" data-part-id="{{ $win_service_part->parts_id }}" data-service-id="{{ $win_service_part->win_service_id }}" data-item-type="part-to-service" data-can-delete>
							@foreach($parts as $part)
								@if($part->id == $win_service_part->parts_id){{ $part->name }}@endif
							@endforeach
						</div>
					@endif
				@endforeach
			</div>
		@endforeach
	</div>
	<div class="subheading">All Service Options</div>
	<div class="targets parts-dnd parts-dnd-service-options">
		@foreach($serviceOptions as $serviceOption)
			<div data-id="{{ $serviceOption->id }}">
				<b>{{ $serviceOption->title }}</b> <span class="sub">[{{ $serviceOption->service }}]</span><hr>
				@foreach($options_parts as $options_part)
					@if($options_part->options_id == $serviceOption->id)
						<div class="already-added" data-part-id="{{ $options_part->parts_id }}" data-option-id="{{ $options_part->options_id }}" data-item-type="part-to-option" data-can-delete>
							@foreach($parts as $part)
								@if($part->id == $options_part->parts_id){{ $part->name }}@endif
							@endforeach
						</div>
					@endif
				@endforeach
			</div>
		@endforeach
	</div>
</div>
<script>

_define('parts_manage', function() {
	$('.parts-list').hide();
	$('.parts-manage').show();
});
_define('parts_list', function() {
	$('.parts-manage').hide();
	$('.parts-list').show();
});
_define('parts_onaddnew_callback', function(_data) {
	$('.item .item[data-item-type="parts"]').click();
});
_define('parts_dnd_init', function() {
	$('.dnd-view .sources.parts-dnd > div').draggable({
		zIndex: 100,
		helper: 'clone'
	});
	$('.dnd-view .targets.parts-dnd.parts-dnd-services > div').droppable({
		accept: ".sources.parts-dnd > div",
		activeClass: "semitrans",
		hoverClass: "ui-state-active",
		drop: function( event, ui ) {
			
			// check if already added
			var option = ui.helper.text(), optionId = ui.helper.data('id'), existing = false;
			$('.already-added', this).each(function() {
				if($(this).text().toLowerCase() === option.toLowerCase()) {
					existing = true;
					return false;
				}
			});
			if(existing) return false;

			// add option in ui
			var newItem = $('<div class="already-added" data-part-id="' + optionId + '" data-service-id="' + $(this).data('id') + '" data-item-type="part-to-service" data-can-delete>' + option + '</div>'),
				target = $(this),
				serviceId = $(this).data('id');
			
			// ajax call to update server
			$.post('/service-settings/add', {
				type: 'part-to-service',
				value: 'n/a', // value not used for this mapping call
				part_id: optionId,
				service_id: $(this).data('id')
			}, function(_data) {
				target.find('.already-added[data-part-id='+optionId+'][data-service-id='+serviceId+']').remove();
				target.append(newItem);
				makeItemsDeletable(newItem);
			});
		}
	});
	$('.dnd-view .targets.parts-dnd.parts-dnd-service-options > div').droppable({
		accept: ".sources.parts-dnd > div",
		activeClass: "semitrans",
		hoverClass: "ui-state-active",
		drop: function( event, ui ) {
			
			// check if already added
			var option = ui.helper.text(), optionId = ui.helper.data('id'), existing = false;
			$('.already-added', this).each(function() {
				if($(this).text().toLowerCase() === option.toLowerCase()) {
					existing = true;
					return false;
				}
			});
			if(existing) return false;

			// add option in ui
			var newItem = $('<div class="already-added" data-part-id="' + optionId + '" data-option-id="' + $(this).data('id') + '" data-item-type="part-to-option" data-can-delete>' + option + '</div>'),
				target = $(this),
				serviceOptionId = $(this).data('id');
			
			// ajax call to update server
			$.post('/service-settings/add', {
				type: 'part-to-option',
				value: 'n/a', // value not used for this mapping call
				part_id: optionId,
				option_id: $(this).data('id')
			}, function(_data) {
				target.find('.already-added[data-part-id='+optionId+'][data-service-id='+serviceOptionId+']').remove();
				target.append(newItem);
				makeItemsDeletable(newItem);
			});
		}
	});

	// initialize the options add/edit menu
	if(!$('.parts-options-menu').length) {
		$('<div class="parts-options-menu"><a href="#" onclick="return parts_addoption()">Add Option</a></div>').appendTo('body');
	}
	
	// display loaded options
	$('[data-options-json]').each(function() {
		var parsed = $(this).data('options-json'),
			holder = $(this).find('> span').first(),
			partOption = false;
		if(!parsed || !parsed.length) {
			partOption = $('<span class="part-option invalid"/>').appendTo(holder);
			partOption.append($('<a class="option-button add-new-option" title="Add Option" onclick="return parts_addoption.call(this)" />').text('Add Option'));
		}
		else if(parsed.forEach) {
			parsed.forEach(function(_option) {
				var partOption = $('<span class="part-option"/>').data('option-json', _option).appendTo(holder);
				partOption.append($('<div class="part-option-name" />').html(_option.name));
				partOption.append($('<a class="option-button add-new-option" title="Add Option" onclick="return parts_addoption.call(this)" />').text('Add Option'));
				partOption.append($('<a class="option-button delete-option" title="Delete Option" onclick="return parts_delete_option.call(this)" />').html('<i class="glyphicon glyphicon-remove"></i>'));
				var partOptionValues = $('<div class="part-option-values" />').appendTo(partOption);
				if(_option.values && _option.values.length) {
					var valuesHTML = _option.values.map(function(_x) {
						return '<span class="part-option-value ' + (_x === _option.defaultValue ? 'default-value' : '') + '" title="Set as Default Value" onclick="return parts_set_option_default.call(this)">' + _x + '<a href="#" title="Delete Value" onclick="return parts_delete_option_value.call(this)"><i class="glyphicon glyphicon-remove"></i></a></span>';
					});
					partOptionValues.append(valuesHTML);
				}
				partOptionValues.append('<a class="part-option-value btn-add-value" title="Add Value" onclick="return parts_add_option_value.call(this)">Add Value</a>')
			});
		}
	});
});
_define('parts_onedit', function() {
	return false;
});
_define('parts_addoption', function() {
	var name = window.prompt('Option Name:', '');
	if(name && $.trim(name) !== '') {
		var partID = $(this).closest('.part-option').closest('[data-can-edit]').data('id');
		var options = $(this).closest('.part-option').closest('[data-can-edit]').data('options-json');
		if(!options || !options.length) options = [];
		options.push({name: name, priceFactor: 0});
		var data = {
			type: 'parts',
			id: partID,
			field: 'options',
			value: JSON.stringify(options)
		};
		$.post('/service-settings/update', data, function(_data) {
			if(_data === 'OK') {
				loadView('parts');
			}
			else {
				alert(_data);
			}
		});
	}
	return false;
});
_define('parts_delete_option', function() {
	var parent = $(this).closest('.part-option').closest('[data-can-edit]');
	$(this).closest('.part-option').remove();
	var options = [];
	parent.find('.part-option').each(function() {
		options.push($(this).data('option-json'));
	});
	var data = {
		type: 'parts',
		id: parent.data('id'),
		field: 'options',
		value: JSON.stringify(options)
	};
	$.post('/service-settings/update', data, function(_data) {
		if(_data === 'OK') {
			loadView('parts');
		}
		else {
			alert(_data);
		}
	});
	return false;
});

_define('parts_add_option_value', function() {
	
	var value = window.prompt('Value:', '');
	var partOption = $(this).closest('.part-option').data('option-json');
	if(!partOption.values || !partOption.values.length) partOption.values = [];
	partOption.values.push(value);

	if(partOption.values.length === 1) partOption.defaultValue = value;
	
	var parent = $(this).closest('.part-option').closest('[data-can-edit]');
	var options = [];
	parent.find('.part-option').each(function() {
		options.push($(this).data('option-json'));
	});

	var data = {
		type: 'parts',
		id: parent.data('id'),
		field: 'options',
		value: JSON.stringify(options)
	};
	$.post('/service-settings/update', data, function(_data) {
		if(_data === 'OK') {
			loadView('parts');
		}
		else {
			alert(_data);
		}
	});

	return false;
});

_define('parts_delete_option_value', function() {

	var partOption = $(this).closest('.part-option').data('option-json');
	if(!partOption.values || !partOption.values.length) return false;
	var valueText = $.trim($(this).closest('.part-option-value').text());
	partOption.values = partOption.values.filter(function(_x) {
		return (_x !== valueText);
	});

	if(partOption.values.length === 0 || partOption.defaultValue === valueText) partOption.defaultValue = '';
	
	var parent = $(this).closest('.part-option').closest('[data-can-edit]');
	var options = [];
	parent.find('.part-option').each(function() {
		options.push($(this).data('option-json'));
	});

	var data = {
		type: 'parts',
		id: parent.data('id'),
		field: 'options',
		value: JSON.stringify(options)
	};
	$.post('/service-settings/update', data, function(_data) {
		if(_data === 'OK') {
			loadView('parts');
		}
		else {
			alert(_data);
		}
	});
	
	return false;
});

_define('parts_set_option_default', function() {

	var partOption = $(this).closest('.part-option').data('option-json');
	if(!partOption.values || !partOption.values.length) return false;
	partOption.defaultValue = $(this).text();

	var parent = $(this).closest('.part-option').closest('[data-can-edit]');
	var options = [];
	parent.find('.part-option').each(function() {
		options.push($(this).data('option-json'));
	});

	var data = {
		type: 'parts',
		id: parent.data('id'),
		field: 'options',
		value: JSON.stringify(options)
	};
	$.post('/service-settings/update', data, function(_data) {
		if(_data === 'OK') {
			loadView('parts');
		}
		else {
			alert(_data);
		}
	});
	
	return false;

});

parts_dnd_init();
</script>