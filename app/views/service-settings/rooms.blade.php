<h2 class="view-heading">Rooms<button class="btn btn-default btn-sm btn-primary btn-add-new" data-item-type="rooms" data-add-simple data-type-name="Room">Add New</button></h2>
<ul class="view-container floating-list">
	@foreach($rooms as $room)
		<li data-id="{{ $room->id }}" data-item-type="rooms" data-can-delete data-can-edit><span>{{ $room->room }}</span></li>
	@endforeach
</ul>