<h2 class="view-heading">Window Services</h2>
<div class="subheading">
Window Style:
<select class="window-style-chooser" onchange="window_services_on_style_change(this.value)">
	<option value="0">Choose a window style ...</option>
	@foreach($windowStyles as $windowStyle)
		<option value="{{ $windowStyle->id }}">{{ $windowStyle->winStyle }}</option>
	@endforeach
</select>
<button class="btn btn-default btn-sm btn-primary btn-add-new" 
	data-item-type="window-services" 
	data-add-simple 
	data-additional-data-function="window_services_get_additional_data" 
	data-callback-function="window_services_onaddnew_callback"
	data-type-name="Window Service" 
	data-hidden-at-first>Add New</button>
</div>
<ul class="view-container tabular-list">
	<li class="head">
		<span class="p40">Title</span
		><span class="p40">Description</span
		><span class="p20">Price</span>
	</li>
	@foreach($windowServices as $windowService)
		<li data-id="{{ $windowService->id }}" data-item-type="window-services" data-child-of="{{ $windowService->win_style }}" data-can-delete data-hidden-at-first>
			<span class="p40" data-id="{{ $windowService->id }}" data-item-type="window-services" data-can-edit data-edit-field="title"><span>{{ $windowService->title }}</span></span
			><span class="p40" data-id="{{ $windowService->id }}" data-item-type="window-services" data-can-edit data-edit-field="description"><span>{{ $windowService->description }}</span></span
			><span class="p20" data-id="{{ $windowService->id }}" data-item-type="window-services" data-can-edit data-edit-field="price"><span>{{ $windowService->price }}</span></span>
		</li>
	@endforeach
</ul>
<script>
if(typeof window['window_services_on_style_change'] !== 'function') {
	window['window_services_on_style_change'] = function(_style) {
		$('.item-view li[data-item-type=window-services]')
			.hide()
			.filter('[data-child-of=' + _style + ']')
				.show();
		if(parseInt(_style, 10)) {
			$('.btn-add-new').show();
		}
		else {
			$('.btn-add-new').hide();	
		}
	};
}
else {
	console.log('window_services_on_style_change() is already defined');
}
if(typeof window['window_services_get_additional_data'] !== 'function') {
	window['window_services_get_additional_data'] = function() {
		return {
			'style_id': $('.window-style-chooser option:selected').attr('value')
		};
	};
}
else {
	console.log('window_services_get_additional_data() is already defined');
}
if(typeof window['window_services_onaddnew_callback'] !== 'function') {
	window['window_services_onaddnew_callback'] = function(_data) {
		$('.window-style-chooser option[value=' + _data['style_id'] + ']').prop('selected', true);
		window['window_services_on_style_change'](_data['style_id']);
	};
}
else {
	console.log('window_services_onaddnew_callback() is already defined');
}
</script>