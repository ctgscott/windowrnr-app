<div class="service-options-list">
	<h2 class="view-heading">Service Options<button class="btn btn-default btn-sm btn-primary btn-copy" onclick="service_options_manage()">Manage</button></h2>
	<div class="subheading">
	Window Style:
	<select class="window-style-chooser" onchange="service_options_on_style_change(this.value)">
		<option value="0">Choose ...</option>
		@foreach($windowStyles as $windowStyle)
			<option value="{{ $windowStyle->id }}">{{ $windowStyle->winStyle }}</option>
		@endforeach
	</select>
	<span class="window-service-chooser-container" data-hidden-at-first>
		Window Service:
		<select class="service-chooser" onchange="service_options_on_service_change(this.value)">
			<option class="default" value="0">Choose ...</option>
			@foreach($windowServices as $windowService)
				<option value="{{ $windowService->id }}" data-child-of="{{ $windowService->win_style }}">{{ $windowService->title }}</option>
			@endforeach
		</select>
	</span>
	<button class="btn btn-default btn-sm btn-primary btn-add-new" 
		data-item-type="service-options" 
		data-add-simple 
		data-additional-data-function="service_options_get_additional_data" 
		data-callback-function="service_options_onaddnew_callback"
		data-type-name="Service Option" 
		data-hidden-at-first>Add New</button>
	</div>
	<ul class="view-container tabular-list">
		<li class="head">
			<span class="p40">Title</span
			><span class="p40">Description</span
			><span class="p20">Price</span>
		</li>
		@foreach($serviceOptions as $serviceOption)
			<li data-id="{{ $serviceOption->id }}" data-item-type="service-options" data-child-of="{{ $serviceOption->serv_id }}" data-can-delete data-hidden-at-first>
				<span class="p40" data-id="{{ $serviceOption->id }}" data-item-type="service-options" data-can-edit data-edit-field="title"><span>{{ $serviceOption->title }}</span></span
				><span class="p40" data-id="{{ $serviceOption->id }}" data-item-type="service-options" data-can-edit data-edit-field="description"><span>{{ $serviceOption->description }}</span></span
				><span class="p20" data-id="{{ $serviceOption->id }}" data-item-type="service-options" data-can-edit data-edit-field="price"><span>{{ $serviceOption->price }}</span></span>
			</li>
		@endforeach
	</ul>
</div>
<div class="service-options-manage dnd-view" data-hidden-at-first>
	<h2 class="view-heading">Service Options <span class="small">▶</span> Manage<button class="btn btn-default btn-sm btn-primary btn-copy" onclick="service_options_list()">Back</button></h2>
	<div class="subheading">All Service Options<span class="small">&nbsp;&nbsp;&nbsp;&nbsp;You can drag and drop the options to their respective services</div>
	<div class="sources service-option-dnd">
		@foreach($serviceOptionsUnique as $serviceOption)
			<div>{{ $serviceOption->title }}</div>
		@endforeach
	</div>
	<div class="subheading">All Window Services</div>
	<div class="targets service-option-dnd">
		@foreach($windowServices as $windowService)
			<div data-id="{{ $windowService->id }}">
				<b>{{ $windowService->title }}</b><hr>
				@foreach($serviceOptions as $serviceOption)
					@if($serviceOption->serv_id == $windowService->id)
						<div data-id="{{ $serviceOption->id }}" class="already-added" data-item-type="service-options" data-can-delete>{{ $serviceOption->title }}</div>
					@endif
				@endforeach
			</div>
		@endforeach
	</div>
</div>
<script>
if(typeof window['service_options_on_style_change'] !== 'function') {
	window['service_options_on_style_change'] = function(_style) {
		$('.service-chooser option:not(.default)').hide();
		$('.service-chooser option.default').prop('selected', true);
		$('.item-view li[data-item-type=service-options]').hide();
		$('.service-chooser option[data-child-of=' + _style + ']').show();
		if($('.service-chooser option[data-child-of=' + _style + ']').length) {
			$('.window-service-chooser-container').show();
		}
		else {
			$('.window-service-chooser-container').show();
		}
		$('.btn-add-new').hide();
	};
}
else {
	console.log('service_options_on_style_change() is already defined');
}
if(typeof window['service_options_on_service_change'] !== 'function') {
	window['service_options_on_service_change'] = function(_service) {
		$('.item-view li[data-item-type=service-options]')
			.hide()
			.filter('[data-child-of=' + _service + ']')
				.show();
		if(parseInt(_service, 10)) {
			$('.btn-add-new').show();
		}
		else {
			$('.btn-add-new').hide();
		}
	};
}
else {
	console.log('service_options_on_service_change() is already defined');
}
if(typeof window['service_options_get_additional_data'] !== 'function') {
	window['service_options_get_additional_data'] = function() {
		return {
			'style_id': $('.window-style-chooser option:selected').attr('value'),
			'service_id': $('.service-chooser option:selected').attr('value')
		};
	};
}
else {
	console.log('service_options_get_additional_data() is already defined');
}
if(typeof window['service_options_onaddnew_callback'] !== 'function') {
	window['service_options_onaddnew_callback'] = function(_data) {
		$('.window-style-chooser option[value=' + _data['style_id'] + ']').prop('selected', true);
		window['service_options_on_style_change'](_data['style_id']);
		$('.service-chooser option[value=' + _data['service_id'] + ']').prop('selected', true);
		window['service_options_on_service_change'](_data['service_id']);
	};
}
else {
	console.log('service_options_onaddnew_callback() is already defined');
}
if(typeof window['service_options_manage'] !== 'function') {
	window['service_options_manage'] = function() {
		$('.service-options-list').hide();
		$('.service-options-manage').show();
	};
}
else {
	console.log('service_options_manage() is already defined');
}
if(typeof window['service_options_list'] !== 'function') {
	window['service_options_list'] = function() {
		$('.service-options-manage').hide();
		$('.service-options-list').show();
	};
}
else {
	console.log('service_options_list() is already defined');
}
if(typeof window['service_options_dnd_init'] !== 'function') {
	window['service_options_dnd_init'] = function() {
		$('.dnd-view .sources.service-option-dnd > div').draggable({
			zIndex: 100,
			helper: 'clone'
		});
		$('.dnd-view .targets.service-option-dnd > div').droppable({
			accept: ".sources.service-option-dnd > div",
			activeClass: "semitrans",
			hoverClass: "ui-state-active",
			drop: function( event, ui ) {
				//console.log('Dropped: ', ui.helper.text(), ' on Service: ', $(this).data('id'), $(this).find('b').first().text());

				// check if already added
				var option = ui.helper.text(), existing = false;
				$('.already-added', this).each(function() {
					if($(this).text().toLowerCase() === option.toLowerCase()) {
						existing = true;
						return false;
					}
				});
				if(existing) return false;

				// add option in ui
				var newItem = $('<div class="already-added" data-item-type="service-options" data-can-delete>' + option + '</div>'),
					target = $(this);
				
				// ajax call to update server
				$.post('/service-settings/add?returnid=1', {
					type: 'service-options',
					value: option,
					service_id: $(this).data('id')
				}, function(_data) {
					newItem.attr('data-id', _data).data('id', _data);
					target.append(newItem);
					makeItemsDeletable(newItem);
				});
			}
		});
	};
}
else {
	console.log('service_options_dnd_init() is already defined');
}
service_options_dnd_init();
</script>