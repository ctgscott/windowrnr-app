@extends('layouts.default')

@section('title')
Service Settings
@stop

@section('styles')
<link href="{{ asset('css/service-settings.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="well well-small">
	<div class="title">
		<h3>Service Settings</h3>
	</div>
	<div class="templates">
		<div class="items">
			<div class="item" data-item-type="rooms" data-default>Rooms<i class="glyphicon glyphicon-forward"></i></div>
			<div class="item" data-item-type="balancer-types">Balancer Types<i class="glyphicon glyphicon-forward"></i></div>
			<!--
			<div class="item" data-item-type="window-styles">Window Styles<i class="glyphicon glyphicon-forward"></i></div>
			<div class="item" data-item-type="window-services">Window Services<i class="glyphicon glyphicon-forward"></i></div>
			<div class="item" data-item-type="service-options">Service Options<i class="glyphicon glyphicon-forward"></i></div>
			-->
			<div class="item" data-item-type="parts">Parts<i class="glyphicon glyphicon-forward"></i></div>
		</div>
		<div class="selected-item" data-current-item="none">
			<div class="item-view" data-item-type="rooms"></div>
			<div class="item-view" data-item-type="balancer-types"></div>
			<!--
			<div class="item-view" data-item-type="window-styles"></div>
			<div class="item-view" data-item-type="window-services"></div>
			<div class="item-view" data-item-type="service-options"></div>
			-->
			<div class="item-view" data-item-type="parts"></div>
		</div>
	</div>
</div>	
@stop

@section('scripts')
	<script type='text/javascript' src="{{ asset('js/service-settings.js') }}"></script>
@stop