@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
Email Templates
@stop

@section('styles')
<link href="{{ asset('css/email-templates.css') }}" rel="stylesheet">
@stop

{{-- Content --}}
@section('content')
	<div class="well well-small">
		<div class="title">
			<h3>Email Templates 
				@if(count($templates) > 0)
					<a href="#" class="add-new-template">Add new template</a>
				@endif
				@if(Input::get('src') === 'est')
					<button type="button" class="btn btn-success pull-right" id="btnBack" onclick="window.close()">Done</button>
				@endif
			</h3>
		</div>
		<div class="well well-small add-template">
			<h4>Add New Template</h4>
			{{ Form::open(array('url' => 'email-templates/update', 'class' => 'email-template-form')) }}
				<input type="hidden" value="0" name="id">
				<input type="hidden" value="{{ Input::get('src') }}" name="src">
				<label class="field">
					<span>Title:</span>
					<input type="text" value="" name="name">
				</label>
				<label class="field">
					<span>Content:</span>
					<span class="content-holder"><div id="email_content" contenteditable="true"></div></span>
					<input type="hidden" name="text" id="email_content_hidden">
				</label>
				<label class="field">
					<span>&nbsp;</span>
					<button type="submit" class="btn btn-primary" id="btnAddTemplate" >Add Template</button>
					<button type="submit" class="btn btn-primary" id="btnUpdateTemplate" >Update Template</button>
					<button type="button" class="btn btn-default" id="btnCancelTemplate">Cancel</button>
				</label>
			{{ Form::close() }}
		</div>
		@if(count($templates) === 0)
			<div class="notemplates">
			You haven't added any email templates - <a href="#" class="add-new-template">Add one now</a>
			</div>
		@else
			<div class="templates">
				<div class="items">
					@foreach($templates as $template)
						<div class="item" data-id="{{ $template->id }}">{{ $template->name }}
						<i class="glyphicon glyphicon-forward"></i>
						</div>
					@endforeach
				</div>
				<div class="selected-item">
					@foreach($templates as $template)
						<div class="item-preview" id="itemPreview{{ $template->id }}">{{ $template->text }}</div>
					@endforeach
					<div class="button-holder">
						<button type="button" class="btn btn-primary" id="btnEditTemplate" >Edit
						</button><button type="button" class="btn btn-danger" id="btnDeleteTemplate" >Delete</button>
					</div>
				</div>
			</div>
		@endif
	</div>

@stop

@section('scripts')
	<script type='text/javascript' src="{{ asset('js/email-templates.js') }}"></script>
@stop