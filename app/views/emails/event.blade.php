<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body style="font-family: arial, sans; font-size: 14px; padding: 15px; width: 600px;">
		<p>Thank you very much for calling Window Restoration and Repair!</p>
		<p>A calendar event has just been created for you with the information below.  Nothing needs to be done now, this is simply a confirmation of the appointment recently created for you.</p>
		<p style="padding: 10px; background-color: #eee; width: 600px; line-height: 1.5; margin: 20px 0; display: block;">
			<span style="display: inline-block; width: 80px">Title:</span>{{ $createdEvent->cust_event_type }}<br>
			<span style="display: inline-block; width: 80px">Customer:</span>@if(!empty($customer->company_name))<b>{{ $customer->company_name }}</b> - @endif{{ $customer->f_name }} {{ $customer->l_name }}<br>
			<span style="display: inline-block; width: 80px">Address:</span>{{ $createdEvent->location }}<br>
			<span style="display: inline-block; width: 80px">Date/Time:</span>{{ date('D, jS M Y - g:i A', strtotime($createdEvent->getStart()->dateTime)) }}<br>
		</p>
		<p>If you use Google Calendar and would like to add this event to your personal calendar, press the button below.</p>
		<p>
			<?php 
				$gcURL = 'https://www.google.com/calendar/event?' . 
						'action=TEMPLATE&'.
						'text='.$createdEvent->cust_event_type.'&'.
						'dates=' . date('Ymd\THis', strtotime($createdEvent->getStart()->dateTime)) . '/' . date('Ymd\THis', strtotime($createdEvent->getEnd()->dateTime)) . '&' .
						'details=If you have any questions, please call the office at (562) 493-1590.  Thanks!&'.
						'location='. urlencode($createdEvent->location) .'&'.
						'trp=false';
			?>
		    <a href="{{ $gcURL }}" style="background-color: #347CE7; color: #fff; text-decoration: none; padding: 15px; border-radius: 6px; margin-right: 10px; display: inline-block;">Add to Google Calendar</a>
		</p>
		<p>If you don't use Google Calendar, you can import the event by opening the attached .ics file and saving the item to the calendar system of your choice.</p>
		<p>If you see any mistakes or if you have any questions, please call our office at (562) 493-1590. We look forward to visiting you soon.  Thanks again!</p>
		<p style="line-height: 1.5;">Sincerely,<br/>{{ $creator->first_name }} {{ $creator->last_name }}<br/><img alt="WindowRnR logo" style="margin-top: 10px; width: 300px" src="{{ URL::to('img/logo2.png') }}"></p></p>
	</body>
</html>