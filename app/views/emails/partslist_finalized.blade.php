<p>Hello,</p>
 
<p>Parts list for Job# {{ $job_id }} ({{ $cust_name }}) has been finalized. The estimate it was created from can be seen here: https://app.windowrnr.com/estimates/edit/{{ $est_id }}</p>
 
<p>To print the parts list, please visit: https://app.windowrnr.com/parts-list/{{ $partslist_id }}</p>
 
<p>Cheers,
Webmaster</p>
