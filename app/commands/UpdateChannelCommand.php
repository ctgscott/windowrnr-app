<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateChannelCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:update_channel';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update Channel for google calendar.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//set the google client API
		$client = new Google_Client();
		$client->setApplicationName("WindowRnR");
		$calendar = new Google_Service_Calendar($client);

		//get all channels and review if expirated
		$user_channel = GoogleCalendarChannels::has('user')->get();
		
		$i = 0;
		foreach ($user_channel as $key => $channel_data) 
		{	
			if($channel_data->expiration != '') 
			{
				$expiration_time = strtotime('-1 days', strtotime(date("d-m-Y", $channel_data->expiration)));

				//if expirated
				if($expiration_time <= time()) 
				{
					//delete all channels from this user_id
					GoogleCalendarChannels::where('user_id', $channel_data->user->id)->delete();

					//set the refresh token
					$client->refreshToken($channel_data->user->google_refresh_token);
					$client->setAccessToken($client->getAccessToken());
					$token = (Array) json_decode($client->getAccessToken());

					$email = str_replace('.', '', str_replace('@', '', $channel_data->user->email));
					$channel_id = $email . '_' . App::environment() . '_' . time();

					$chan = new Google_Service_Calendar_Channel();
					$chan->setId($channel_id);
					$chan->setToken($token['access_token']);
					$chan->setType("web_hook");
					$chan->setAddress("https://app.windowrnr.com/api/calpush");
					//$chan->setExpiration(strtotime("+1 week"));

					$watch = $calendar->events->watch('primary', $chan);

					if($watch) 
					{
						$channel = GoogleCalendarChannels::insert(array(
							'user_id' 			 => $channel_data->user->id,
							'google_channel_id'  => $watch->id,
							'google_resource_id' => $watch->resourceId,
							'resource_uri' 		 => $watch->resourceUri,
							//'expiration' 		 => $watch->expiration,
							'token' 			 => $watch->token,
						));
					}
					$i++;
				}
			}
		}

		$this->info($i . ' Channels created');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
