<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth.basic', function()
{
    return Auth::basic('username');
});

Route::filter('auth', function()
{
	if (!Sentry::check()) return Redirect::to('/');

	$path = Request::path();
	$client = AuthController::makeGoogleClient($path);

	$user = Sentry::getUser();

	//check if token is stored in database of if token is invalid
	if(!$user->google_refresh_token)
	{
		AuthController::makeNewToken($path);
	}
});

Route::filter('admin_auth', function()
{
	if (!Sentry::check())
	{
		// if not logged in, redirect to login
		return Redirect::to('users/login');
//		return Redirect::to('/');
	}

	if (!Sentry::getUser()->hasAccess('admin'))
	{
		// has no access
		return Response::make('Access Forbidden', '403');
	}
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

// API auth filter - used for ALL api methods except login
Route::filter('auth.api', function() {

	$exceptions = [
		'api/login',
		'api/resetPassword',
		'api/calpush',
		'api/sashWeight'
	];

	$override = false;
	foreach ($exceptions as $exception) {
		if(strpos(Request::path(), $exception) !== false) {
			$override = true;
			break;
		}
	}

	// Allow ajax calls from webapp with valid user session
	if(Sentry::check()) $override = true;

	if($override === false) {

		$error = false;

		/* Temp code to check header check in local env from browser */
		/*
		if(!Request::header('X-WindowRnR-API') && !Sentry::check()) {
		*/

		if(!Request::header('X-WindowRnR-API')) {
			$error = 'X-WindowRnR-API header not found';
		}
		else {

			/* Temp code to check header check in local env from browser */
			/*
			if(Sentry::check()) {
				$headerArray = [
					Input::get('user_id'),
					Input::get('client_id'),
					Input::get('persist_code')
				];
			}
			else {
				$header = Request::header('X-WindowRnR-API');
				$headerArray = json_decode($header);
			}
			*/

			$header = Request::header('X-WindowRnR-API');
			$headerArray = json_decode($header);

			// NEW: format of header changed. New expected format = [user_id, client_key, persist_code]

			if (count($headerArray) == 3) {

				$id = $headerArray[0];
				$client_key = $headerArray[1];
				$persistCode = base64_decode($headerArray[2]);

				// Let's find our user
				$user = false;
				try	{
					$user = Sentry::getUserProvider()->findById($id);
				}
				catch (Exception $e) {
					$error = 'User not found for ID: ' . $id . '[' . $e . ']';
				}

				if($user === false) {
					$error = 'User not found for ID: ' . $id;
				}
				else {

					// Great! Let's check the session's persist code against the user. If it fails, somebody has tampered
					// with the cookie / session data and we're not allowing a login
					if ($user->persist_code !== $persistCode) {
						$strcmp = levenshtein($user->persist_code, $persistCode);
						$error = 'Persist code check failed.  $user->persist_code ='.$user->persist_code.' and $persistCode (header) ='.$persistCode.' Levenshtein comparison = '.$strcmp;
					}
					
					// NEW: Same persist_code used for a user across all logins
					/*
					// NEW: Allow multiple simultaneous sessions for the same user 
					// Check if the provided persist_code/client_key combo exists in user_sessions
					$user_session = DB::table('user_sessions')
										->where('user_id', $user->id)
										->where('client_key', $client_key)
										->where('persist_code', $persistCode)
										->first();
					if(!$user_session) {
						$error = 'Persist code check failed. No valid user sessions matching $persistCode (header) =' . $persistCode;
					}
					else {
						// update last_used_at field
						$user_session = DB::table('user_sessions')
										->where('user_id', $user->id)
										->where('client_key', $client_key)
										->where('persist_code', $persistCode)
										->update(['last_used_at' => date("Y-m-d H:i:s")]);
					}
					*/
				}
				
			}
			else {
				$error = 'X-WindowRnR-API header exists but does not have two strings.  Here it is: ' . $header;
			}
		}
		if($error !== false) {
			return Response::make('Access Denied. ' . $error, '403');
		}

	}

});
