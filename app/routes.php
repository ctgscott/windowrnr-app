<?php
 
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
Route::get('/', function()
{
	return View::make('users/login');
});
*/

// API auth filter
Route::when('api/*', 'auth.api');

Route::get('/', 'UserController@getLogin');
Route::get('/oauth2', 'AuthController@getOauth2');
Route::get('formtest', 'UserController@formLoad');

Route::get('admin', 'AdminController@index');
Route::get('admin/refreshMap', 'AdminController@refreshMap');
Route::get('appt/getAppt/{job_id}', 'AppointmentsController@getAppt');

Route::get('api/calpush', 'APIController@calPush');
Route::post('api/calpush', 'APIController@calPush');
Route::get('api/customer/{id}', 'APIController@customerDetail');
Route::post('api/customerUpdate', 'APIController@postCustUpdate');
Route::get('api/estimate/create/{jobID?}', 'APIController@getEstimateStart');
Route::get('api/estimate/delete/{estID}', 'APIController@deleteEstimate');
Route::get('api/estimate/edit/{estID?}', 'APIController@getEstimate');
Route::get('api/estimate/list/{jobID}', 'APIController@getEstimateList');
Route::get('api/estimate/options/{servID}', 'APIController@getOptions');
Route::post('api/estimate/save', 'APIController@estimateSave');
Route::get('api/estimate/syncData', 'APIController@estimateSyncData');
Route::post('api/estimate/update', 'APIController@estimateUpdate');
Route::get('api/estimate/style/{styleID}', 'APIController@getStyle');
Route::get('api/event/{id}', 'APIController@eventDetail');
Route::get('api/event/delete/{id}', 'APIController@eventDelete');
Route::get('api/eventID/{eventID}', 'APIController@eventIDDetail');
Route::get('api/events/{start}/{end}/{calID?}', 'APIController@eventsList');
Route::post('api/eventUpdate', 'APIController@postEventUpdate');
Route::get('api/folderCreate/{job_id}/{folder_id}/{parent_id}', 'APIController@createFolder');
Route::get('api/getFolderID/{job_id}', 'APIController@getFolderID');
Route::post('api/imageUp/{owner}/{id}', 'APIController@imageUp');
Route::post('api/imageUpload', 'APIController@imageUpload');
Route::get('api/job/{id}', 'APIController@jobDetail');
Route::get('api/jobList/{jobStatus?}', 'APIController@getJobList');
Route::post('api/login', 'APIController@login');
Route::get('api/login/{email}/{password}/{client_key}', 'APIController@login');
Route::post('api/notes/add', 'APIController@addNote');
Route::get('api/notes/list/{job_id}', 'APIController@listNotes');
Route::post('api/payment', 'APIController@postPayment');
Route::get('api/payments/{job_id}', 'APIController@getPayments');
Route::get('api/rebuildCustomersTable', 'APIController@rebuildCustomersTable');
Route::get('api/rebuildJobsTable', 'APIController@rebuildJobsTable');
Route::get('api/sashWeight/{width}/{height}/{glass}/{material}/{sashThickness}', 'APIController@sashWeight');
Route::get('api/testlogin', 'CustomersController@getTestLogin');
Route::get('api/userDetail/{userID}', 'APIController@userDetail');
Route::get('api/userList', 'APIController@userList');
Route::post('api/calendarEvent', 'APIController@createCalendarEvent');
Route::get('api/parts/{win_service_id}', 'APIController@getServicePartMapping');

Route::get ('api/email-template/{name}', 'EmailTemplatesController@getEmailTemplate');
Route::get ('api/email-template-from-id/{id}', 'EmailTemplatesController@getEmailTemplateByID');
Route::get ('api/email-templates', 'EmailTemplatesController@getAllEmailTemplates');
Route::get ('api/parts-list/{id}', 'PartsListsController@apiGet');
Route::post('api/parts-list/finalize', 'PartsListsController@apiFinalize');

Route::get('util/sash-weight', 'UtilController@sashWeight');

Route::post('api/resetPassword', 'APIController@resetPassword');

// file upload should be a POST multipart request
Route::post('api/saveFile', 'APIController@saveFile');

Route::get('calendar', 'CustomersController@calendar');
Route::get('job-parts-list/{job_id}', 'CustomersController@redirectToPartsList');
Route::get('assign', 'CustomersController@assign');

Route::post('/calsync', array('as' => 'calsync.create', 'uses' => 'CalsyncController@create'));
Route::get('/calsync/new', array('as' => 'calsync.new', 'uses' => 'CalsyncController@add'));
Route::get('/calsync/delete_event/{id}', array('uses' => 'CalsyncController@deleteEvent'));
Route::get('/calsync/get_event/{id}', array('uses' => 'CalsyncController@getEvent'));
Route::get('/calsync/update_event/{id}', array('uses' => 'CalsyncController@updateEvent'));
Route::get('/calsync/create_watch_channel', 'CalsyncController@createWatchChannel');
Route::get('/calsync/list_all', 'CalsyncController@listAllEvents');
Route::get('/calsync/make_update', 'CalsyncController@makeUpdates');
Route::get('/calsync/stop_channel/{id}', 'CalsyncController@stopChannel');

// Vj: Temp routes for testing calendar push notifications
// ---- start -----
Route::get('/calsync/watch/{calendar}', 'CalsyncController@startWatching');
Route::get('/calsync/notification', 'CalsyncController@onPushNotification');
Route::post('/calsync/notification', 'CalsyncController@onPushNotification');
Route::get('/calsync/update/{calendar?}', 'CalsyncController@updateEvents');
Route::get('/calsync/stoptemp', 'CalsyncController@stopTempPrev');
// ---- end -----

// update al user channels (cron job)
Route::get('/system/update_all_channels', 'AdminController@updateAllUserChannels');

Route::get('customers/{id}', 'CustomersController@getDetailID');
Route::get('customers/archive/{id}', 'CustomersController@archive');

Route::get('customers/{id}', 'CustomersController@getDetailID');
Route::get('customers/autocomplete', 'CustomersController@autocomplete');
Route::post('customers/calNewLead', 'CustomersController@calNewLead');
Route::get('customers/complete/{id}', 'CustomersController@complete');
Route::get('customers/estimateschedule', 'CustomersController@EstimateSchedule');
Route::get('customers/estimateschedule2', 'CustomersController@EstimateSchedule2');
Route::post('customers/jobDetail', 'JobsController@postJobDetailByCustID');
Route::post('customers/newLead', 'CustomersController@newLead');
Route::get('customers/oldsearch/{term}', 'CustomersController@search');
Route::post('customers/postGoogleInsert', 'CustomersController@postGoogleInsert');
Route::get('customers/schedule/{id}', 'CustomersController@getScheduleID');
Route::get('customers/typeahead/{term}', 'CustomersController@typeahead');
Route::post('customers/update', 'CustomersController@update');
Route::get('customers/updategoogle', 'CustomersController@updategoogle');

Route::get('search/{term?}', 'CustomersController@searchSolr');
Route::get('admin/reindex', 'AdminController@reindexSolr');

Route::get('estimates/admin', 'EstimatesController@admin');
Route::post('estimates/admin/style/save', 'EstimatesController@adminStyleSave');
Route::post('estimates/admin/style/saveroom', 'EstimatesController@adminStyleSaveRoom');
Route::post('estimates/admin/style/addroom', 'EstimatesController@adminStyleAddRoom');
Route::post('estimates/admin/style/addwinservice', 'EstimatesController@adminStyleAddWinService');
Route::post('estimates/admin/style/addwinserviceoption', 'EstimatesController@adminStyleAddWinServiceOption');
Route::post('estimates/admin/style/deleteroom', 'EstimatesController@adminStyleDeleteRoom');
Route::post('estimates/admin/style/addwinstyle', 'EstimatesController@adminStyleAddWinStyle');
Route::post('estimates/admin/style/updatewinstyle', 'EstimatesController@adminStyleSaveWinstyle');
Route::get('estimates/create/{id}', 'EstimatesController@create');
Route::get('estimates/delete/{id}', 'EstimatesController@delete');
Route::get('estimates/edit/{id}', 'EstimatesController@edit');
Route::post('estimate/save', 'EstimatesController@save');
Route::get('estimate/createPDF/{data}', 'EstimatesController@createPDF');
Route::get('estimate/share', 'EstimatesController@shareEst2');
Route::post('estimate/share/{estID}', 'EstimatesController@shareEst');
Route::post('estimate/share3/{estID}', 'EstimatesController@shareEst3');
Route::post('estimate/savePDF/{estID}', 'EstimatesController@saveEstimatePDF');
Route::post('estimate/emailPDF', 'EstimatesController@emailEstimatePDF');
Route::post('estimate/test/roomSave', 'EstimatesController@roomSave');
Route::get('estimate/test/create', 'EstimatesController@testCreate');
Route::post('estimate/test/winstyleSave', 'EstimatesController@winstyleSave');
Route::post('estimate/test/winserviceoptionsSave', 'EstimatesController@winserviceoptionsSave');
Route::post('estimate/test/winstyleserviceSave', 'EstimatesController@winstyleserviceSave');
Route::post('estimate/test/adminListWinService/{id}', 'EstimatesController@adminListWinService');
Route::post('estimate/test/adminListWinServiceOptions/{id}', 'EstimatesController@adminListWinServiceOptions');
Route::post('estimate/test/adminListMultiWinService/{id}', 'EstimatesController@adminListMultiWinService');
Route::post('estimate/test/adminCheckMultiWinService/{id}/{stid}', 'EstimatesController@adminCheckMultiWinService');
Route::post('estimate/test/adminUpdateMultiWinService/{styleid}/{servid}/{optionid}/{ischk}', 'EstimatesController@adminUpdateMultiWinService');
Route::get('estimates/todo', 'JobsController@listEstimates');
Route::post('estimate/update', 'EstimatesController@update');
Route::post('estimate/update1', 'EstimatesController@update1');
Route::post('estimate/updateAjax', 'EstimatesController@updateAjax');
Route::post('estimate/copy', 'EstimatesController@copyEstimate');
Route::post('estimate/print', 'EstimatesController@printEstimate');
Route::post('estimate/marksold/{id}', 'EstimatesController@markSoldEstimate');
Route::post('estimate/undosold/{id}', 'EstimatesController@undoSoldEstimate');
Route::get('estimate/service-parts/{id}', 'EstimatesController@serviceParts');
Route::get('estimate/option-parts/{id}', 'EstimatesController@optionParts');

// Twilio notifications
Route::get('twilio-incoming', 'TwilioController@incoming');
Route::post('twilio-incoming', 'TwilioController@incoming');
Route::get('twilio-incoming-fallback', 'TwilioController@incomingFallback');
Route::get('twilio-process', 'TwilioController@process');
Route::get('flatten-numbers', 'TwilioController@flattenCustomerNumbers');
Route::get('msgs', 'TwilioController@main');
Route::post('textOut', 'TwilioController@textOut');
Route::get('getThread/{phone}', 'TwilioController@getThread');

// new service settings page & associated
Route::get('service-settings', 'ServiceSettingsController@index');
Route::get('service-settings/view/{type}', 'ServiceSettingsController@view');
Route::post('service-settings/add', 'ServiceSettingsController@add');
Route::post('service-settings/update', 'ServiceSettingsController@update');
Route::post('service-settings/delete', 'ServiceSettingsController@delete');

// newer horizontally expanding services/options/parts manager
Route::get('sop-admin', 'SOPAdminController@index');
Route::get('sop-admin/styles.json', 'SOPAdminController@getStyles');
Route::get('sop-admin/services.json', 'SOPAdminController@getServices');
Route::get('sop-admin/options.json', 'SOPAdminController@getOptions');
Route::get('sop-admin/parts.json', 'SOPAdminController@getParts');
Route::get('sop-admin/service-parts.json', 'SOPAdminController@getServiceParts');

Route::post('sop-admin/styles/add', 'SOPAdminController@addStyle');
Route::post('sop-admin/services/add', 'SOPAdminController@addService');
Route::post('sop-admin/options/add', 'SOPAdminController@addOption');
Route::post('sop-admin/parts/add-to-option', 'SOPAdminController@addPartToOption');
Route::post('sop-admin/parts/add-to-service', 'SOPAdminController@addPartToService');
Route::post('sop-admin/styles/update', 'SOPAdminController@updateStyle');
Route::post('sop-admin/services/update', 'SOPAdminController@updateService');
Route::post('sop-admin/options/update', 'SOPAdminController@updateOption');
Route::post('sop-admin/parts/update-service-part', 'SOPAdminController@updateServicePart');
Route::post('sop-admin/parts/update-option-part', 'SOPAdminController@updateOptionPart');
Route::post('sop-admin/styles/delete', 'SOPAdminController@deleteStyle');
Route::post('sop-admin/services/delete', 'SOPAdminController@deleteService');
Route::post('sop-admin/options/delete', 'SOPAdminController@deleteOption');
Route::post('sop-admin/parts/delete', 'SOPAdminController@deletePart');

Route::get('sop-admin/get-service-for-style-count', 'SOPAdminController@getServicesToStylesCount');
Route::get('sop-admin/get-option-for-service-count', 'SOPAdminController@getOptionsToServiceCount');

Route::post('sop-admin/styles/map-services', 'SOPAdminController@postMapServicesToStyle');

Route::get('events/{id}', 'EventsController@show');
Route::post('events/{id}', 'EventsController@update');
Route::get('events/getGoogleCalEvents/{start}/{end}/{cal}', 'EventsController@getGoogleCalEvents');
Route::get('events/getCalEvents/{start?}/{end?}/{cal?}', 'EventsController@getCalEvents');
Route::get('events/updateEvents/{start?}/{end?}/{cal?}', 'EventsController@updateEvents');
Route::get('events/updateEventsTable', 'EventsController@updateEventsTable');
Route::delete('events/delete/{id}', array('as' => 'event.delete', 'uses' => 'EventsController@delete'));
Route::get('events/delete/{id}', 'EventsController@delete');
Route::post('event-assign', 'EventsController@setAssignee');
Route::post('event-edit-app', 'EventsController@editEventApp');
Route::post('event-edit-goog', 'EventsController@editEventGoog');
Route::post('event-duplicate-app', 'EventsController@duplicateEventApp');
Route::post('event-duplicate-goog', 'EventsController@duplicateEventGoog');
Route::post('event-update', 'EventsController@updateEvent');
Route::post('event-map', 'EventsController@mapEvent');
Route::post('event-map-new', 'EventsController@mapEventNew');
Route::post('event-unmap', 'EventsController@unmapEvent');
Route::post('event-discard', 'EventsController@discardEvent');
Route::get('event-test', 'EventsController@test');
Route::get('event-test-get', 'EventsController@testGet');
Route::post('event-test-set-property', 'EventsController@testSetProperty');
Route::get('event-test-log', 'EventsController@testGet_calPush_Log');
Route::get('events-set-property', 'EventsController@setProperty');
Route::post('events-set-single-property', 'EventsController@setSingleProperty');

Route::get('frame/estimate/{est_id}', 'EstimatesController@iframeEstPDF');
Route::get('frame/{job_id}/{appt_id}', 'CustomersController@iframePDF');

Route::get('job/{id}', 'JobsController@jobCustDetail');
Route::get('job/archive/{id}/{note?}', 'JobsController@archive');
Route::post('job/archiveNote', 'JobsController@archiveNote');
Route::get('job/updateStatus/{id}/{statusID}', 'JobsController@updateStatus');
Route::get('job/unarchiveDirect/{id}', 'JobsController@unarchiveDirect');
Route::post('job/unarchive', 'JobsController@unarchive');
Route::get('leadsheet/{job_id}/{appt_id}', 'CustomersController@leadSheet');

Route::get('pdf/estimate/{est_id}', 'EstimatesController@EstimatePDF');
Route::get('pdf/jobs/{job_id}/{event_id}', 'CustomersController@custDetailPDF');

Route::post('profiles/postSalesCheckBox', 'ProfilesController@postSalesCheckBox');
Route::post('profiles/postVisibleCheckBox', 'ProfilesController@postVisibleCheckBox');
Route::get('profiles/getSalesCheckBox', 'ProfilesController@getSalesCheckBox');
Route::get('profiles/getSalesCalID', 'ProfilesController@getSalesCalID');
Route::get('users/register', 'UserController@getRegister');

Route::get('custMap', 'CustomersController@custMapV2');

Route::get('msgboard', 'MsgboardController@listMessages');

Route::get('api/test', function()
{
	header('X-WindowRnR-API: 1, "testing123!"');
	return View::make('customers/test');
});

// image uploads
Route::get('images/{owner_type}/{owner_id}', 'ImageController@images');
Route::post('upload/{owner_type}/{owner_id}', 'ImageController@upload');
Route::post('delete-image', 'ImageController@delete');
Route::post('rotate-image', 'ImageController@rotate');

// email templates
Route::get ('email-templates', 'EmailTemplatesController@manage');
Route::post('email-templates/update', 'EmailTemplatesController@update');
Route::post('email-templates/delete', 'EmailTemplatesController@delete');
Route::get ('email-templates/list', 'EmailTemplatesController@getTemplates');

// parts list
Route::post('parts-list/create', 'PartsListsController@create');
Route::get('parts-list/{id}', 'PartsListsController@editSimple'); // editSimple() for now, instead of edit()
Route::post('parts-list/update', 'PartsListsController@update');
Route::post('parts-list/delete', 'PartsListsController@remove');
Route::post('parts-list/finalize', 'PartsListsController@finalize');
Route::post('parts-list/unfinalize', 'PartsListsController@unfinalize');

// lead check via ajax
Route::get('lead-check', 'CustomersController@leadCheck');

// drive test
Route::get('drive-test', 'ImageController@driveTest');

Route::controller('users', 'UserController');
Route::controller('admin', 'AdminController');
Route::controller('customers/schedule', 'CustomersController'); 
Route::controller('customers', 'CustomersController');

Route::resource('groups', 'GroupController');






Route::resource('customers', 'CustomersController');

Route::resource('jobs', 'JobsController');

Route::resource('services', 'ServicesController');

Route::resource('windows', 'WindowsController');

Route::resource('parts', 'PartsController');

Route::resource('stock_parts', 'Stock_partsController');

Route::resource('stock_part_cats', 'Stock_part_catsController');

Route::resource('notes', 'NotesController');

Route::resource('statuses', 'StatusesController');

Route::resource('photos', 'PhotosController');

Route::resource('window_totals', 'Window_totalsController');

Route::resource('appointments', 'AppointmentsController');

Route::resource('profiles', 'ProfilesController');

//Route::resource('estimates', 'EstimatesController');

Route::resource('events', 'EventsController');

//Calendar Test
Route::get('testCalendar', 'CalendarTestController@insertLostEvents');

//Gmail test
Route::get('testGmail', 'MsgboardController@getGmail');
Route::get('testGmail2', 'MsgboardController@getGmail2');