/* change collation of fname and lname in testSheet - needed to be able to customers.f_name and customers.l_name */
ALTER TABLE `testSheet1` CHANGE `lname` `lname` VARCHAR(32)  CHARACTER SET utf8  COLLATE utf8_unicode_ci  NULL  DEFAULT NULL;
ALTER TABLE `testSheet1` CHANGE `fname` `fname` VARCHAR(20)  CHARACTER SET utf8  COLLATE utf8_unicode_ci  NULL  DEFAULT NULL;

/* add lat and lng fields in testSheet - to store results of geolocation requests */
ALTER TABLE `testSheet1` ADD `lat` DECIMAL(11,8)  DEFAULT NULL  AFTER `value`;
ALTER TABLE `testSheet1` ADD `lng` DECIMAL(11,8)  DEFAULT NULL  AFTER `lat`;

/* the table does not have an id, add one (we need to identify rows for setting retreived lat and lng) */
ALTER TABLE `testSheet1` ADD `md5_id` VARCHAR(50)  NULL  DEFAULT NULL  AFTER `value`;

/* populate our new id as the md5 checksum of overall record data */
UPDATE testSheet1 SET md5_id = MD5(CONCAT_WS('|', fname, lname, phone, alt_phone, `address`, city, state, zip, install_date, `value`));

/* temporary column in customers - for identifying the customer to get lat/lng from when populating jobs table */
/* this column can be removed once the import operation is complete */
ALTER TABLE `customers` ADD `temp_md5_id` VARCHAR(50)  NULL  DEFAULT NULL  AFTER `billing_zip`;

/* in testSheet1 */
/* copy the date value from field name "??" to install_date for rows without an install date */
UPDATE testSheet1 SET install_date = `??` where install_date = '' and `??` <> '';

/*
-- customers considered for importing in pass-1
SELECT md5_id, address, city, state, zip FROM testSheet1 WHERE address <> '' && city <> '' && install_date regexp '^[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}$' ORDER BY md5_id

-- customers to be considered for importing in pass-2
SELECT md5_id, address, city, state, zip FROM testSheet1 WHERE address = '' || city = '' || install_date not regexp '^[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}$' ORDER BY md5_id
*/