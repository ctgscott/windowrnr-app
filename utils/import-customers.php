<?php

/*
Script to import customers from table:testSheet1 to table:customers
*/

date_default_timezone_set("UTC");

$db = mysql_connect('127.0.0.1', 'root', 'password');
mysql_select_db('windowcustomer');

$sql = "SELECT * FROM testSheet1 WHERE address <> '' && city <> '' && install_date regexp '^[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}$' ORDER BY md5_id";
$result = mysql_query($sql);

$insertSQL = "INSERT INTO customers (`l_name`, `f_name`, `phone`, `alt_phone`, `email`, `billing_address`, `billing_city`, `billing_state`, `billing_zip`, `temp_md5_id`, `created_at`, `updated_at`) VALUES ";

// l_name, f_name, phone, alt_phone, email, billing_address, billing_city, billing_state, billing_zip, temp_md5_id, created_at, updated_at
$count = 0;
while( ($row = mysql_fetch_array($result)) !== FALSE ) {

	foreach ($row as $key => $value) {
		$row[$key] = mysql_real_escape_string(trim($row[$key]));
	}

	if($count == 0) $insertSQL .= "\n"; else $insertSQL .= ",\n";
	$count++;

	$date = strftime("%G-%m-%d 00:00:00", strtotime($row['install_date']));
	$insertSQL .= "('{$row['lname']}', '{$row['fname']}', '{$row['phone']}', '{$row['alt_phone']}', '', '{$row['address']}', '{$row['city']}', '{$row['state']}', '{$row['zip']}', '{$row['md5_id']}', '$date', '$date')";
}

$insertSQL .= ";\n";

echo $insertSQL;

?>

