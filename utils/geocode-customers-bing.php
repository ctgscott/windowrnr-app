<?php

/*
Script to geolocate customers in testSheet1 using Bing Geospatial
*/

$db = mysql_connect('127.0.0.1', 'root', 'password');
mysql_select_db('windowcustomer');

$offset = ($argc == 2 ? intval($argv[1]) : 0);

do {

	echo "Processing records " . ($offset+1) . " to " . ($offset+50) . " ... \n";

	// create CSV format to upload
	$csv = [];
	$id = $offset + 1;
	$sql = "SELECT md5_id, address, city, state, zip FROM testSheet1 WHERE address <> '' && city <> '' ORDER BY md5_id LIMIT 50 OFFSET $offset";
	$result = mysql_query($sql);
	while( ($row = mysql_fetch_array($result)) !== FALSE ) {
		array_push($csv, "{$row['md5_id']},en-US,,{$row['address']},{$row['state']},,,,{$row['city']},{$row['zip']},,,,,,,,,,,,,,,,,,,,,,,,");
		$id++;
	}
	$offset += 50;
	$csv = implode("\n", $csv) . "\n";
	//echo $csv;

	// make geocode request to bing
	doBingRequest($csv, $offset+1, $offset+50);

	// wait 5 seconds before next request
} while(mysql_num_rows($result) > 0);

/* be blocking - return only when the job is done */
function doBingRequest($_csv, $_start, $_end) {

	echo "Making request to Bing ...\n";

	$key = 'An-iht2ueb6XDeq9k1vob5u089navkKIVrt2QOfZ2Fzz7h-JpaZfQdaJOo6oMMOC';
	$url = "http://spatial.virtualearth.net/REST/v1/Dataflows/Geocode?description=MyJob&input=csv&key=$key";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $_csv);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/csv'));
	$result = curl_exec($ch);
	curl_close($ch);

/*
// format of successful response
{
	"authenticationResultCode":"ValidCredentials",
	"brandLogoUri":"http:\/\/spatial.virtualearth.net\/Branding\/logo_powered_by.png",
	"copyright":"Copyright © 2014 Microsoft and its suppliers. All rights reserved. This API cannot be accessed and the content and any results may not be used, reproduced or transmitted in any manner without express written permission from Microsoft Corporation.",
	"resourceSets":[
		{
			"estimatedTotal":1,
			"resources":[
				{
					"__type":"DataflowJob:http:\/\/schemas.microsoft.com\/search\/local\/ws\/rest\/v1",
					"id":"9f65dde11c134959be9d672869bcba60",
					"links":[
						{
							"role":"self",
							"url":"https:\/\/spatial.virtualearth.net\/REST\/v1\/dataflows\/Geocode\/9f65dde11c134959be9d672869bcba60"
						}
					],
					"createdDate":"Wed, 29 Oct 2014 07:25:31 GMT",
					"description":"Geocode",
					"failedEntityCount":0,
					"processedEntityCount":0,
					"status":"Pending",
					"totalEntityCount":50
				}
			]
		}
	],
	"statusCode":201,
	"statusDescription":"Created",
	"traceId":"476c02a875e549e083ee4ed0950e949c|HK20230528|02.00.106.1500|"
}
*/

	$result = json_decode($result);

	if($result->statusDescription === 'Created') {
		$jobId = $result->resourceSets[0]->resources[0]->id;
		$jobStatus = $result->resourceSets[0]->resources[0]->status;
		echo "Job created: " . $jobId . "\n";

		// Call the API to determine the status of all geocode jobs associated with a Bing Maps key
		echo "Waiting for geocode job completion ...\n";
		while ($jobStatus != "Completed") 
		{
			// Wait 5 seconds, then check the job’s status
			sleep(5);

			// Construct the URL to check the job status, including the jobId
			$checkUrl = "http://spatial.virtualearth.net/REST/v1/Dataflows/Geocode/".$jobId."?output=xml&key=".$key;
			$checkResponse = file_get_contents($checkUrl);
			$responseBody = new SimpleXMLElement($checkResponse);

			// Get and print the description and current status of the job  
			$jobDesc = $responseBody->ResourceSets->ResourceSet->Resources->DataflowJob->Description;
			$jobStatus = $responseBody->ResourceSets->ResourceSet->Resources->DataflowJob->Status;

			echo $jobDesc." - ".$jobStatus."\n";

			//var_dump($responseBody);
		}

		echo "Downloading results...\n";

		$successUrl = '';
		$links = $responseBody->ResourceSets->ResourceSet->Resources->DataflowJob->Link;
		foreach ($links as $link) {
			if ($link['name'] == "succeeded") { 
				$successUrl = $link; 
				break; 
			}
		}

		if($successUrl != '') {
			$successUrl .= "?output=xml&key=".$key;
			$successResponse = file_get_contents($successUrl);
			file_put_contents("response-$_start-$_end.csv", $successResponse);
		}
	}
	else {
		echo "Job could not be created: " . $result->statusDescription . "\n";
	}

}

?>
