function printSheet(jobID, apptID)
{
	window.location.href = "/frame/"+jobID+"/"+apptID;
}

function returnID(elem)
{
	// Identify the element from the event
	var elementID = $(elem).attr("id");
	
	// Break out the # from the element's ID
	// ie. expandBtn4 = 4
	var split = elementID.split(/(\d+)/);
	
	return split[1];
}

function printEstimate(elem) {
	window.location.href = '/estimates/edit/' + $(elem).data('id') + '?print=1';
}

function deleteEstimate(elem)
{
	var itemNum = returnID(elem);
	$.get( "/estimates/delete/"+itemNum, function( data ) {
//		alert( "Estimate was deleted" );
		location.reload();
	});
}

function deletePartsList(elem) {
	var itemNum = returnID(elem);
	$.post( "/parts-list/delete", {id: itemNum}, function( data ) {
		location.reload();
	});
}

function createFormattedDateString(date) {
	var amOrPm;
	var dateArray = date.split(/[- :]/);
	
	if (parseInt(dateArray[3]) >= 0 && parseInt(dateArray[3]) < 12) {
		amOrPm = "am"
	} 
	else {
		amOrPm = "pm"
	}
	var dateString = dateArray[1] + "/" + dateArray[2] + "/" + dateArray[0] + " - " + parseInt(dateArray[3]) + ":" + dateArray[4] + " " + amOrPm;
	
	return dateString;
}

function copyEstimate(elem) {
	var itemNum = returnID(elem), tr = $(elem).closest('tr');
	var newName = window.prompt('Name of new estimate:', 'Copy of ' + tr.find('.btnEst').text());
	if(newName) {
		$.post('/estimate/copy', {id: itemNum, name: newName}, function(_data) {
			_data = JSON.parse(_data);
			if(_data.status === 'ok') {
				var newRow = $('<tr/>').html(tr.html());
				newRow.find('.btnEst').text(_data.name);

				var dateString = createFormattedDateString(_data.date);
				//newRow.find('td:eq(1)').text(_data.date);
				newRow.find('td:eq(1)').text(dateString);

				newRow.find('.btnEst').attr('href', '/estimates/edit/' + _data.id);
				newRow.find('.printEst').attr('id', 'printEst' + _data.id);
				newRow.find('.copyEst').attr('id', 'copyEst' + _data.id);
				newRow.find('.deleteEst').attr('id', 'deleteEst' + _data.id);

				newRow.insertAfter(tr);
			}
			else {
				alert("Estimate couldn't be copied!");
			}
		});
	}
}

function markasSoldEstimate(elem) {
	$.post( "/estimate/marksold/"+returnID(elem), function( data ) {
		location.reload();
	});
}

function undoMarkasSoldEstimate(elem) {
	$.post( "/estimate/undosold/"+returnID(elem), function( data ) {
		location.reload();
	});
}

/*function billingCheck()
{
	if (document.getElementById('billCheck').checked) {
		alert('checked');
	} else {
		alert('NOT checked');
	}
}
*/

function equalizeTopCellHeights() {
	$('.top-cells > .cell').css('min-height', Math.max.apply(null, $('.top-cells > .cell').map(function(_x, _y) {
		return $(_y).outerHeight();
	})) + 'px');
}
$(window).load(equalizeTopCellHeights);

$(document).ready(function(){

	$('.btn-archive-job').click(function() {
		$('#modalJobID').val($(this).data('job-id'));
		$('#archiveNote').modal('toggle');
		return false;
	});
	
	$('.job_estimates').on('click', "[id^='printEst']", function() {
		printEstimate(this);
	});

	$('.job_estimates').on('click', "[id^='deleteEst']", function() {
		deleteEstimate(this);
	});
	
	$('.job_estimates').on('click', "[id^='copyEst']", function() {
		copyEstimate(this);
	});

	$('.job_estimates').on('click', "[id^='soldEst']", function() {
		markasSoldEstimate(this);
	});

	$('.job_estimates').on('click', "[id^='resetSoldEst']", function() {
		undoMarkasSoldEstimate(this);
	});

	$('.job_parts_lists').on('click', "[id^='deletePartsList']", function() {
		deletePartsList(this);
	});
	
	$("#btnBillDetail").click(function () {
		$('#divBillDetail').collapse('toggle');
	});

	// Job status - Accepted, On standby, Completed
	$('input[type="checkbox"][name="status-job-accepted"]').change(function() {
		$('input[type="checkbox"][name="status-on-standby"], input[type="checkbox"][name="status-completed"]')
			.prop('disabled', !this.checked)
			.closest('.line-label')[this.checked ? 'removeClass' : 'addClass']('disabled');
		if(!this.checked) $('.btn-assign-techs').hide(); else $('.btn-assign-techs').show();
	});
	$('input[type="checkbox"][name="status-on-standby"]').change(function() {
		$('input[type="checkbox"][name="status-completed"]')
			.prop('disabled', this.checked)
			.closest('.line-label')[!this.checked ? 'removeClass' : 'addClass']('disabled');
	});
	$('input[type="checkbox"][name="status-on-service-call"]').change(function() {
		$('input[type="checkbox"][name="status-completed"]')
			.prop('disabled', this.checked)
			.closest('.line-label')[!this.checked ? 'removeClass' : 'addClass']('disabled');
	});
	
	// show system notes checkbox
	$('.chk-show-system-notes').change(function() {
		$('ol.job-notes li.system')[this.checked ? 'show' : 'hide']();
	});
	
	// DYMO printer section to print labels
	function startupCode() {
		var printers = dymo.label.framework.getPrinters();
		if(!printers) {
			alert("Dymo web service not running");
			return;
		}
		if (printers.length == 0)
		{
			alert("No DYMO printers are installed. Install DYMO printers.");
			return;
		}
		for (var i = 0; i < printers.length; i++)
		{
			$('#btn_printLabel').attr('disabled', false);
			var printerName = printers[i].name;
			var company = $('#company').val();
			var fName = $('#f_name').val();
			var lName = $('#l_name').val();
			var city = $('#job_city').val();
			if(!$('#company').val()) {
				var labelText = lName+", "+fName+"\r\n"+"("+city+")";
			} else {
				var labelText = company+"\r\n"+lName+", "+fName+"\r\n"+"("+city+")";
			}
			$.ajax({
				type: "GET",
				url: "/js/ext/dymo/addressLabel.label",
				dataType: "text",
				success: function (xml) {
					var label = dymo.label.framework.openLabelXml(xml);
					label.setAddressText(0,labelText);
					label.print(printerName);
				}
			});
		}
	}

	$('#btn_printLabel').click(function() {
		dymo.label.framework.init(startupCode);
	});
});
