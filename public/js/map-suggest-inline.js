$(window).load(function() {

	var map_init = false;

	var autocomplete;

	$('[data-address-group]').each(function() {
		
		var addressBox = $(this).find('.map-suggest-address'),
			autocomplete = new google.maps.places.Autocomplete(addressBox[0], { types: ['geocode'] }),
			form = $(this);

		// prevent auto-submit on place-select
		addressBox.bind('keydown', function(_e) {
			if(_e.keyCode === 13) {
				_e.preventDefault();
				return false;
			}
		});

		// limit suggestions to addresses in the area of business
		var location = new google.maps.LatLng(33.81061710, -118.07312390);
		autocomplete.setBounds(new google.maps.LatLngBounds(location, location));

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			
			var place = autocomplete.getPlace();

			var streetnum = getAddressPart(place.address_components, 'street_number'),
				route = getAddressPart(place.address_components, 'route'),
				address = [];
			if(streetnum) address.push(streetnum);
			if(route) address.push(route);
			address = address.join(' ');

			form.find('.map-suggest-address').val(address).data('value', address);
			form.find('.map-suggest-city').val(getAddressPart(place.address_components, 'locality'));
			form.find('.map-suggest-state').val(getAddressPart(place.address_components, 'administrative_area_level_1', true));
			form.find('.map-suggest-zip').val(getAddressPart(place.address_components, 'postal_code'));

			return false;
		});

		// override google's onblur - that causes the address box to be filled with the full address on blur
		addressBox[0].addEventListener('blur', function(_e) {
			setTimeout(function() {
				if(addressBox.data('value')) addressBox[0].value = addressBox.data('value');
			}, 5);
		});

		// if  data-open-in-gmap is specified add a button at the end to open the address in gmaps
		if(form.is('[data-open-in-gmap]')) {
			form.append('<button class="btn btn-default-btn-sm btn-info btn-open-in-gmap" id="btnOpenMap">Map</button>');
		}

		form.on('click', '.btn-open-in-gmap', function() {
			var lookupAddress = [];
			if($.trim(form.find('.map-suggest-address').val()) !== '') lookupAddress.push($.trim(form.find('.map-suggest-address').val()));
			if($.trim(form.find('.map-suggest-city').val()) !== '') lookupAddress.push($.trim(form.find('.map-suggest-city').val()));
			if($.trim(form.find('.map-suggest-state').val()) !== '') lookupAddress.push($.trim(form.find('.map-suggest-state').val()));
			if($.trim(form.find('.map-suggest-zip').val()) !== '') lookupAddress.push($.trim(form.find('.map-suggest-zip').val()));
			lookupAddress = lookupAddress.join(', ');
			if($.trim(lookupAddress) !== '') {
				window.open('https://www.google.co.in/maps/place/' + encodeURIComponent($.trim(lookupAddress)), 
								'lookup-address', 
								'height=' + screen.height + ',width=' + screen.width + ',modal=no,alwaysRaised=yes');
			}
			return false;
		});

	});

});

function getAddressPart(_components, _part, _shortname) {
	var result = '';
	_components.forEach(function(_component) {
		if(_component.types[0] === _part) result = _component[_shortname? 'short_name' : 'long_name'];
	});
	return result;
}
