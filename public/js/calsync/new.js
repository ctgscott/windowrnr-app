    jQuery( document ).ready( function( $ ) {
     
        $( '#form-add-setting' ).on( 'submit', function() {
     
            //.....
            //show some spinner etc to indicate operation in progress
            //.....
     
            $.post(
                $( this ).prop( 'action' ),
                {
                    "_token": $( this ).find( 'input[name=_token]' ).val(),
                    "setting_name": $( '#setting_name' ).val(),
                    "setting_value": $( '#setting_value' ).val()
                },
                 function(data, textStatus, jqXHR) {
					 var a1 = data;
                    //alert("Data: " + a1 + "\nStatus: " + textStatus + "\njqXHR: " + jqXHR);//do something with data/response returned by server
					
					jqXHR.done($( "#res" ).html(data));
					//jqXHR.fail();
					//jqXHR.always();
                },
                'text'
            );
     
            //.....
            //do anything else you might want to do
            //.....
     
            //prevent the form from actually submitting in browser
            return false;
        } );
     
    } );

