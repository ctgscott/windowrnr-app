$(document).ready(function() {

	// change events
	$('.parts').on('change keyup', '.qty-chooser, .option-chooser', reTotalParts);

	// delete part
	$('.parts').on('click', '.delete', function() {
		$(this).closest('tr.part-row').remove();
		reTotalParts();
		return false;
	});

	// add new part
	$('.new-part-chooser li a').click(function() {
		var chosenPartID = $(this).data('part-id'),
			part = allParts.filter(function(_part) {
				return _part.id === chosenPartID;
			});
		if(!part.length) return false;
		part = part[0];

		tr = $('<tr/>');
		tr.addClass('part-row');
		tr.data('part', part);
		tr.append($('<td/>'));
		tr.append($('<td/>').addClass('count').text(count++));
		tr.append($('<td/>').text(part.name));
		options = '';
		for(var option in part.options) {
			option = part.options[option];
			options += '<label class="option-label"><input data-option-name="'+option.name+'" data-option-price="'+option.priceFactor+'" type="checkbox" class="option-chooser" ' + (option['selected'] ? 'checked' : '') + '>' + option.name + ' ($ ' + option.priceFactor + ')</label>';
		}
		tr.append($('<td/>').html(options)); // options
		tr.append($('<td/>').addClass('unit-price').text('$ ' + parseInt(part.price, 10).toFixed(1)));
		tr.append($('<td/>').html('<input type="number" class="qty-chooser" min="1" value="' + part.expected_qty + '">'));
		tr.append($('<td/>').addClass('row-total').text('$ ' + (parseInt(part.expected_qty, 10) * parseInt(part.price, 10)).toFixed(1)));
		tr.append($('<td/>').html('<a class="delete" data-part-id="'+part.id+'"><i class="glyphicon glyphicon-trash"></i></a>')); // options
		tr.insertBefore('.parts tr.noprint');

		reTotalParts();
	});

	// print
	$('#btnPrint').click(function() {
		window.print();
	});

	// save
	$('#btnSave').click(savePartsList);

	// show the parts
	var tr, part, count = 1, options = '';
	for(var part in parts) {
		part = parts[part];
		tr = $('<tr/>');
		tr.addClass('part-row');
		tr.data('part', part);
		tr.append($('<td/>'));
		tr.append($('<td/>').addClass('count').text(count++));
		tr.append($('<td/>').text(part.name));
		options = '';
		for(var option in part.options) {
			option = part.options[option];
			options += '<label class="option-label"><input data-option-name="'+option.name+'" data-option-price="'+option.priceFactor+'" type="checkbox" class="option-chooser" ' + (option['selected'] ? 'checked' : '') + '>' + option.name + ' ($ ' + option.priceFactor + ')</label>';
		}
		tr.append($('<td/>').html(options)); // options
		tr.append($('<td/>').addClass('unit-price').text('$ ' + parseInt(part.price, 10).toFixed(1)));
		tr.append($('<td/>').html('<input type="number" class="qty-chooser" min="1" value="' + part.expected_qty + '">'));
		tr.append($('<td/>').addClass('row-total').text('$ ' + (parseInt(part.expected_qty, 10) * parseInt(part.price, 10)).toFixed(1)));
		tr.append($('<td/>').html('<a class="delete" data-part-id="'+part.id+'"><i class="glyphicon glyphicon-trash"></i></a>')); // options
		tr.insertBefore('.parts tr.noprint');
	}

	reTotalParts();
});

function reTotalParts() {

	var total = 0, count = 1;

	$('tr.part-row').each(function() {

		// update count
		$(this).find('.count').text(count++);

		var part = $(this).data('part');

		// look at options selected and set the unit-price & row-total
		var unitPrice = parseFloat(part.price), option;
		$(this).find('.option-label').removeClass('selected');
		for(option in part.options) {
			delete part.options[option]['selected'];
		}

		$(this).find('.option-chooser:checked').each(function() {
			unitPrice += parseInt($(this).data('option-price'), 10);
			$(this).closest('.option-label').addClass('selected');

			// update option selection data in tr.part-row's data()
			for(option in part.options) {
				option = part.options[option];
				if(option.name == $(this).data('option-name') && option.priceFactor == $(this).data('option-price')) {
					option.selected = 1;
					break;
				}
			}
		});

		// update unit price cell
		$(this).find('.unit-price').text('$ ' + unitPrice.toFixed(1));

		// update row total cell
		var qty = parseInt($(this).find('.qty-chooser').val(), 10);
		$(this).find('.row-total').text('$ ' + (qty * unitPrice).toFixed(1));

		total += qty * unitPrice;

		// update qty in tr.part-row
		part.expected_qty = qty;

	});

	$('.grand-total').text('$ ' + total.toFixed(1));
}

function savePartsList() {

	var parts = [];
	$('tr.part-row').each(function() {
		parts.push($(this).data('part'));
	});
	parts = JSON.stringify(parts);

	$.post('/parts-list/update', {id: $('#partslist_id').val(), parts: parts}, function(_data) {
		if(_data === 'OK') {
			var successDiv = $('<div class="well well-small success" id="notifications">Parts List Saved Successfully</div>').insertBefore('#printable');
			setTimeout(function() {
				successDiv.remove();
			}, 3000);
		}
	});

	return false;
}