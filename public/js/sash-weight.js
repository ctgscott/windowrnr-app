$(window).load(function() {

	$('#sash_form').submit(function() {

		if(!$('[name=material]:checked').length || !$('[name=thickness]:checked').length) {
			alert('Please specify all fields');
			return false;
		}
		
		var args = [
			$('#width').val(), 
			$('#height').val(), 
			$('#glass').val(), 
			$('[name=material]:checked').first()[0].value, 
			$('[name=thickness]:checked').first()[0].value
		];

		// validate
		for(var i=0; i<args.length; i++) {
			if($.trim(args[i]) === '') {
				alert('Please specify all fields');
				return false;
			}
		}

		args = args.map(function(_value) {
			return encodeURIComponent(_value);
		}).join('/');

		$.get('/api/sashWeight/' + args, function(_data) {
			var output = [];
			for(var key in _data.data) {
				if(_data.data.hasOwnProperty(key)) {
					output.push('<div><span class="key">' + toReadableKey(key) + '</span><span class="value">' + _data.data[key] + '</span></div>');
					console.log(key);
				}
			}
			$('#results').empty().html(output.join('')).css({display: 'inline-block'}).append('<span class="close-results"><i class="glyphicon glyphicon-remove"></i></span>');
		});

		return false;
	});

	$('#btnReset').click(function() {
		$('#results').empty().hide();
	});

	$('#results').on('click', '.close-results', function() {
		$('#results').empty().hide();
		return false;
	});
});

function toReadableKey(_key) {
	return _key.split('_').map(function(_x) {
		return _x.substr(0, 1).toUpperCase() + _x.substr(1);
	}).join(' ');
}