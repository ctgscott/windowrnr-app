$("#btnCopyCancel").click(function () {
	$("#jobList").collapse('hide');
});

$(document).ready(function(){
	$(function() {
		var customers = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: '/customers/typeahead/%QUERY'
		});
		customers.initialize();
		
		$('.typeahead').typeahead(null, {
			name: 'test',
			displayKey: 'value',
			source: customers.ttAdapter(),
			templates: {
				suggestion: Handlebars.compile([
					'<p class="tt-name">{{l_name}}, {{f_name}}',
					'<span class="tt-custID">Cust. ID: {{id}}</span></p>',
					'<p class="tt-address">{{address}}, {{city}}</p>'
				].join(''))
			},
		});
		
		$('#l_name').bind('typeahead:selected', function(obj, datum) {        
			$.ajax({
				type: "POST",
				url: "/customers/jobDetail",
				data: datum,
				success: function(data) {
					$('[name="l_name"]').val(datum.l_name);
					$('[name="f_name"]').val(datum.f_name);
					$('[name="phone"]').val(datum.phone);
					$('[name="email"]').val(datum.email);
/*					document.getElementByName("l_name").value = datum.l_name;
					document.getElementByName("f_name").value = datum.f_name;
					document.getElementByName("phone").value = datum.phone;
					document.getElementByName("email").value = datum.email;
*/					
					var i=0;
					var n=Object.keys(data).length;
						
					while (i<n)
					{
						//alert(data[i].toSource());
						
						var jobs_table = document.getElementById("jobs_table");
						$("tr", jobs_table).remove();
						var tr = document.createElement("tr");
						jobs_table.appendChild(tr);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);						
						var radio = document.createElement("input");
						radio.type = "radio";
						radio.name = "job_radios";
						radio.id = i;
						radio.val = i;
						td.appendChild(radio);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = data[i].id;
						input.id = "jobListID";
						input.className = "input-mini";
						input.disabled = true;
						td.appendChild(input);
						
						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = data[i].address;
						input.className = "address";
						input.disabled = true;
						td.appendChild(input);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = data[i].city;
						input.className = "city";
						input.disabled = true;
						td.appendChild(input);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = moment(data[i].lead_scheduled).format("MM/DD/YYYY, hh:mm a");
						input.className = "timestamp";
						input.disabled = true;
						td.appendChild(input);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = moment(data[i].job_scheduled).format("MM/DD/YYYY, hh:mm a");
						input.className = "timestamp";
						input.disabled = true;
						td.appendChild(input);

						var td = document.createElement("td");
						td.className = "prior_jobs_last";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = moment(data[i].job_completed).format("MM/DD/YYYY, hh:mm a");
						input.className = "timestamp";
						input.disabled = true;
						td.appendChild(input);

						i++;
					}
					
					$("#jobList").collapse('show');
					
					document.getElementById("copyButton").onclick=function(){
						var radioID = $('input[name=job_radios]:checked').attr('id')
						var radioVal = $('input[name=job_radios]:checked').val() 
						//alert(radioID)
						
						$('[name="address"]').val(data[radioID].address);
						$('[name="city"]').val(data[radioID].city);
						$('[name="zip"]').val(data[radioID].zip);
						$('[name="built"]').val(data[radioID].built);
//						document.getElementByName("address").value = data[radioID].address;
/*						document.getElementByName("city").value = data[radioID].city;
						document.getElementByName("zip").value = data[radioID].zip;
						document.getElementByName("built").value = data[radioID].built;
*/						
						$("#jobList").collapse('hide')
					};
				}
			});
		});
	});
});
