function returnID(elem)
{
	var elementID = $(elem).attr("id");
	console.log("elementID = "+elementID);
	
	// Break out the # from the element's ID
	// ie. expandBtn4 = 4
	var split = elementID.split(/(\d+)/);
	return split[1];
}

$(document).ready(function(){
	$("#mapFile").click(function(){
		$.get("admin/refreshMap", function(data,status){
			alert("Data: " + data + "\nStatus: " + status);
		});	
	});

	$("#users").click(function(){
		window.location.href = "/users";	
	});

	$("#groups").click(function(){
		window.location.href = "/groups";	
	});
	
	$("[id^='btnCloseRoom']").click(function() {
		// Get Item ID#
		var itemNum = returnID(this);
		
		// Set value of input field to ''
		$("#roomid"+itemNum).val('');
		
		// create empty items array
		var items = [];

		// Gather up all room inputs for POST
		$('div[id^=divRoom]').each(function( index ) {
			var id = returnID(this);
			var item = {
				id: id,
				room: $('#roomid'+id).val()
			}
			items.push(item);
			console.log( items );
		});
	
		$.ajax({
			type: "POST",
			url: "/estimate/test/roomSave",
			data: { 
				items: items
			}
		});
		
		// hide the div that was just cleared
		$("#divRoom"+itemNum).hide();
	});
	
	
	$("[id^='btnUpdateRoom']").click(function() {	
		
		// create empty items array
		var items = [];

		// Gather up all room inputs for POST
		$('div[id^=divRoom]').each(function( index ) {
			var id = returnID(this);
			var item = {
				id: id,
				room: $('#roomid'+id).val()
			}
			items.push(item);
			console.log( items );
		});
	
		$.ajax({
			type: "POST",
			url: "/estimate/test/roomSave",
			data: { 
				items: items
			}
		});
		
		
	});
	
	
	$("[id^='btnCloseWinStyle']").click(function() {
		// Get Item ID#
		var itemNum = returnID(this);
		
		// Set value of input field to ''
		$("#winstyleid"+itemNum).val('');
		
		// create empty items array
		var items = [];

		// Gather up all room inputs for POST
		$('div[id^=divWinStyle]').each(function( index ) {
			var id = returnID(this);
			var item = {
				id: id,
				winstyle: $('#winstyleid'+id).val()
			}
			items.push(item);
			console.log( items );
		});
	
		$.ajax({
			type: "POST",
			url: "/estimate/test/winstyleSave",
			data: { 
				items: items
			}
		});
		
		// hide the div that was just cleared
		$("#divWinStyle"+itemNum).hide();
	});
	
	
	$("[id^='btnUpdateWinStyle']").click(function() {	
		
		// create empty items array
		var items = [];

		// Gather up all room inputs for POST
		$('div[id^=divWinStyle]').each(function( index ) {
			var id = returnID(this);
			var item = {
				id: id,
				winstyle: $('#winstyleid'+id).val()
			}
			items.push(item);
			console.log( items );
		});
	
		$.ajax({
			type: "POST",
			url: "/estimate/test/winstyleSave",
			data: { 
				items: items
			}
		});
		
		
	});
	
	var winservlist = "";
	
	/* 
	* List window services in table. Fired 'onchange' event of 'Window Style' dropdown in 'Window Service' group.
	*/	
	$("[id^='ddwinService']").change(function() {
		var itemNum = this.value;
		 $("#winservicestyleadd").val(itemNum);
		$.ajax({
			type: "POST",
			url: "/estimate/test/adminListWinService/"+itemNum,
			error: function(e){
				$("#my-containing-data").html('<thead><th style="width:29%;">Title</th><th style="width:56%;">Description</th><th style="width:90px;">Price</th><th>&nbsp;Delete</th></thead><tbody class=\"existing\">');
				$("[id='divwinserviceadd']").hide();
			},
			success: function(data){
				//$("#my-containing-data").html('<thead><th>Title</th><th>Description</th><th>Price</th><th>Delete</th></thead><tbody class=\"existing\">');
				if(data['winservice'][0])
				{
					// Display data in tabular form.
					$("#my-containing-data").html('<thead><th style="width:29%;">Title</th><th style="width:56%;">Description</th><th style="width:90px;">Price</th><th>&nbsp;Delete</th></thead><tbody class=\"existing\">');
					$.each(data['winservice'], function(index){
							var tr = $("<tr id=\"optrw"+data['winservice'][index]['id']+ "\">");
														
							//tr.append("<td>"+ data['winservice'][index]['id'] +"</td>");
							tr.append("<td><input class=\"form-control\" id=\"winservicetitleid"+data['winservice'][index]['id']+"\" style=\"height: 25px; border:1px solid #428bca; width:250px; padding:4px;  \" name=\"winservicetitle_"+data['winservice'][index]['id']+"\" type=\"text\" value=\""+ data['winservice'][index]['title'] +"\"</td>");
							tr.append("<td><input class=\"form-control\" id=\"winservicedescid"+data['winservice'][index]['id']+"\" style=\"height: 25px; border:1px solid #428bca; width:500px; padding:4px;  \" name=\"winservicedesc_"+data['winservice'][index]['id']+"\" type=\"text\" value=\""+ data['winservice'][index]['description'] +"\"</td>");
							tr.append("<td><input class=\"form-control\" id=\"winservicepriceid"+data['winservice'][index]['id']+"\" style=\"height: 25px; border:1px solid #428bca; width:70px; padding:4px;  \" name=\"winserviceprice_"+data['winservice'][index]['id']+"\" type=\"text\" value=\""+ data['winservice'][index]['price'] +"\"</td>");
							tr.append("<td align=\"center\"><button id=\"btnDelwinservice"+data['winservice'][index]['id']+"\" class=\"no-border\" style=\"margin-left:10px;background-color:transparent;\" type=\"button\" onclick=\"winstyleservicesave(this)\"><i class=\"glyphicon glyphicon-remove\" style=\"color: red; margin-top: 5px\"></i></button></td>");
							tr.append("</tr>");
					
							$("#my-containing-data").append(tr);
							
							//$("#chkmultiserviceoption"+id).attr("checked","checked");
					});
							
					$("#my-containing-data").append("</tbody>");
					
					//$("[id='divserviceoptionsadd']").show();
					
					//$("#modalbody1").html(winservlist);
					
					//Button for 'Update'
					$("#divwinserviceupdatebtn").html("<button class=\"btn btn-primary\" id=\"btnUpdateWinService\" type=\"button\" onclick=\"winstyleserviceupdate(this)\">Update</button>");
				}
				else
				{
					$("#my-containing-data").html('<thead><th style="width:29%;">Title</th><th style="width:56%;">Description</th><th style="width:90px;">Price</th><th>&nbsp;Delete</th></thead><tbody class=\"existing\">');
					//$("#my-containing-data").html('No records found');
					$("#divwinserviceupdatebtn").html('');
					
				}							
			}
		});
		
		if(itemNum > 0)
		 {
		 	$("#btnAddWinService").removeAttr('disabled');
		 }
		 else
		 {
			 $("#btnAddWinService").attr('disabled','disabled');
		 }
		 
	});
	
	
	
	
	/*
	*  1. Create dropdown control dynamically for 'Service Options' in 'Service Options' group. Fired 'onchange' event of 'Window Style' dropdown in 'Service Options' group.
	*  2. 'divDDWinServiceOption' is the <div> where 2nd dropdownlist containing "Service Options" will be created dynamically when 'ddwinStyleServiceOption' change event is fired.
	*/
	$("[id='ddwinStyleServiceOption']").change(function() {
		var itemNum = this.value;
		 
		$.ajax({
			type: "POST",
			url: "/estimate/test/adminListWinService/"+itemNum,
			error: function(e){
				$("#dataDDwinStyleServiceOption").html('');
				$("#divserviceoptionsadd").hide('');
				//alert('error: ' + e.Message);
			},
			success: function(data){
				$("#divserviceoptionsadd").hide('');
				$("#dataDDwinStyleServiceOption").html('');
				if(data['winservice'][0])
				{
					$("#divDDWinServiceOption").html('');
					$("#dataDDwinStyleServiceOption").html('');
					//Create dropdown control for 'Window Service'
					var opt = 'Window Service: <select class="form-control winService" id="ddwinServiceOption1" name="ddwinServiceOption1" onchange="ddwinServiceOption1(this.value)"><option value="-1">Please select a window service</option>';
					$.each(data['winservice'], function(index){
						opt = opt + '<option value="'+data['winservice'][index]['id']+'">' + data['winservice'][index]['title'] + '</option>';
						winservlist += "<div><input type=\"checkbox\" class=\"chkmultiserviceopt\" name=\"chkmultiserviceoption[]\" id=\"chkmultiserviceoption" + data['winservice'][index]['id'] + "\" value=\"" + data['winservice'][index]['id'] + "\" />"+ data['winservice'][index]['title']+"&nbsp;</div>";
					});
					
					$("#divDDWinServiceOption").append(opt + '</select> ');
					$("#modalbody1").html('');
					$("#modalbody1").html(winservlist);
				}
				else
				{
					$("#divDDWinServiceOption").html('');
					$("#dataDDwinStyleServiceOption").html('');
				}
			}
		});
		
	});
	
	/*
	$("[id='btnwinserviceadd']").click(function() {
		
		$("[id='divwinserviceadd']").toggle();
	});
	
	
	$("[id='btnserviceoptionsadd']").click(function() {
		
		$("[id='divserviceoptionsadd']").toggle();
	});
	*/
	
});

	// This function is called onclick of RED 'X' button in 'Window Service' group
	function winstyleservicesave(el)
	{
	
		// Get Item ID#
		var itemNum = returnID(el);
		
		// Set value of input field to ''
		$("#winservicetitleid"+itemNum).val('');
		
		// create empty itemsoption array
		var itemsoption = [];

		// Gather up all room inputs for POST
		var id = returnID(el);
		var itemoption = {
			id: id,
			winservicetitle: '',
			winservicedesc: $('#winservicedescid'+id).val(),
			winserviceprice: $('#winservicepriceid'+id).val()
			
		}
		itemsoption.push(itemoption);
		console.log( itemsoption );

	
		$.ajax({
			type: "POST",
			url: "/estimate/test/winstyleserviceSave",
			data: { 
				itemsoption: itemsoption
			},
			error: function(e){
				//alert('error');
			},
			success: function(data){
				//alert(data);
			}
		});
		
		// hide the div that was just cleared
		$("#winservicetitleid"+itemNum).hide();
		
		//hide row.
		$("#optrw"+itemNum).hide();
	
	}
	
	// This function is called onclick of BLUE 'Update' button in 'Window Service' group
	function winstyleserviceupdate(el)
	{	
		// create empty itemsoption array
		var itemsoption = [];

		// Gather up all room inputs for POST
		$('[id^=winservicepriceid]').each(function( index ) {
			var id = returnID(this);
			var itemoption = {
				id: id,
				winservicetitle: $('#winservicetitleid'+id).val(),
				winservicedesc: $('#winservicedescid'+id).val(),
				winserviceprice: $('#winservicepriceid'+id).val()
				
			}
			itemsoption.push(itemoption);
			console.log( itemsoption );

		});
			
		$.ajax({
			type: "POST",
			url: "/estimate/test/winstyleserviceSave",
			data: { 
				itemsoption: itemsoption
			},
			error: function(e){
				//alert('error');
			},
			success: function(data){
				//alert(data);
			}
		});
	}
	
	
function updatemultiservice(styleid, servid, optionid, ischk)
{
	//alert(styleid+','+servid+','+optionid+','+ischk);	
	$('#loading').show();
	$.ajax({
		  type: "POST",
		  async: false,
		  url: "/estimate/test/adminUpdateMultiWinService/"+styleid+"/"+servid+"/"+optionid+"/"+ischk,
		  error: function(e){
			  alert('e');
		  //serop += "<div><input type=\"checkbox\" name=\"multiserviceoption"+data['winserviceoptions'][index]['id']+ index2+"\" id=\"multiserviceoption"+data['winserviceoptions'][index]['id']+ index2+"\" />"+ data['winserviceoptions'][index2]['title'] +"&nbsp;</div>";
		  },
		  
		  success: function(data){
			  
		  }
		  
	});
	
}

function getms(servid)
{
	$('#loading').show();
	
	//alert(servid);
	//alert($("[id^='ddwinStyleServiceOption']").val());
	//$.each(data['winserviceoptions'], function(index2){
		var a1 = $("[id^='ddwinStyleServiceOption']").val();
		var serop = "";				
	$.ajax({
		  type: "POST",
		  async: false,
		  url: "/estimate/test/adminListMultiWinService/"+a1,
//		  url: "/estimate/test/adminListWinService/"+a1,
		  error: function(e){
			  alert(e);
		  //serop += "<div><input type=\"checkbox\" name=\"multiserviceoption"+data['winserviceoptions'][index]['id']+ index2+"\" id=\"multiserviceoption"+data['winserviceoptions'][index]['id']+ index2+"\" />"+ data['winserviceoptions'][index2]['title'] +"&nbsp;</div>";
		  },
		  
		  success: function(data){
			  //alert(data['winserviceoptions'][0]);
			  $.each(data['winserviceoptions'], function(index){
				  $.ajax({
						  type: "POST",
						  async: false,
						  url: "/estimate/test/adminCheckMultiWinService/"+servid+"/"+data['winserviceoptions'][index]['id'],
						  error: function(e){
							  //alert('e');
						  //serop += "<div><input type=\"checkbox\" name=\"multiserviceoption"+data['winserviceoptions'][index]['id']+ index2+"\" id=\"multiserviceoption"+data['winserviceoptions'][index]['id']+ index2+"\" />"+ data['winserviceoptions'][index2]['title'] +"&nbsp;</div>";
						  },
						  
						  success: function(data2){
							  
						  
						 // //alert(data2['winserviceoptions'][0]['title']);
						 // //alert("<div><input type=\"checkbox\" checked=\"checked\" name=\"multiserviceoption"+data2['winserviceoptions'][0]['id']+"\" id=\"multiserviceoption"+data2['winserviceoptions'][0]['id']+"\" />"+ data2['winserviceoptions'][0]['title'] +"&nbsp;</div>");
						  
						 // //alert(serop);
						  if(data2['winserviceoptions'][0])
						  {
							serop += "<div><input type=\"checkbox\" checked=\"checked\" onclick=\"updatemultiservice("+a1+","+ data['winserviceoptions'][index]['id']+","+ servid+",this.checked)\" name=\"multiserviceoption"+'_' + a1+ '_' + servid+ '_' + data['winserviceoptions'][index]['id']+ "\" id=\"multiserviceoption"+'_' + a1+ '_' + servid+ '_' + data['winserviceoptions'][index]['id']+ "\" />"+ data['winserviceoptions'][index]['title']+"&nbsp;</div>";
						  }
						  else
						  {
							 //serop = index;
							  serop += "<div><input type=\"checkbox\" onclick=\"updatemultiservice("+a1+","+ data['winserviceoptions'][index]['id']+","+ servid+",this.checked)\" name=\"multiserviceoption"+'_' + a1+ '_' + servid+ '_' + data['winserviceoptions'][index]['id']+"\" id=\"multiserviceoption"+'_' + a1+ '_' + servid+ '_' + data['winserviceoptions'][index]['id']+"\" />"+ data['winserviceoptions'][index]['title']+"&nbsp;</div>";
						  }
						 }
		  
				  });
		 });
		 // //window.tem1 = serop;
		 // //$("#temp1").append(serop);
		  }

		});

	////console.log(tem1);//alert(serop);
	////});
	//serop="";
	//console.log(serop);//alert(serop);
	return serop;
	//alert(serop);
	//alert(divid);
	//$(divid).append(serop);
	
}


	/* 
	* List service options in table. Fired 'onchange' event of 'Window Service' dropdown control in 'Service Options' group.
	*/
	function ddwinServiceOption1(id)
	{
		$('#loading').show();
		
		var serop = "";
		var tem1 = "";
		$.ajax({
			type: "POST",
			url: "/estimate/test/adminListWinServiceOptions/"+id,
			error: function(e){
				$("#dataDDwinStyleServiceOption").html('<thead><th style="width:29%;">Title</th><th style="width:48%;">Description</th><th style="width:90px;">Price</th><th>&nbsp;Delete</th></thead><tbody class=\"existing\">');
				$("[id='divwinserviceoptionsadd']").hide();
				$("#divserviceoptionsadd").show('');
			},
			success: function(data){
				$("#divserviceoptionsadd").show('');
				if(data['winserviceoptions'][0])
				{			
					$("#dataDDwinStyleServiceOption").html('<thead><th style="width:29%;">Title</th><th style="width:48%;">Description</th><th style="width:90px;">Price</th><th style="width:100px;">Services</th><th>&nbsp;Delete</th></thead><tbody class=\"existing\">');
					
					$.each(data['winserviceoptions'], function(index){
						var s1 = getms(data['winserviceoptions'][index]['id']);

							var tr = $("<tr id=\"optrow"+data['winserviceoptions'][index]['id']+"\">");
							
							//tr.append("<td>"+ data['winserviceoptions'][index]['id'] +"</td>");
							tr.append("<td><input class=\"form-control\" id=\"winserviceoptiontitleid"+data['winserviceoptions'][index]['id']+"\" style=\"height: 25px; border:1px solid #428bca; width:250px; padding:4px;  \" name=\"winservicetitle_"+data['winserviceoptions'][index]['id']+"\" type=\"text\" value=\""+ data['winserviceoptions'][index]['title'] +"\"</td>");
							tr.append("<td><input class=\"form-control\" id=\"winserviceoptiondescid"+data['winserviceoptions'][index]['id']+"\" style=\"height: 25px; border:1px solid #428bca; width:450px; padding:4px;  \" name=\"winservicedesc_"+data['winserviceoptions'][index]['id']+"\" type=\"text\" value=\""+ data['winserviceoptions'][index]['description'] +"\"</td>");
							tr.append("<td><input class=\"form-control\" id=\"winserviceoptionpriceid"+data['winserviceoptions'][index]['id']+"\" style=\"height: 25px; border:1px solid #428bca; width:70px; padding:4px;  \" name=\"winserviceprice_"+data['winserviceoptions'][index]['id']+"\" type=\"text\" value=\""+ data['winserviceoptions'][index]['price'] +"\"</td>");
							tr.append("<td>&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-link btn-sm\" data-toggle=\"modal\" data-target=\"#myModal"+data['winserviceoptions'][index]['id']+"\">View</button></td>");
							tr.append("<td align=\"center\"><button id=\"btnDelwinserviceoption"+data['winserviceoptions'][index]['id']+"\" class=\"no-border\" style=\"margin-left:10px; background-color:transparent;\" type=\"button\" onclick=\"winserviceoptionsave(this)\"><i class=\"glyphicon glyphicon-remove\" style=\"color: red; margin-top: 5px\"></i></button>");
							tr.append("<div class=\"modal fade\" id=\"myModal"+data['winserviceoptions'][index]['id']+"\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">  <div class=\"modal-dialog\">    <div class=\"modal-content\">      <div class=\"modal-header\">        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>        <h4 class=\"modal-title\" id=\"myModalLabel\">Select Services</h4>      </div>      <div class=\"modal-body\">"+ s1 +"<br /> </div><div class=\"modal-footer\"><button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button></div>    </div>  </div></div></td>");
							tr.append("</tr>");
					
							$("#dataDDwinStyleServiceOption").append(tr);
							//serop = "";
							//serop = checkmultiserviceoption(data);
							//alert($("#temp1").html());
							
							//$("#chkmultiserviceoption"+id).attr("checked","");
							
						});
							
					$("#dataDDwinStyleServiceOption").append("</tbody>");
					
					$("#divwinserviceoptionupdatebtn").html("<button class=\"btn btn-primary\" id=\"btnUpdateServiceOptions\" type=\"button\" onclick=\"winserviceoptionsupdate(this)\">Update</button>");
					
					$('#loading').hide(); 
				}
				else
				{
					$("#dataDDwinStyleServiceOption").html('<thead><th style="width:29%;">Title</th><th style="width:48%;">Description</th><th style="width:90px;">Price</th><th style="width:100px;">Services</th><th>&nbsp;Delete</th></thead><tbody class=\"existing\">');
					//$("#my-containing-data").html('No records found');
					$("#divwinserviceoptionupdatebtn").html('');
				}
										
			}
		});
		
		$("#winserviceoptionstyleadd").val(id);
		//$("#modalbody1").html('');
		//$("#modalbody1").html(winservlist);
		$(".chkmultiserviceopt").removeAttr("checked");
		$("#chkmultiserviceoption"+id).attr("checked","checked");
		 
		 if(id > 0)
		 {
		 	$("#btnAddWinServiceOption").removeAttr('disabled');
		 }
		 else
		 {
			 $("#btnAddWinServiceOption").attr('disabled','disabled');
		 }
		
	}
	
	
	// This function is called onclick of RED 'X' button in 'Service Options' group
	function winserviceoptionsave(el)
	{
		// Get Item ID#
		var itemNum = returnID(el);
		
		// Set value of input field to ''
		$("#winserviceoptiontitleid"+itemNum).val('');
		
		// create empty itemsoption array
		var itemsoption = [];

		// Gather up all room inputs for POST
		var id = returnID(el);
		var itemoption = {
			id: id,
			winservicetitle: '',
			winservicedesc: $('#winserviceoptiondescid'+id).val(),
			winserviceprice: $('#winserviceoptionpriceid'+id).val()
			
		}
		itemsoption.push(itemoption);
		console.log( itemsoption );

	
		$.ajax({
			type: "POST",
			url: "/estimate/test/winserviceoptionsSave",
			data: { 
				itemsoption: itemsoption
			},
			error: function(e){
				//alert('error');
			},
			success: function(data){
				//alert(data);
			}
		});
		
		// hide the div that was just cleared
		
		$("#winserviceoptiontitleid"+itemNum).hide();
		$("#optrow"+itemNum).hide();
	
	}
	
	
	// This function is called onclick of BLUE 'Update' button in 'Service Options' group
	function winserviceoptionsupdate(el)
	{
		// create empty itemsoption array
		var itemsoption = [];

		// Gather up all room inputs for POST
		$('[id^=winserviceoptionpriceid]').each(function( index ) {
			var id = returnID(this);
			var itemoption = {
				id: id,
				winservicetitle: $('#winserviceoptiontitleid'+id).val(),
				winservicedesc: $('#winserviceoptiondescid'+id).val(),
				winserviceprice: $('#winserviceoptionpriceid'+id).val()
			
		}
		itemsoption.push(itemoption);
		console.log( itemsoption );

		});
			
		$.ajax({
			type: "POST",
			url: "/estimate/test/winserviceoptionsSave",
			data: { 
				itemsoption: itemsoption
			},
			error: function(e){
				//alert('error');
			},
			success: function(data){
				//alert(data);
			}
		});
	}