$(document).ready(function(){
	$("button[id^='contactBtn']").mask("(999) 999-9999");
	$("button[id^='contactBtn']").css("background-color: #ccc;");

	$("#inputNewNum").focus();
});

function formatPhone(phone)
{
	var strPhone = phone.toString();
	phone = "("+strPhone.substring(0,3)+") "+strPhone.substring(3,6)+'-'+strPhone.substring(6,10);
	return phone;
}

function getThread(phone)
{
	$("#threadDiv").empty();
	$.get( "/getThread/"+phone, function(data) {

		var phone2 = phone.toString();
		$("#contactTitle").text('Message History - '+formatPhone(phone));
		$("#hiddenContact").val(phone);
	
		$("#threadDiv").empty();
		$.each( data, function( i, data ) {
			var dateTime = formatDateTime(data.created_at);
			if (data.from == phone) {
				$("#threadDiv").append("<div class='threadMsgDateTime dateLeft' id='timeStamp"+i+"'>"+dateTime+"</div>");
				$("#threadDiv").append("<div class='threadMsg left' id='threadMsg"+i+"'>"+data.body+"</div>");
			} else {
				$("#threadDiv").append("<div class='threadMsgDateTime dateRight right' id='timeStamp"+i+"'>"+dateTime+"</div>");
				$("#threadDiv").append("<div class='threadMsg right' id='threadMsg"+i+"'>"+data.body+"</div>");
			};
		});
	});
	$("#inputNewNum").prop('disabled', true);
	$("#btnNewNum").css('display', 'none');
	$("#inputNewNum").css('display', 'none');
	$("#contactTitle").css('display', 'inline-block');
	$("#inputMsg").prop('disabled', false);
	$("#btnSend").prop('disabled', false);
	$("#inputMsg").focus();
}

function startThread()
{
	var test = $("#inputNewNum").val();
	$("#inputMsg").prop('disabled', false);
	$("#btnSend").prop('disabled', false);
	$("#contactTitle").css('display', 'inline-block');
	$("#btnNewNum").prop('disabled', true);
	$("#inputNewNum").prop('disabled', true);
	$("#btnNewNum").css('display', 'none');
	$("#inputNewNum").css('display', 'none');
	
	$("#threadDiv").empty();
	refreshContacts($("#contactTitle").prop('name'));
	$("#inputMsg").val();
	$("#inputMsg").focus();
}

function refreshContacts(phone)
{
	var html = "<div  class='input-small form-control contact' style='margin-bottom: 15px;' id='contactNew'>";
	html += "<button type='button' id='contactBtnNew' class='btn btn-success btn-small' onclick='getThread("+phone+")' value=''>"+formatPhone(phone)+"</button>";
	html += "</div>";
	$("#contactHead").after(html);
}

function createThread()
{
	$("#inputMsg").prop('disabled', true);
	$("#btnSend").prop('disabled', true);
	$("#contactTitle").css('display', 'none');
	$("#btnNewNum").css('display', 'inline-block');
	$("#inputNewNum").css('display', 'inline-block');
	$("#inputNewNum").val("");
	$("#inputNewNum").prop('disabled', false);
	$("#threadDiv").empty();
	$("#inputNewNum").focus();
}

function enableThreadBtn()
{
	var val = $("#inputNewNum").val();
	numArray = val.match(/\d/g);
	num = numArray.join("");

	if (num.length == 10) {
		console.log(num.length);
		$("#btnNewNum").prop('disabled', false);
		$("#contactTitle").text('Text History: '+val);
		$("#contactTitle").prop('name', num);
		$("#btnNewNum").focus();
	} 
}

function formatDateTime(msgDateTime)
{
	var dateStamp = null;
	var today = moment();
	var yesterday = moment().subtract(1, 'd');
	var weekAgo = moment().subtract(7, 'd');

	if(moment(msgDateTime).isSame(today, 'd')) {
		dateStamp = moment(msgDateTime).format("h:mm a ");
	} else if(moment(msgDateTime).isSame(yesterday, 'd')) {
		dateStamp = "yesterday "+moment(msgDateTime).format("h:mm a ");
	} else if (moment(msgDateTime).isAfter(weekAgo, 'd')) {
		dateStamp = moment(msgDateTime).format("dddd, h:mm a ");
	} else if (moment(msgDateTime).isSameOrBefore(weekAgo.toDate(), 'd')) {
		dateStamp = moment(msgDateTime).format("M/D/YY, h:mm a ");
	}

	return dateStamp;
}

function sendMsg()
{
	var to = $("#hiddenContact").val();
	var from = "5624931590";
	var message = $("#inputMsg").val();
	
	$.post('/textOut', {to, from, message}, function(_data) {
//		if(_data == 'ok') {
			console.log(_data);
			getThread(to);
//		}
	});
	
	$("#inputMsg").val("");
	$("#inputMsg").focus();
}