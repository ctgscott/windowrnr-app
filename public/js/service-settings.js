$(document).ready(function() {

	$('.items .item[data-item-type]')
		.click(function() {

			$(this).addClass('selected').siblings().removeClass('selected');

			// hide all views and show please wait...
			$('.selected-item')
				.addClass('loading')
				.find('> .item-view')
					.hide();

			loadView($(this).data('item-type'));
		})
		.filter('[data-default]')
			.first()
			.trigger('click');

	$('.selected-item').on('click', '.btn-add-new[data-add-simple]', function() {
		var data = undefined,
			callback = $(this).data('callback-function');
		if($(this).data('additional-data-function')) {
			data = window[$(this).data('additional-data-function')]();
		}
		if(callback) callback = window[callback]
		var windowServiceID = 0;
		if($(this).data('item-type') === 'balancer-types') {
			windowServiceID = window.prompt('Services: \n' + $('#services').text() + '\nEnter a Service ID:');
			if(!windowServiceID) return;
			if(!data) data = {};
			data.service_id = windowServiceID;
		}
		simpleAddItem($(this).data('item-type'), $(this).data('type-name'), data, callback);
	});

});

function loadView(_type, _callback, _callbackData) {

	var view = $('.item-view[data-item-type=' + _type + ']');

	// load admin view for selected item
	$.get('/service-settings/view/' + _type, function(_data) {
		populateView(view, _data);
		if(typeof _callback === 'function') _callback(_callbackData);
	});

}

function simpleAddItem(_type, _name, _additionalData, _callback) {

	var newValue = window.prompt(_name + ':', '');
	if(!newValue) return false;

	var data = (_additionalData ? _additionalData : {});
	data['type'] = _type;
	data['value'] = newValue;

	// post the value to the server
	$.post('/service-settings/add', data, function(_data) {
		if(_data === 'OK') {
			loadView(_type, _callback, _additionalData);
		}
	});
}

function makeItemsEditable(_view) {
	$('[data-can-edit] span', _view)
		.each(function() {
			if($.trim($(this).text()) === '' || Number($.trim($(this).text())) === 0) {
				$(this).text('(empty)').addClass('empty');
			}
		})
		.click(function() {
			editItem($(this));
			return false;
		});
}

function makeItemsDeletable(_view) {
	var items = _view;
	if(_view.is('[data-can-delete]')) {
		items = _view;
	}
	else {
		items = $('[data-can-delete]', _view);
	}
	items.append(
			$('<button/>')
				.addClass('delete')
				.attr('title', 'Click to delete')
				.html('<i class="glyphicon glyphicon-remove"></i>'))
		.find('.delete')
			.click(function() {
				deleteItem($(this));
				return false;
			});
}

function editItem(_field) {

	if(_field.closest('[data-can-edit]').is('[data-custom-edit-function]')) {
		window[_field.closest('[data-can-edit]').data('custom-edit-function')].call(_field.closest('[data-can-edit]'));
	}
	else {
		
		var type = _field.closest('[data-can-edit]').data('item-type'),
			id = _field.closest('[data-can-edit]').data('id'),
			value = _field.is('.empty') ? '' : _field.text(),
			field = _field.closest('[data-can-edit]').data('edit-field')
		var newValue = window.prompt('Enter new value:', value);
		if(!newValue) return false;
		_field.text(newValue).removeClass('empty');

		// post the value to the server
		$.post('/service-settings/update', {type: type, id: id, value: newValue, field: (field ? field : 0)}, function(_data) {
			if(_data !== 'OK') {
				_field.text(value);
			}
		});
	}
}

function deleteItem(_field) {
		
	if(!window.confirm('Are you sure you want to delete this?')) return false;

	// post the value to the server
	var params = {type: _field.closest('[data-can-delete]').data('item-type')};
	if(_field.closest('[data-can-delete]').data('id')) params['id'] = _field.closest('[data-can-delete]').data('id');
	if(_field.closest('[data-can-delete]').data('service-id')) params['service_id'] = _field.closest('[data-can-delete]').data('service-id');
	if(_field.closest('[data-can-delete]').data('option-id')) params['option_id'] = _field.closest('[data-can-delete]').data('option-id');
	if(_field.closest('[data-can-delete]').data('part-id')) params['part_id'] = _field.closest('[data-can-delete]').data('part-id');
	$.post('/service-settings/delete', params, function(_data) {
		if(_data === 'OK') {
			_field.closest('[data-can-delete]').remove();
		}
	});
}

function populateView(_view, _data) {
	$('.selected-item').removeClass('loading').find('> .item-view').hide();
	_view
		.html(_data)
		.show()
		.find('input, select, textarea')
			.first()
			.focus();

	makeItemsEditable(_view);
	makeItemsDeletable(_view);
}

function _define(_name, _body) {
	if(typeof window[_name] !== 'function') {
		window[_name] = _body;
	}
	else {
		console.log(_name + '() is already defined');
	}
}