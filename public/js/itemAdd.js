function addItem(_after) {
	
	// create new item from markup in div.markupTemplate
	var html = $('#new_lineitem_markup').html(), newID = $('.clonedSection:not(.markupTemplate)').length + 1;
	html = html.replace(/NEWITEMID/g, newID);
	var item = $(html);
	if(_after) item.insertAfter(_after); else $('#parent').append(item);
	$('#parent .markupTemplate').removeClass('markupTemplate');
	$('#item_group_' + newID).find('input[type="text"], select').first().focus().select();
	
	return newID;

}

// Use custom entered service price and description when cloning
var servicePriceOrig = null, 
	serviceDescOrig = null,
	useExistingServiceOptions = false,
	doRenumbering = false;

function cloneItem(_from) {
	
	// add new item
	var newID = addItem(_from);

	// copy values
	$('input[id^="servOptions"], input[type="text"], input[type="radio"], select', '#item_group_' + newID).each(function(_index) {
		if($(this).is('select.winStyle')) {
			$(this).html(_from.find('select.winStyle').first().html());
			$(this).val(_from.find('select.winStyle').first().val());
		}
		else if($(this).is('select.service')) {
			$(this).html(_from.find('select.service').first().html());
			$(this).val(_from.find('select.service').first().val());
		}
		else if($(this).is('[id^="servOptions"]')) {
			$(this).val(_from.find('.serv_options_value').first().val());
		}
		else if($(this).is(':not(.itemNum)')) {
			var source = _from.find(this.tagName + '.' + this.className.split(/\s+/).join('.')).first();
			$(this).val(source.val());
		}
	});

	// copy parts column and json
	$('.loaded-parts', '#item_group_' + newID).val(_from.find('.loaded-parts').first().val());
	$('.parts_column', '#item_group_' + newID).html(_from.find('.parts_column').first().html());

	// enable service dropdown if it has items
	if($('#item_group_' + newID + ' select.service option').length > 0) {
		$('#item_group_' + newID + ' select.service').prop('disabled', false);
	}

	// setup balancer-type dropdown for selected winStyle
	var balancerSelect = $('#balancer' + newID).html('<option value="0">N/A</option>').prop('disabled', true);
	if(winStylesWithBalancer.indexOf(Number(_from.find('select.winStyle').first().val())) !== -1) {
		balancerSelect.append(balancers.map(function(_x) {
			return '<option value="' + _x.id + '">' + _x.name + '</option>';
		}).join('')).prop('disabled', false);
	}

	// copy balancer chosen from source
	balancerSelect.val(_from.find('select.balancer').val());

	// simulate retotal by triggering change on service
	servicePriceOrig = _from.find('.servPrice').first().val();
	serviceDescOrig = _from.find('.description').first().val();
	useExistingServiceOptions = true;
	doRenumbering = true;

	rootEvent = 'clone';
	serviceChange($('#item_group_' + newID + ' select.service')[0]);

}