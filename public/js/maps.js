var markersArray = [];
var mapOptions = {disableDefaultUI: true};
//var map1, map2, map3, map4, map5;
var map = {};
//var bounds1, bounds2, bounds3, bounds4, bounds5;
var bounds = {};
var defaultLat = 32.0908529;//34.0671537;
var defaultLng = -118.2084854;//-135.1379035;//-111.8908332;
var infoWindows = [];

function mapAdd(mapNum, latLng, mapOptions) {
//	var bounds = new google.maps.LatLngBounds();
//	var bounds = {};
//	bounds[mapNum] = window['bounds'+mapNum];
//	var map = window['map'+mapNum];
	var marker = new google.maps.Marker({
		position: latLng,
		icon: '/img/WinPin3.png',
		map: map[mapNum],
		animation: google.maps.Animation.DROP,
		title: 'Proposed Appointment',
	});
	bounds[mapNum].extend(latLng);
	map[mapNum].fitBounds(bounds[mapNum]);
	map[mapNum].setCenter(bounds[mapNum].getCenter());
	return mapNum;
}

function initMapWithEvents(_mapNum, _events, _options, _idleZoom) {
	
	map[_mapNum] = new google.maps.Map(document.getElementById('map_' + _mapNum), _options);
	bounds[_mapNum] = new google.maps.LatLngBounds();

	_idleZoom = _idleZoom || 9;
	var listener = google.maps.event.addListener(map[_mapNum], "idle", function() { 
		if (map[_mapNum].getZoom() > _idleZoom) map[_mapNum].setZoom(_idleZoom); 
		google.maps.event.removeListener(listener); 
	});

	var myLatLng = new google.maps.LatLng(defaultLat, defaultLng);

	if(_events && _events.length) {

		$.each( _events, function(n, event) {
			
			if (event.lat == null || event.lng == null) return true;
			
			myLatLng = new google.maps.LatLng(event.lat, event.lng);

			bounds[_mapNum].extend(myLatLng);
			
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map[_mapNum],
				icon: '/img/' + event.avatar,
				animation: google.maps.Animation.DROP,
				title: event.title,
			});

			var infoWindow = new google.maps.InfoWindow({
				content: '<div style="padding: 10px 0">' + event.title + '</div>'
			});
			
			google.maps.event.addListener(marker, 'click', function() {
				// close all currently opwn infowindows
				infoWindows.forEach(function(_item) {
					_item.close();
				});
				infoWindow.open(map[_mapNum], marker);
			});

			infoWindows.push(infoWindow);
	
		});

	}
	else {

		bounds[_mapNum].extend(myLatLng);

	}

	map[_mapNum].fitBounds(bounds[_mapNum]);
	map[_mapNum].setCenter(bounds[_mapNum].getCenter());
}

function mapDay(events, mapOptions) {
	console.log("mapDay fired with events = ", events);
	var mapDay = new google.maps.Map(document.getElementById('map_day'), mapOptions);
	
	var boundsDay = new google.maps.LatLngBounds();
	
	if(events == "")
	{	
		var myLatLng = new google.maps.LatLng(defaultLat, defaultLng);
		
		boundsDay.extend(myLatLng);
		mapDay.fitBounds(boundsDay);
		mapDay.setCenter(boundsDay.getCenter());
		
		var listener = google.maps.event.addListener(mapDay, "idle", function() { 
			if (mapDay.getZoom() > 5) mapDay.setZoom(5); 
			google.maps.event.removeListener(listener); 
		});
	}
	else
	{
		$.each( events, function(n, event) {
			if ((event.lat == null || event.lng == null) || (event.lat == null && event.lng == null)){
				return true;
			}

			var assignee = '- NONE -';
			if($('.assign').length && event.assignee) {
				assignee = event.assignee.split('|').join(', ');
			}

			var myLatLng = new google.maps.LatLng(event.lat, event.lng);
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: mapDay,
				icon: '/img/'+event.avatar,
				animation: google.maps.Animation.DROP,
				title: event.title + ($('.assign').length ? "\n" + 'Assigned To: ' + assignee : ''),
			});
			
			boundsDay.extend(myLatLng);
			mapDay.fitBounds(boundsDay);
			mapDay.setCenter(boundsDay.getCenter());
			
			var infoWindow = new google.maps.InfoWindow({
				content: event.title
			});
			
			google.maps.event.addListener(marker, 'click', function() {
				infoWindow.open(mapDay,marker);
			});

			if($('.assign').length) {
				google.maps.event.addListener(marker, 'mouseover', function() {
					$('a[href="'+event.url+'"]').css({'box-shadow': '0 0 20px orange'});
				});
				google.maps.event.addListener(marker, 'mouseout', function() {
					$('a[href="'+event.url+'"]').css({'box-shadow': 'none'});
				});
			}
		});
	}
};

function removeMarkers() {
	$.each( markersArray, function(n) {
		this.setMap(null);
	});	
	console.log("markersArray (after) = ", markersArray);
};


	
