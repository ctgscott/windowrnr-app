function returnID(elem)
{
	// Identify the element from the event
	var elementID = $(elem).attr("id");
	
	// Break out the # from the element's ID
	// ie. expandBtn4 = 4
	var split = elementID.split(/(\d+)/);
	
	return split[1];
}

$(document).ready(function(){
	$("[id^='btnArchive']").click(function () {
		var itemNum = returnID(this);
		$('#modalJobID').val(itemNum);
		$('#archiveNote').modal('toggle');
	});

	$("[id^='btnUnarchive']").click(function () {
		$.post('/job/unarchive', {id: returnID(this)}, function() {
			window.location.reload();
		})
	});

	$("#btnBillDetail")
		.append('<i class="left10 glyphicon glyphicon-chevron-down"></i>')
		.click(function () {
			$('#divBillDetail').slideToggle('fast', function() {
				if($(this).is(':visible')) {
					$("#btnBillDetail i").removeClass().addClass('left10 glyphicon glyphicon-chevron-up');
				}
				else {
					$("#btnBillDetail i").removeClass().addClass('left10 glyphicon glyphicon-chevron-down');
				}
			});
		});
	
	$("#btnSchedule").click(function(){
		window.location.href = '/customers/schedule/' + document.getElementById("jobID").value;
	});
	
	$("#alert").collapse('show');

	$('#saveNewLead').click(function(){
		$("#alert").collapse('hide')
	});

	$('#scheduleBtn').click(function(){
		$("#alert").collapse('hide')
	});
	$("#completeBtn").click(function(){
		window.location.href = '/customers/complete/' + $(this).data('id');
	});

	$('#scheduleNewLead').click(function(){
		$("#alert").collapse('hide')
	});
		
    $('#typeCheckbox6').click(function(){
		if (this.checked) {
			$('#type_other').removeAttr("disabled");
		}
		else {
			$('#type_other').attr("disabled", true);
		}
    });

    $('#sourceCheckbox5').click(function(){
		if (this.checked) {
			$('#source_referral').removeAttr("disabled");
		}
		else {
			$('#source_referral').attr("disabled", true);
		}
    });

    $('#sourceCheckbox6').click(function(){
		if (this.checked) {
			$('#source_other').removeAttr("disabled");
		}
		else {
			$('#source_other').attr("disabled", true);
		}
    });

    $('#radio_lead_other').click(function(){
		if (this.checked) {
			$('#text_lead_other').removeAttr("disabled");
		}
		else {
			$('#text_lead_other').attr("disabled", true);
		}
    });
	
    $('#radio_lead_other').click(function(){
		if (this.checked) {
			$('#text_lead_other').removeAttr("disabled");
		}
		else {
			$('#text_lead_other').attr("disabled", true);
		}
    });

    $('#radio_lead_main1').click(function(){
		if (this.checked) {
			$('#text_lead_other').attr("disabled", true);
		}
    });

    $('#radio_lead_main2').click(function(){
		if (this.checked) {
			$('#text_lead_other').attr("disabled", true);
		}
    });

    $('#radio_lead_main3').click(function(){
		if (this.checked) {
			$('#text_lead_other').attr("disabled", true);
		}
    });

    $('#radio_lead_main4').click(function(){
		if (this.checked) {
			$('#text_lead_other').attr("disabled", true);
		}
    });

    $('#schedButton').click(function(){
		$("#tab_1").removeClass('active')
		$("#tab_2").addClass('active')		
	});
	
	$(function() {
		
		var customers = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: '/customers/typeahead/%QUERY'
		});
		customers.initialize();
		
		$('.typeahead').typeahead(null, {
			name: 'test',
			displayKey: 'value',
			source: customers.ttAdapter(),
			templates: {
				suggestion: Handlebars.compile([
					'<p class="tt-name">{{l_name}}, {{f_name}}',
					'<span class="tt-custID">Cust. ID: {{id}}</span></p>',
					'<p class="tt-address">{{address}}, {{city}}</p>'
				].join(''))
			},
		});
		
		$('#l_name').bind('typeahead:selected', function(obj, datum) {        
			$.ajax({
				type: "POST",
				url: "/customers/jobDetail",
				data: datum,
				success: function(data) {
					document.getElementById("l_name").value = datum.l_name;
					document.getElementById("f_name").value = datum.f_name;
					document.getElementById("phone").value = datum.phone;
					document.getElementById("email").value = datum.email;
					
					var i=0;
					var n=Object.keys(data).length;
						
					while (i<n)
					{
						//alert(data[i].toSource());
						
						var jobs_table = document.getElementById("jobs_table");
						$("tr", jobs_table).remove();
						var tr = document.createElement("tr");
						jobs_table.appendChild(tr);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);						
						var radio = document.createElement("input");
						radio.type = "radio";
						radio.name = "job_radios";
						radio.id = i;
						radio.val = i;
						td.appendChild(radio);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = data[i].id;
						input.id = "jobListID";
						input.className = "input-mini";
						input.disabled = true;
						td.appendChild(input);
						
						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = data[i].address;
						input.className = "address";
						input.disabled = true;
						td.appendChild(input);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = data[i].city;
						input.className = "city";
						input.disabled = true;
						td.appendChild(input);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = moment(data[i].lead_scheduled).format("MM/DD/YYYY, hh:mm a");
						input.className = "timestamp";
						input.disabled = true;
						td.appendChild(input);

						var td = document.createElement("td");
						td.className = "prior_jobs";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = moment(data[i].job_scheduled).format("MM/DD/YYYY, hh:mm a");
						input.className = "timestamp";
						input.disabled = true;
						td.appendChild(input);

						var td = document.createElement("td");
						td.className = "prior_jobs_last";
						tr.appendChild(td);
						var input = document.createElement("input");
						input.type = "text";
						input.value = moment(data[i].job_completed).format("MM/DD/YYYY, hh:mm a");
						input.className = "timestamp";
						input.disabled = true;
						td.appendChild(input);

						i++;
					}
					
					$("#jobList").collapse('show');
					
					document.getElementById("copyButton").onclick=function(){
						var radioID = $('input[name=job_radios]:checked').attr('id')
						var radioVal = $('input[name=job_radios]:checked').val() 
						//alert(radioID)
						
						document.getElementById("job_address").value = data[radioID].address;
						document.getElementById("job_city").value = data[radioID].city;
						document.getElementById("zip").value = data[radioID].zip;
						document.getElementById("built").value = data[radioID].built;
						
						$("#jobList").collapse('hide')
					};
				}
			});
		});
	});	
	
	/*
	
	// Note event changes will automatically reach us via push notifications

	$calendar = $.get("/profiles/getSalesCalID", function(data,status) {
		$now = $.now()/1000;
		$lastWeek = $now-604800;
		$nextMonth = $now+2592000;
		$.each( data, function(n, calID) {
			$googCal = calID.google_calendar_id;
			$.get("/events/updateEvents/"+$lastWeek+"/"+$nextMonth+"/"+$googCal, function(data,status) {
				// alert("Data: " + data + "\nStatus: " + status);
			});
		});
		$.get("/events/updateEventsTable", function(data,status) {
			// alert("Data: " + data + "\nStatus: " + status);
		});
	});
	
	*/

	$('#btnAddNewLead').click(function() {
		$(this).fadeOut('fast');
		$('#leads_outer').hide();
		$('#addCustomerForm').slideDown('fast').find('input[type="text"]').first().focus();
		return false;
	});

	$('#btnCloseNewLeadForm').click(function() {
		$('#btnAddNewLead').fadeIn('fast');
		$('#addCustomerForm').slideUp('fast');
		$('#leads_outer').show();
	});

	if(window.location.href.indexOf('add=1') !== -1) {
		$('#btnAddNewLead').trigger('click');
		$('#addCustomerForm').slideDown('fast').find('input[type="text"]').first().focus();
	}

	// Since the table cells can possibly be truncated with ellipsis, put their values as tooltip
	$('table.table-leads tbody tr td:not(:last-child)').attr('title', function() {
		return $(this).text();
	});

	// check for new enquiries in brand site
	setTimeout(function() {
		$.get('/lead-check', function(_data) {
			$('#notifications').empty().append(_data);
			$('#notifications #alert').slideDown(function() {
				setTimeout(function() {
					$('#notifications #alert').slideUp();
				}, 15000)
			});
		});
	}, 5000);

});
