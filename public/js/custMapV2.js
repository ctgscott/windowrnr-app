
var map, markers = [], infoWindow;

$(document).ready(function() {
	
	var statusColors = {
		'1': 'ffffff,ffffff,444444', // lead created - white
		'2': 'ddffdd,ddffdd,448844', // lead scheduled - light green
		'3': 'ddddff,ddddff,444488', // quote created - light blueish 
		'4': 'ffa500,ffa500,aa8500', // accepted/in-progress - orangish
		'5': 'aaeeaa,aaeeaa,449944', // complete - green
		'6': 'f4726d,f4726d,884444'  // standby - red
	};

	var standbyColors = [
		{max: 30, colors: 'f4726d,f4726d,884444'},
		{max: 90, colors: 'be4e4a,be4e4a,884444'},
		{max: 180, colors: '893734,893734,884444'},
		{max: 9999, colors: '56302f,56302f,330000'}
	];

	function initialize() {

		// create map
		var mapOptions = {
			zoom: 10,
			center: new google.maps.LatLng(33.77887246148013, -118.21752573875),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true
		};
		map = window.cgMap = new google.maps.Map(document.getElementById('custMap'), mapOptions);

		infoWindow = new google.maps.InfoWindow({content: '', maxWidth: 250});

		// add markers for each client
		for (var i = 0; i < data.jobs.length; ++i) {

			var colors = statusColors[data.jobs[i].job_status];

			// if standby, icon color based on age
			if(data.jobs[i].job_status == 6) {
				var jobTime = new Date(data.jobs[i].job_put_on_standby).getTime(), now = (new Date()).getTime();
				var days = Math.floor((now - jobTime) / 1000 / 3600 / 24);
				var colors = '';
				for(var j=0; j<standbyColors.length; j++) {
					if(standbyColors[j].max >= days) {
						colors = standbyColors[j].colors;
						break;
					}
				}
			}
			
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(data.jobs[i].lat, data.jobs[i].lng),
				map: map,
				title: [data.jobs[i].l_name, data.jobs[i].address, data.jobs[i].city].join(', '),
				visible: false,
				icon: 'http://chart.apis.google.com/chart?cht=mm&chs=24x32&ext=.png&chco=' + colors
			});
			marker.data_city = data.jobs[i].city;
			marker.data_job_status = data.jobs[i].job_status;
			marker.data_job = data.jobs[i];
			markers.push(marker);
			google.maps.event.addListener(marker, 'click', onMarkerClick);
			google.maps.event.addListener(marker, 'mouseover', onMarkerHover);
		}

		// city filter
		var cities = data.jobs.map(function(_item) { // get a sorted list of cities
		    return _item.city;
		}).sort();
		var cityNames = [];
		cities.forEach(function(_city) { // get uniques and their counts
			cityNames[_city] = (typeof cityNames[_city] === 'undefined' ? 1 : cityNames[_city] + 1); 
		});
		var select = $('select.cities').empty().append('<option value="all" selected>All Cities ('+data.jobs.length+')</option>');
		for(var i in cityNames) {
			select.append('<option value="'+i+'">'+i+' ('+cityNames[i]+')</option>');
		}
		select.change(applyFilter);

		// job status filter
		$('ul.job-status-legend li input[type="checkbox"]').change(applyFilter);

		// filter once on load
		applyFilter();

	}
	
	google.maps.event.addDomListener(window, 'load', initialize);
});

function applyFilter() {
	infoWindow.close();
	var city = $('select.cities').val();
	var statuses = $('ul.job-status-legend li input[type="checkbox"]:checked').map(function() {
		return $(this).data('status');
	}).get();
	markers.forEach(function(_marker) { _marker.setVisible(false); });
	markers
		.filter(function(_marker) {
			return ((city === 'all' || _marker.data_city === city) && (status === 'all' || statuses.indexOf(_marker.data_job_status) !== -1));
		})
		.forEach(function(_marker) {
			_marker.setVisible(true);
		});
}

function onMarkerClick() {
	
	var job = this.data_job, outer = $('<div/>');

	var content = $('<div/>').addClass('job-info');

	var jobStatusNames = {'1': 'Lead Created', '2': 'Lead Scheduled', '3': 'Quote Created', '4': 'Job Scheduled', '5': 'Job Complete', '6': 'On Standby'};
	var status = jobStatusNames['' + job.job_status];
	
	$('<h5/>')
		.append('<a target="_blank" href="/job/'+job.job_id+'">Job #' + job.job_id + ' - ' + status + '</a>')
		.appendTo(content);

	if(job.job_status == 6) {
		$('<div/>')
			.addClass('standby-date')
			.html('On Standby from ' + job.job_put_on_standby.replace(/\s+.*/, ''))
			.appendTo(content);
	}
	else if(job.job_status == 5) {
		$('<div/>')
			.addClass('standby-date')
			.html('Completed on ' + job.job_completed.replace(/\s+.*/, ''))
			.appendTo(content);
	}

	var name = $.trim((job.f_name ? job.f_name : '') + ' ' + (job.l_name ? job.l_name : ''));
	$('<h5/>')
		.addClass('name')
		.append('<a target="_blank" href="/customers/'+job.id+'">' + name + '</a>')
		.appendTo(content);

	$('<div/>')
		.addClass('address')
		.html($.trim([job.address, job.city, job.zip].join(' ')))
		.appendTo(content);

	content.appendTo(outer);

	infoWindow.setContent(outer.html());
	infoWindow.open(map, this);

}

var prevTop = null;
function onMarkerHover() {
	if (prevTop) prevTop.setZIndex();
	prevTop = this;
	this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
}
