$(document).ready(function() {

	// change events
	$('.parts').on('change keyup', '.qty-chooser, .option-chooser', reTotalParts);

	// delete part
	$('.parts').on('click', '.delete', function() {
		$(this).closest('tr.part-row').remove();
		reTotalParts();
		return false;
	});

	// add new part
	$('.new-part-chooser li a').click(function() {
		var chosenPartID = $(this).data('part-id'),
			part = allParts.filter(function(_part) {
				return _part.id === chosenPartID;
			});
		if(!part.length) return false;
		part = part[0];

		addPart(part, count++);

		reTotalParts();
	});

	// print
	$('#btnPrint').click(function() {
		window.print();
	});

	// save
	$('#btnSave').click(savePartsList);

	// add new option
	$('.parts').on('click', '.new-option-menu li a', addNewOption);

	// sel value for option
	$('.parts').on('click', '.new-option-value-menu li a', setOptionValue);

	// remove option
	$('.parts').on('click', '.remove-option', function() {
		$(this).closest('.custom-option').remove();
		reTotalParts();
	});

	// finalize/unfinalize
	$('#btnFinalize').click(finalize);
	$('#btnUnfinalize').click(unFinalize);



	// show the parts
	var tr, part, count = 1, options = '', consolidatedParts = consolidateParts(JSON.parse(JSON.stringify(parts)));
	for(var x in consolidatedParts) {
		part = consolidatedParts[x];
		addPart(part, count++);
	}

	reTotalParts();
});

function consolidateParts(parts) {
	
	var consolidatedParts = [], part, i, j, found = false, foundPart = false;
	
	for(i in parts) {
		part = parts[i];

		// check if already there in consolidatedParts
		found = false;
		for(j in consolidatedParts) {
			if(consolidatedParts[j].id === part.id) {
				found = true; 
				foundPart = consolidatedParts[j];
				break;
			}
		}

		// if found, check if options and their values match
		if(found && compareOptions(part.options, foundPart.options) == true) {
			if(!part.expected_qty || isNaN(part.expected_qty)) part.expected_qty = 1;
			foundPart.expected_qty = parseInt(foundPart.expected_qty, 10) + parseInt(part.expected_qty, 10);
		}
		// if not found or if options differ add afresh
		else {
			if(!part.expected_qty || isNaN(part.expected_qty)) part.expected_qty = 1;
			consolidatedParts.push(part);
		}
	}

	return consolidatedParts;

}

function compareOptions(options1, options2) {
	
	if(!options1 && !options2) return true;
	if(!options1 && options2) return false;
	if(options1 && !options2) return false;
	if(options1.length !== options2.length) return false;

	var i, j, option1, equal = true, found = false;
	for(i=0; i<options1.length; i++) {
		option1 = options1[i];
		found = false;
		for(j=0; j<options2.length; j++) {
			if(options2[j].name === option1.name && options2[j].value === option1.value) {
				found = true;
				break;
			}
		}
		if(!found) {
			equal = false;
			break;
		}
	}
	return equal;
}

function reTotalParts() {

	var total = 0, count = 1;

	$('tr.part-row').each(function() {

		// update count
		$(this).find('.count').text(count++);

		var part = $(this).data('part');

		// look at options selected and set the unit-price & row-total
		var options = [];
		for(var option in part.options) {
			delete part.options[option]['selected'];
		}
		$(this).find('.custom-option:not(.new-option-holder)').each(function() {
			options.push({
				name: $.trim($(this).find('.btn-add-option').text()),
				value: $.trim($(this).find('.btn-add-option-value').text()),
				priceFactor: 0
			});
		});
		part.options = options;

		// update qty in tr.part-row
		part.expected_qty = parseInt($(this).find('.qty-chooser').val(), 10);

	});

	$('.grand-total').text('$ ' + total.toFixed(1));
}

function savePartsList() {

	var parts = [];
	$('tr.part-row').each(function() {
		parts.push($(this).data('part'));
	});
	parts = JSON.stringify(parts);

	$.post('/parts-list/update', {id: $('#partslist_id').val(), parts: parts}, function(_data) {
		if(_data === 'OK') {
			var successDiv = $('<div class="well well-small success" id="notifications">Parts List Saved Successfully</div>').insertBefore('#printable');
			setTimeout(function() {
				successDiv.remove();
			}, 3000);
		}
	});

	return false;
}

// when an option is selected from ther add-option menu (button)
function addNewOption() {

	var part = $(this).closest('tr').data('part'), 
		factoryPart = getFactoryPart(part),
		type = $(this).data('option-type'),
		factoryOption = getFactoryOption(factoryPart, type),
		name = $(this).text();

	if(type === 'other') {
		name = prompt('Option Name:', '');
		if(!name) return;
	}

	$(this).closest('.custom-option').find('.btn-add-option').addClass('chosen').text(name);

	var optionValues = ['Other'];
	
	if(factoryOption && factoryOption.values && factoryOption.values.length) {
		optionValues = factoryOption.values.concat(optionValues);
	}

	$(this).closest('.custom-option').find('.btn-add-option-value').text('<choose>');
	$(this).closest('.custom-option').find('.new-option-value-menu').html(optionValues.map(function(_x) {
		return (_x === 'Other' ? '<li class="divider"></li><li><a>' + _x + '</a></li>' : '<li><a>' + _x + '</a></li>');
	}).join(''));

	$(this).closest('.custom-option').find('.new-option-value-group').show();

	$(this).closest('.custom-option').removeClass('new-option-holder');

	$(this).closest('td').find('.new-option-holder').remove();
	var newOptionSectionHTML = getAddNewOptionSectionHTML(part, factoryPart);
	if(newOptionSectionHTML !== '') {
		$('<div class="custom-option new-option-holder">' + newOptionSectionHTML + '</div>').appendTo($(this).closest('td'));
	}

	reTotalParts();

	if(event && event.type === 'click') {
		$(this).closest('.custom-option').find('.btn-add-option-value').trigger('click');
	}

	return false;
}

function setOptionValue() {
	var value = $(this).text();
	if(value === 'Other') {
		value = prompt('Option Value:', '');
		if(!value) return;
	}
	$(this).closest('.custom-option').find('.btn-add-option-value').addClass('chosen').text(value);

	reTotalParts();
}

function addPart(part, count) {
	var tr = $('<tr/>');
	tr.addClass('part-row');
	tr.data('part', part);

	// picked
	tr.append($('<td/>'));

	// si no:
	tr.append($('<td/>').addClass('count').text(count));

	// qty
	tr.append($('<td/>').html('<input type="text" class="qty-chooser" min="1" value="' + part.expected_qty + '" readonly disabled>'));

	// options
	var options = '',
		factoryPart = getFactoryPart(part);

	// options + add-new-option section
	var optionsHTML = getOptionsHTML(part, factoryPart);
	if(optionsHTML !== '') {
		options += optionsHTML;
	}
	var newOptionSectionHTML = getAddNewOptionSectionHTML(part, factoryPart);
	if(newOptionSectionHTML !== '') {
		options += '<div class="custom-option new-option-holder">' + newOptionSectionHTML + '</div>';
	}
	
	tr.append($('<td/>').html(options));

	// name
	tr.append($('<td/>').text(part.name));

	// delete button
	//tr.append($('<td/>').html('<a class="delete" data-part-id="'+part.id+'"><i class="glyphicon glyphicon-trash"></i></a>')); // options

	tr.insertBefore('.parts tr.noprint');

	// check if there is a matchin option available (in optionValues), if so trigger a click on it (to populate values list)
	tr.find('.btn-add-option').each(function() {
		var name = $.trim($(this).text()), 
			value = $.trim($(this).parent().next().find('.btn-add-option-value').text()),
			valueElement = $(this).parent().next().find('.btn-add-option-value'),
			option = $(this).closest('.custom-option'),
			found = false;
		$('.new-option-menu li a', option).each(function() {
			if($.trim($(this).text()) === name) {
				found = true;
				$(this).trigger('click');
				valueElement.text(value);
				return false;
			}
		});
		if(!found) {
			option.find('.new-option-value-menu').html('<li><a>Other</a></li>');
			valueElement.text(value);
		}
	});
}
function getFactoryPart(part) {
	var factoryPart = allParts.filter(function(_x) {
		return _x.id === part.id;
	});
	if(factoryPart && factoryPart.length === 1) {
		factoryPart = factoryPart[0];
	}
	else {
		factoryPart = false;
	}
	return factoryPart;
}
function getFactoryOption(factoryPart, optionName) {
	var factoryOption = factoryPart && factoryPart.options && factoryPart.options.length && factoryPart.options.filter(function(_x) {
		return _x.name === optionName;
	});
	if(factoryOption && factoryOption.length == 1) 
		factoryOption = factoryOption[0]; 
	else
		factoryOption - false;
	return factoryOption;
}
function getOptionsHTML(part, factoryPart) {
	if(!factoryPart) return '';
	var html = '';
	for(var option in part.options) {
		var optionsHTML = $('.add-option-elements').clone();
		option = part.options[option];
		if(!option['name'] || !option['value']) continue;
		// optionsHTML.find('.btn-add-option').addClass('chosen').text(option.name);
		// optionsHTML.find('.btn-add-option-value').addClass('chosen').text(option.value);
		// optionsHTML.find('.new-option-menu, .new-option-value-menu').empty();
		// for(var op in factoryPart.options) {
		// 	optionsHTML.find('.new-option-menu').append('<li><a data-option-type="' + factoryPart.options[op].name + '">' + factoryPart.options[op].name + '</a></li>');
		// }
		// optionsHTML.find('.new-option-menu').append('<li class="divider"></li><li><a data-option-type="other">Other</a></li>');
		// optionsHTML.find('.new-option-value-group').css('display', 'inline-block');
		// html += '<div class="custom-option">' + optionsHTML.html() + '</div>';

		html += '<div class="custom-option">' + 
					'<span class="option-name">' + option.name + '</span>' +
					'<span class="option-value">' + option.value + '</span>' +
				'</div>';
	}
	return html;
}
function getAddNewOptionSectionHTML(part, factoryPart) {
	// if(!factoryPart) return '';
	// var newOptionHTML = $();
	// newOptionHTML = $('.add-option-elements').clone();
	// newOptionHTML.find('.new-option-menu').empty();
	// for(var op in factoryPart.options) {
	// 	var alreadyAdded = false;
	// 	for(var aaop in part.options) {

	// 	}
	// 	newOptionHTML.find('.new-option-menu').append('<li><a data-option-type="' + factoryPart.options[op].name + '">' + factoryPart.options[op].name + '</a></li>');
	// }
	// newOptionHTML.find('.new-option-menu').append('<li class="divider"></li><li><a data-option-type="other">Other</a></li>');
	// return (newOptionHTML.length ? newOptionHTML.html() : '');
	return '';
}

function finalize() {
	$.post('/parts-list/finalize', {id: $('#partslist_id').val()}, function() {
		window.location.reload();
	});
}
function unFinalize() {
	$.post('/parts-list/unfinalize', {id: $('#partslist_id').val()}, function() {
		window.location.reload();
	});
}
