$(window).load(function() {
	$('[data-image-owner-type]').each(function() {
		refreshImages(this); // load current images
		if($(this).data('allow-upload')) { // setup upload facility
			setupUploader(this);
		}
	});
	$('[data-image-owner-type]').on('click', '.delete-image', function() {
		if(confirm('Are you sure?')) {
			var container = $(this).closest('[data-image-owner-type]');
			$.post('/delete-image', {id: $(this).data('image-id'), type: $(this).data('type')}, function(_data) {
				refreshImages(container[0]);
			});
		}
		return false;
	});

	$('[data-image-owner-type]').on('click', '.rotate-left, .rotate-right', function() {
		var img = $(this).closest('li').find('img.user-image').first(), rotation = img.data('rotation');
		if($(this).is('.rotate-left')) rotation -= 90; else rotation += 90;
		if(Math.abs(rotation) === 360) rotation = 0;
		img.removeClass().addClass('user-image rotate'+rotation).data('rotation', rotation);
		fixParentAfterRotation(rotation, img, $(this).closest('li'));
		saveImageRotation(img.data('id'), rotation);
		return false;
	});

});

function fixParentAfterRotation(_angle, _img, _parent) {
	_angle = Math.abs(_angle);
	if(_angle === 90 || _angle === 270) {
		if(_img.width() > _parent.height()) {
			_parent.height(_img.width());
			_img.css('margin-top', (_parent.offset().top - _img.offset().top) + 'px');
		}
		else {
			_parent.css('height', 'auto');
			_img.css('margin-top', 0);
		}
	}
}

function saveImageRotation(_imageID, _angle) {
	$.post('/rotate-image', {id: _imageID, angle: _angle}, function() {

	});
}

function refreshImages(_container, _callback) {
	_container = $(_container);
	var images = _container.find('.images');
	if(images.length === 0) images = $('<div/>').addClass('images').appendTo(_container);
	images.html('Loading...');
	$.get('/images/' + _container.data('image-owner-type') + '/' + _container.data('image-owner-id'), function(_data) {
		images.empty();
		_data = JSON.parse(_data);
		if(_data.length > 0) {
			
			var ul = $('<ul/>'), ulFiles = $('<ul class="files"/>');
			
			_data.forEach(function(_img) {

				var li, a, removeA;
				
				// image
				if(typeof _img.type === 'undefined') {
					
					li = $('<li/>').appendTo(ul);
					a = $('<a/>')
						.addClass(_container.data('image-owner-type') + '-image uploaded-image')
						.attr('href', _img.path)
						.attr('rel', 'lightbox')
						.attr('data-lightbox', 'images-' + _container.data('image-owner-type') + '-' + _container.data('image-owner-id'))
						.appendTo(li);
					
					$('<img/>')
						.addClass('user-image')
						.addClass('rotate' + _img.rotation)
						.attr('src', _img.path.replace('&export=download', ''))
						.data('rotation', _img.rotation)
						.data('id', _img.id)
						.appendTo(a);

					$('<a class="rotate-link rotate-left" title="Rotate 90 degrees left"><i class="glyphicon glyphicon-arrow-left"></i></a>').appendTo(li);
					$('<a class="rotate-link rotate-right" title="Rotate 90 degrees right"><i class="glyphicon glyphicon-arrow-right"></i></a>').appendTo(li);

					removeA = $('<a/>')
						.addClass('delete-image')
						.html('Remove')
						.data('type', 'image')
						.data('image-id', _img.id)
						.appendTo(li);
				}
				// other file
				else {

					li = $('<li/>').appendTo(ulFiles);
					$('<i/>')
						.addClass('glyphicon glyphicon-file')
						.appendTo(li);
					$('<a/>')
						.addClass(_container.data('image-owner-type') + '-image uploaded-image')
						.attr('href', _img.path)
						.text(_img.title)
						.appendTo(li);
					
					removeA = $('<a/>')
						.addClass('delete-image')
						.html('Remove')
						.data('type', 'file')
						.data('image-id', _img.id)
						.appendTo(li);
				}

			});
			ul.appendTo(images);
			ulFiles.appendTo(images);

			window.setTimeout(function() {
				$('img.user-image').each(function() {
					fixParentAfterRotation($(this).data('rotation'), $(this), $(this).closest('li'));
				});
			}, 1000);

			if(typeof _callback === 'function') _callback();
			
		}
		else {
			images.html('<span class="noimages">No files have been added yet</span>');
		}
		
	});
}

function addTempFileLine(_container, _id, _file) {
	$('.temp-files', _container).append(
		$('<div/>').attr('tempid', 'temp-file-' + _id).html(_file + ' <progress class="progressbar" value="0" max="100"></progress><span class="percent">0%</span>')
	).show();
}

function updateTempFileLine(_container, _id, _percent) {
	$('.temp-files [tempid="temp-file-'+_id+'"] .progressbar', _container).val(_percent);
	$('.temp-files [tempid="temp-file-'+_id+'"] .percent', _container).text(_percent + '%');
}

function removeTempFileLine(_container, _id) {
	$('.temp-files [tempid="temp-file-'+_id+'"]', _container).remove();
	if($('.temp-files', _container).find('> div').length === 0) $('.temp-files', _container).hide();
}

function setupUploader(_container) {
	_container = $(_container);

	_container.data('uploads', []);

	var tempFiles = $('<div/>').addClass('temp-files').appendTo(_container);

	var dropzone = $('<div/>')
		.html('Drag and Drop files (images, documents, etc.) here - OR - <span class="file-browser"><span>Open File Browser</span><input type="file" name="files[]" multiple="multiple"></span>')
		.addClass('dropzone')
		.appendTo(_container);
	dropzone.dmUploader({
		
		// uncomment this after API testing
		url: '/upload/' + _container.data('image-owner-type') + '/' + _container.data('image-owner-id'),

		// start - api testing
		/*
		url: '/api/saveFile',
		extraData: {
			title: 'Image attachment for ' + _container.data('image-owner-type') + '# ' + _container.data('image-owner-id'),
			description: 'Image attachment for ' + _container.data('image-owner-type') + '# ' + _container.data('image-owner-id'),
			owner_type: _container.data('image-owner-type'),
			owner_id: _container.data('image-owner-id'),
			type: 'image'
		},
		*/
		// end - api testing

		allowedTypes: '*',
		extFilter: null,
		dataType: 'json',

		onNewFile: function(_id, _file){
	        var uploads = _container.data('uploads');
	        uploads[_id] = _file.name;
	        _container.data('uploads', uploads);
	    },

		onBeforeUpload: function(_id) {
			var uploads = _container.data('uploads');
			addTempFileLine(_container, _id, uploads[_id]);
		},

		onUploadProgress: function(_id, _percent){
	        updateTempFileLine(_container, _id, _percent * 0.85);
	    },

		onUploadSuccess: function(_id, _data) {
			updateTempFileLine(_container, _id, 100);
			if(_data.status === 'ok') {
				refreshImages(_container[0], function() {
					removeTempFileLine(_container, _id);
				});
			}
			else {
				if(_data['message']) alert('ERROR: ' + _data['message']);
				else alert('Unknown error while uploading file');
			}
		},
		onUploadError: function(_id, _data) {
			//console.log('onUploadError', _id, _data);
			removeTempFileLine(_container, _id);
			alert('Unknown error while uploading file');
		},
		onFileTypeError: function(_file) {
			//console.log('onFileTypeError', _file);
			removeTempFileLine(_container, _id);
			alert('Error uploading file: Invalid file type');
		},
		onFileExtError: function(_file) {
			//console.log('onFileExtError', _file);
			removeTempFileLine(_container, _id);
			alert('Error uploading file: Invalid file type');
		}
	});
}
