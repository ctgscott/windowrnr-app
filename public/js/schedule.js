var eventsList = [];
var date = new Date(), y = date.getFullYear(), m = date.getMonth();
var firstDay = new Date(y, m, 1);
var lastDay = new Date(y, m + 1, 0);
var firstTime = firstDay.getTime();
var lastTime = lastDay.getTime();
var leadAddress;
var leadCity;
var leadZip;
var leadLName;
var leadFName;
var leadPhone;
var leadBuilt;
var eventsByDay = {}, days = [];

var currentEvent = false, currentEventElement = false, evDDTimer = false;

function copyLead() {
	$('#l_name2').val($('#l_name').val());
	$('#f_name2').val($('#f_name').val());
	$('#company_name2').val($('#company_name').val());
	$('#phone2').val($('#phone').val());
	$('#job_address2').val($('#job_address').val());
	$('#job_city2').val($('#job_city').val());
	$('#zip2').val($('#zip').val());
	$('#built2').val($('#built').val());
	$('#email2').val($('#email').val());
	$('#alt_phone2').val($('#alt_phone').val());
}

function addLocationMarkers(_mode) {
	var mapNum;
	var latLng;
	var lat;
	var lng;
	var googMapApiKey = $("meta[name='googMapApiKey']").attr("content");

	if(_mode === 'schedule') {
		fullAddress = $('#location').val();
	}
	else {
		leadAddress = $('#job_address').val();
		leadCity = $('#job_city').val();
		leadZip = $('#zip').val();
		fullAddress = encodeURIComponent(leadAddress+', '+leadCity+', '+leadZip);
	}
	$.ajax({
		url: "https://maps.googleapis.com/maps/api/geocode/json?address="+fullAddress+"&sensor=false&key="+googMapApiKey,
		async: false,
		success: function(result){
			latLng = result;
			if (latLng.status === "OK")
			{
				lat = latLng.results[0].geometry.location.lat;
				lng = latLng.results[0].geometry.location.lng;
				for (mapNum = 1; mapNum <= 5; ++mapNum) {
					var gLatLng = new google.maps.LatLng(lat,lng);
					var mapOptions = {
						mapTypeId: google.maps.MapTypeId.ROADMAP,
					};
					var test = {};
					test[mapNum] = mapAdd(mapNum, gLatLng, mapOptions);
				}				
			} 
			else 
			{
				alert('Could not resolve address to map coordinates');
			}
		}
	});
}


$(document).ready(function(){

	// when personal time event is selected, customer radios to be hidden
	$('select[name=event_type]').change(function() {
		if(this.value == 9) {
			$(this).closest('div').next().hide();
		}
		else {
			$(this).closest('div').next().show();
		}
	});

	$("#btnMap").click(addLocationMarkers);

	$("#btnBillDetail")
		.append('<i class="left10 glyphicon glyphicon-chevron-down"></i>')
		.click(function () {
			$('#divBillDetail').slideToggle('fast', function() {
				if($(this).is(':visible')) {
					$("#btnBillDetail i").removeClass().addClass('left10 glyphicon glyphicon-chevron-up');
				}
				else {
					$("#btnBillDetail i").removeClass().addClass('left10 glyphicon glyphicon-chevron-down');
				}
			});
		});
	
	$("#btnBillDetail2")
		.append('<i class="left10 glyphicon glyphicon-chevron-down"></i>')
		.click(function () {
			$('#divBillDetail2').slideToggle('fast', function() {
				if($(this).is(':visible')) {
					$("#btnBillDetail2 i").removeClass().addClass('left10 glyphicon glyphicon-chevron-up');
				}
				else {
					$("#btnBillDetail2 i").removeClass().addClass('left10 glyphicon glyphicon-chevron-down');
				}
			});
		});

	$("#btnBillDetail3")
		.append('<i class="left10 glyphicon glyphicon-chevron-down"></i>')
		.click(function () {
			$('#divBillDetail3').slideToggle('fast', function() {
				if($(this).is(':visible')) {
					$("#btnBillDetail3 i").removeClass().addClass('left10 glyphicon glyphicon-chevron-up');
				}
				else {
					$("#btnBillDetail3 i").removeClass().addClass('left10 glyphicon glyphicon-chevron-down');
				}
			});
		});
	
	$("#btnMore").click(function () {
		$('#divMore').collapse('toggle');
		$('#expandGlyph').toggleClass("glyphicon-chevron-up glyphicon-chevron-down");
	});
	
	$("#btnMore2").click(function () {
		$('#divMore2').collapse('toggle');
		$('#expandGlyph2').toggleClass("glyphicon-chevron-up glyphicon-chevron-down");
	});
	
    $('#sourceCheckbox5').click(function(){
		if (this.checked) {
			$('#source_referral').removeAttr("disabled");
		}
		else {
			$('#source_referral').attr("disabled", true);
		}
    });

    $('#sourceCheckbox5a').click(function(){
		if (this.checked) {
			$('#source_referral2').removeAttr("disabled");
		}
		else {
			$('#source_referral2').attr("disabled", true);
		}
    });

	/* Full Calendar - initialize the external events
	-----------------------------------------------------------------*/
	
	$('#external-events div.external-event').each(function() {
	
		// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
		// it doesn't need to have a start or end
		var eventObject = {
			title: $.trim($(this).text()) // use the element's text as the event title
		};
		
		// store the Event Object in the DOM element so we can get to it later
		$(this).data('eventObject', eventObject);
		
		// make the event draggable using jQuery UI
		$(this).draggable({
			zIndex: 999,
			revert: true,      // will cause the event to go back to its
			revertDuration: 0  //  original position after the drag
		});
		
	});

	// event hover dropdown
	$('body').click(function() {
		closeEventHoverDropDown();
	});
	$('body').on('click', '#evbtn_viewincal', function() {
		window.open(currentEvent.htmlLink, '_blank');
	});
	$('body').on('click', '#evbtn_view', function() {
		window.open('/events/' + currentEvent.id, '_blank');
	});
	$('body').on('click', '#evbtn_jobdetail', function() {
		window.open('/job/' + currentEvent.job_id, '_blank');
	});
	$('body').on('click', '#evbtn_map', function() {
		$('#eventTitle').text(currentEvent.title);
		$('#eventStart').text(currentEvent.start);
		$('#eventEnd').text(currentEvent.end);
		$('#jobsModal').find('.new-customer-row,.existing-customer-row').hide();
		$('#jobsModal').find('[name=customer_type]').prop('checked', false);
		$('#jobsModal').modal('toggle');
	});
	$('#btnMapEvent').click(function() {
		
		// map to new or existing event?
		var evType = $('#jobsModal [name=customer_type]:checked').first().val();
		if(evType === 'existing') {

			if(!parseInt($('#map_job_id').val(), 10)) return;
			
			$.post('/event-map', {event_id: currentEvent.google_event_id, job_id: $('#map_job_id').val()}, function(_data) {
				if(_data === 'OK') {
					currentEventElement.removeClass('unmapped');
					currentEvent.job_id = $('#map_job_id').val();
					$('#jobsModal').modal('toggle');
				}
				else {
					alert(_data);
				}
			});

		}
		else if(evType === 'new') {

			// compose params
			var params = $('form.map-event-new-job-form').serialize();
			params += '&event_id=' + currentEvent.google_event_id + '&job_id=' + $('#map_job_id').val();

			$.post('/event-map-new', params, function(_data) {
				if(_data.indexOf('OK:') === 0) {
					currentEventElement.removeClass('unmapped');
					currentEvent.job_id = parseInt(_data.substring(3), 10);
					$('#jobsModal').modal('toggle');
				}
				else {
					alert(_data);
				}
			});

		}
		
	});
	$('body').on('click', '#evbtn_unmap', function() {
		$.post('/event-unmap', {event_id: currentEvent.google_event_id}, function(_data) {
			if(_data === 'OK') {
				currentEventElement.addClass('unmapped');
				currentEvent.job_id = 0;
			}
			else {
				alert(_data);
			}
		});
	});

	$('body').on('click', '#eventMapEvent', function() {
		$('#eventModal').modal('hide');
		$('body #evbtn_map').trigger('click');
	});
	
	$('body').on('click', '#evbtn_discard, #evbtn_remove, #btnEventDelete', function() {
		$.post('/event-discard', {event_id: currentEvent.google_event_id, app_event_id: currentEvent.id}, function(_data) {
			if(_data === 'OK') {
				currentEventElement.remove();
				closeEventHoverDropDown();
				$('#eventModal').modal('hide');
			}
			else {
				alert(_data);
			}
		});
	});
	
	// page is now ready, initialize the calendar...
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	var fullCalendarOptions = {
		// put your options and callbacks here
		selectable: true,
		selectHelper: true,
		select: function(start, end, allDay) {	
			closeEventHoverDropDown();
			$('.customer_type input[type="radio"]').prop('checked', false);
			$('.existing-customer-row, .new-customer-row').hide();
			$('#myModal').modal('toggle');
			copyLead();
			if(allDay) {
				var allDayStart = moment(start).add(fullCalendarOptions.minTime, 'hours');
				var allDayEnd = moment(end).add(fullCalendarOptions.maxTime, 'hours');
				$('#start').val(moment(allDayStart).toDate());
				$('#end').val(moment(allDayEnd).toDate());
				$("#startTimeSpan").text(moment(allDayStart).format('MMMM Do, h:mm:ss a'));
				$("#endTimeSpan").text(moment(allDayEnd).format('MMMM Do, h:mm:ss a'));
			} else {
				$('#start').val(start);
				$('#end').val(end);
				$("#startTimeSpan").text(moment(start).format('MMMM Do, h:mm:ss a'));
				$("#endTimeSpan").text(moment(end).format('MMMM Do, h:mm:ss a'));
			}
			
			//#btnEditJob : is the id of the 'Edit Job' button in the modal view of 'Schedule Appointments' calendar page.
			if($('#btnEditJob').length) {
				var job_id = $("#job_id").val();
				$('#btnEditJob').attr('href','/job/'+job_id);
			}
		},

		weekends: false, 	// will hide Saturdays and Sundays
		editable: true,  	// enables drag, drop and resize
		weekMode: 'liquid',
		titleFormat: {
			week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d, yyyy}",
			day: 'dddd, MMM d, yyyy'
		},
		url:'/customers/estimateschedule/',
		allDayText: 'All Day',
		minTime: 7,
		maxTime: 18,

		header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
		},
		defaultView: 'agendaWeek',

		timezone: 'America/Los_Angeles',
		
		eventMouseover: function( event, jsEvent, view ) {

			closeEventHoverDropDown();

			var self = $(this), 
				top = self.offset().top + self.outerHeight() - 1,
				left = self.offset().left + 5;

			event.job_id = parseInt(event.job_id, 10);
			if(isNaN(event.job_id)) event.job_id = 0;
			
			if(event.job_id) {
				$('#evbtn_map, #evbtn_discard').hide();
				$('#evbtn_unmap, #evbtn_jobdetail, #evbtn_view, #evbtn_remove').show();
			}
			else {
				$('#evbtn_unmap, #evbtn_jobdetail, #evbtn_view, #evbtn_remove').hide();
				$('#evbtn_map, #evbtn_discard').show();
			}

			currentEvent = event;
			currentEventElement = self;

			$('#event_modd')
				.css({
					left: left + 'px',
					top: top + 'px'
				})
				.show();

			currentEventElement.addClass('infocus');

			// clearTimeout(evDDTimer);
			// evDDTimer = false;
		},
		
		eventClick:  function(event, jsEvent, view) {
			$('#modalTitle').html(event.title);
			$('#modalJobId').html("Job ID: "+event.job_id);
            $('#modalBody').html(event.description);
            $('#eventUrl').attr('href',event.url);
			$('#eventJobId').attr('href','/job/'+event.job_id);
			$('#eventGoogDetail').attr('href', event.htmlLink);
			$('#hiddenEventID').attr('value', event._id);
			//console.log('event=',event);
			if(event.job_id === 0) {
				$('#btnJobDetail').hide();
				$('#btnMapEventJob').show();
				$('#btnGoogDetail').hide();
				$('#divEventBtn').hide();
				$('#btnDupEvent').hide();
				$('#btnPartsList').hide();
				$('#btnEventDelete').hide();
				if(event.event_type == 'Personal or Office') {
					$('#btnMapEventJob').hide();
				}
			} else {
				$('#btnJobDetail').show();
				$('#btnMapEventJob').hide();
				$('#btnGoogDetail').show();
				$('#divEventBtn').show();
				$('#btnDupEvent').show();
				$('#btnPartsList').show();
				$('#jobPartsList').attr('href', '/job-parts-list/' + event.job_id);
				$('#btnEventDelete').show();
			}
            $('#eventModal').modal();
        },

		eventAfterRender: function( event, element, view ) {
			// if in assign page
			if($('.assign').length) {

				// set id as data attribute
				$(element).data('event-db-id', event.id);

				// add assignees to event element
				if(event.assignee) {
					try {
						var parsed = JSON.parse(event.assignee);
						if(parsed.length) {
							$('.fc-event-time', element).append('<div class="assignees">');
							parsed.forEach(function(_assignee) {
								$('.fc-event-time', element).find('div.assignees').append(
									'<span profile=\'' + JSON.stringify(_assignee) + '\' member="'+_assignee.first_name+'">'+_assignee.first_name+'</span>'
								);
							});
						}
					}
					catch(_e) {
						console.error('Old assignee value ignored');
					}
				}
			}
		},
		
		eventResize: onEventResize,
		eventDrop: onEventMove,

		eventSources: [
		{
			url: '/events/getCalEvents/x/x/all' + ($('.assign').length ? '?assign=1' : ''),
			type: 'GET',
			success: function(events) {

				var day;
				eventsByDay = {};

				$(events).each(function() {

					if($('#color-uid-' + this.cal_user_id).length) {
						this['backgroundColor'] = $('#color-uid-' + this.cal_user_id).val();
/*						this['textColor'] = getOppositeColor(this['backgroundColor']);
						this['borderColor'] = getOppositeColor(this['backgroundColor']);
*/						this['textColor'] = '#000000';
						this['borderColor'] = '#ffffff';
					}
					else {
						this['textColor'] = 'white';

						// this used to be grey (same as dev's avatar color) and was causing problems with avatar functionality
						// setting to cyan temporarily for debugging
						this['backgroundColor'] = 'cyan';
						this['borderColor'] = 'black';
					}

					this.job_id = parseInt(this.job_id, 10);
					if(isNaN(this.job_id)) this.job_id = 0;
					
					if(!this.job_id && this.event_type != 'Personal or Office') {
						this['className'] = 'unmapped';
					}

					var evUID = 'uid-' + this.cal_user_id;
					if(typeof eventsByDay[evUID] === 'undefined') eventsByDay[evUID] = {};

					// populate eventsByDay array
					day = new Date('' + this.start).toLocaleDateString();
					if(typeof eventsByDay[evUID][day] === 'undefined') eventsByDay[evUID][day] = [];
					eventsByDay[evUID][day].push({
						lat: this.lat,
						lng: this.lng,
						time: new Date('' + this.start).toLocaleTimeString()
					});

				});
				eventsList = events;
				mapPaint(events);

				for(var j=1; j<=5; j++) {
					showRoutes(days[j-1], j);
					fitMapToMarkers(days[j-1], j);
				}

				// show marker for easy route optimized scheduling
				if($('#location').length) {
					addLocationMarkers('schedule');
				}

			},
			error: function() {
				alert('there was an error while fetching events!');
			},
		}]
	};

	if($('.assign').length && $('#forDate').length) {
		var initialDateParts = $('#forDate').val().split('-');
		fullCalendarOptions.year = initialDateParts[0];
		fullCalendarOptions.month = parseInt(initialDateParts[1], 10) - 1;
		fullCalendarOptions.date = parseInt(initialDateParts[2], 10);
	}

	// for testing only (this week has event for multiple users. good for testing multi-routes in map)
	// will remove after full implementation
	/*
	fullCalendarOptions.year = 2014;
	fullCalendarOptions.month = 5;
	fullCalendarOptions.date = 30;
	*/

	// add event onmouseover dropdown
	$('<div id="event_modd">' +
		'<div id="evbtn_viewincal" title="View in Google Calendar"><i class="glyphicon glyphicon-map-marker"></i></div>' +
		'<div id="evbtn_view" title="View Event Detail"><i class="glyphicon glyphicon-list-alt"></i></div>' +
		'<div class="vsep"><i class="glyphicon glyphicon-map-marker"></i></div>' +
		'<div id="evbtn_map" title="Map to Job"><i class="glyphicon glyphicon-log-in"></i></div>' +
		'<div id="evbtn_jobdetail" title="View Job Detail"><i class="glyphicon glyphicon-file"></i></div>' +
		'<div id="evbtn_unmap" title="Unmap from Job"><i class="glyphicon glyphicon-log-out"></i></div>' +
		'<div class="vsep"><i class="glyphicon glyphicon-map-marker"></i></div>' +
		'<div id="evbtn_discard" title="Discard"><i class="glyphicon glyphicon-remove"></i></div>' +
		'<div id="evbtn_remove" title="Delete/Cancel"><i class="glyphicon glyphicon-remove"></i></div>' +
	'</div>').appendTo('body');
	
	$('#calendar').fullCalendar(fullCalendarOptions);	
/*  Full Calendar - End */

	// Add maps to header
	$('.fc-header > tbody:last').append('<tr id="map_tr" class="collapse"><td id="map_td" class="mapContainer" colspan="3"><div id="maps_block"></div></td></tr>');
	for(var x=1; x<=5; x++) {
		$('#maps_block').append(
						'<div class="map_holder">'+
							'<span class="maps map' + x + '" id="map_' + x + '"></span>'+
							'<span class="map_buttons btn-group dropup">'+
								'<button class="btn btn-primary dirBtn dropdown-toggle" data-toggle="dropdown">Directions</button>'+
								'<ul class="dropdown-menu" role="menu">'+
								    '<li><a href="#" data-evuid="uid-1">Scott</a></li>'+
								    '<li><a href="#" data-evuid="uid-3">Ed</a></li>'+
								    '<li><a href="#" data-evuid="uid-4">Norm</a></li>'+
								    '<li><a href="#" data-evuid="uid-5">Norm Test</a></li>'+
								    '<li><a href="#" data-evuid="uid-2">Dev</a></li>'+
								  '</ul>'+
								'<button class="btn btn-primary hideMapsBtn" title="Hide Maps">Hide</button>'+
								'<button class="btn btn-primary expandMapBtn" title="Expand Map"><i class="glyphicon glyphicon-fullscreen"></i></button>'+
							'</span>'+
						'</div>');
	}
	$('#maps_block').css('margin-left', $('.fc-content thead th').first().outerWidth() + 'px').resizable();

	// directions button click handler
	// $('#maps_block .dirBtn').click(function() {
	// 	var idx = $(this).closest('.map_holder').index();
	// 	getDrivingDirections(days[idx], idx+1, false, true);
	// });

	$('#maps_block').on('click', 'a[data-evuid]', function() {
		var idx = $(this).closest('.map_holder').index();
		getDrivingDirectionsForUser(days[idx], idx+1, $(this).data('evuid'));
		return false;
	});

	$('#maps_block .hideMapsBtn').click(function() {
		$('.map_holder').slideUp(function() {
			$('.fc-content thead th').first().html('<div class="btn btn-primary showMapsBtn">Maps</div>');
		});
	});

	$('#maps_block .expandMapBtn').click(function() {
		var idx = $(this).closest('.map_holder').index() + 1;
		var center = map[idx].getCenter();
		$(this).closest('.map_holder').find('span.maps').appendTo('.expanded-map-outer');
		$('.expanded-map-outer').data({map_index: $(this).closest('.map_holder').index()}).show();
		google.maps.event.trigger(map[idx], "resize");
		map[idx].setCenter(center);
		$('html, body').css({overflow: 'hidden'});
		$('.closeExpandedMapBtn').css({
			left: ($('.expanded-map-outer span.maps').offset().left + $('.expanded-map-outer span.maps').width() - $('.closeExpandedMapBtn').outerWidth()) + 'px',
			top: ($('.expanded-map-outer span.maps').offset().top - $('.closeExpandedMapBtn').outerHeight()) + 'px'
		});
	});

	$('.expanded-map-outer .closeExpandedMapBtn').click(function() {
		var expanded_holder = $(this).parent();
		var idx = $(expanded_holder).data('map_index') + 1;
		var center = map[idx].getCenter();
		var map_holder = $('.map_holder:eq(' + $(expanded_holder).data('map_index') + ')');
		$(expanded_holder).find('span.maps').appendTo(map_holder);
		google.maps.event.trigger(map[idx], "resize");
		map[idx].setCenter(center);
		$(expanded_holder).fadeOut();
		$('html, body').css({overflow: 'auto'});
	});

	$(document).on('click', '.showMapsBtn', function() {
		$('.map_holder').slideDown(function() {
			$('.fc-content thead th').first().html('');
		});
	});

	function setMapWidth() {
		window.setTimeout(function() {
			$('.map_holder').width(function() {
				return $('.fc-content thead th:eq(' + ($(this).index() + 1) + ')').width() - 10;
			});
		}, 500);
	}
	$(window).resize(setMapWidth);

	$('#map_tr').show();

	$('#map_day').hide();

	$( 'input[id^="salescheckbox"]' ).click(function() {
		var id = $( this ).val();
		var value = null;
		if($(this).is(':checked')) {
			value = 1;
		} else {
			value = 0;
		}
		$.post( "/profiles/postSalesCheckBox", { id: id, value: value });
		$('#calendar').fullCalendar('refetchEvents');
	});

	$( 'input[id^="visiblebox"]' ).click(function() {
		var id = $( this ).val();
		var value = null;
		if($(this).is(':checked')) {
			value = 1;
		} else {
			value = 0;
		}
		$.post( "/profiles/postVisibleCheckBox", { id: id, value: value });
		$('#calendar').fullCalendar('refetchEvents');
	});

	$( "#reset_page" ).click(function() {
		    location.reload();
	});
	
	$( ".fc-button-agendaDay" ).click(function() {
		//alert('1111');
		//$('#calendar').fullCalendar('refetchEvents');
		$('#maps_block').hide();
		// $('.fc-header').width('1130px');
		// $('.fc-header').css( "max-width", "1130px" )		
		$("#map_day_container").css('display', 'block');
		$('#map_day').show();

		// not applicable for assign page
		if($("#calendar").parents('.assign').length === 0) {
			$("#calendar").css( "width", "49%" );
		}

		$('#calendar').fullCalendar('option', 'aspectRatio', .8);
		$('#calendar').fullCalendar('refetchEvents');
		$('#calendar').fullCalendar( 'rerenderEvents' );
		$('#calendar').fullCalendar('render');
		//alert('1111-1');

		$('.tech_team_members').show();
		
		mapPaint(eventsList, initAssign);
	});

	if($('.assign').length) {
		$( ".fc-button-agendaDay" ).trigger('click');
	}

	$( ".fc-button-agendaWeek" ).click(function() {
		//alert('2222');
		$('#calendar').fullCalendar('refetchEvents');
		$('#map_day').hide();
		$("#calendar" ).css( "width", "100%" );		
		$('#calendar').fullCalendar('render');
		$('#maps_block').show();

		$('.tech_team_members').hide();

		mapPaint(eventsList);
	});

	$( ".fc-button-month" ).click(function() {
		$('#maps_block').hide();
		$("#calendar" ).css( "width", "100%" );
		$('#calendar').fullCalendar('render');
		$('#map_day').hide();

		$('.tech_team_members').hide();

		mapPaint(eventsList);
	});

	$( "#rerender" ).click(function() {
		removeMarkers();
	});
	
	$( "#calendar" ).load(function() {
		//alert('3333');
		//$('#calendar').fullCalendar('refetchEvents');
		$('#calendar').fullCalendar('render');
		mapPaint(eventsList);
	});
	
	$( ".fc-button-next, .fc-button-prev, .fc-button-today" ).click(function() {
		////$('#calendar').fullCalendar('render');
		////$('#calendar').fullCalendar('refetchEvents');
			////$('#calendar').fullCalendar( 'rerenderEvents' );
		//if ($(".fc-button-agendaDay").hasClass("fc-state-active")) {
			//$('#calendar').fullCalendar('refetchEvents');
			//$('#calendar').fullCalendar( 'rerenderEvents' );
			//alert("refetched");
			//mapPaint(eventsList);
			//$('#calendar').fullCalendar('render');
		//}
	});

	$('.customer_type input[type="radio"]').change(function() {
		$('.existing-customer-row, .new-customer-row').hide();
		$('.'+this.value+'-customer-row').show();
		if(this.value === 'new') {
			$('.lead-info-note').attr({
				placeholder: 'Notes *',
				required: 1
			});
			$('#l_name2').attr('required', 'required');
			$('#f_name2').attr('required', 'required');
			$('#job_address2').attr('required', 'required');
		}
		else {
			$('.lead-info-note').attr({placeholder: 'Notes'}).removeAttr('required');
			$('#l_name2').removeAttr('required');
			$('#f_name2').removeAttr('required');
			$('#job_address2').removeAttr('required');
		}

		if(this.value === 'existing' && $('#myModal select[name="job_id"]').val() != 0 ) {
			$('#btnEditJobCal').show();
			$('#myModal #btnSchedEventCal').removeAttr('disabled');
		}
		else {
			if(this.value === 'new') {
				$('#myModal #btnSchedEventCal').removeAttr('disabled');
			} else {
				$('#myModal #btnSchedEventCal').attr('disabled', 'disabled');
			}
			$('#btnEditJobCal').hide();
		}
 	});

 	$('#myModal select[name="job_id"]').change(function(){
 		var jobIdSelected = $('#myModal select[name="job_id"]').val();
 		if( jobIdSelected != 0 ) {
			var link = "/job/"+jobIdSelected;
			//#btnEditJobCal : is the id of the 'Edit Job' button in the modal view of 'Calendar' page.
			$('#btnEditJobCal').show();
			$('#btnEditJobCal').attr("href", link);
			$('#myModal #btnSchedEventCal').removeAttr('disabled');
		}
		else{
			$('#btnEditJobCal').hide();
			$('#myModal #btnSchedEventCal').attr('disabled', 'disabled');
		}
 	});
	
 	$('#myModal').on('hidden.bs.modal', function() {
 		$('#myModal form').trigger('reset');
 		$('#multiDatePick').datepicker('update', '');
 		$('#myModal #btnSchedEventCal').attr('disabled', 'disabled');
 		$('#btnEditJobCal').hide();
 	});

	function mapPaint(events, _callback) {

		var mapEvents = [];
		mapEvents[1] = '';
		mapEvents[2] = '';
		mapEvents[3] = '';
		mapEvents[4] = '';
		mapEvents[5] = '';
		var mapEvents1 = [], mapEvents2 = [], mapEvents3 = [], mapEvents4 = [], mapEvents5 = [], map_day  = [];
		var start = Date.parse($('#calendar').fullCalendar('getView').visStart)/1000;
		var day2Start = start+86400;
		var day3Start = day2Start+86400;
		var day4Start = day3Start+86400;
		var day5Start = day4Start+86400;

		days = [
			new Date(start * 1000).toLocaleDateString(),
			new Date(day2Start * 1000).toLocaleDateString(),
			new Date(day3Start * 1000).toLocaleDateString(),
			new Date(day4Start * 1000).toLocaleDateString(),
			new Date(day5Start * 1000).toLocaleDateString()
		];
		
		var mapOptions = {};
		function initialize() {	
			mapOptions = {
				center: new google.maps.LatLng(34.052234, -118.243685),
				zoom: 8,
				streetViewControl: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			};
		};
		
		google.maps.event.addDomListener(window, 'load', initialize);
		
		if ( $(".fc-button-agendaWeek").hasClass("fc-state-active")) {
			$('#calendar').fullCalendar('render');
			var test = [];
			$(events).each(function(index) {
				this.start = moment(this.start).unix();
				this.end = moment(this.end).unix();
				if (( this.start >= start && this.start <= start+86399) || (this.end >= start && this.end <= start+86399)) {
					mapEvents1.push(this);
				} else if (( this.start >= day2Start && this.start <= day2Start+86399) || (this.end >= day2Start && this.end <= day2Start+86399)) {
					mapEvents2.push(this);
				} else if (( this.start >= day3Start && this.start <= day3Start+86399) || (this.end >= day3Start && this.end <= day3Start+86399)) {
					mapEvents3.push(this);
				} else if (( this.start >= day4Start && this.start <= day4Start+86399) || (this.end >= day4Start && this.end <= day4Start+86399)) {
					mapEvents4.push(this);
				} else if (( this.start >= day5Start && this.start <= day5Start+86399) || (this.end >= day5Start && this.end <= day5Start+86399)) {
					mapEvents5.push(this);
				} else {
					test.push(this);
				}
			});

			mapOptions['disableDefaultUI'] = true;
			
			initMapWithEvents(1, mapEvents1, mapOptions);
			initMapWithEvents(2, mapEvents2, mapOptions);
			initMapWithEvents(3, mapEvents3, mapOptions);
			initMapWithEvents(4, mapEvents4, mapOptions);
			initMapWithEvents(5, mapEvents5, mapOptions);

			eventsList = events;
			$('#calendar').fullCalendar('render');

			if(typeof _callback === 'function') _callback();
		}
		if ( $(".fc-button-agendaDay").hasClass("fc-state-active")) {
			$('#calendar').fullCalendar('render');
			var test = [];
			$(events).each(function(index) {
				this.start = moment(this.start).unix();
				this.end = moment(this.end).unix();
				if (( this.start >= start && this.start <= start+86399) || (this.end >= start && this.end <= start+86399)) {
					//mapEvents1.push(this);
					map_day.push(this);
				} else {
					test.push(this);
				}
			});
//			console.log("map_day = ", map_day);
			
//			console.log('start = '+start);
			
			mapDay(map_day, mapOptions);
			
			eventsList = events;
			$('#calendar').fullCalendar('render');

			if(typeof _callback === 'function') _callback();

		}

		setMapWidth();
	};

	$("#alert").collapse('show');

});

function initAssign() {
	// set layout
	$('.tech_team_members, #map_day_container').css({
		marginTop: $('.fc-header').height() + 'px',
		height: $('.fc-content').height() + 'px'
	});

	// init member drag
	$('.tech_team_members .member').draggable({ revert: true, helper: "clone", zIndex: 100 });

	window.setInterval(function() {
		$('.fc-event-container > .fc-event:not(.ui-droppable)').droppable({
			accept: ".member",
			tolerance: "pointer",
			over: function(event, ui) {
				$(this).css({'box-shadow': '0 0 10px #777'});
			},
			out: function(event, ui) {
				$(this).css({'box-shadow': 'none'});
			},
			drop: function(event, ui) {
				$(this).css({'box-shadow': 'none'});
				if($(this).find('.fc-event-time .assignees').length === 0) $('<div class="assignees"></div>').appendTo($(this).find('.fc-event-time'));
				if($(this).find('.fc-event-time .assignees span[member="' + ui.draggable.text() + '"]').length === 0) {
					$('<span profile=\''+ JSON.stringify(ui.draggable.data('profile')) +'\' member="'+ui.draggable.text()+'">'+ui.draggable.text()+'</span>').appendTo($(this).find('.fc-event-time .assignees').first());
					updateAssignee(this);
				}
			}
		});
	}, 500);

	// removing assignees
	$(document).on('click', '.fc-event-time .assignees span[member]', function() {
		if(window.confirm('Remove ' + $(this).attr('member') + ' from this event?')) {
			var eventElement = $(this).closest('.fc-event')[0];
			$(this).remove();
			updateAssignee(eventElement);
		}
		return false;
	});
	
}

function updateAssignee(_eventElement) {
	// ajax post to save this data
	var assignee = [];
	$(_eventElement).find('.fc-event-time .assignees span[member]').each(function() {
		var parsed = JSON.parse($(this).attr('profile'));
		assignee.push(parsed);
	});
	$.post('/event-assign', {id: $(_eventElement).data('event-db-id'), assignee: JSON.stringify(assignee)}, function(_data) {
//		console.log('Event Assignee Updated in DB');
	});
}

function onEventResize( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {
	eventChanged(event.google_cal_id, event.google_event_id, event.start, event.end);
}
function onEventMove( event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view ) {
	eventChanged(event.google_cal_id, event.google_event_id, event.start, event.end);
}
function eventChanged(_google_cal_id, _google_event_id, _start, _end) {
//	console.log(_google_cal_id, _google_event_id, _start.toUTCString(), _end.toUTCString());

	$.post('/event-update', {cal_id: _google_cal_id, event_id: _google_event_id, start: _start.toUTCString(), end: _end.toUTCString()}, function(_data) {
//		console.log('Event Updated in DB');
		//window.location.reload();
	});
}

function getDrivingDirections(_date, _mapNum) {
	for(var evUID in eventsByDay) {
		if(eventsByDay.hasOwnProperty(evUID)) {
			getDrivingDirectionsForUser(_date, _mapNum, evUID);
		}
	}
}

function getDrivingDirectionsForUser(_date, _mapNum, _evUID) {

	var origDate = _date;

	if(typeof eventsByDay[_evUID][_date] === 'undefined' || eventsByDay[_evUID][_date].length === 1) {
		window.alert('No driving directions available for selected user');
		return;
	}

	_date = eventsByDay[_evUID][_date];

	var origin = _date[0], dest = _date[_date.length - 1];

	var wayPoints = [];
	for(var i=0; i<_date.length - 1; i++) {
		if(!!_date[i].lat && !!_date[i].lng) {
			wayPoints.push({
				location: new google.maps.LatLng(_date[i].lat, _date[i].lng),
				stopover: true,
			});
		}
	}

	if(!wayPoints.length) {
		window.alert('No driving directions available for selected user');
		return;
	}

	var request = {
		origin: '3377 West Cerritos Avenue, Los Alamitos, CA 90720, United States', //new google.maps.LatLng(origin.lat, origin.lng),
		destination: new google.maps.LatLng(dest.lat, dest.lng),
		travelMode: google.maps.TravelMode.DRIVING,
		waypoints: wayPoints,
		provideRouteAlternatives: true
	};

	var directionsService = new google.maps.DirectionsService();
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			var route = response.routes[0];
			var html = [], prefix = '';
			html.push('<h2>Route directions for '+ origDate +'</h2>')
			for (var i = 0; i < route.legs.length; i++) {
				if(i === 0) html.push('<h3><b>START - </b>' + route.legs[i].start_address +'</h3>');
				html.push('<div>' + route.legs[i].steps.map(function(_x) { return _x.instructions; }).join('<br>') + '</div>');
				if(i < route.legs.length - 1) html.push('<h3><b>APPT - ' + _date[i].time + ' - </b>' + route.legs[i].end_address +'</h3>');
			}
			html.push('<h3><b>APPT - ' + _date[route.legs.length - 1].time + ' - </b>'+ route.legs[route.legs.length - 1].end_address +'</h3>')
			$('#directions').html(html.join(''));
			$('html, body').animate({scrollTop: ($('#directions').offset().top - $('.navbar').first().height()) + 'px'}, '400');
		}
		else {
			window.alert('No driving directions available for selected user');
		}
	});

}

function showRoutes(_date, _mapNum) {
	for(var evUID in eventsByDay) {
		if(eventsByDay.hasOwnProperty(evUID)) {
			showRoutesForUser(_date, _mapNum, evUID);
		}
	}
}

function showRoutesForUser(_date, _mapNum, _evUID) {

	var origDate = _date;

	if(typeof eventsByDay[_evUID][_date] === 'undefined' || eventsByDay[_evUID][_date].length === 1) return;

	_date = eventsByDay[_evUID][_date];

	var origin = _date[0], dest = _date[_date.length - 1];

	var wayPoints = [];
	for(var i=0; i<_date.length - 1; i++) {
		if(!!_date[i].lat && !!_date[i].lng) {
			wayPoints.push({
				location: new google.maps.LatLng(_date[i].lat, _date[i].lng),
				stopover: true,
			});
		}
	}

	if(!wayPoints.length) return;

	var startLocation = '3377 West Cerritos Avenue, Los Alamitos, CA 90720, United States';
	if($('#start-location-' + _evUID).length) {
		startLocation = $('#start-location-' + _evUID).val();
	}

	var request = {
		origin: startLocation,
		destination: new google.maps.LatLng(dest.lat, dest.lng),
		travelMode: google.maps.TravelMode.DRIVING,
		waypoints: wayPoints,
		provideRouteAlternatives: true
	};

	var directionsService = new google.maps.DirectionsService();
	var color = $('#color-' + _evUID).val();
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			var directionsDisplay = new google.maps.DirectionsRenderer({
				suppressMarkers: true,
				polylineOptions: {
					strokeColor: color,
					strokeOpacity: 0.6,
					strokeWeight: 4
				}
			});
			directionsDisplay.setMap(map[_mapNum]);
			directionsDisplay.setDirections(response);	
		}
		else {
			console.error('Possibly invalid/incomplete waypoints.');
		}
	});
}

function fitMapToMarkers(_date, _mapNum) {
	if(!eventsByDay[_date]) return;
	map[_mapNum].setZoom(1);
	var locations = [];
	for(var i=0; i<eventsByDay[_date].length; i++) {
		locations.push(new google.maps.LatLng(eventsByDay[_date][i].lat, eventsByDay[_date][i].lng));
	}
	var bounds = new google.maps.LatLngBounds();
	for(i=0;i<locations.length;i++) {
		bounds.extend(locations[i]);
	}
	map[_mapNum].fitBounds(bounds);
}

function closeEventHoverDropDown() {
	if(currentEventElement) currentEventElement.removeClass('infocus');
	$('#event_modd').hide();
}

function getOppositeColor(_color) {
	var r, g, b, defaultColor = '#000000';
	if(_color.indexOf('#') !== 0) return defaultColor;

	_color = _color.substr(1);

	if(_color.length === 3) {
		r = _color.substr(0, 1) + _color.substr(0, 1);
		g = _color.substr(1, 1) + _color.substr(1, 1);
		b = _color.substr(2, 1) + _color.substr(2, 1);
	}
	else if(_color.length === 6) {
		r = _color.substr(0, 2);
		g = _color.substr(2, 2);
		b = _color.substr(4, 2);
	}
	else return defaultColor;

	return [
		'#',
		getOppositeColorPart(r), 
		getOppositeColorPart(g), 
		getOppositeColorPart(b)
	].join('');
}
function getOppositeColorPart(_part) {
	var opp = (255 - parseInt(_part, 16)).toString(16);
	if(opp.length < 2) opp = '0' + opp;
	return opp;
}

$('#datePick').datepicker({
	    autoclose: true,
		todayHighlight: true
});

$('#eventModal').on('hidden.bs.modal', function () {
	if($('#eventData').hasClass("in")) {
		$('#eventData form').trigger('reset');	
		$('#eventData').collapse('hide');
	}

	if($('#dateSelect').hasClass("in")) {
		$('#dateSelect form').trigger('reset');
		$('#dateSelect').collapse('hide');	
	}
});

$('#eventEdit').click(function () {
	$('#dateSelect').collapse({'toggle': false});
});

$('#btnEditSubmit').click(function (event) {
	var data = {};
	data['google_cal_id'] = $('#selectCalendar').val();
	data['type'] = $('#selectEvent').val();
	data['eventID'] = $('#hiddenEventID').val();
	
	$('#eventModal').modal('hide');

	$.ajax({
		type: "POST",
	    url: "/event-edit-app",
	    data: data,
	    success: function (result) {
			if(result.status == 'success') {
				$('#calendar').fullCalendar( 'refetchEvents' );

				data['old_google_cal_id'] = result.old_google_cal_id;

				$.ajax({
					type: "POST",
					url: "/event-edit-goog",
					data: data,
					success: function (result2) {
						if(result2.status == 'success') {
							$('#msgSuccess').text(result2.message);
							$('#alertSuccess').collapse('toggle');
							setTimeout(function() {
								$('#alertSuccess').collapse('toggle');
							}, 5000);								
						} 
						else {
							$('#msgFail').text(result2.message);
							$('#alertFail').collapse('toggle');
							setTimeout(function() {
								$('#alertFail').collapse('toggle');
							}, 5000);								
						}
					}
				});
			}
			else {
				$('#msgFail').text(result.message);
				$('#alertFail').collapse('toggle');
				setTimeout(function() {
					$('#alertFail').collapse('toggle');
				}, 5000);					
			}
		}
	});
});


$('#btnDupSubmit').click(function (event) {
	var data = {};
	data['date'] = $('#datePick').val();
	data['eventID'] = $('#hiddenEventID').val();
	
	$('#eventModal').modal('hide');
	
	$.ajax({
		type: "POST",
	    url: "/event-duplicate-app",
	    data: data,
	    success: function (result) {
			if(result.status == 'success') {
				$('#calendar').fullCalendar( 'refetchEvents' );
				data['eventID'] = result.eventID;
				$.ajax({
					type: "POST",
					url: "/event-duplicate-goog",
					data: data,
					success: function (result2) {
						if(result2.status == 'success') {
							$('#msgSuccess').text(result.message);
							$('#alertSuccess').collapse('toggle');
							setTimeout(function() {
								$('#alertSuccess').collapse('toggle');
							}, 3000);
						} else {
							$('#msgFail').text(result2.message);
							$('#alertFail').collapse('toggle');
							setTimeout(function() {
								$('#alertFail').collapse('toggle');
							}, 5000);
						}
					}
				});
			} else {
				$('#msgFail').text(result.message);
				$('#alertFail').collapse('toggle');
				setTimeout(function() {
					$('#alertFail').collapse('toggle');
				}, 5000);
			}
		}
	});
});

$('#btnDelete').click(function() {
	$('#eventModal').modal('hide');
    $('#modalYouSure').modal();
	
	$('#btnConfirmDelete').click(function() {
		eventID = $('#hiddenEventID').val();
		$('#modalYouSure').modal('hide');
		$.ajax({
			type: "GET",
			url: "/events/delete/"+eventID,
			success: function (result) {
				$('#msgSuccess').text(result.message);
				$('#calendar').fullCalendar( 'refetchEvents' );
				setTimeout(function() {
					$('#alertSuccess').collapse('toggle');
				}, 3000);
			}
		});
	});
});

$('#btnMultiDay').click(function (event) {
	var data = {};
	data['date'] = $('#datePick').val();
	data['eventID'] = $('#hiddenEventID').val();
	
});

$('#multiDatePick').datepicker({
		todayHighlight: true,
//	    autoclose: true,
		multidate: true
});