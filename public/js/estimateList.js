function returnID(elem)
{
	// Identify the element from the event
	var elementID = $(elem).attr("id");
	
	// Break out the # from the element's ID
	// ie. expandBtn4 = 4
	var split = elementID.split(/(\d+)/);
	
	return split[1];
}

$(document).ready(function(){
	$("[id^='btnArchive']").click(function () {
		var itemNum = returnID(this);
		$('#modalJobID').val(itemNum);
		$('#archiveNote').modal('toggle');
	});

});
