$(window).load(function() {
	$("#alert").collapse('show');
	$('.add-new-template').click(function() {
		showTemplateForm();
		return false;
	});
	$('#btnCancelTemplate').click(function() {
		$('.add-template').slideUp('fast');
	});
	$('.items .item').click(function() {
		$(this).addClass('selected').siblings().removeClass('selected');
		selectTemplate($(this).data('id'));
	});
	$('#btnEditTemplate').click(function() {
		var selected = $('.items .item.selected').first();
		if(selected.length) {
			showTemplateForm(selected.data('id'));
		}
	});
	$('#btnDeleteTemplate').click(function() {
		var selected = $('.items .item.selected').first();
		if(selected.length) {
			deleteTemplate(selected.data('id'));
		}
	});
	if($('.items .item').length) {
		$('.items .item').first().addClass('selected');
		selectTemplate($('.items .item').first().data('id'));
	}
	$('#btnAddTemplate, #btnUpdateTemplate').click(function() {
		$('.add-template').find('[name="text"]').val($('#email_content').html());
	})
});

function showTemplateForm(_id) {
	var parent = $('.add-template');
	if(_id) {
		parent.find('h4').text('Edit Template');
		parent.find('[name="id"]').val(_id);
		parent.find('[name="name"]').val($.trim($('.item[data-id="'+_id+'"]').text()));
		parent.find('#email_content').html($('#itemPreview'+_id).html());
		parent.find('[name="text"]').val($('#itemPreview'+_id).html());
		parent.find('#btnAddTemplate').hide();
		parent.find('#btnUpdateTemplate').show();
	}
	else {
		parent.find('h4').text('Add Template');
		parent.find('[name="id"]').val('0');
		parent.find('[name="name"]').val('');
		parent.find('#email_content').html('');
		parent.find('[name="text"]').val('');
		parent.find('#btnUpdateTemplate').hide();
		parent.find('#btnAddTemplate').show();
	}
	parent.slideDown('fast', function() {
		parent.find('[name="name"]').focus().select();
	});
}
function deleteTemplate(_id) {
	if(window.confirm('Are you sure you want to delete this template?')) {
		var form = $('<form/>')
			.attr('method', 'post')
			.attr('action', '/email-templates/delete')
			.append($('<input type="hidden" name="id" />').val(_id))
			.append($('<input type="hidden" name="src" />').val($('input[name="src"]').val()))
			.submit();
	}
}
function selectTemplate(_id) {
	if($('#itemPreview' + _id).is(':visible')) return;
	$('.item-preview:visible').hide();
	$('#itemPreview' + _id).fadeIn('fast');
}