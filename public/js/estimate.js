var service = new Array();
var rootEvent = false;

function returnID(elem)
{
	// Identify the element from the event
	var elementID = $(elem).attr("id");
	
	// Break out the # from the element's ID
	// ie. expandBtn4 = 4
	var split = elementID.split(/(\d+)/);
	
	return split[1];
}

function expandItem(elem)
{
	var itemNum = returnID(elem);
	$('#subGroup'+itemNum).collapse('toggle');
	$("#expandGlyph"+itemNum).toggleClass("glyphicon-chevron-up glyphicon-chevron-down");
}

function thick(elem)
{
	var itemNum = returnID(elem);
	$(elem).addClass("active");
	$('#sashThin'+itemNum).removeClass("active");
}
	
function thin(elem)
{
	var itemNum = returnID(elem);
	$(elem).addClass("active");
	$('#sashThick'+itemNum).removeClass("active");
}

function winStyleChange(elem, _dontResetServicePrice)
{
	var itemNum = returnID(elem);

	$('#shortService'+itemNum).empty();
	if(!_dontResetServicePrice) {
		$('#servPrice'+itemNum).val('');
		$('#description'+itemNum).val('');
	}
	
	var styleSelect = elem;
	var styleID = styleSelect.options[styleSelect.selectedIndex].value;
	var balancerSelect = $('#balancer' + itemNum).html('<option value="0">N/A</option>').prop('disabled', true);
	
	if(Number(styleID) !== 0) {

		// setup balancer-type dropdown for selected winStyle
		if(winStylesWithBalancer.indexOf(Number(styleID)) !== -1) {
			balancerSelect.append(balancers.map(function(_x) {
				return '<option value="' + _x.id + '">' + _x.name + '</option>';
			}).join('')).prop('disabled', false);
			if(balancerSelect.data('loaded-value')) {
				$('option[value='+balancerSelect.data('loaded-value')+']', balancerSelect).prop('selected', true);
				balancerSelect.removeData('loaded-value');
			}
		}

		// get service options for selected winStyle
		var select = document.getElementById("shortService"+itemNum);
		$.getJSON( "/api/estimate/style/"+styleID, function(data) {
			$.each( data.data, function(i, val) {
				service[val.id] = val;
				var text = val.title;
				var value = val.id;
				var el = document.createElement("option");
				el.textContent = text;
				el.setAttribute('data-description', val.description);
				el.value = value;
				if($(select).data('loaded-value') == value) {
					el.selected = true;
				}
				select.appendChild(el);
			});
			serviceChange(elem, _dontResetServicePrice);
		}).fail(function() {
			alert( "error" );
		});
	}
	else {
		serviceChange(elem, false);
	}
}

function serviceChange(elem, _dontResetServicePrice) {

	var itemNum = returnID(elem);

	$('#shortService'+itemNum).prop('disabled', false);
	$('#servPrice'+itemNum).prop('disabled', false);

	var serviceID = $("select.service", $(elem).closest('.clonedSection')).first().val();

	if(typeof service[serviceID] !== 'undefined') {
		$('#price'+itemNum).val(service[serviceID].price);
		if(!_dontResetServicePrice) {
			$('#servPrice'+itemNum).val(service[serviceID].price);
			$('#description'+itemNum).val(service[serviceID].description);
		}
		$('#description'+itemNum).prop('disabled', false);

		if(servicePriceOrig != null) {
			$('#servPrice'+itemNum).val(servicePriceOrig);
			servicePriceOrig = null;
		}

		if(serviceDescOrig != null) {
			$('#description'+itemNum).val(serviceDescOrig);
			serviceDescOrig = null;
		}

		// clear options, populate options based on chosen service
		$('#options' + itemNum).find('.option-chooser option').hide().filter('[data-service='+ serviceID +'], :not([data-service])').show().first().prop('selected', true);
		$('#options' + itemNum).find('.optionsPrice').val('');

		// populate options if loading/cloning
		if(_dontResetServicePrice || useExistingServiceOptions === true) {
			if($.trim($('#servOptions' + itemNum).val()) != '') {
				var options = $('#servOptions' + itemNum).val().split(',');
				options.forEach(function(_value) {
					_value = _value.split('-');
					addOption(itemNum, _value[0], (_value.length === 2 ? _value[1] : undefined), true);
				});
			}
		}

		// clear previously added service options
		if(rootEvent === 'select') {
			$('#servOptions' + itemNum).val('');
			$('#options' + itemNum + ' .service-options').html('<span class="none" style="display: block;">No options chosen</span>');
		}

		useExistingServiceOptions = false;

		reprice(elem);

		if(doRenumbering) {
			renumberItems();
			doRenumbering = false;
		}
	
		// get associated parts
		addAssociatedParts('service', serviceID, itemNum);

	}
	else {
		$('#price'+itemNum).val('');
		$('#servPrice'+itemNum).val('');
		$('#description'+itemNum).val('');
	}

	reTotal();
	
}

function addAssociatedParts(mappingType, itemID, itemNum) {

	// don't do on page-load or cloning
	if(rootEvent === 'page-load' || rootEvent === 'clone') return;

	var itemParts = $('#itemparts' + itemNum).val();
	if(itemParts) {
		itemParts = JSON.parse(itemParts);
		// if mappingType is 'service', empty parts json for this line item
		if(mappingType === 'service') {
			itemParts = [];
		}
	}
	else {
		itemParts = [];
	}

	// save it now
	$('#itemparts' + itemNum).val(JSON.stringify(itemParts));

	// update parts column
	populatePartsColumns();

	$.getJSON( "/estimate/" + mappingType + "-parts/" + itemID, function(data) {

		if(data && data.length) {
			processMappedPartsRecursive(data, 0, mappingType, itemID, itemNum, itemParts);
		}

	});
}

function processMappedPartsRecursive(parts, idx, mappingType, itemID, itemNum, itemParts) {

	var part = parts[idx];

	// to check later, store type of entity this part is mapped to and its id
	part.mappingType = mappingType;
	part.mappingID = itemID;

	// check if this same part is already there (with possibly different options)
	var found = false, existingPart = false;
	
	// look for part with same id
	for(i=0; i<itemParts.length; i++) {
		existingPart = itemParts[i];
		if(existingPart.id === part.id) {
			found = true;
			break;
		}
	}

	// confirm with user whether to add again
	if(found) {
		var partDisplayName = existingPart.name;
		if(existingPart.options && existingPart.options.length) {
			partDisplayName += ' - ';
			for(i=0; i<existingPart.options.length; i++) {
				partDisplayName += existingPart.options[i].name + ': ' + existingPart.options[i].value + (i < existingPart.options.length - 1 ? ', ' : '');
			}
		}
		if(!window.confirm('The part "' + partDisplayName + '" already exists in the parts list for this line item. Add another?')) {
			if(idx < parts.length - 1) processMappedPartsRecursive(parts, idx + 1, mappingType, itemID, itemNum, itemParts);
			else return;
		}
	}

	// to be called once all option values for this part are obtained
	var allOptionValuesObtained = function() {
		itemParts.push(part);
		$('#itemparts' + itemNum).val(JSON.stringify(itemParts));
		populatePartsColumns();
		if(idx < parts.length - 1) processMappedPartsRecursive(parts, idx + 1, mappingType, itemID, itemNum, itemParts);
	};

	// check if part has any mandatory options, if yes, get the values
	if(part.options && part.options.length) { 
		processOptionValuesRecursive(part, 0, allOptionValuesObtained);
	}
	else {
		allOptionValuesObtained();
	}

}

function processOptionValuesRecursive(_part, _idx, _completedCallback) {

	var value = false;

	getPartOptionValue(_part, _part.options[_idx], function(_value) {
		value = _value;
		if(value === '') value = _part.options[_idx].defaultValue;

		_part.options[_idx].value = value;

		if(_idx < _part.options.length - 1) processOptionValuesRecursive(_part, _idx + 1, _completedCallback);

		else _completedCallback();

	});

}

function getPartOptionValue(_part, _option, _callback) {

	// part-options with no option values now supported	
	if(!_option['values'] || !_option.values.length) return _callback('', _part, _option);

	var modal = $('#modalPartOptionValue').clone(true);
	modal.find('.modal-title').text(_part.name);
	modal.find('.modal-heading').text(_option.name);
	modal.find('.choosable-options').empty().append(
		_option.values.map(function(_x) {
			return '<li onclick="return partOptionValueSelected.call(this, this.innerHTML)" class="' + (_x === _option.defaultValue ? 'default' : '') + '">' + _x + '</li>';
		}).join('')
	);
	modal.data({
		part: _part,
		option: _option,
		callback: _callback
	})
	modal.modal('show');
}

function partOptionValueSelected(_value) {
	var modal = $(this).closest('.modal');
	modal.modal('hide');
	var callback = modal.data('callback');
	callback(_value, modal.data('part'), modal.data('option'));
	return false;
}

function removeAssociatedParts(mappingType, mappingID, itemNum) {
	var itemParts = $('#itemparts' + itemNum).val();
	if(itemParts) {
		itemParts = JSON.parse(itemParts);
		// remove the mathcing parts
		itemParts = itemParts.filter(function(_x) {
			return !(_x.mappingType == mappingType && _x.mappingID == mappingID);
		});
	}
	else {
		itemParts = [];
	}
	// save it now
	$('#itemparts' + itemNum).val(JSON.stringify(itemParts));
	// update parts column
	populatePartsColumns();
} 

function reprice(elem)
{

	var itemNum = returnID(elem);
	
	var servicePrice = parseInt(document.getElementById("servPrice"+itemNum).value);
	
	var optionsPrice = 0;
	$('#options' + itemNum + ' .service-option-line .option-price').each(function() {
		optionsPrice += Number($(this).val());
	});
	
	var totalPrice = 0;
	totalPrice = servicePrice + optionsPrice;
	$('#price'+itemNum).val(totalPrice);
	
	reTotal();
}

function reTotal()
{

	var totalPrice = 0;
//	var items = $('div#parent > div');
	$('div[id^=item_group]:not(.markupTemplate)').each(function( index ) {
		var status = Number($("select.status", this).val());
		if(status === 1) { // include only 'pending' items in total
			var num = $(this).data('id');
			if(!num) num = $(this).attr('data-id');
			var linePrice = Number($("input.price", this).val());
			if(isNaN(linePrice)) linePrice = 0;
			totalPrice = totalPrice + linePrice;
		}
	});
	$('#totalPrice').val(totalPrice);

	// if deposit not manually set, set as 10% of total!
	var depositElem = $('#deposit');
	var deposit = parseInt(depositElem.val());
	deposit = isNaN(deposit) ? 0 : deposit;
	if(!depositElem.attr('data-modified')) {
		deposit = Math.round(totalPrice*-.1);
		depositElem.val(deposit);
	}
	$('#balance').val(totalPrice+deposit);
}

function renumberItems()
{
	var items = $('div#parent div.clonedSection');
	
	// renumber IDs
	$.each(items, function(i, item) {
		this.id = 'item_group_' + (i+1);
		var elements = $(item).find('[id]');
		$.each(elements, function(n, elem) {
			var elemID = elem.id;
			var split = elemID.split(/(\d+)/);
			var elemName = split[0];
			var elemNum = i+1;
			if(elemName != '') elem.id = elemName+elemNum;
			if(elemName === 'item') {
				elem.value = elemNum;
			}
		});
	});

	// renumber name attribute
	$.each(items, function(i, item) {
		var elements = $(item).find('[name]');
		$.each(elements, function(n, elem) {
			var elemNum = i+1;
			var elemName = elem.name;
			if(elemName.match(/^item\[\d+\]\[/)) {
				elemName = elemName.replace(/\[\d+\]/, '[' + elemNum + ']');
				elem.name = elemName;
			}
		});
	});
}

//Hides the Remove button if and only if one row is present.
function hideRemoveButton() {
	var rowCount = $('#parent div.clonedSection').length;
	if(rowCount == 1) {
		$("#parent :button[id^=closeItem]").hide();
	}
}

//Shows the Remove button if and only if more than one row is present.
function showRemoveButton() {
	var rowCount = $('#parent div.clonedSection').length;
	if(rowCount == 1) {
		$("#parent :button[id^=closeItem]").show();
	}
}

$(document).ready(function(){
	service = {};

	$('#partsModalBody').on('change', '.manual-part-chooser', function() {

		$(this).closest('label').siblings().remove();

		if(this.value == '0') return false;

		// if value is 'other', get name and description from user
		if(this.value == 'other') {
			var partName = prompt('Part name:'), partDesc = '', partQty = 1;
			if(partName && $.trim(partName) !== '') {
				partDesc = prompt('Description/Specifics (optional):');
				if(!partDesc || $.trim(partDesc) === '') partDesc = '';

				partQty = prompt('Quantity:', partQty);
				while(!partQty || $.trim(partQty) === '' || /^[\d+]$/.test(partQty) !== true) {
					partQty = prompt('Invalid quantity apecified. Enter a valid quantity:', partQty);
				}
				partQty = parseInt(partQty, 10);

				// add right away
				var loadedParts, itemParts, part = {};
				if($('#partsModalBody').is('[data-section-id]')) {
					loadedParts = $('#' + $('#partsModalBody').attr('data-section-id')).find('.loaded-parts');
					itemParts = loadedParts.val();
					if(!itemParts) itemParts = [];
					else itemParts = JSON.parse(itemParts);
				}
				else {
					itemParts = estimateParts;
				}

				part.name = partName;
				part.description = partDesc;
				part.expected_qty = partQty;
				part.options = [];

				// mark as custom part
				part.other = true;

				// mark this part as manually added (as oppposed to auto-included due to a service/service-option)
				part.manual = true;

				// assign a unique manual part id - for later identifying and removing this part if needed
				part.manual_part_id = Math.floor((Math.random() * 100000) + 1);

				itemParts.push(part);

				if($('#partsModalBody').is('[data-section-id]')) {
					loadedParts.val(JSON.stringify(itemParts));
					populatePartsColumns();
				}

				// reload the modal (hide, reload and show)
				$('#modalParts').modal('hide');
				if($('#partsModalBody').is('[data-section-id]'))
					showItemParts.call($('#' + $('#partsModalBody').attr('data-section-id')).find('.view-item-parts-link')[0]);
				else
					showAllParts();
			}
			else {
				// reload the modal (hide, reload and show)
				$('#modalParts').modal('hide');
				if($('#partsModalBody').is('[data-section-id]'))
					showItemParts.call($('#' + $('#partsModalBody').attr('data-section-id')).find('.view-item-parts-link')[0]);
				else
					showAllParts();
				return false;
			}
		}

		// if a defined part is selected
		else { 

			// get the selected part from allParts
			var part = false, i = 0;
			for(i=0; i<allParts.length; i++) {
				if(allParts[i].id == this.value) {
					part = allParts[i];
					break;
				}
			}
			// part not found in allParts - SHOULD NEVER HAPPEN
			if(!part) return false;

			// get its mandatory options, and add a chooser for each
			var option = false, optionChooser = false;
			if(part.options && part.options.length) {
				for(i=0; i<part.options.length; i++) {
					option = part.options[i];
					if(!option.values || !option.values.length) option.values = [];
					optionChooser = $('<select class="option-chooser" data-option="' + option.name + '" />').html(
						option.values.map(function(_x) {
							return '<option value="' + _x + '" ' + (option.defaultValue == _x ? 'selected' : '') + '>' + _x + '</option>';
						}).join('')
					);
					$('<label><span>' + option.name + '</span></label>').append(optionChooser).insertAfter($(this).closest('label'));
				}
			}

		}

		// quantity chooser
		$('<label><span>Qty</span></label>').append($('<select/>').addClass('qty').html(
			[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(function(_x) {
				return '<option value="' + _x + '">' + _x + '</option>';
			}).join('')
		)).appendTo($(this).closest('label').parent());


		// the 'add' button
		$('<label></label>').append('<button class="btn btn-xs btn-primary" id="btnAddPartManual">Add</button>').appendTo($(this).closest('label').parent());

		// the 'cancel' button
		$('<label></label>').append('<button class="btn btn-xs btn-primary" id="cancelAddPartManual">Cancel</button>').appendTo($(this).closest('label').parent());
	});

	$('#partsModalBody').on('click', '#btnAddPartManual', function() {

		var loadedParts, itemParts;

		if($('#partsModalBody').is('[data-section-id]')) {
			loadedParts = $('#' + $('#partsModalBody').attr('data-section-id')).find('.loaded-parts'),
			itemParts = loadedParts.val();
			if(!itemParts) itemParts = [];
			else itemParts = JSON.parse(itemParts);
		}
		else {
			itemParts = estimateParts;
		}

		// get the selected part from allParts
		var partID = $('.new-part-row .manual-part-chooser').val(),
			part = false, 
			i = 0;
		for(i=0; i<allParts.length; i++) {
			if(allParts[i].id == partID) {
				part = allParts[i];
				break;
			}
		}
		if(!part) return false; // part not found in allParts - SHOULD NEVER HAPPEN

		// fill the selected option values into part
		$('.new-part-row .option-chooser').each(function() {
			for(i=0; i<part.options.length; i++) {
				if(part.options[i].name == $(this).data('option')) {
					part.options[i].value = $(this).val();
				}
			}
		});

		// set qty
		part.expected_qty = parseInt($('.new-part-row .qty').val(), 10);

		// mark this part as manually added (as oppposed to auto-included due to a service/service-option)
		part.manual = true;

		// assign a unique manual part id - for later identifying and removing this part if needed
		part.manual_part_id = Math.floor((Math.random() * 100000) + 1);

		// save it back to the DOM element for submission
		itemParts.push(part);

		if($('#partsModalBody').is('[data-section-id]')) {
			loadedParts.val(JSON.stringify(itemParts));
			populatePartsColumns();
		}

		// reload the modal (hide, reload and show)
		$('#modalParts').modal('hide');
		if($('#partsModalBody').is('[data-section-id]')) {
			showItemParts.call($('#' + $('#partsModalBody').attr('data-section-id')).find('.view-item-parts-link')[0]);
		}
		else {
			showAllParts();
		}

	});

	$('#partsModalBody').on('click', '#cancelAddPartManual', function() {
		var parent = $(this).closest('label').parent();
		parent.find('label:first').siblings().remove();
		parent.find('select option:first').attr('selected', 'selected');
	});

	$('#partsModalBody').on('click', '.btn-remove-manual-part', function() {

		var loadedParts, itemParts;

		if($(this).data('section-id') !== 'common') {
			loadedParts = $('#' + $(this).data('section-id')).find('.loaded-parts'),
			itemParts = loadedParts.val();
			if(!itemParts) itemParts = [];
			else itemParts = JSON.parse(itemParts);
		}
		else {
			itemParts = estimateParts;
		}

		// find the manual-part to be removed using manual-part-id and remove it from the form element's value
		var foundAt = false, i = 0;
		for(i=0; i<itemParts.length; i++) {
			if(itemParts[i]['manual_part_id'] == $(this).data('manual-part-id')) {
				foundAt = i;
				break;
			}
		}
		if(foundAt === false) return false; // part not found in itemParts, SHOULD NEVER HAPPEN

		// remove the part from itemParts
		itemParts.splice(foundAt, 1);

		// save it back to the DOM element for submission
		if($(this).data('section-id') !== 'common') {
			loadedParts.val(JSON.stringify(itemParts));
			populatePartsColumns();
		}

		// reload the modal (hide, reload and show)
		$('#modalParts').modal('hide');
		if($('#partsModalBody').is('[data-section-id]')) {
			showItemParts.call($('#' + $('#partsModalBody').attr('data-section-id')).find('.view-item-parts-link')[0]);
		}
		else {
			showAllParts();
		}
	});

	$('#partsModalBody').on('click', '.show-line-parts', function() {
		$('#modalParts').modal('hide');
		showItemParts.call($('#item_group_' + $(this).data('line-id')).find('.view-item-parts-link')[0]);
		return false;
	});

	$('#modalParts').on('click', '.show-all-parts', function() {
		$('#modalParts').modal('hide');
		showAllParts();
		return false;
	});

	$('#modalParts').on('click', '.btn-finalize-parts-list', function() {
		var button = $(this);

		ajaxSaveEstimate(function() {
			$.post('/parts-list/finalize', {id: estimatePartsList.id}, function(_data) {
				if(_data === 'OK') {
					window.location.href = '/estimates/edit/' + $('input[name=est_id]').first().val() + '?ap=1';
				}
				else {
					alert(_data);
				}
			});
		});

		return false;
	});
	$('#modalParts').on('click', '.btn-unfinalize-parts-list', function() {
		var button = $(this);
		
		ajaxSaveEstimate(function() {
			$.post('/parts-list/unfinalize', {id: estimatePartsList.id}, function(_data) {
				if(_data === 'OK') {
					window.location.href = '/estimates/edit/' + $('input[name=est_id]').first().val() + '?ap=2';
				}
				else {
					alert(_data);
				}
			});
		});

		return false;
	});

	$('#modalParts').on('click', '.print-all-parts', function() {
		var url = this.href;

		ajaxSaveEstimate(function() {
			window.location.href = url;
		});

		return false;
	});

    $('#btnPrintEstimate').click(function () {
		var estID = $('#jobID').val();
		window.location.href = '/frame/estimate/46';
	});

    $('.addItem').click(function () {
    	showRemoveButton();
		addItem(); // function defn in itemAdd.js
	});

	$('body').on('click','[id^=closeItem]',function (e) {
		$(this).closest('.clonedSection').remove();
		renumberItems();
		reTotal();
		hideRemoveButton();
    });

	$("#parent").on('click', '[id^="expandItem"]', function() {
		expandItem(this);
	});

	$("#parent").on('click', '[id^="cloneItem"]', function() {
		cloneItem($(this).closest('.clonedSection'));
	});
	
	$("#parent").on('change', '[id^="shortService"]', function() {
		rootEvent = 'select';
		serviceChange(this, false, false);
	});

	$("#parent").on('change', '[id^="winStyle"]', function() {
		rootEvent = 'select';
		winStyleChange(this, false, false);
	});

	$("#parent").on('change', '[id^="status"]', function() {
		rootEvent = 'select';
		serviceChange($(this).closest('.clonedSection').find('select.service').first()[0], false, false);
	});

	$('#parent').on('keyup', '.itemNum', function() {
		$(this).next().val(this.value);
	});

	$('#parent').on('keyup', '.servPrice', function() {
		reprice($(this).closest('.subGroup')[0]);
	});

	$('#deposit').on('keyup', function() {
		$(this).attr('data-modified', 1);
		reTotal();
		$('#btnResetDeposit').show();
	});

	$('#btnResetDeposit').on('click', function() {
		$('#deposit').removeAttr('data-modified');
		reTotal();
		$('#btnResetDeposit').hide();
		return false;
	});

	$('#parent').on('keyup', '.service-option-line .option-price', function() {
		var itemNum = $(this).closest('.custom-fieldset')[0].id.replace('options', '');
		var options = [];
		$(this).closest('.custom-fieldset').find('.option-name[data-option-id]').each(function() {
			options.push($(this).data('option-id') + '-' + $(this).closest('.service-option-line').find('.option-price').val());
		});
		$('#servOptions' + itemNum).val(options.join(','));
		reprice($(this).closest('.subGroup')[0]);
	});

	$('#parent').on('change', '.balancer', function() {
		$(this).removeData('loaded-value').removeAttr('data-loaded-value');
	});

	$('#parent').on('change', '.option-chooser', function() {
		if($(this).find('option:selected')[0].value == '0') return;
		$(this).closest('.new-service-option').find('.optionsPrice').val($(this).find('option:selected').data('price'));
		$(this).closest('.new-service-option').find('.btn-add-option').trigger('click');
	});

	$('#parent').on('click', '.btn-add-option', function() {
		var par = $(this).closest('.new-service-option'),
			option = par.find('select.option-chooser option:selected'),
			price = par.find('.optionsPrice'),
			itemNum = par.closest('.custom-fieldset')[0].id.replace('options', '');

		rootEvent = 'add-option';

		if(option.data('service') != 0 && price.val() != '') {
			addOption(itemNum, option[0].value, false, false);
			par.find('select.option-chooser option:first').prop('selected', true);
			par.find('.optionsPrice').val('');
			reprice($('#options' + itemNum)[0]);
			reTotal();
		}
		else {
			console.log('No option selected!');
		}
		return false;
	});

	$('#parent').on('click', '.btn-remove-option', function() {
		var par = $(this).closest('.service-option-line'),
			optionID = par.find('.option-name').data('option-id'),
			itemNum = par.closest('.custom-fieldset')[0].id.replace('options', '');

		var options = $('#servOptions' + itemNum).val().split(',');
		options = options.filter(function(_value) {
			return _value.indexOf(optionID + '-') === -1;
		});
		$('#servOptions' + itemNum).val(options.join(','));

		if($(this).closest('.service-options').find('.service-option-line').length === 1) {
			$(this).closest('.service-options').find('.none').show();
		}
		par.remove();

		removeAssociatedParts('option', optionID, itemNum);

		reprice($('#options' + itemNum)[0]);
		reTotal();

		return false;
	});

	$('#parent').sortable({
		handle: '.estimate-drag-handle',
		stop: function(event, ui) {
			renumberItems();
		}
	});

	// load email templates for user
	refreshEmailTemplates();

	$('#refreshTemplates').click(function() {
		refreshEmailTemplates();
		return false;
	});

	$('#email_template').change(function() {
		var option = $(this).find('option:selected');
		if(option.attr('value') == 0) {
			$('#email_content').html($('#email_default_content').text());
		}
		else {
			$('#email_content').html(replaceVariables(option.data('text'), {
				customer_fname: $('#vr_customer_fname').val(),
				customer_lname: $('#vr_customer_lname').val(),
				signature: $('#vr_signature').val()
			}));
		}
	});

	$('.estimate-form').submit(function() {
		$(this).find('.itemNum').each(function() {
			$(this).next().val(this.value);
		});

		// if deposit wasn't manually modified, don't send it
		// if(!$('#deposit').attr('data-modified')) {
		// 	$('#deposit').remove();
		// }

		// add estimate level parts
		$(this).append($('<input type="hidden" name="parts" />').val(JSON.stringify(estimateParts)));

		return true; // continue with submit
	});
	
    $('#btnShareEstimate').click(function () {
		var estID = $('input[name=est_id]').val();
//		window.location.href = '/estimate/share/'+estID;
		
		var jobID = $("#job_id").val();
		var description = $("#description").val();
		var date = $("#updated_at").val();
		var name = $("#custName").text();
		var address = $('#jobAddress').text();
		var city = $('#jobCity').text();
		var phone = $('#jobPhone').text();
		var email = $('#jobEmail').text();
		var total = $('#totalPrice').val();
		var dep = $('#deposit').val();
		var baldue = $('#balance').val();
		var items = [];
		
		$('div[id^=item_group]:not(.markupTemplate)').each(function( index ) {
			num = index+1;
			var optPrice = 0, opts = [];
			$('.service-option-line .option-price', this).each(function() {
				optPrice += parseFloat(this.value, 10);
				opts.push($(this).prev().data('option-id'));
			});

			var servText = $('#description'+num).val();
			if($.trim(servText) === '') {
				servText = $('#shortService'+num+' option:selected').attr('data-description');
			}
			if($.trim(servText) === '') {
				servText = $('#shortService'+num+' option:selected').text();
			}

			var item = {
				itemNum: $('#item'+num).val(),
				room: $('#room'+num+' option:selected').text(),
				dir: $('#dir'+num+' option:selected').text(),
				style: $('#winStyle'+num+' option:selected').text(),
				serv: servText,
				dim: $('#dimWidth'+num).val()+'x'+$('#dimHeight'+num).val(),
				qty: '1',
				item: $('#servPrice'+num).val(),
				bal: $('option:selected', '#balancer'+num).text(),
				opt: optPrice,
				opts: '[' + opts.join(', ') + ']',
				lineTotal: $('#price'+num).val()
			}
			items.push(item);
		});
		
		$.ajax({
			type: "POST",
			url: "/estimate/share3/"+estID,
			data: { 
				jobID: jobID,
				description: description,
				date: date,
				name: name, 
				address: address,
				city: city,
				phone: phone,
				email: email,
				total: total,
				dep: dep,
				baldue: baldue,
				items: items,
				notes: $.trim($('.estimate-notes').val())
			}
		})
		.success(function( result ) {
			$('#modalShare').modal('show');
			$('.pdf-status-line').html($('.saved-pdf').html());
			$('.email-status-line').html('');
			var pdfBox = document.getElementById("pdfBox");
			pdfBox.innerHTML = result;
		})
		.fail(function( msg ) {
			console.log(msg);
		});
	});

	// in modal button
	$('#modalShare').on('click', '#btnSavePDF', function() {

		var estID = $('input[name=est_id]').val();		
		var jobID = $("#job_id").val();
		var description = $("#description").val();
		var date = $("#updated_at").val();
		var name = $("#custName").text();
		var address = $('#jobAddress').text();
		var city = $('#jobCity').text();
		var phone = $('#jobPhone').text();
		var email = $('#jobEmail').text();
		var total = $('#totalPrice').val();
		var dep = $('#deposit').val();
		var baldue = $('#balance').val();
		var items = [];
		
		$('div[id^=item_group]:not(.markupTemplate)').each(function( index ) {
			num = index+1;
			var optPrice = 0, opts = [];
			$('.service-option-line .option-price', this).each(function() {
				optPrice += parseFloat(this.value, 10);
				opts.push($(this).prev().data('option-id'));
			});

			var servText = $('#description'+num).val();
			if($.trim(servText) === '') {
				servText = $('#shortService'+num+' option:selected').attr('data-description');
			}
			if($.trim(servText) === '') {
				servText = $('#shortService'+num+' option:selected').text();
			}

			var item = {
				itemNum: $('#item'+num).val(),
				room: $('#room'+num+' option:selected').text(),
				dir: $('#dir'+num+' option:selected').text(),
				style: $('#winStyle'+num+' option:selected').text(),
				serv: servText,
				dim: $('#dimWidth'+num).val()+'x'+$('#dimHeight'+num).val(),
				qty: '1',
				item: $('#servPrice'+num).val(),
				bal: $('option:selected', '#balancer'+num).text(),
				opt: optPrice,
				opts: '[' + opts.join(', ') + ']',
				lineTotal: $('#price'+num).val()
			}
			items.push(item);
		});

		$('.pdf-status-line').html('<div class="doing">Saving PDF ...</div>');

		$.ajax({
			type: "POST",
			url: "/estimate/savePDF/"+estID,
			data: { 
				jobID: jobID,
				description: description,
				date: date,
				name: name, 
				address: address,
				city: city,
				phone: phone,
				email: email,
				total: total,
				dep: dep,
				baldue: baldue,
				items: items,
				notes: $.trim($('.estimate-notes').val())
			}
		})
		.success(function(result) {
			result = JSON.parse(result);
			if(result.status === 'ok') {
				result.path = result.path.replace('&export=download', ''); // to be able to view directly in drive
				$('.pdf-status-line').empty();
				$('<div/>')
					.addClass('done')
					.html('PDF Saved Successfully: <a href="'+result.path+'" target="_blank">'+result.path+'</a>')
					.appendTo('.pdf-status-line');
				$('#pdf_download_url').val(result.download_url);
			}
		})
		.fail(function( msg ) {
			console.log(msg);
			alert( "Failed: "+msg );
		});
	});

	$('#modalShare').on('click', '#btnEmailPDF', function() {
		// check if PDF is saved, else error
		if($('#pdf_download_url').val()) {
			switchToView('email');
			$('#email_to').focus().select();	
		}
		else {
			alert('You need to save the estimate as a PDF before you can email it to the customer.');
		}
	});

	$('#modalShare').on('click', '#btnSendEmail', function() {

		// TODO: validate

		$('.email-status-line').html('<div class="doing">Sending Email ...</div>');

		// send post request
		$.post('/estimate/emailPDF', {
			to: $('#email_to').val(),
			cc: $('#email_cc').val(),
			bcc: $('#email_bcc').val(),
			from: $('#email_from').val(),
			subject: $('#email_subject').val(),
			content: $('#email_content').html(),
			attachment: $('#pdf_download_url').val(),
			attachment_name: $("#description").val(),
			job_id: $('#job_id').val()
		}, function(_data) {
			if(_data === 'OK') {
				//alert('Mail sent successfully');
				$('.email-status-line').html('<div class="done">Email Sent Successfully.</div>');
				setTimeout(function() {
					switchToView('pdf');
				}, 1000);
			}
		});

	});

	$('#btnPrintEstimatePDF').click(printEstimate);

	$('#btnShowAllParts').click(function() {
		$('#modalParts').modal('hide');
		showAllParts();
		return false;
	});

	$('.btnGeneratePartsList').click(function () {
		var title = window.prompt('Parts List Title', 'Parts List for Job# ' + $(this).data('job-id') + '(Est# ' + $(this).data('estimate-id') + ')');
		if(!title) return false;
		title = $.trim(title);
		if(!title) return false;
		$.post('/parts-list/create', {title: title, job_id: $(this).data('job-id'), estimate_id: $(this).data('estimate-id')}, function(_data) {
			if(_data.indexOf('ERROR') !== 0) {
				window.location.href = '/parts-list/' + _data;
			}
			else {
				alert(_data);
			}
		});
		return false;
	});

	$('#btnBackToJob').click(function() {
		window.location.href = '/job/' + $(this).data('id');
	});

	$('#modalShare').on('click', '#btnPrintPDF', printEstimate);

	$('#modalShare').on('click', '#btnCloseEmail', function() {
		switchToView('pdf');
	});

	if($('#deposit').attr('data-modified')) {
		$('#btnResetDeposit').show();
	}

	populatePartsColumns();

	$('#parent').on('click', '.view-item-parts-link', showItemParts);

	if( window['printMe'] != undefined ) {
		if(printMe === true) {
			window.setTimeout(printEstimate, 500); // give a little time for totalling to complete
		}
	}

	window.setTimeout(function() {
		// if sold estimate, disable/hide elements as necessary
		if(window.estimateIsSold === true) {
			$('input, select, ' +
				'textarea, ' +
				'button.addItem, ' +
				'button.btnOption, ' +
				'button.cloneItem, ' +
				'button.closeItem, ' +
				'button#saveEstimate'
				).attr('disabled', 'disabled');
			$('.subGroup > .images').hide();
		}
	}, 250);

	hideRemoveButton();
});

$(window).load(function() {
	if(showAP) {
		$('#notifications').empty().append('<div class="alert alert-success">Parts list successfully ' + (showAP == 1 ? 'Finalized&nbsp;&nbsp;&nbsp;<a href="/parts-list/' + estimatePartsList.id + '" class="btn btn-xs btn-success">Print</a>' : 'Un-Finalized') + '</div>');
		if(showAP == 2) {
			setTimeout(function() {
				$('#notifications').empty();
			}, 3000);
		}
	}
});

function populatePartsColumns() {
	// parse loaded parts and update parts column
	$('.loaded-parts').each(function() {
		var json = $(this).val();
		if(json) json = JSON.parse(json); else json = false;
		if(!json.length) json = false;
		var partsColumnHTML = json ? (json.length + '&nbsp;<a class="view-item-parts-link" href="">View</a>') : 'N/A';
		$(this).closest('.clonedSection').find('.parts_column').html(partsColumnHTML);
	});
}

function addOption(_itemNum, _optionID, _price) {
	
	var par = $('#options' + _itemNum + ' .service-options');

	// if already existing, ignore
	if(par.find('.option-name[data-option-id=' + _optionID + ']').length) return false;

	par.find('.none').hide();

	var opt = $('#options' + _itemNum + ' .option-chooser option[value=' + _optionID + ']');

	par.append('<div class="service-option-line"><span class="option-name" data-option-id="'+ _optionID +'">' + opt.html() + '</span>'+
				'<input type="text" class="option-price" value="' + (_price ? _price : opt.data('price')) + '">'+
				'<button class="btn btn-sm btn-danger btn-remove-option"><i class="glyphicon glyphicon-remove"></i></button></div>');

	var options = [];
	par.find('.option-name[data-option-id]').each(function() {
		options.push($(this).data('option-id') + '-' + $(this).closest('.service-option-line').find('.option-price').val());
	});
	$('#servOptions' + _itemNum).val(options.join(','));

	// get parts associated with the option
	addAssociatedParts('option', _optionID, _itemNum);

}

function switchToView(_view) {
	$('#pdfBox, .pdfFooter')[_view === 'email' ? 'hide' : 'show']();
	$('.email-header, #emailBox, .email-footer')[_view === 'email' ? 'show' : 'hide']();
	$('.email-status-line').html('');
}

function refreshEmailTemplates() {
	var select = $('#email_template').prop('disabled', true);
	select.parent().addClass('disabled');
	$.get('/email-templates/list', function(_data) {
		var templates = JSON.parse(_data);
		select.empty().append('<option value="0" selected>None</option>');
		templates.forEach(function(_item) {
			var option = $('<option/>');
			option.data('id', _item.id);
			option.data('text', _item.text);
			option.text(_item.name);
			option.appendTo(select);
		});
		select.prop('disabled', false);
		select.parent().removeClass('disabled');
	});
}

function replaceVariables(_template, _data) {
	return _template.replace(/\{([^}]+)\}/g, function(_match) {
	    return _data[_match.replace(/[{}]/g, '')];
	});
}

function printEstimate(){

	var estID = $('input[name=est_id]').val();
//		window.location.href = '/estimate/share/'+estID;
	
	var jobID = $("#job_id").val();
	var description = $("#description").val();
	var date = $("#updated_at").val();
	var name = $("#custName").text();
	var address = $('#jobAddress').text();
	var city = $('#jobCity').text();
	var phone = $('#jobPhone').text();
	var email = $('#jobEmail').text();
	var total = $('#totalPrice').val();
	var dep = $('#deposit').val();
	var baldue = $('#balance').val();
	var items = [];
	
	$('div[id^=item_group]:not(.markupTemplate)').each(function( index ) {
		num = index+1;
		var optPrice = 0, opts = [];
		$('.service-option-line .option-price', this).each(function() {
			optPrice += parseFloat(this.value, 10);
			opts.push($(this).prev().data('option-id'));
		});

		var servText = $('#description'+num).val();
		if($.trim(servText) === '') {
			servText = $('#shortService'+num+' option:selected').attr('data-description');
		}
		if($.trim(servText) === '') {
			servText = $('#shortService'+num+' option:selected').text();
		}

		var item = {
			itemNum: $('#item'+num).val(),
			room: $('#room'+num+' option:selected').text(),
			dir: $('#dir'+num+' option:selected').text(),
			style: $('#winStyle'+num+' option:selected').text(),
			serv: servText,
			dim: $('#dimWidth'+num).val()+'x'+$('#dimHeight'+num).val(),
			qty: '1',
			item: $('#servPrice'+num).val(),
			bal: $('option:selected', '#balancer'+num).text(),
			opt: optPrice,
			opts: '[' + opts.join(', ') + ']',
			lineTotal: $('#price'+num).val()
		}
		items.push(item);
	});

	var params = { 
		jobID: jobID,
		description: description,
		date: date,
		name: name, 
		address: address,
		city: city,
		phone: phone,
		email: email,
		total: total,
		dep: dep,
		baldue: baldue,
		items: JSON.stringify(items),
		notes: $.trim($('.estimate-notes').val())
	};

	var postForm = $('<form/>');
	postForm.attr({
		method: 'post',
		action: '/estimate/print',
		target: 'printIFrame'
	});
	for(var key in params) {
		if(params.hasOwnProperty(key)) {
			postForm.append($('<input/>').attr({
				type: 'hidden',
				name: key,
				value: params[key]
			}));
		}
	}
	$(document.body).append(postForm);
	postForm.submit();

}

function showItemParts() {
	var section = $(this).closest('.clonedSection'),
		parts = section.find('.loaded-parts').val();
	if(!parts || $.trim(parts) === '') parts = [];
	else parts = JSON.parse(parts);

	var table = $('<table/>').addClass('parts-table');
	$('<thead><tr><th>Part</th><th>Qty</th><th>Needed for</th></tr></thead>').appendTo(table);
	var tbody = $('<tbody/>').appendTo(table);

	var part, tr, cellText, i, j;

	for(i=0; i<parts.length; i++) {

		part = parts[i];

		tr = $('<tr/>');

		// part name
		cellText = ['<b>' + part.name + '</b>'];

		if(!part.other && part.options && part.options.length) {
			for(j=0; j<part.options.length; j++) {
				cellText.push(part.options[j].name + ': ' + part.options[j].value);
			}
		}
		
		// if "other" item, show description
		if(part.other) {
			cellText.push(part.description);
		}

		tr.append('<td>' + cellText.join('<br>&nbsp;&nbsp;') + '</td>');

		// qty
		tr.append('<td>' + part.expected_qty + '</td>');

		// needed for
		var itemNum = returnID(section[0]);
		if(part.mappingType == 'service') {
			cellText = 'Service: <b>' + section.find('select.service').first().find('option:selected').text() + '</b>';
		}
		else if(part.mappingType == 'option') {
			cellText = 'Option: <b>' + section.find('.service-options span.option-name[data-option-id=' + part.mappingID + ']').text() + '</b>';
		}
		else if(part['manual']) {
			cellText = 'Manually Added.&nbsp;&nbsp;' + '<button class="btn btn-xs btn-warning btn-remove-manual-part" data-section-id="' + section.attr('id') + '" data-manual-part-id="' + part.manual_part_id + '">Remove</button>';
		}
		tr.append('<td>' + cellText + '</td>');

		// if manually added, add special class to highlight that
		if(part['manual']) {
			tr.addClass('manual');
		}

		// add to table
		tr.appendTo(tbody);
	}

	// ability to add parts manually
	var addTR = $('<tr/>').addClass('new-part-row'),
		addTD = $('<td/>').attr({colspan: 3}).appendTo(addTR),
		partChooser = $('<select class="manual-part-chooser" />').html(
			'<option value="0" selected>Choose..</option>' + allParts.map(function(_x) {
				return '<option value="' + _x.id + '">' + _x.name + '</option>';
			}).join('') + '<option value="other">Other</option>'
		);
	$('<label><span>Add to Line Item #' + section.data('id') + '</span></label>').append(partChooser).appendTo(addTD);
	addTR.appendTo(table)

	$('#partsModalBody').empty().append(table).attr('data-section-id', section.attr('id'));

	$('#modalParts .modal-title').html('Parts for Line Item #' + section.data('id') + '<a href="#" class="btn btn-xs btn-info show-all-parts">Show All Parts</a>');
	$('#modalParts').modal('show');

	return false;
}


function showAllParts() {

	var table = $('<table/>').addClass('parts-table');
	$('<thead><tr><th>Part</th><th>Qty</th><th>Needed for</th></tr></thead>').appendTo(table);
	var tbody = $('<tbody/>').appendTo(table);

	var section = false;

	var displayParts = [];

	$('#parent .clonedSection').each(function() {
		
		section = $(this);

		var itemNum = returnID(section[0]),
			parts = section.find('.loaded-parts').val(),
			part, tr, cellText, i, j,
			displayPart, sortedOptions = false, existing = false;

		if(!parts || $.trim(parts) === '') parts = [];
		else parts = JSON.parse(parts);

		// consolidate all parts into the array displayParts
		for(i=0; i<parts.length; i++) {

			part = parts[i];

			displayPart = {};

			// part name (with options)
			cellText = ['<b>' + part.name + '</b>'];

			if(!part.other && part.options && part.options.length) {
				sortedOptions = part.options.sort(function(a, b) {
			        var x = a['name'].toLowerCase(), y = b['name'].toLowerCase();
			        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
			    });
				for(j=0; j<sortedOptions.length; j++) {
					cellText.push(sortedOptions[j].name + ': ' + sortedOptions[j].value);
				}
			}

			if(part.other) {
				cellText.push(part.description);
			}

			cellText = cellText.join('<br>&nbsp;&nbsp;');

			// check if we already have a part with same options in displayParts, if so increase qty & update neededFor column
			existing = false
			for(j=0; j<displayParts.length; j++) {
				if(displayParts[j].name === cellText) {
					displayPart = displayParts[j];
					existing = true;
					break;
				}
			}
			if(!existing) displayPart.name = cellText;

			// qty
			if(!existing) displayPart.qty = 0;
			displayPart.qty += part.expected_qty;			

			// needed for
			if(part.mappingType == 'service') {
				cellText = '<a href="#" class="show-line-parts" data-line-id="' + itemNum + '">Line Item: ' + itemNum + '</a> - <b>' + section.find('select.service').first().find('option:selected').text() + '</b>';
			}
			else if(part.mappingType == 'option') {
				cellText = '<a href="#" class="show-line-parts" data-line-id="' + itemNum + '">Line Item: ' + itemNum + '</a> - <b>' + section.find('.service-options span.option-name[data-option-id=' + part.mappingID + ']').text() + '</b>';
			}
			else if(part['manual']) {
				cellText = '<a href="#" class="show-line-parts" data-line-id="' + itemNum + '">Line Item: ' + itemNum + '</a> - Manually Added.&nbsp;&nbsp;' + '<button class="btn btn-xs btn-warning btn-remove-manual-part" data-section-id="' + section.attr('id') + '" data-manual-part-id="' + part.manual_part_id + '">Remove</button>';
			}
			
			if(!existing) displayPart.neededFor = cellText;
			else displayPart.neededFor = $.trim([displayPart.neededFor, cellText].join('<br>'));

			if(!existing) displayParts.push(displayPart);
		}

	});

	// include estimate level parts
	var part, displayPart, cellText, sortedOptions;
	for(var i=0; i<estimateParts.length; i++) {

		part = estimateParts[i];

		displayPart = {};

		// part name (with options)
		cellText = ['<b>' + part.name + '</b>'];

		if(!part.other && part.options && part.options.length) {
			sortedOptions = part.options.sort(function(a, b) {
		        var x = a['name'].toLowerCase(), y = b['name'].toLowerCase();
		        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		    });
			for(var j=0; j<sortedOptions.length; j++) {
				cellText.push(sortedOptions[j].name + ': ' + sortedOptions[j].value);
			}
		}

		if(part.other) {
			cellText.push(part.description);
		}

		cellText = cellText.join('<br>&nbsp;&nbsp;');

		// check if we already have a part with same options in displayParts, if so increase qty & update neededFor column
		displayPart.name = cellText;

		// qty
		displayPart.qty = part.expected_qty;			

		displayPart.neededFor = 'Common Part&nbsp;&nbsp;' + (estimatePartsList.status == 0 ? '<button class="btn btn-xs btn-warning btn-remove-manual-part" data-section-id="common" data-manual-part-id="' + part.manual_part_id + '">Remove</button>' : '');
		
		displayParts.push(displayPart);
	}

	// now iterate displayParts and add rows
	for(var j=0; j<displayParts.length; j++) {
		tr = $('<tr/>');
		tr.append('<td>' + displayParts[j].name + '</td>');
		tr.append('<td>' + displayParts[j].qty + '</td>');
		tr.append('<td>' + displayParts[j].neededFor + '</td>');
		tr.appendTo(tbody);
	}

	// ability to add parts manually
	if(estimatePartsList.status != 1) {
		var addTR = $('<tr/>').addClass('new-part-row'),
			addTD = $('<td/>').attr({colspan: 3}).appendTo(addTR),
			partChooser = $('<select class="manual-part-chooser" />').html(
				'<option value="0" selected>Choose..</option>' + allParts.map(function(_x) {
					return '<option value="' + _x.id + '">' + _x.name + '</option>';
				}).join('') + '<option value="other">Other</option>'
			);
		$('<label><span>Add to Job</span></label>').append(partChooser).appendTo(addTD);
		addTR.appendTo(table);
	}
		
	$('#partsModalBody').empty().append(table).removeAttr('data-section-id');

	$('#modalParts .modal-title').html('All Parts');

	if(editPage) {
		var rhsButtons = $('<div/>').addClass('rhs-buttons');

		// finalize/unfinalize button
		if(estimatePartsList.status == 0) {
			rhsButtons.append('<a href="#" class="btn btn-xs btn-info btn-finalize-parts-list">&nbsp;&nbsp;Finalize&nbsp;&nbsp;<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;</a>');
		}
		else {
			rhsButtons.append('<a href="#" class="btn btn-xs btn-warning btn-unfinalize-parts-list">&nbsp;&nbsp;Un-Finalize&nbsp;&nbsp;<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;</a>');
		}

		// print button
		rhsButtons.append('<a target="_blank" href="/parts-list/' + estimatePartsList.id + '" class="btn btn-xs btn-success print-all-parts">&nbsp;&nbsp;Print&nbsp;&nbsp;<span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;</a>');

		$('#modalParts .modal-title').append(rhsButtons);
	}
	
	$('#modalParts').modal('show');

	return false;

}

function ajaxSaveEstimate(_doneCallback) {

	$('.estimate-form').find('.itemNum').each(function() {
		$(this).next().val(this.value);
	});

	// if deposit wasn't manually modified, don't send it
	// if(!$('#deposit').attr('data-modified')) {
	// 	$('#deposit').remove();
	// }

	// add estimate level parts
	$('.estimate-form').append($('<input type="hidden" name="parts" />').val(JSON.stringify(estimateParts)));

	// serialize it
	$.post('/estimate/updateAjax', $('.estimate-form').serialize(), function(_data) {
		$('.estimate-form input[name=parts]').remove();
		if(_data !== 'OK') alert(_data);
		else if(_doneCallback) _doneCallback();
	});

	return false;
}
