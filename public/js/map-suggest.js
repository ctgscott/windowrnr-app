$(window).load(function() {

	var map_init = false;

	var map, autocomplete, marker, input, infoWindow;

	// add suggest model dialog if not present
	if($('#mapSuggestModal').length === 0) {
		$('<div class="modal" id="mapSuggestModal" tabindex="-1" role="dialog" aria-labelledby="Address Lookup" aria-hidden="true"><div class="modal-dialog">'+
		'<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title">Address Lookup</h4></div>'+
		'<div class="modal-body"><input id="pac-input" class="controls" type="text" placeholder="Enter a location"><div id="map-suggest-canvas"></div></div>'+
		'</div></div></div>').appendTo('body');		
	}

	$('[data-address-group]').each(function() {
		$('<button class="btn btn-small btn-info map-suggest" title="Address Lookup"><i class="glyphicon glyphicon-map-marker"></i></button>')
			.insertAfter($(this).find('.map-suggest-zip'));
	});
	$('[data-address-group]').on('click', 'button.map-suggest', function() {

		// form
		window.map_suggest_form = $(this).closest('[data-address-group]');

		// show modal
		$('#mapSuggestModal').modal();

		if(!map_init) {

			// init map
			map = new google.maps.Map(document.getElementById('map-suggest-canvas'), {
			    center: new google.maps.LatLng(-33.8688, 151.2195),
			    zoom: 13
			});

			// init autocomplete
			input = document.getElementById('pac-input');
			autocomplete = new google.maps.places.Autocomplete(input);
			map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
			autocomplete.bindTo('bounds', map);
			//autocomplete.setTypes(['address']);
			infowindow = new google.maps.InfoWindow({
				maxWidth: 300
			});
			marker = new google.maps.Marker({
				map: map,
				anchorPoint: new google.maps.Point(0, -29)
			});

			// on change listener
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
			
				infowindow.close();
				marker.setVisible(false);
				var place = autocomplete.getPlace();
				if (!place.geometry) return;

				// If the place has a geometry, then present it on a map.
				if (place.geometry.viewport) {
					map.fitBounds(place.geometry.viewport);
				} 
				else {
					map.setCenter(place.geometry.location);
					map.setZoom(17);
				}
				marker.setIcon(({
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(35, 35)
				}));
				marker.setPosition(place.geometry.location);
				marker.setVisible(true);

				var address = '';
				if (place.address_components) {
					address = [
						(place.address_components[0] && place.address_components[0].short_name || ''),
						(place.address_components[1] && place.address_components[1].short_name || ''),
						(place.address_components[2] && place.address_components[2].short_name || '')
						].join(' ');

					console.log(place.address_components);

					window.map_suggest_form.find('.map-suggest-address').val(getAddressPart(place.address_components, 'street_number') + ', ' + getAddressPart(place.address_components, 'route'));
					window.map_suggest_form.find('.map-suggest-city').val(getAddressPart(place.address_components, 'locality'));
					window.map_suggest_form.find('.map-suggest-state').val(getAddressPart(place.address_components, 'administrative_area_level_1'));
					window.map_suggest_form.find('.map-suggest-zip').val(getAddressPart(place.address_components, 'postal_code'));
				}

				infowindow.setContent('<div class="suggested-address"><strong>' + place.name + '</strong><br>' + address);
				infowindow.open(map, marker);
			
			});

			map_init = true;

		}
		else {
			infowindow.close();
			marker.setVisible(false);
		}

		setTimeout(function() {
			$(input).val('').focus();
		}, 500);

		return false;
	});
});

function getAddressPart(_components, _part) {
	var result = '';
	_components.forEach(function(_component) {
		if(_component.types[0] === _part) result = _component.long_name;
	});
	return result;
}


