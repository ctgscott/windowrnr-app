
$(document).ready(function(){
	var map;
    var markerClusterer = null;
	var imageUrl = 'http://chart.apis.google.com/chart?cht=mm&chs=24x32&' +
          'chco=FFFFFF,008CFF,000000&ext=.png';

/*	var myMarkers = [];
	$.getJSON( "custMap.json", function( data ) {
		console.log(data);
		$.each( data.jobs.lat, function( key, val ) {
			console.log(val);
		});
	});
*/	
	function refreshMap() {
        if (markerClusterer) {
          markerClusterer.clearMarkers();
        }   
		
		var markers = [];
		  
		var markerImage = new google.maps.MarkerImage(imageUrl, new google.maps.Size(24, 32));
	console.log(data);
		for (var i = 0; i < data.jobs.length; ++i) 
		{
			var myLat = data.jobs[i].lat;
			var myLng = data.jobs[i].lng;
			var latLng = new google.maps.LatLng(myLat, myLng);
			var marker = new google.maps.Marker({
				position: latLng,
				draggable: true,
				icon: markerImage
			});
			markers.push(marker);
		}
	
		var zoom = parseInt(10);
		var size = parseInt(10);
		var style = parseInt(10);
		zoom = zoom == -1 ? null : zoom;
		size = size == -1 ? null : size;
		style = style == -1 ? null: style;

	
		markerClusterer = new MarkerClusterer(map, markers, {
			maxZoom: zoom,
			gridSize: size,
		//    styles: styles[style]
		});
	}

	function initialize() {
		var mapOptions = {
			zoom: 6,
			center: new google.maps.LatLng(33.928263, -117.949734),
			mapTypeId: google.maps.MapTypeId.HYBRID
		};
		
		map = new google.maps.Map(document.getElementById('custMap'), mapOptions);
		
		refreshMap();
	}
	
	google.maps.event.addDomListener(window, 'load', initialize);
});