<?php

// Create connection
//$mysqli_wp = new mysqli("173.201.214.39","windowrrwp1","vIdbg6ICh,m22!","windowrrwp1");
//$mysqli_wp = new mysqli("windowrrwp1.db.6712605.hostedresource.com","windowrrwp1","WeLoveWindow2!","windowrrwp1");
$mysqli_wp = new mysqli("68.178.253.29","ba452732566565","j4nV+G9wY+z","ba452732566565","3313");

if (mysqli_connect_errno($mysqli_wp)) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$query = "SELECT count FROM form_counter WHERE id='1'";

if ($result = $mysqli_wp->query($query)) {

    /* fetch object */
	while ($row = $result->fetch_object()) {
		$count = $row->count;
    }

	/* free result set */
    $result->free();
}

// Grab all lead data after the last count
//$query2 = "SELECT id, lead_id, field_number, value FROM wp_rg_lead_detail WHERE lead_id>".$count;
$query2 = "SELECT id, lead_id, field_number, value FROM wp_5zrt4ws6ch_rg_lead_detail WHERE lead_id > ".$count;
$result2 = $mysqli_wp->query($query2);
//echo "New rows in database: ".$result2->num_rows."<br/>";

if ($result2->num_rows > 0) {

    /* fetch associative array */
	$leadID = $count;
    while ($row2 = $result2->fetch_assoc()) {
		if ($row2['lead_id'] > $leadID) {
			$leadID++;
		}
		
		// Build the arrays so they can be inserted
		if ($row2['field_number'] == '11') {
			$leads[$leadID]['f_name'] = $mysqli_wp->real_escape_string($row2['value']);
		} else if ($row2['field_number'] == '14') {
			$leads[$leadID]['l_name'] = $mysqli_wp->real_escape_string($row2['value']);
		} else if ($row2['field_number'] == '3') {
			$leads[$leadID]['email'] = $mysqli_wp->real_escape_string($row2['value']);
		} else if ($row2['field_number'] == '4') {
			$leads[$leadID]['phone'] = $mysqli_wp->real_escape_string($row2['value']);
		} else if ($row2['field_number'] == '9') {
			$leads[$leadID]['address'] = $mysqli_wp->real_escape_string($row2['value']);
		} else if ($row2['field_number'] == '10') {
			$leads[$leadID]['city'] = $mysqli_wp->real_escape_string($row2['value']);
		} else if ($row2['field_number'] == '6') {
			$query3 = "SELECT value FROM wp_5zrt4ws6ch_rg_lead_detail_long WHERE lead_detail_id=".$row2['id'];
			$result3 = $mysqli_wp->query($query3);
			$row3 = $result3->fetch_assoc();
			if ($row3['value'] == NULL) {
				$leads[$leadID]['note'] = $mysqli_wp->real_escape_string($row2['value']);
			} else {
				$leads[$leadID]['note'] = $mysqli_wp->real_escape_string($row3['value']);
			}
			
		}
	}
	
	$totalLeads = $leadID - $count;
	
	/* free result set */
    $result2->free();

	// Insert data into Customer database
//	$mysqli_cust = new mysqli("50.63.244.198","windowcustomer","Windowrandr01!","windowcustomer");
	$mysqli_cust = new mysqli("104.237.153.78","windowcustomer","Windowrandr01!","windowcustomer");

	// 1st - Check connection
	if (mysqli_connect_errno($mysqli_cust)) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}

	date_default_timezone_set('America/Los_Angeles');
	$now = date("Y-m-d H:i:s");

	foreach ($leads as $lead) {
		$insert_cust = "INSERT INTO customers (`l_name`, `f_name`, `phone`, `email`, `created_at`) VALUES ('".$lead['l_name']."', '".$lead['f_name']."', '".$lead['phone']."', '".$lead['email']."', '".$now."')";
		$mysqli_cust->query($insert_cust);
		$customerID = $mysqli_cust->insert_id;
		
		if (isset($lead['address']) && isset($lead['city'])) {
			$address = urlencode($lead['address'].', '.$lead['city']);
			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=";
			$latlng = json_decode(file_get_contents($url.$address.'&sensor=false'));
			$length = count($latlng->results);
			$status = $latlng->status;
			if ($status=="OK") {
				$Lat = $latlng->results[0]->geometry->location->lat;
				$Lng = $latlng->results[0]->geometry->location->lng;
			} else {
				$Lat = "0";
				$Lng = "0";
			}
		}

		$insert_job = "INSERT INTO jobs (`customer_id`, `status`, `archive`, `created_by`, `address`, `city`, `lat`, `lng`, `created_at`) VALUES ('".$customerID."', 1, 0, 'Web Form', '".$lead['address']."', '".$lead['city']."', '".$Lat."', '".$Lng."', '".$now."')";
		$mysqli_cust->query($insert_job);
		$jobID = $mysqli_cust->insert_id;

		$insert_note = "INSERT INTO notes (`job_id`, `user_id`, `note`, `created_at`) VALUES ('".$jobID."', 999, '".$lead['note']."', '".$now."')";
		$mysqli_cust->query($insert_note);
	}

	// update new_enquiries
	$nn_sql = "INSERT INTO new_enquiries (`count`, `added_at`, `checked`) VALUES (".$totalLeads.", NOW(), 0)";
	$mysqli_cust->query($nn_sql);

	/* close connection */
	$mysqli_cust->close();

	//Set the lead counter to the new value
	$update = "UPDATE form_counter SET count=".$leadID." WHERE id='1'";
	$mysqli_wp->query($update);

	echo $totalLeads;
} else {
	// There were no new leads
	echo 0;
}

/* close connection */
$mysqli_wp->close();
?>