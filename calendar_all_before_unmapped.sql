
-- orginial calendar_all view

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `calendar_all`
AS SELECT
   distinct `a`.`id` AS `id`,
   `a`.`google_event_id` AS `google_event_id`,
   `a`.`google_cal_id` AS `google_cal_id`,
   `a`.`cust_id` AS `cust_id`,
   `a`.`job_id` AS `job_id`,
   `a`.`cal_user_id` AS `cal_user_id`,
   `a`.`cal_user_id` AS `className`,
   `a`.`avatar` AS `avatar`,
   `a`.`start` AS `start`,
   `a`.`end` AS `end`,
   `a`.`location` AS `location`,
   `a`.`allDay` AS `allDay`,
   `a`.`title` AS `title`,
   `a`.`htmlLink` AS `url`,
   `a`.`assignee` AS `assignee`,
   `a`.`event_type` AS `event_type`,
   `a`.`lat` AS `lat`,
   `a`.`lng` AS `lng`,
   `a`.`created_at` AS `created_at`,
   `a`.`updated_at` AS `updated_at`,
   `b`.`sales_check` AS `sales_check`
FROM (`events` `a` left join `profiles` `b` on((`a`.`cal_user_id` = `b`.`user_id`))) where (`b`.`sales_check` = 1);