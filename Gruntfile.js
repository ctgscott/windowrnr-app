var jsSources = [
	'public/js/maps.js',
	'public/js/schedule.js',

	/*'public/js/admin.js',
	'public/js/custMapV2.js',
	'public/js/custTypeAhead.js',
	'public/js/customer.js',
	'public/js/email-templates.js',
	'public/js/estimate.js',
	'public/js/estimateEdit.js',
	'public/js/estimateList.js',
	'public/js/fieldAdd.js',
	'public/js/image-upload.js',
	'public/js/itemAdd.js',
	'public/js/job.js',
	'public/js/leadDetail.js',
	'public/js/main.js',
	'public/js/map-suggest-inline.j',
	'public/js/map-suggest.js',
	'public/js/sash-weight.js',
	'public/js/user.js'*/
];

module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		
		jshint: {
			files: jsSources,

			options: {
				
				"eqnull": true,
				"eqeqeq": true,
				"undef": true,
				"unused": false,
				"debug": true,
				"loopfunc": true,
				  
				globals: { 
					jQuery: true,
					console: true,
					document: true,
					$: true,
					window: true,
					screen: true
				}
			}
		}

	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-jshint');

}; 
